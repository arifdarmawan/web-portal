﻿var initializeFormValidate = function () {
    $("form[name='form-reset-password']").validate({
        // Specify validation rules
        rules: {
            resetCredential: {
                required: true,
                email: true
            }
        },
        // Specify validation error messages
        messages: {
            resetCredential: {
                required: "Please provide a email",
                email: "Please provide a valid email"
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            showLoading($(".reset-btn"));

            setTimeout(function () {
                $.when(submitCredential()).done(function (result) {
                    removeLoading($(".reset-btn span"));
                });
            }, 500);
        }
    });
};

var submitCredential = function () {
    var deferred = $.Deferred();
    var credential = document.getElementById('resetCredential').value;
    var getParams = {
        "email": credential
    };

    $.ajax({
        type: "POST",
        traditional: true,
        cache: false,
        url: '/Account/ForgotPassword',
        context: document.body,
        data: getParams,
        success: function (result, status, xhr) {
            if (result.code === 200) {
                window.location.href = '/Account/ForgotPasswordVerification/?email=' + result.message;
            } else {
                alert(result.message);
            }

            deferred.resolve();
        },
        error: function (result, status, xhr) {
            deferred.reject(result);
        }
    });

    return deferred.promise();
};

$(function () {
    initializeFormValidate();
});