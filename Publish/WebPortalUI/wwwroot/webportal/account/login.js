﻿var initializeFormValidate = function () {
    // Initialize form validation on the registration form.
    // It has the name attribute "registration"
    $("form[name='login-form']").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
            loginUserId: "required",
            password: {
                required: true,
                minlength: 5
            }
        },
        // Specify validation error messages
        messages: {
            //firstname: "Please enter your firstname",
            //lastname: "Please enter your lastname",
            loginUserId: "Please enter your username",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            showLoading($(".login-btn"));
            setTimeout(function () {
                $.when(Login()).done(function (data) {
                    removeLoading($(".login-btn span"));
                });
            }, 500);
        }
    });
};

var Login = function () {
    var deferred = $.Deferred();

    var userId = document.getElementById('loginUserId').value;
    var password = document.getElementById('loginPassword').value;
    var getParams = {
        "UserId": userId,
        "Password": password
    };

    $.ajax({
        type: "POST",
        traditional: true,
        async: false,
        cache: false,
        url: '/Account/Login',
        data: getParams,
        success: function (result, status, xhr) {
            if (result.code === 200) {
                if (result.isUserActive) {
                    window.location.href = '/Home/Index';
                } else {
                    $(".wrap-btn").before('<label id="login-global-msg-error" class="error">Please activate your email before login.</label>');
                }
            } else if (result.code === 601) {
                $(".wrap-btn").before('<label id="login-global-msg-error" class="error">Username and password not match.</label>');
            } else {
                $(".wrap-btn").before('<label id="login-global-msg-error" class="error">Username not registered.</label>');
            }

            deferred.resolve(result);
        },
        error: function (result, status, xhr) {
            deferred.reject(result);
            formMessageHandler(result);
        }
    });

    return deferred.promise();
}

var formMessageHandler = function (msg) {
    $('<label for="wrap-msg-global" generated="true" class="error">' + msg + '</label>').insertAfter("#wrap-msg-global");
}

$(function () {
    initializeFormValidate();
});