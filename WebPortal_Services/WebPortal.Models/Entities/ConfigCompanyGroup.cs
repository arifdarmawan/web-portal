﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class ConfigCompanyGroup
    {
        public int Id { get; set; }
        [Required]
        [StringLength(10)]
        public string CompanyGroupCode { get; set; }
        [Required]
        [StringLength(50)]
        public string CompanyGroupName { get; set; }
        public DateTime CreatedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        public bool? IsDelete { get; set; }
    }
}
