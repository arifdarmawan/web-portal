﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterQuotationComparationForm
    {
        public int Id { get; set; }
        [Column("PRId")]
        public int Prid { get; set; }
        [Column("QCFNumber")]
        [StringLength(50)]
        public string Qcfnumber { get; set; }
        [StringLength(5)]
        public string CompanyCode { get; set; }
        public int? MasterDataVendorId { get; set; }
        public int? MasterItemCategoryId { get; set; }
        public int? MasterItemId { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? Qty { get; set; }
        public int? CurrencyId { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? UnitPrice { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? TotalPrice { get; set; }
        [Column(TypeName = "character varying")]
        public string Notes { get; set; }
        [Column("QCFStatus")]
        [StringLength(50)]
        public string Qcfstatus { get; set; }
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
        public bool? IsDelete { get; set; }
    }
}
