﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataProcurementStatus
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string Status { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
