﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MappingDataVendorCategories
    {
        public int MasterDataVendorId { get; set; }
        public int CategoriesId { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("CategoriesId")]
        [InverseProperty("MappingDataVendorCategories")]
        public virtual MasterItemCategories Categories { get; set; }
    }
}
