﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterPurchaseOrder
    {
        public MasterPurchaseOrder()
        {
            MasterPurchaseOrderItem = new HashSet<MasterPurchaseOrderItem>();
        }

        public int Id { get; set; }
        [Column("PONumber")]
        [StringLength(30)]
        public string Ponumber { get; set; }
        [Column("MasterPRId")]
        public int MasterPrid { get; set; }
        public int MasterDataVendorId { get; set; }
        [Column("POStatus")]
        [StringLength(50)]
        public string Postatus { get; set; }
        [StringLength(50)]
        public string PaymentStatus { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? GrandTotal { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? GrandTotalAfterTax { get; set; }
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        public DateTime ModifyDate { get; set; }
        public bool? IsDelete { get; set; }

        [ForeignKey("MasterDataVendorId")]
        [InverseProperty("MasterPurchaseOrder")]
        public virtual MasterDataVendor MasterDataVendor { get; set; }
        [ForeignKey("MasterPrid")]
        [InverseProperty("MasterPurchaseOrder")]
        public virtual MasterPurchaseRequisition MasterPr { get; set; }
        [InverseProperty("MasterPurchaseOrder")]
        public virtual ICollection<MasterPurchaseOrderItem> MasterPurchaseOrderItem { get; set; }
    }
}
