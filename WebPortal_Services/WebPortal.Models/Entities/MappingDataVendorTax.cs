﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MappingDataVendorTax
    {
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        [Column("TypeNPWP")]
        [StringLength(20)]
        public string TypeNpwp { get; set; }
        [Column("NPWP")]
        [StringLength(100)]
        public string Npwp { get; set; }
        [Column("TypePKP")]
        [StringLength(20)]
        public string TypePkp { get; set; }
        [Column("PKP")]
        [StringLength(100)]
        public string Pkp { get; set; }
        [Column("TypeBKP")]
        [StringLength(20)]
        public string TypeBkp { get; set; }
        public int? MasterDataBkpCategoryId { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }
        [Column("PPN")]
        public int? Ppn { get; set; }

        [ForeignKey("MasterDataBkpCategoryId")]
        [InverseProperty("MappingDataVendorTax")]
        public virtual MasterDataBkpCategory MasterDataBkpCategory { get; set; }
        [ForeignKey("MasterDataUserId")]
        [InverseProperty("MappingDataVendorTax")]
        public virtual MasterDataUser MasterDataUser { get; set; }
    }
}
