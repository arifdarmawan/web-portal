﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataCity
    {
        public MasterDataCity()
        {
            MasterDataVendor = new HashSet<MasterDataVendor>();
        }

        public int Id { get; set; }
        public int MasterDataCountryId { get; set; }
        public int MasterDataRegionId { get; set; }
        [StringLength(10)]
        public string Code { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifyBy { get; set; }
        public bool? IsDelete { get; set; }

        [ForeignKey("MasterDataCountryId")]
        [InverseProperty("MasterDataCity")]
        public virtual MasterDataCountry MasterDataCountry { get; set; }
        [ForeignKey("MasterDataRegionId")]
        [InverseProperty("MasterDataCity")]
        public virtual MasterDataRegion MasterDataRegion { get; set; }
        [InverseProperty("MasterDataCity")]
        public virtual ICollection<MasterDataVendor> MasterDataVendor { get; set; }
    }
}
