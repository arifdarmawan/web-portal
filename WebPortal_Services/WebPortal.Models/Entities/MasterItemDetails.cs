﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterItemDetails
    {
        public MasterItemDetails()
        {
            MasterItems = new HashSet<MasterItems>();
        }

        [StringLength(10)]
        public string ItemCode { get; set; }
        [Column(TypeName = "jsonb")]
        public string ItemSpecification { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(100)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [StringLength(100)]
        public string ModifyBy { get; set; }
        public bool? IsDelete { get; set; }

        [InverseProperty("ItemCodeNavigation")]
        public virtual ICollection<MasterItems> MasterItems { get; set; }
    }
}
