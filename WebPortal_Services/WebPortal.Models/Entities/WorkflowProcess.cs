﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class WorkflowProcess
    {
        public WorkflowProcess()
        {
            MappingWorkflowPurchaseRequisition = new HashSet<MappingWorkflowPurchaseRequisition>();
            WorkflowActionApproval = new HashSet<WorkflowActionApproval>();
            WorkflowRequest = new HashSet<WorkflowRequest>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(10)]
        public string WorkflowTypeCode { get; set; }
        public int MasterDataUserId { get; set; }
        [Required]
        [StringLength(20)]
        public string ProcessStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        public bool? IsDelete { get; set; }

        [ForeignKey("MasterDataUserId")]
        [InverseProperty("WorkflowProcess")]
        public virtual MasterDataUser MasterDataUser { get; set; }
        [InverseProperty("Process")]
        public virtual ICollection<MappingWorkflowPurchaseRequisition> MappingWorkflowPurchaseRequisition { get; set; }
        [InverseProperty("WorkflowProcess")]
        public virtual ICollection<WorkflowActionApproval> WorkflowActionApproval { get; set; }
        [InverseProperty("WorkflowProcess")]
        public virtual ICollection<WorkflowRequest> WorkflowRequest { get; set; }
    }
}
