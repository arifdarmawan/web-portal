﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterRequestForQuotation
    {
        public int Id { get; set; }
        [Column("PRId")]
        public int Prid { get; set; }
        [Column("RFQNumber")]
        [StringLength(50)]
        public string Rfqnumber { get; set; }
        [StringLength(10)]
        public string CompanyCode { get; set; }
        public int? MasterDataVendorId { get; set; }
        public int? MasterItemCategoryId { get; set; }
        public int? MasterItemId { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? Qty { get; set; }
        public int? CurrencyId { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? UnitPrice { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? TotalPrice { get; set; }
        public DateTime? PriceValidDate { get; set; }
        public int? DeliveryTime { get; set; }
        [Column("RFQStatus")]
        [StringLength(50)]
        public string Rfqstatus { get; set; }
        [Column(TypeName = "character varying")]
        public string Notes { get; set; }
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
        public bool? IsDelete { get; set; }
    }
}
