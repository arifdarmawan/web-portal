﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    [Table("MasterDataTOP")]
    public partial class MasterDataTop
    {
        public MasterDataTop()
        {
            MappingDataBusinessFieldTopconfig = new HashSet<MappingDataBusinessFieldTopconfig>();
        }

        public int Id { get; set; }
        [Column("TOP")]
        public int Top { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }

        [InverseProperty("MasterDataTop")]
        public virtual ICollection<MappingDataBusinessFieldTopconfig> MappingDataBusinessFieldTopconfig { get; set; }
    }
}
