﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MappingDataBusinessFieldPaymentConfig
    {
        public int Id { get; set; }
        public int MasterDataBusinessFieldId { get; set; }
        [Column(TypeName = "numeric(5,2)")]
        public decimal? DownPayment { get; set; }
        [Column(TypeName = "numeric(5,2)")]
        public decimal? Progressive { get; set; }
        [Column(TypeName = "numeric(5,2)")]
        public decimal? FullPayment { get; set; }
        [Column(TypeName = "numeric(5,2)")]
        public decimal? Retention { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }

        [ForeignKey("MasterDataBusinessFieldId")]
        [InverseProperty("MappingDataBusinessFieldPaymentConfig")]
        public virtual MasterDataBusinessFields MasterDataBusinessField { get; set; }
    }
}
