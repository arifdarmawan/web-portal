﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataVendor
    {
        public MasterDataVendor()
        {
            MappingVendorItemPrice = new HashSet<MappingVendorItemPrice>();
            MasterPurchaseOrder = new HashSet<MasterPurchaseOrder>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(256)]
        public string Name { get; set; }
        public int MasterDataUserId { get; set; }
        public int MasterDataBusinessTypeId { get; set; }
        public int MasterDataCountryId { get; set; }
        public int MasterDataRegionId { get; set; }
        public int MasterDataCityId { get; set; }
        [Column(TypeName = "character varying")]
        public string Address { get; set; }
        [Required]
        [StringLength(10)]
        public string PostalCode { get; set; }
        [Required]
        [StringLength(100)]
        public string CompanyEmail { get; set; }
        [Required]
        [Column("PIC")]
        [StringLength(100)]
        public string Pic { get; set; }
        [Required]
        [Column("PICEmail")]
        [StringLength(100)]
        public string Picemail { get; set; }
        [Required]
        [StringLength(100)]
        public string IdCardNumber { get; set; }
        [Column(TypeName = "character varying")]
        public string Logo { get; set; }
        public DateTime? CreatedDate { get; set; }
        [StringLength(20)]
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        [StringLength(20)]
        public string ModifiyBy { get; set; }
        [Column(TypeName = "character varying")]
        public string MailingAddress { get; set; }
        [StringLength(50)]
        public string CompanyPhone { get; set; }
        [Column("PICPhone")]
        [StringLength(50)]
        public string Picphone { get; set; }
        public bool? IsDelete { get; set; }
        public bool? IntegrityPact { get; set; }
        public bool? MasterAgreement { get; set; }
        [StringLength(20)]
        public string Status { get; set; }
        public DateTime? ContractDate { get; set; }

        [ForeignKey("MasterDataBusinessTypeId")]
        [InverseProperty("MasterDataVendor")]
        public virtual MasterDataBusinessType MasterDataBusinessType { get; set; }
        [ForeignKey("MasterDataCityId")]
        [InverseProperty("MasterDataVendor")]
        public virtual MasterDataCity MasterDataCity { get; set; }
        [ForeignKey("MasterDataCountryId")]
        [InverseProperty("MasterDataVendor")]
        public virtual MasterDataCountry MasterDataCountry { get; set; }
        [ForeignKey("MasterDataRegionId")]
        [InverseProperty("MasterDataVendor")]
        public virtual MasterDataRegion MasterDataRegion { get; set; }
        [ForeignKey("MasterDataUserId")]
        [InverseProperty("MasterDataVendor")]
        public virtual MasterDataUser MasterDataUser { get; set; }
        [InverseProperty("MasterDataVendor")]
        public virtual ICollection<MappingVendorItemPrice> MappingVendorItemPrice { get; set; }
        [InverseProperty("MasterDataVendor")]
        public virtual ICollection<MasterPurchaseOrder> MasterPurchaseOrder { get; set; }
    }
}
