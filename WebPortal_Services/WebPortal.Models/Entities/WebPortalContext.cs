﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebPortal.Models.Entities
{
    public partial class WebPortalContext : DbContext
    {
        public WebPortalContext()
        {
        }

        public WebPortalContext(DbContextOptions<WebPortalContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ConfigCompany> ConfigCompany { get; set; }
        public virtual DbSet<ConfigCompanyBusinessUnitGroup> ConfigCompanyBusinessUnitGroup { get; set; }
        public virtual DbSet<ConfigCompanyDivision> ConfigCompanyDivision { get; set; }
        public virtual DbSet<ConfigCompanyGroup> ConfigCompanyGroup { get; set; }
        public virtual DbSet<ConfigDivision> ConfigDivision { get; set; }
        public virtual DbSet<MappingDataBusinessFieldPaymentConfig> MappingDataBusinessFieldPaymentConfig { get; set; }
        public virtual DbSet<MappingDataBusinessFieldTopconfig> MappingDataBusinessFieldTopconfig { get; set; }
        public virtual DbSet<MappingDataVendorBusinessFields> MappingDataVendorBusinessFields { get; set; }
        public virtual DbSet<MappingDataVendorCategories> MappingDataVendorCategories { get; set; }
        public virtual DbSet<MappingDataVendorCurrencyBank> MappingDataVendorCurrencyBank { get; set; }
        public virtual DbSet<MappingDataVendorLegal> MappingDataVendorLegal { get; set; }
        public virtual DbSet<MappingDataVendorPayment> MappingDataVendorPayment { get; set; }
        public virtual DbSet<MappingDataVendorTax> MappingDataVendorTax { get; set; }
        public virtual DbSet<MappingVendorItemPrice> MappingVendorItemPrice { get; set; }
        public virtual DbSet<MappingWorkflowPurchaseRequisition> MappingWorkflowPurchaseRequisition { get; set; }
        public virtual DbSet<MasterDataBank> MasterDataBank { get; set; }
        public virtual DbSet<MasterDataBkpCategory> MasterDataBkpCategory { get; set; }
        public virtual DbSet<MasterDataBusinessFields> MasterDataBusinessFields { get; set; }
        public virtual DbSet<MasterDataBusinessFieldsType> MasterDataBusinessFieldsType { get; set; }
        public virtual DbSet<MasterDataBusinessType> MasterDataBusinessType { get; set; }
        public virtual DbSet<MasterDataCity> MasterDataCity { get; set; }
        public virtual DbSet<MasterDataCountry> MasterDataCountry { get; set; }
        public virtual DbSet<MasterDataCurrency> MasterDataCurrency { get; set; }
        public virtual DbSet<MasterDataDomain> MasterDataDomain { get; set; }
        public virtual DbSet<MasterDataJobTitle> MasterDataJobTitle { get; set; }
        public virtual DbSet<MasterDataProcurementStatus> MasterDataProcurementStatus { get; set; }
        public virtual DbSet<MasterDataRegion> MasterDataRegion { get; set; }
        public virtual DbSet<MasterDataResetPassword> MasterDataResetPassword { get; set; }
        public virtual DbSet<MasterDataStatus> MasterDataStatus { get; set; }
        public virtual DbSet<MasterDataTop> MasterDataTop { get; set; }
        public virtual DbSet<MasterDataUser> MasterDataUser { get; set; }
        public virtual DbSet<MasterDataUserGroup> MasterDataUserGroup { get; set; }
        public virtual DbSet<MasterDataVendor> MasterDataVendor { get; set; }
        public virtual DbSet<MasterDataVendorNotification> MasterDataVendorNotification { get; set; }
        public virtual DbSet<MasterItemCategories> MasterItemCategories { get; set; }
        public virtual DbSet<MasterItemDetails> MasterItemDetails { get; set; }
        public virtual DbSet<MasterItems> MasterItems { get; set; }
        public virtual DbSet<MasterPurchaseOrder> MasterPurchaseOrder { get; set; }
        public virtual DbSet<MasterPurchaseOrderItem> MasterPurchaseOrderItem { get; set; }
        public virtual DbSet<MasterPurchaseRequisition> MasterPurchaseRequisition { get; set; }
        public virtual DbSet<MasterPurchaseRequisitionItem> MasterPurchaseRequisitionItem { get; set; }
        public virtual DbSet<MasterQuotationComparationForm> MasterQuotationComparationForm { get; set; }
        public virtual DbSet<MasterRequestForQuotation> MasterRequestForQuotation { get; set; }
        public virtual DbSet<MasterUnitOfMeasure> MasterUnitOfMeasure { get; set; }
        public virtual DbSet<WorkflowActionApproval> WorkflowActionApproval { get; set; }
        public virtual DbSet<WorkflowActivity> WorkflowActivity { get; set; }
        public virtual DbSet<WorkflowGroupMember> WorkflowGroupMember { get; set; }
        public virtual DbSet<WorkflowProcess> WorkflowProcess { get; set; }
        public virtual DbSet<WorkflowRequest> WorkflowRequest { get; set; }
        public virtual DbSet<WorkflowType> WorkflowType { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Server=10.251.1.17;Port=5432;Database=webportal-dev;User Id=postgres;Password=FR*#q1q2q3q4;Trust Server Certificate=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<ConfigCompany>(entity =>
            {
                entity.HasIndex(e => e.CompanyCode)
                    .HasName("CompanyCode_uniquekey")
                    .IsUnique();

                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");

                entity.HasOne(d => d.ConfigCompanyBusinessUnitGroup)
                    .WithMany(p => p.ConfigCompany)
                    .HasForeignKey(d => d.ConfigCompanyBusinessUnitGroupId)
                    .HasConstraintName("ConfigCompany_ConfigCompanyBusinessUnitGroupId_fkey");

                entity.HasOne(d => d.ConfigCompanyGroup)
                    .WithMany(p => p.InverseConfigCompanyGroup)
                    .HasForeignKey(d => d.ConfigCompanyGroupId)
                    .HasConstraintName("ConfigCompany_ConfigCompanyGroupId_fkey");
            });

            modelBuilder.Entity<ConfigCompanyBusinessUnitGroup>(entity =>
            {
                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");
            });

            modelBuilder.Entity<ConfigCompanyDivision>(entity =>
            {
                entity.HasKey(e => new { e.ConfigCompanyId, e.ConfigDivisionId })
                    .HasName("PK_ConfigCompaniesDivision");

                entity.HasOne(d => d.ConfigCompany)
                    .WithMany(p => p.ConfigCompanyDivision)
                    .HasForeignKey(d => d.ConfigCompanyId)
                    .HasConstraintName("ConfigCompaniesDivision_ConfigCompanyId_fkey");

                entity.HasOne(d => d.ConfigDivision)
                    .WithMany(p => p.ConfigCompanyDivision)
                    .HasForeignKey(d => d.ConfigDivisionId)
                    .HasConstraintName("ConfigCompaniesDivision_ConfigDivisionId_fkey");
            });

            modelBuilder.Entity<ConfigCompanyGroup>(entity =>
            {
                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");
            });

            modelBuilder.Entity<ConfigDivision>(entity =>
            {
                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");
            });

            modelBuilder.Entity<MappingDataBusinessFieldPaymentConfig>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("nextval('mappingdatabusinessfieldpaymentconfig_id_seq'::regclass)");

                entity.HasOne(d => d.MasterDataBusinessField)
                    .WithMany(p => p.MappingDataBusinessFieldPaymentConfig)
                    .HasForeignKey(d => d.MasterDataBusinessFieldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingVendorPaymentConfig_MasterDataBusinessFields");
            });

            modelBuilder.Entity<MappingDataBusinessFieldTopconfig>(entity =>
            {
                entity.HasOne(d => d.MasterDataBusinessField)
                    .WithMany(p => p.MappingDataBusinessFieldTopconfig)
                    .HasForeignKey(d => d.MasterDataBusinessFieldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataBusinessFieldTOPConfig_MasterDataBusinessFields");

                entity.HasOne(d => d.MasterDataTop)
                    .WithMany(p => p.MappingDataBusinessFieldTopconfig)
                    .HasForeignKey(d => d.MasterDataTopId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataBusinessFieldTOPConfig_MasterDataTOP");
            });

            modelBuilder.Entity<MappingDataVendorBusinessFields>(entity =>
            {
                entity.HasOne(d => d.MasterDataBusinessField)
                    .WithMany(p => p.MappingDataVendorBusinessFields)
                    .HasForeignKey(d => d.MasterDataBusinessFieldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataVendorBusinessFields_MasterDataBusinessFields");
            });

            modelBuilder.Entity<MappingDataVendorCategories>(entity =>
            {
                entity.HasKey(e => new { e.MasterDataVendorId, e.CategoriesId })
                    .HasName("MappingDataVendorCategories_pkey");

                entity.HasOne(d => d.Categories)
                    .WithMany(p => p.MappingDataVendorCategories)
                    .HasForeignKey(d => d.CategoriesId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataVendorCategories_MasterItemCategories");
            });

            modelBuilder.Entity<MappingDataVendorCurrencyBank>(entity =>
            {
                entity.HasOne(d => d.MasterDataBank)
                    .WithMany(p => p.MappingDataVendorCurrencyBank)
                    .HasForeignKey(d => d.MasterDataBankId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataVendorCurrencyBank_MasterDataBank");

                entity.HasOne(d => d.MasterDataCurrency)
                    .WithMany(p => p.MappingDataVendorCurrencyBank)
                    .HasForeignKey(d => d.MasterDataCurrencyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataVendorCurrencyBank_MasterDataCurrency");

                entity.HasOne(d => d.MasterDataUser)
                    .WithMany(p => p.MappingDataVendorCurrencyBank)
                    .HasForeignKey(d => d.MasterDataUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataVendorCurrencyBank_MasterDataUser");
            });

            modelBuilder.Entity<MappingDataVendorLegal>(entity =>
            {
                entity.HasIndex(e => e.MasterDataUserId)
                    .HasName("MappingDataVendorLegal_MasterDataUserId_key")
                    .IsUnique();

                entity.HasOne(d => d.MasterDataUser)
                    .WithOne(p => p.MappingDataVendorLegal)
                    .HasForeignKey<MappingDataVendorLegal>(d => d.MasterDataUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataVendorLocal_MasterDataUser");
            });

            modelBuilder.Entity<MappingDataVendorPayment>(entity =>
            {
                entity.HasIndex(e => e.MasterDataUserId)
                    .HasName("MappingDataVendorPayment_MasterDataUserId_key")
                    .IsUnique();

                entity.HasOne(d => d.MasterDataUser)
                    .WithOne(p => p.MappingDataVendorPayment)
                    .HasForeignKey<MappingDataVendorPayment>(d => d.MasterDataUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataVendorPayment_MasterDataUser");
            });

            modelBuilder.Entity<MappingDataVendorTax>(entity =>
            {
                entity.HasIndex(e => e.MasterDataUserId)
                    .HasName("MappingDataVendorTax_MasterDataUser_key")
                    .IsUnique();

                entity.HasOne(d => d.MasterDataBkpCategory)
                    .WithMany(p => p.MappingDataVendorTax)
                    .HasForeignKey(d => d.MasterDataBkpCategoryId)
                    .HasConstraintName("FK_MappingDataVendorTax_MasterDataBkpCategory");

                entity.HasOne(d => d.MasterDataUser)
                    .WithOne(p => p.MappingDataVendorTax)
                    .HasForeignKey<MappingDataVendorTax>(d => d.MasterDataUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MappingDataVendorTax_MasterDataUser");
            });

            modelBuilder.Entity<MappingVendorItemPrice>(entity =>
            {
                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");

                entity.HasOne(d => d.MasterDataItem)
                    .WithMany(p => p.MappingVendorItemPrice)
                    .HasForeignKey(d => d.MasterDataItemId)
                    .HasConstraintName("FK_MasterVendorItemPrice_MasterItems");

                entity.HasOne(d => d.MasterDataVendor)
                    .WithMany(p => p.MappingVendorItemPrice)
                    .HasForeignKey(d => d.MasterDataVendorId)
                    .HasConstraintName("FK_MappingVendorItemPrice_MasterDataVendor");
            });

            modelBuilder.Entity<MappingWorkflowPurchaseRequisition>(entity =>
            {
                entity.HasKey(e => new { e.PurchaseRequisitionId, e.ProcessId })
                    .HasName("workflowpurchaserequisition_pkey");

                entity.HasOne(d => d.Process)
                    .WithMany(p => p.MappingWorkflowPurchaseRequisition)
                    .HasForeignKey(d => d.ProcessId)
                    .HasConstraintName("MappingWorkflowPurchaseRequisition_ProcessId_fkey");

                entity.HasOne(d => d.PurchaseRequisition)
                    .WithMany(p => p.MappingWorkflowPurchaseRequisition)
                    .HasForeignKey(d => d.PurchaseRequisitionId)
                    .HasConstraintName("MappingWorkflowPurchaseRequisition_PurchaseRequisitionId_fkey");
            });

            modelBuilder.Entity<MasterDataBank>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("nextval('masterdatabank_id_seq'::regclass)");
            });

            modelBuilder.Entity<MasterDataBusinessFields>(entity =>
            {
                entity.HasOne(d => d.MasterDataBusinessFieldType)
                    .WithMany(p => p.MasterDataBusinessFields)
                    .HasForeignKey(d => d.MasterDataBusinessFieldTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataBusinessFields_MasterDataBusinessFieldsType");
            });

            modelBuilder.Entity<MasterDataCity>(entity =>
            {
                entity.HasOne(d => d.MasterDataCountry)
                    .WithMany(p => p.MasterDataCity)
                    .HasForeignKey(d => d.MasterDataCountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataCity_MasterDataCountry");

                entity.HasOne(d => d.MasterDataRegion)
                    .WithMany(p => p.MasterDataCity)
                    .HasForeignKey(d => d.MasterDataRegionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataCity_MasterDataRegion");
            });

            modelBuilder.Entity<MasterDataCurrency>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("nextval('\"masterdatacurrency_Id_seq\"'::regclass)");
            });

            modelBuilder.Entity<MasterDataDomain>(entity =>
            {
                entity.HasKey(e => e.DomainLink)
                    .HasName("MasterDataDomain_pkey");

                entity.Property(e => e.DomainLink).ValueGeneratedNever();
            });

            modelBuilder.Entity<MasterDataProcurementStatus>(entity =>
            {
                entity.Property(e => e.Id).UseNpgsqlIdentityAlwaysColumn();
            });

            modelBuilder.Entity<MasterDataRegion>(entity =>
            {
                entity.HasOne(d => d.MasterDataCountry)
                    .WithMany(p => p.MasterDataRegion)
                    .HasForeignKey(d => d.MasterDataCountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataCountry_MasterDataRegion");
            });

            modelBuilder.Entity<MasterDataStatus>(entity =>
            {
                entity.Property(e => e.Id).UseNpgsqlIdentityByDefaultColumn();
            });

            modelBuilder.Entity<MasterDataUser>(entity =>
            {
                entity.HasOne(d => d.UserLevelNavigation)
                    .WithMany(p => p.MasterDataUser)
                    .HasForeignKey(d => d.UserLevel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataUser_MasterDataUserGroup");
            });

            modelBuilder.Entity<MasterDataUserGroup>(entity =>
            {
                entity.HasKey(e => e.UserLevel)
                    .HasName("MasterDataUserGroup_pkey");

                entity.Property(e => e.UserLevel).ValueGeneratedNever();
            });

            modelBuilder.Entity<MasterDataVendor>(entity =>
            {
                entity.HasOne(d => d.MasterDataBusinessType)
                    .WithMany(p => p.MasterDataVendor)
                    .HasForeignKey(d => d.MasterDataBusinessTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataVendor_MasterDataBusinessType");

                entity.HasOne(d => d.MasterDataCity)
                    .WithMany(p => p.MasterDataVendor)
                    .HasForeignKey(d => d.MasterDataCityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataVendor_MasterDataCity");

                entity.HasOne(d => d.MasterDataCountry)
                    .WithMany(p => p.MasterDataVendor)
                    .HasForeignKey(d => d.MasterDataCountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataVendor_MasterDataCountry");

                entity.HasOne(d => d.MasterDataRegion)
                    .WithMany(p => p.MasterDataVendor)
                    .HasForeignKey(d => d.MasterDataRegionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataVendor_MasterDataRegion");

                entity.HasOne(d => d.MasterDataUser)
                    .WithMany(p => p.MasterDataVendor)
                    .HasForeignKey(d => d.MasterDataUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataVendor_MasterDataUser");
            });

            modelBuilder.Entity<MasterDataVendorNotification>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("nextval('\"masterdatavendornotification_Id_seq\"'::regclass)");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");

                entity.HasOne(d => d.MasterDataUser)
                    .WithMany(p => p.MasterDataVendorNotification)
                    .HasForeignKey(d => d.MasterDataUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataVendorNotification");
            });

            modelBuilder.Entity<MasterItemCategories>(entity =>
            {
                entity.HasKey(e => e.CategoryId)
                    .HasName("MasterItemCategories_pkey");

                entity.HasIndex(e => e.CategoryCode)
                    .HasName("MasterItemCategories_CategoryCode_key")
                    .IsUnique();

                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");
            });

            modelBuilder.Entity<MasterItemDetails>(entity =>
            {
                entity.HasKey(e => e.ItemCode)
                    .HasName("MasterItemDetails_pkey");

                entity.Property(e => e.ItemCode).ValueGeneratedNever();

                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");
            });

            modelBuilder.Entity<MasterItems>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("MasterItems_pkey");

                entity.HasIndex(e => e.Hscode)
                    .HasName("MasterItems_HSCode_key")
                    .IsUnique();

                entity.HasIndex(e => e.OracleCode)
                    .HasName("MasterItems_OracleCode_key")
                    .IsUnique();

                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");

                entity.HasOne(d => d.ItemCodeNavigation)
                    .WithMany(p => p.MasterItems)
                    .HasForeignKey(d => d.ItemCode)
                    .HasConstraintName("FK_MasterITems_MasterItemDetails");

                entity.HasOne(d => d.MasterItemCategory)
                    .WithMany(p => p.MasterItems)
                    .HasForeignKey(d => d.MasterItemCategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterItems_MasterItemCategories");

                entity.HasOne(d => d.Uom)
                    .WithMany(p => p.MasterItems)
                    .HasForeignKey(d => d.Uomid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterItems_MasterUnitOfMeasure");
            });

            modelBuilder.Entity<MasterPurchaseOrder>(entity =>
            {
                entity.HasIndex(e => e.MasterDataVendorId)
                    .HasName("fki_FK_MasterVendor_MasterPurchaseOrder");

                entity.HasIndex(e => e.MasterPrid)
                    .HasName("fki_FK_MasterPurchaseRequisition_MasterPurchaseOrder");

                entity.HasOne(d => d.MasterDataVendor)
                    .WithMany(p => p.MasterPurchaseOrder)
                    .HasForeignKey(d => d.MasterDataVendorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterVendor_MasterPurchaseOrder");

                entity.HasOne(d => d.MasterPr)
                    .WithMany(p => p.MasterPurchaseOrder)
                    .HasForeignKey(d => d.MasterPrid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterPurchaseRequisition_MasterPurchaseOrder");
            });

            modelBuilder.Entity<MasterPurchaseOrderItem>(entity =>
            {
                entity.HasIndex(e => e.CurrencyId)
                    .HasName("fki_FK_MasterCurrency_MasterPurchaseOrderItem");

                entity.HasIndex(e => e.MasterItemId)
                    .HasName("fki_FK_MasterItem_MasterPurchaseOrderItem");

                entity.HasIndex(e => e.MasterPurchaseOrderId)
                    .HasName("fki_FK_MasterPurchaseOrder_MasterPurchaseOrderItem");

                entity.HasOne(d => d.Currency)
                    .WithMany(p => p.MasterPurchaseOrderItem)
                    .HasForeignKey(d => d.CurrencyId)
                    .HasConstraintName("FK_MasterCurrency_MasterPurchaseOrderItem");

                entity.HasOne(d => d.MasterItem)
                    .WithMany(p => p.MasterPurchaseOrderItem)
                    .HasForeignKey(d => d.MasterItemId)
                    .HasConstraintName("FK_MasterItem_MasterPurchaseOrderItem");

                entity.HasOne(d => d.MasterPurchaseOrder)
                    .WithMany(p => p.MasterPurchaseOrderItem)
                    .HasForeignKey(d => d.MasterPurchaseOrderId)
                    .HasConstraintName("FK_MasterPurchaseOrder_MasterPurchaseOrderItem");
            });

            modelBuilder.Entity<MasterPurchaseRequisition>(entity =>
            {
                entity.HasIndex(e => e.Prnumber)
                    .HasName("pr_number_unique_key")
                    .IsUnique();

                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");
            });

            modelBuilder.Entity<MasterPurchaseRequisitionItem>(entity =>
            {
                entity.HasOne(d => d.Currency)
                    .WithMany(p => p.MasterPurchaseRequisitionItem)
                    .HasForeignKey(d => d.CurrencyId)
                    .HasConstraintName("FK_MasterCurrency_MasterPurchaseRequisitionItem");

                entity.HasOne(d => d.MasterItem)
                    .WithMany(p => p.MasterPurchaseRequisitionItem)
                    .HasForeignKey(d => d.MasterItemId)
                    .HasConstraintName("FK_MasterItem_MasterPurchaseRequisitionItem");

                entity.HasOne(d => d.MasterPurchaseRequisition)
                    .WithMany(p => p.MasterPurchaseRequisitionItem)
                    .HasForeignKey(d => d.MasterPurchaseRequisitionId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_MasterPurchaseRequisition_MasterPurchaseRequisitionItem");
            });

            modelBuilder.Entity<MasterUnitOfMeasure>(entity =>
            {
                entity.HasKey(e => e.Uomid)
                    .HasName("MasterUnitOfMeasure_pkey");

                entity.HasIndex(e => e.Uomcode)
                    .HasName("MasterUnitOfMeasure_UOMCode_key")
                    .IsUnique();

                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");
            });

            modelBuilder.Entity<WorkflowActionApproval>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("false");

                entity.Property(e => e.IsComplete).HasDefaultValueSql("true");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");

                entity.HasOne(d => d.WorkflowProcess)
                    .WithMany(p => p.WorkflowActionApproval)
                    .HasForeignKey(d => d.WorkflowProcessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("WorkflowActionApproval_WorkflowProcessId");

                entity.HasOne(d => d.WorkflowRequest)
                    .WithMany(p => p.WorkflowActionApproval)
                    .HasForeignKey(d => d.WorkflowRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("WorkflowActionApproval_WorkflowRequestId");
            });

            modelBuilder.Entity<WorkflowActivity>(entity =>
            {
                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");

                entity.HasOne(d => d.WorkflowActionApproval)
                    .WithMany(p => p.WorkflowActivity)
                    .HasForeignKey(d => d.WorkflowActionApprovalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("WorkflowActivity_WorkflowActionApprovalId");
            });

            modelBuilder.Entity<WorkflowGroupMember>(entity =>
            {
                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");

                entity.HasOne(d => d.ConfigCompany)
                    .WithMany(p => p.WorkflowGroupMemberConfigCompany)
                    .HasForeignKey(d => d.ConfigCompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("WorkflowGroupMember_ConfigCompanyId");

                entity.HasOne(d => d.ConfigDivision)
                    .WithMany(p => p.WorkflowGroupMemberConfigDivision)
                    .HasForeignKey(d => d.ConfigDivisionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("WorkflowGroupMember_ConfigDivisionId");
            });

            modelBuilder.Entity<WorkflowProcess>(entity =>
            {
                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");

                entity.HasOne(d => d.MasterDataUser)
                    .WithMany(p => p.WorkflowProcess)
                    .HasForeignKey(d => d.MasterDataUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterDataUser_WorkflowProcess");
            });

            modelBuilder.Entity<WorkflowRequest>(entity =>
            {
                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");

                entity.HasOne(d => d.MasterDataUser)
                    .WithMany(p => p.WorkflowRequest)
                    .HasForeignKey(d => d.MasterDataUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("WorkflowRequest_UserId");

                entity.HasOne(d => d.WorkflowProcess)
                    .WithMany(p => p.WorkflowRequest)
                    .HasForeignKey(d => d.WorkflowProcessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("WorkflowRequest_WorkflowProcessId");
            });

            modelBuilder.Entity<WorkflowType>(entity =>
            {
                entity.Property(e => e.IsDelete).HasDefaultValueSql("false");
            });

            modelBuilder.HasSequence<int>("mappingdatabusinessfieldpaymentconfig_id_seq");

            modelBuilder.HasSequence<int>("masterdatabank_id_seq");

            modelBuilder.HasSequence<int>("masterdatacurrency_Id_seq");

            modelBuilder.HasSequence("masterdatavendornotification_Id_seq");

            modelBuilder.HasSequence("masterpurchaseorder_id_seq").HasMax(2147483647);

            modelBuilder.HasSequence("masterpurchaseorderitem_id_seq").HasMax(2147483647);

            modelBuilder.HasSequence("masterpurchaserequisition_id_seq");

            modelBuilder.HasSequence("masterpurchaserequisitionitem_id_seq").HasMax(2147483647);
        }
    }
}
