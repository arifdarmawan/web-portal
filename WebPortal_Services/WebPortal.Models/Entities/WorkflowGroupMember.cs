﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class WorkflowGroupMember
    {
        public int Id { get; set; }
        public int ConfigCompanyId { get; set; }
        public int ConfigDivisionId { get; set; }
        [Required]
        [StringLength(10)]
        public string WorkflowTypeCode { get; set; }
        public int MasterDataUserId { get; set; }
        public int ApprovalLevel { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        public bool? IsDelete { get; set; }

        [ForeignKey("ConfigCompanyId")]
        [InverseProperty("WorkflowGroupMemberConfigCompany")]
        public virtual ConfigCompany ConfigCompany { get; set; }
        [ForeignKey("ConfigDivisionId")]
        [InverseProperty("WorkflowGroupMemberConfigDivision")]
        public virtual ConfigCompany ConfigDivision { get; set; }
    }
}
