﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterPurchaseRequisition
    {
        public MasterPurchaseRequisition()
        {
            MappingWorkflowPurchaseRequisition = new HashSet<MappingWorkflowPurchaseRequisition>();
            MasterPurchaseOrder = new HashSet<MasterPurchaseOrder>();
            MasterPurchaseRequisitionItem = new HashSet<MasterPurchaseRequisitionItem>();
        }

        public int Id { get; set; }
        [Required]
        [Column("PRNumber")]
        [StringLength(30)]
        public string Prnumber { get; set; }
        [StringLength(50)]
        public string RequestedFrom { get; set; }
        [StringLength(10)]
        public string CompanyCode { get; set; }
        [StringLength(20)]
        public string CompanyLocationCode { get; set; }
        [StringLength(10)]
        public string DepartementCode { get; set; }
        [Column("PRStatus")]
        [StringLength(50)]
        public string Prstatus { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? GrandTotal { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? GrandTotalAfterTax { get; set; }
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        public DateTime ModifyDate { get; set; }
        public bool? IsDelete { get; set; }

        [InverseProperty("PurchaseRequisition")]
        public virtual ICollection<MappingWorkflowPurchaseRequisition> MappingWorkflowPurchaseRequisition { get; set; }
        [InverseProperty("MasterPr")]
        public virtual ICollection<MasterPurchaseOrder> MasterPurchaseOrder { get; set; }
        [InverseProperty("MasterPurchaseRequisition")]
        public virtual ICollection<MasterPurchaseRequisitionItem> MasterPurchaseRequisitionItem { get; set; }
    }
}
