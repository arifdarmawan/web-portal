﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MappingWorkflowPurchaseRequisition
    {
        public int PurchaseRequisitionId { get; set; }
        public int ProcessId { get; set; }

        [ForeignKey("ProcessId")]
        [InverseProperty("MappingWorkflowPurchaseRequisition")]
        public virtual WorkflowProcess Process { get; set; }
        [ForeignKey("PurchaseRequisitionId")]
        [InverseProperty("MappingWorkflowPurchaseRequisition")]
        public virtual MasterPurchaseRequisition PurchaseRequisition { get; set; }
    }
}
