﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class WorkflowActionApproval
    {
        public WorkflowActionApproval()
        {
            WorkflowActivity = new HashSet<WorkflowActivity>();
        }

        public int Id { get; set; }
        public int WorkflowProcessId { get; set; }
        public int WorkflowRequestId { get; set; }
        public int MasterDataUserId { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsComplete { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        public bool? IsDelete { get; set; }

        [ForeignKey("WorkflowProcessId")]
        [InverseProperty("WorkflowActionApproval")]
        public virtual WorkflowProcess WorkflowProcess { get; set; }
        [ForeignKey("WorkflowRequestId")]
        [InverseProperty("WorkflowActionApproval")]
        public virtual WorkflowRequest WorkflowRequest { get; set; }
        [InverseProperty("WorkflowActionApproval")]
        public virtual ICollection<WorkflowActivity> WorkflowActivity { get; set; }
    }
}
