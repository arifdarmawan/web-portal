﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class ConfigPurchaseRequisitionUser
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Email { get; set; }
        public int ConfigCompanyId { get; set; }
        public int ConfigDivisionId { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        public bool? IsDelete { get; set; }

        [ForeignKey("ConfigCompanyId")]
        [InverseProperty("ConfigPurchaseRequisitionUser")]
        public virtual ConfigCompany ConfigCompany { get; set; }
        [ForeignKey("ConfigDivisionId")]
        [InverseProperty("ConfigPurchaseRequisitionUser")]
        public virtual ConfigDivision ConfigDivision { get; set; }
        [ForeignKey("Id")]
        [InverseProperty("ConfigPurchaseRequisitionUser")]
        public virtual MasterDataUser IdNavigation { get; set; }
    }
}
