﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MappingVendorItemPrice
    {
        public int Id { get; set; }
        public int? MasterDataVendorId { get; set; }
        public int? MasterDataItemId { get; set; }
        public int? MasterDataCurrencyId { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? Price { get; set; }
        [Column(TypeName = "date")]
        public DateTime? PriceValidDate { get; set; }
        [StringLength(100)]
        public string CreatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        [StringLength(100)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }

        [ForeignKey("MasterDataItemId")]
        [InverseProperty("MappingVendorItemPrice")]
        public virtual MasterItems MasterDataItem { get; set; }
        [ForeignKey("MasterDataVendorId")]
        [InverseProperty("MappingVendorItemPrice")]
        public virtual MasterDataVendor MasterDataVendor { get; set; }
    }
}
