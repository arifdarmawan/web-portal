﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class ConfigCompanyBusinessUnitGroup
    {
        public ConfigCompanyBusinessUnitGroup()
        {
            ConfigCompany = new HashSet<ConfigCompany>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string CompanyBusinessUnitName { get; set; }
        [Required]
        [StringLength(100)]
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        public bool? IsDelete { get; set; }

        [InverseProperty("ConfigCompanyBusinessUnitGroup")]
        public virtual ICollection<ConfigCompany> ConfigCompany { get; set; }
    }
}
