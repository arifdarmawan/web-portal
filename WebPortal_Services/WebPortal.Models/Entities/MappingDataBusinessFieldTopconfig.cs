﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    [Table("MappingDataBusinessFieldTOPConfig")]
    public partial class MappingDataBusinessFieldTopconfig
    {
        public int Id { get; set; }
        public int MasterDataBusinessFieldId { get; set; }
        public int MasterDataTopId { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }

        [ForeignKey("MasterDataBusinessFieldId")]
        [InverseProperty("MappingDataBusinessFieldTopconfig")]
        public virtual MasterDataBusinessFields MasterDataBusinessField { get; set; }
        [ForeignKey("MasterDataTopId")]
        [InverseProperty("MappingDataBusinessFieldTopconfig")]
        public virtual MasterDataTop MasterDataTop { get; set; }
    }
}
