﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataStatus
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
    }
}
