﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterItems
    {
        public MasterItems()
        {
            MappingVendorItemPrice = new HashSet<MappingVendorItemPrice>();
            MasterPurchaseOrderItem = new HashSet<MasterPurchaseOrderItem>();
            MasterPurchaseRequisitionItem = new HashSet<MasterPurchaseRequisitionItem>();
        }

        public int ItemId { get; set; }
        public int MasterItemCategoryId { get; set; }
        [StringLength(10)]
        public string ItemCode { get; set; }
        [Required]
        [StringLength(20)]
        public string OracleCode { get; set; }
        [Column("HSCode")]
        [StringLength(20)]
        public string Hscode { get; set; }
        [Required]
        [StringLength(100)]
        public string ItemName { get; set; }
        [Column("UOMId")]
        public int Uomid { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(100)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [StringLength(100)]
        public string ModifyBy { get; set; }
        public bool? IsDelete { get; set; }
        [StringLength(20)]
        public string Status { get; set; }
        [StringLength(10)]
        public string BimsCode { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? UnitPrice { get; set; }

        [ForeignKey("ItemCode")]
        [InverseProperty("MasterItems")]
        public virtual MasterItemDetails ItemCodeNavigation { get; set; }
        [ForeignKey("MasterItemCategoryId")]
        [InverseProperty("MasterItems")]
        public virtual MasterItemCategories MasterItemCategory { get; set; }
        [ForeignKey("Uomid")]
        [InverseProperty("MasterItems")]
        public virtual MasterUnitOfMeasure Uom { get; set; }
        [InverseProperty("MasterDataItem")]
        public virtual ICollection<MappingVendorItemPrice> MappingVendorItemPrice { get; set; }
        [InverseProperty("MasterItem")]
        public virtual ICollection<MasterPurchaseOrderItem> MasterPurchaseOrderItem { get; set; }
        [InverseProperty("MasterItem")]
        public virtual ICollection<MasterPurchaseRequisitionItem> MasterPurchaseRequisitionItem { get; set; }
    }
}
