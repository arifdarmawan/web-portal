﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class ConfigCompanyDivision
    {
        public int ConfigCompanyId { get; set; }
        public int ConfigDivisionId { get; set; }

        [ForeignKey("ConfigCompanyId")]
        [InverseProperty("ConfigCompanyDivision")]
        public virtual ConfigCompany ConfigCompany { get; set; }
        [ForeignKey("ConfigDivisionId")]
        [InverseProperty("ConfigCompanyDivision")]
        public virtual ConfigDivision ConfigDivision { get; set; }
    }
}
