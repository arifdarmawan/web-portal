﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataCurrency
    {
        public MasterDataCurrency()
        {
            MappingDataVendorCurrencyBank = new HashSet<MappingDataVendorCurrencyBank>();
            MasterPurchaseOrderItem = new HashSet<MasterPurchaseOrderItem>();
            MasterPurchaseRequisitionItem = new HashSet<MasterPurchaseRequisitionItem>();
        }

        public int Id { get; set; }
        [StringLength(5)]
        public string Code { get; set; }
        [Required]
        [StringLength(20)]
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifiyBy { get; set; }
        public bool? IsDelete { get; set; }

        [InverseProperty("MasterDataCurrency")]
        public virtual ICollection<MappingDataVendorCurrencyBank> MappingDataVendorCurrencyBank { get; set; }
        [InverseProperty("Currency")]
        public virtual ICollection<MasterPurchaseOrderItem> MasterPurchaseOrderItem { get; set; }
        [InverseProperty("Currency")]
        public virtual ICollection<MasterPurchaseRequisitionItem> MasterPurchaseRequisitionItem { get; set; }
    }
}
