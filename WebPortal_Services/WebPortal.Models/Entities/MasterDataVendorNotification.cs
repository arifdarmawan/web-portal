﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataVendorNotification
    {
        public long Id { get; set; }
        public int MasterDataUserId { get; set; }
        [Required]
        [StringLength(20)]
        public string NotificationType { get; set; }
        [StringLength(256)]
        public string Message { get; set; }
        public bool IsRead { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }

        [ForeignKey("MasterDataUserId")]
        [InverseProperty("MasterDataVendorNotification")]
        public virtual MasterDataUser MasterDataUser { get; set; }
    }
}
