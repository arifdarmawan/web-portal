﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataUser
    {
        public MasterDataUser()
        {
            MappingDataVendorCurrencyBank = new HashSet<MappingDataVendorCurrencyBank>();
            MasterDataVendor = new HashSet<MasterDataVendor>();
            MasterDataVendorNotification = new HashSet<MasterDataVendorNotification>();
            WorkflowProcess = new HashSet<WorkflowProcess>();
            WorkflowRequest = new HashSet<WorkflowRequest>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string UserId { get; set; }
        [Required]
        [StringLength(50)]
        public string UserName { get; set; }
        [StringLength(50)]
        public string Password { get; set; }
        [StringLength(50)]
        public string ForgetPassword { get; set; }
        [StringLength(50)]
        public string Region { get; set; }
        [Required]
        [StringLength(100)]
        public string Email { get; set; }
        [StringLength(1000)]
        public string SecondaryEmail { get; set; }
        [StringLength(50)]
        public string Jabatan { get; set; }
        [StringLength(50)]
        public string Divisi { get; set; }
        [StringLength(20)]
        public string ExtensionNumber { get; set; }
        public DateTime? LastLogin { get; set; }
        public DateTime? LastLogout { get; set; }
        public string Session { get; set; }
        public int UserLevel { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        [Column(TypeName = "character varying")]
        public string Image { get; set; }
        [Column("IP")]
        [StringLength(50)]
        public string Ip { get; set; }
        public DateTime? StartAccess { get; set; }
        public DateTime? EndAccess { get; set; }
        [StringLength(50)]
        public string Domain { get; set; }
        [StringLength(32)]
        public string ActivateLink { get; set; }
        public bool? IsDelete { get; set; }
        public bool IsUserAd { get; set; }
        public bool IsActive { get; set; }
        public bool? IsTemporary { get; set; }
        [StringLength(20)]
        public string RegistrationNumber { get; set; }

        [ForeignKey("UserLevel")]
        [InverseProperty("MasterDataUser")]
        public virtual MasterDataUserGroup UserLevelNavigation { get; set; }
        [InverseProperty("MasterDataUser")]
        public virtual MappingDataVendorLegal MappingDataVendorLegal { get; set; }
        [InverseProperty("MasterDataUser")]
        public virtual MappingDataVendorPayment MappingDataVendorPayment { get; set; }
        [InverseProperty("MasterDataUser")]
        public virtual MappingDataVendorTax MappingDataVendorTax { get; set; }
        [InverseProperty("MasterDataUser")]
        public virtual ICollection<MappingDataVendorCurrencyBank> MappingDataVendorCurrencyBank { get; set; }
        [InverseProperty("MasterDataUser")]
        public virtual ICollection<MasterDataVendor> MasterDataVendor { get; set; }
        [InverseProperty("MasterDataUser")]
        public virtual ICollection<MasterDataVendorNotification> MasterDataVendorNotification { get; set; }
        [InverseProperty("MasterDataUser")]
        public virtual ICollection<WorkflowProcess> WorkflowProcess { get; set; }
        [InverseProperty("MasterDataUser")]
        public virtual ICollection<WorkflowRequest> WorkflowRequest { get; set; }
    }
}
