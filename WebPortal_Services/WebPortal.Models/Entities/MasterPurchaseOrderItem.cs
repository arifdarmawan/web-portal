﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterPurchaseOrderItem
    {
        public int Id { get; set; }
        [StringLength(225)]
        public string Specification { get; set; }
        [StringLength(225)]
        public string PurposeDesc { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? Qty { get; set; }
        public int? CurrencyId { get; set; }
        public int? MasterPurchaseOrderId { get; set; }
        public int? MasterItemId { get; set; }
        [Column("UOMId")]
        public int? Uomid { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? UnitPrice { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? TotalPrice { get; set; }
        [Column("PPN")]
        public int? Ppn { get; set; }
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }

        [ForeignKey("CurrencyId")]
        [InverseProperty("MasterPurchaseOrderItem")]
        public virtual MasterDataCurrency Currency { get; set; }
        [ForeignKey("MasterItemId")]
        [InverseProperty("MasterPurchaseOrderItem")]
        public virtual MasterItems MasterItem { get; set; }
        [ForeignKey("MasterPurchaseOrderId")]
        [InverseProperty("MasterPurchaseOrderItem")]
        public virtual MasterPurchaseOrder MasterPurchaseOrder { get; set; }
    }
}
