﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataBkpCategory
    {
        public MasterDataBkpCategory()
        {
            MappingDataVendorTax = new HashSet<MappingDataVendorTax>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(256)]
        public string Desc { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }

        [InverseProperty("MasterDataBkpCategory")]
        public virtual ICollection<MappingDataVendorTax> MappingDataVendorTax { get; set; }
    }
}
