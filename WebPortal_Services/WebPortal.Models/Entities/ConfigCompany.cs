﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class ConfigCompany
    {
        public ConfigCompany()
        {
            ConfigCompanyDivision = new HashSet<ConfigCompanyDivision>();
            InverseConfigCompanyGroup = new HashSet<ConfigCompany>();
            WorkflowGroupMemberConfigCompany = new HashSet<WorkflowGroupMember>();
            WorkflowGroupMemberConfigDivision = new HashSet<WorkflowGroupMember>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(10)]
        public string CompanyCode { get; set; }
        [Required]
        [StringLength(256)]
        public string CompanyName { get; set; }
        public int ConfigCompanyGroupId { get; set; }
        public int ConfigCompanyBusinessUnitGroupId { get; set; }
        public DateTime CreatedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        public bool? IsDelete { get; set; }
        [StringLength(225)]
        public string Address { get; set; }
        [Column("NPWP")]
        [StringLength(100)]
        public string Npwp { get; set; }

        [ForeignKey("ConfigCompanyBusinessUnitGroupId")]
        [InverseProperty("ConfigCompany")]
        public virtual ConfigCompanyBusinessUnitGroup ConfigCompanyBusinessUnitGroup { get; set; }
        [ForeignKey("ConfigCompanyGroupId")]
        [InverseProperty("InverseConfigCompanyGroup")]
        public virtual ConfigCompany ConfigCompanyGroup { get; set; }
        [InverseProperty("ConfigCompany")]
        public virtual ICollection<ConfigCompanyDivision> ConfigCompanyDivision { get; set; }
        [InverseProperty("ConfigCompanyGroup")]
        public virtual ICollection<ConfigCompany> InverseConfigCompanyGroup { get; set; }
        [InverseProperty("ConfigCompany")]
        public virtual ICollection<WorkflowGroupMember> WorkflowGroupMemberConfigCompany { get; set; }
        [InverseProperty("ConfigDivision")]
        public virtual ICollection<WorkflowGroupMember> WorkflowGroupMemberConfigDivision { get; set; }
    }
}
