﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataCountry
    {
        public MasterDataCountry()
        {
            MasterDataCity = new HashSet<MasterDataCity>();
            MasterDataRegion = new HashSet<MasterDataRegion>();
            MasterDataVendor = new HashSet<MasterDataVendor>();
        }

        public int Id { get; set; }
        [StringLength(20)]
        public string Code { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(10)]
        public string PhoneCode { get; set; }
        [StringLength(5)]
        public string Currency { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifiyBy { get; set; }
        public bool? IsDelete { get; set; }

        [InverseProperty("MasterDataCountry")]
        public virtual ICollection<MasterDataCity> MasterDataCity { get; set; }
        [InverseProperty("MasterDataCountry")]
        public virtual ICollection<MasterDataRegion> MasterDataRegion { get; set; }
        [InverseProperty("MasterDataCountry")]
        public virtual ICollection<MasterDataVendor> MasterDataVendor { get; set; }
    }
}
