﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterPurchaseRequisitionItem
    {
        public int Id { get; set; }
        [StringLength(225)]
        public string Specification { get; set; }
        [StringLength(225)]
        public string PurposeDesc { get; set; }
        public int? MasterPurchaseRequisitionId { get; set; }
        public int? MasterItemId { get; set; }
        [Column("UOMId")]
        public int? Uomid { get; set; }
        public int? CurrencyId { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? Qty { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? UnitPrice { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? TotalPrice { get; set; }
        [Column("PPN")]
        public int? Ppn { get; set; }
        [StringLength(50)]
        public string BudgetStatus { get; set; }
        [StringLength(50)]
        public string ItemVendorStatus { get; set; }
        public DateTime DateRequired { get; set; }
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
        public bool? IsRequestVendor { get; set; }

        [ForeignKey("CurrencyId")]
        [InverseProperty("MasterPurchaseRequisitionItem")]
        public virtual MasterDataCurrency Currency { get; set; }
        [ForeignKey("MasterItemId")]
        [InverseProperty("MasterPurchaseRequisitionItem")]
        public virtual MasterItems MasterItem { get; set; }
        [ForeignKey("MasterPurchaseRequisitionId")]
        [InverseProperty("MasterPurchaseRequisitionItem")]
        public virtual MasterPurchaseRequisition MasterPurchaseRequisition { get; set; }
    }
}
