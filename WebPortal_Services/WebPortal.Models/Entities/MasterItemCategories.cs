﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterItemCategories
    {
        public MasterItemCategories()
        {
            MappingDataVendorCategories = new HashSet<MappingDataVendorCategories>();
            MasterItems = new HashSet<MasterItems>();
        }

        public int CategoryId { get; set; }
        [Required]
        [StringLength(10)]
        public string CategoryCode { get; set; }
        [Required]
        [StringLength(100)]
        public string CategoryName { get; set; }
        public int? ParentCategoryId { get; set; }
        [StringLength(256)]
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(100)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [StringLength(100)]
        public string ModifyBy { get; set; }
        public bool? IsDelete { get; set; }

        [InverseProperty("Categories")]
        public virtual ICollection<MappingDataVendorCategories> MappingDataVendorCategories { get; set; }
        [InverseProperty("MasterItemCategory")]
        public virtual ICollection<MasterItems> MasterItems { get; set; }
    }
}
