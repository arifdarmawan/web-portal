﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterUnitOfMeasure
    {
        public MasterUnitOfMeasure()
        {
            MasterItems = new HashSet<MasterItems>();
        }

        [Column("UOMId")]
        public int Uomid { get; set; }
        [Required]
        [Column("UOMCode")]
        [StringLength(10)]
        public string Uomcode { get; set; }
        [Required]
        [Column("UOMName")]
        [StringLength(50)]
        public string Uomname { get; set; }
        [StringLength(256)]
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(100)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [StringLength(100)]
        public string ModifyBy { get; set; }
        public bool? IsDelete { get; set; }

        [InverseProperty("Uom")]
        public virtual ICollection<MasterItems> MasterItems { get; set; }
    }
}
