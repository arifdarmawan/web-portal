﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MappingDataVendorLegal
    {
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        [Column("NIB")]
        [StringLength(100)]
        public string Nib { get; set; }
        [Column("NIBAttachment")]
        [StringLength(256)]
        public string Nibattachment { get; set; }
        [StringLength(100)]
        public string BusinessPermit { get; set; }
        [StringLength(256)]
        public string BusinessPermitAttachment { get; set; }
        [Column("SIUP")]
        [StringLength(100)]
        public string Siup { get; set; }
        [Column("SIUPExpiredDate", TypeName = "date")]
        public DateTime? SiupexpiredDate { get; set; }
        [Column("SIUPAttachment")]
        [StringLength(256)]
        public string Siupattachment { get; set; }
        [Column("TDP")]
        [StringLength(100)]
        public string Tdp { get; set; }
        [Column("TDPExpiredDate", TypeName = "date")]
        public DateTime? TdpexpiredDate { get; set; }
        [Column("TDPAttachment")]
        [StringLength(256)]
        public string Tdpattachment { get; set; }
        [StringLength(100)]
        public string MemorandumOfAssociation { get; set; }
        [StringLength(256)]
        public string MemorandumOfAssociationAttachment { get; set; }
        [StringLength(100)]
        public string DecissionLetterMenkumham { get; set; }
        [StringLength(256)]
        public string DecissionLetterMenkumhamAttachment { get; set; }
        [StringLength(100)]
        public string MemorandumOfAssociationChanging { get; set; }
        [StringLength(256)]
        public string MemorandumOfAssociationChangingAttachment { get; set; }
        [StringLength(100)]
        public string DecissionLetterMenkumhamChanging { get; set; }
        [StringLength(256)]
        public string DecissionLetterMenkumhamChangingAttachment { get; set; }
        [StringLength(256)]
        public string Signing { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifiyBy { get; set; }
        public bool? IsDelete { get; set; }
        [Column(TypeName = "jsonb")]
        public string ManagementStructure { get; set; }
        public int? SigningId { get; set; }
        [StringLength(256)]
        public string SigningAttachment { get; set; }
        [StringLength(100)]
        public string DocumentOther { get; set; }
        [StringLength(256)]
        public string OtherAttachment { get; set; }

        [ForeignKey("MasterDataUserId")]
        [InverseProperty("MappingDataVendorLegal")]
        public virtual MasterDataUser MasterDataUser { get; set; }
    }
}
