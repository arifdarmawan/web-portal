﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class WorkflowRequest
    {
        public WorkflowRequest()
        {
            WorkflowActionApproval = new HashSet<WorkflowActionApproval>();
        }

        public int Id { get; set; }
        public int WorkflowProcessId { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        public int MasterDataUserId { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        public bool? IsDelete { get; set; }

        [ForeignKey("MasterDataUserId")]
        [InverseProperty("WorkflowRequest")]
        public virtual MasterDataUser MasterDataUser { get; set; }
        [ForeignKey("WorkflowProcessId")]
        [InverseProperty("WorkflowRequest")]
        public virtual WorkflowProcess WorkflowProcess { get; set; }
        [InverseProperty("WorkflowRequest")]
        public virtual ICollection<WorkflowActionApproval> WorkflowActionApproval { get; set; }
    }
}
