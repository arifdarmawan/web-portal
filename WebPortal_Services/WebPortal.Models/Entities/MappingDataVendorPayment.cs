﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MappingDataVendorPayment
    {
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        public int MappingDataBusinessFieldTopConfigId { get; set; }
        public int MappingDataBusinessFieldPaymentConfigId { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(20)]
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }

        [ForeignKey("MasterDataUserId")]
        [InverseProperty("MappingDataVendorPayment")]
        public virtual MasterDataUser MasterDataUser { get; set; }
    }
}
