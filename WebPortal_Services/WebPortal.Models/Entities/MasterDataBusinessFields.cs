﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class MasterDataBusinessFields
    {
        public MasterDataBusinessFields()
        {
            MappingDataBusinessFieldPaymentConfig = new HashSet<MappingDataBusinessFieldPaymentConfig>();
            MappingDataBusinessFieldTopconfig = new HashSet<MappingDataBusinessFieldTopconfig>();
            MappingDataVendorBusinessFields = new HashSet<MappingDataVendorBusinessFields>();
        }

        public int Id { get; set; }
        public int MasterDataBusinessFieldTypeId { get; set; }
        [Required]
        [StringLength(250)]
        public string Name { get; set; }
        [Column("KBLI")]
        public int Kbli { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifiyBy { get; set; }
        public bool? IsDelete { get; set; }

        [ForeignKey("MasterDataBusinessFieldTypeId")]
        [InverseProperty("MasterDataBusinessFields")]
        public virtual MasterDataBusinessFieldsType MasterDataBusinessFieldType { get; set; }
        [InverseProperty("MasterDataBusinessField")]
        public virtual ICollection<MappingDataBusinessFieldPaymentConfig> MappingDataBusinessFieldPaymentConfig { get; set; }
        [InverseProperty("MasterDataBusinessField")]
        public virtual ICollection<MappingDataBusinessFieldTopconfig> MappingDataBusinessFieldTopconfig { get; set; }
        [InverseProperty("MasterDataBusinessField")]
        public virtual ICollection<MappingDataVendorBusinessFields> MappingDataVendorBusinessFields { get; set; }
    }
}
