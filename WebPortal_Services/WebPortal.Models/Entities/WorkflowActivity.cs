﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebPortal.Models.Entities
{
    public partial class WorkflowActivity
    {
        public int Id { get; set; }
        public int WorkflowActionApprovalId { get; set; }
        [Required]
        [StringLength(50)]
        public string Activity { get; set; }
        [StringLength(250)]
        public string Desc { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        public bool? IsDelete { get; set; }
        [StringLength(250)]
        public string Noted { get; set; }

        [ForeignKey("WorkflowActionApprovalId")]
        [InverseProperty("WorkflowActivity")]
        public virtual WorkflowActionApproval WorkflowActionApproval { get; set; }
    }
}
