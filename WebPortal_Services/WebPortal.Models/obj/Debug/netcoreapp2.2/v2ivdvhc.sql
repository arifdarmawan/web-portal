﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [MasterDataBank] (
    [Id] int NOT NULL IDENTITY,
    [Code] varchar(10) NULL,
    [Name] varchar(20) NOT NULL,
    [CreatedDate] datetime NOT NULL,
    [CreatedBy] varchar(20) NOT NULL,
    [ModifiedDate] datetime NOT NULL,
    [ModifiyBy] varchar(20) NOT NULL,
    [IsDelete] bit NULL,
    CONSTRAINT [PK_MasterDataBank] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [MasterDataBusinessFieldsType] (
    [Id] int NOT NULL IDENTITY,
    [Code] varchar(20) NULL,
    [Name] varchar(100) NOT NULL,
    [Desc] varchar(100) NULL,
    [CreatedDate] datetime NOT NULL,
    [CreatedBy] varchar(50) NOT NULL,
    [ModifiedDate] datetime NOT NULL,
    [ModifiyBy] varchar(50) NOT NULL,
    [IsDelete] bit NULL,
    CONSTRAINT [PK_MasterDataBusinessFieldsType] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [MasterDataBusinessType] (
    [Id] int NOT NULL IDENTITY,
    [Code] varchar(5) NULL,
    [Name] varchar(100) NOT NULL,
    [Desc] varchar(100) NULL,
    [CreatedDate] datetime NOT NULL,
    [CreatedBy] varchar(20) NOT NULL,
    [ModifiedDate] datetime NOT NULL,
    [ModifiyBy] varchar(20) NOT NULL,
    [IsDelete] bit NULL,
    CONSTRAINT [PK_MasterDataBusinessType] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [MasterDataCountry] (
    [Id] int NOT NULL IDENTITY,
    [Code] varchar(20) NULL,
    [Name] varchar(50) NOT NULL,
    [PhoneCode] varchar(10) NULL,
    [Currency] varchar(5) NULL,
    [CreatedDate] datetime NOT NULL,
    [CreatedBy] varchar(50) NOT NULL,
    [ModifiedDate] datetime NOT NULL,
    [ModifiyBy] varchar(50) NOT NULL,
    [IsDelete] bit NULL,
    CONSTRAINT [PK_MasterDataCountry] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [MasterDataCurrency] (
    [Id] int NOT NULL IDENTITY,
    [Code] varchar(5) NULL,
    [Name] varchar(20) NOT NULL,
    [CreatedDate] datetime NOT NULL,
    [CreatedBy] varchar(20) NOT NULL,
    [ModifiedDate] datetime NOT NULL,
    [ModifiyBy] varchar(20) NOT NULL,
    [IsDelete] bit NOT NULL,
    CONSTRAINT [PK_MasterDataCurrency] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [MasterDataPaymentMethod] (
    [Id] int NOT NULL IDENTITY,
    [Code] varchar(10) NULL,
    [Name] varchar(20) NOT NULL,
    [PaymentDuration] int NOT NULL,
    [DP] decimal(18, 0) NULL,
    [Progressive] decimal(18, 0) NULL,
    [FullPayment] decimal(18, 0) NULL,
    [Retention] nvarchar(10) NULL,
    [CreatedDate] datetime NOT NULL,
    [CreatedBy] varchar(20) NOT NULL,
    [ModifiedDate] datetime NOT NULL,
    [ModifiyBy] varchar(20) NOT NULL,
    [IsDelete] nvarchar(10) NULL,
    CONSTRAINT [PK_MasterDataPaymentMethod] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_Chat] (
    [id] int NOT NULL IDENTITY,
    [FromName] varchar(50) NULL,
    [ToName] varchar(50) NULL,
    [Field] varchar(max) NULL,
    [Date] datetime NULL,
    [ConnectionId] varchar(max) NULL,
    CONSTRAINT [PK_T_Chat] PRIMARY KEY ([id])
);

GO

CREATE TABLE [T_ContactUs] (
    [ContactUsId] int NOT NULL IDENTITY,
    [Topic] varchar(100) NOT NULL,
    [Feedback] varchar(512) NOT NULL,
    [PostBy] varchar(50) NULL,
    [PostDate] datetime NULL,
    [IsClosed] bit NOT NULL,
    CONSTRAINT [PK_ContactUs] PRIMARY KEY ([ContactUsId])
);

GO

CREATE TABLE [T_ContactUsDetail] (
    [Id] int NOT NULL IDENTITY,
    [ContactUsId] int NOT NULL,
    [Reply] varchar(max) NOT NULL,
    [PostBy] varchar(50) NULL,
    [PostDate] datetime NULL,
    CONSTRAINT [PK_T_ContactUsDetail] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_ContentChanges] (
    [Id] int NOT NULL IDENTITY,
    [Modul] varchar(50) NOT NULL,
    [Type] varchar(50) NOT NULL,
    [NewsId] int NULL,
    [Judul] varchar(max) NULL,
    [Caption] varchar(max) NULL,
    [Isi] varchar(max) NULL,
    [CategoryCode] int NULL,
    [Image] varchar(max) NULL,
    [ImagePath] varchar(max) NULL,
    [Attach] varchar(max) NULL,
    [AttachPath] varchar(max) NULL,
    [FileId] int NULL,
    [FileName] varchar(max) NULL,
    [FileGroup] int NULL,
    [Description] varchar(max) NULL,
    [LocationFile] varchar(max) NULL,
    [TutorialCode] varchar(50) NULL,
    [TutorialTitle] varchar(200) NULL,
    [TutorialDescription] varchar(max) NULL,
    [VideoName] varchar(200) NULL,
    [VideoPath] varchar(max) NULL,
    [TutorialParent] varchar(50) NULL,
    [FAQId] int NULL,
    [ProjectCode] varchar(10) NULL,
    [TypeFAQ] int NULL,
    [GroupFAQ] varchar(200) NULL,
    [Question] varchar(max) NULL,
    [Answer] varchar(max) NULL,
    [PostBy] varchar(50) NOT NULL,
    [PostDate] datetime NOT NULL,
    [IsResponded] bit NOT NULL,
    [Response] varchar(1) NULL,
    [RespondDate] datetime NULL,
    CONSTRAINT [PK_T_ContentChanges] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_FAQ] (
    [Id] int NOT NULL IDENTITY,
    [ProjectCode] varchar(10) NULL,
    [TypeFAQ] int NULL,
    [GroupFAQ] varchar(200) NULL,
    [Q] varchar(max) NOT NULL,
    [A] varchar(max) NOT NULL,
    [CreatedBy] varchar(50) NOT NULL,
    [CreatedDate] date NOT NULL,
    [ModifyBy] varchar(50) NULL,
    [ModifyDate] date NULL,
    CONSTRAINT [PK_T_FAQ] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_Files] (
    [ID] int NOT NULL IDENTITY,
    [FileGroup] int NOT NULL DEFAULT (((99))),
    [NameFile] varchar(max) NOT NULL,
    [Description] varchar(max) NOT NULL,
    [LocationFile] varchar(max) NOT NULL,
    [CreatedBy] nvarchar(50) NULL,
    [CreatedDate] datetime NULL,
    CONSTRAINT [PK_T_Files] PRIMARY KEY ([ID])
);

GO

CREATE TABLE [T_HistoryLogin] (
    [Id] int NOT NULL IDENTITY,
    [UserId] varchar(50) NOT NULL,
    [DateLogin] datetime NOT NULL,
    [IP] varchar(50) NOT NULL,
    CONSTRAINT [PK_HistoryLogin] PRIMARY KEY ([Id], [UserId])
);

GO

CREATE TABLE [T_MsApproval] (
    [UserLevel] int NOT NULL,
    [Id] int NOT NULL IDENTITY,
    CONSTRAINT [PK_T_MsApproval] PRIMARY KEY ([UserLevel])
);

GO

CREATE TABLE [T_MsCategory] (
    [CategoryCode] int NOT NULL,
    [CategoryName] varchar(50) NOT NULL,
    [CreatedBy] varchar(50) NOT NULL,
    [CreatedDate] date NOT NULL,
    CONSTRAINT [PK_T_MsCategory] PRIMARY KEY ([CategoryCode])
);

GO

CREATE TABLE [T_MsDomain] (
    [DomainLink] varchar(100) NOT NULL,
    [DomainName] varchar(50) NULL,
    CONSTRAINT [PK_T_MsDomain] PRIMARY KEY ([DomainLink])
);

GO

CREATE TABLE [T_MsFileGroup] (
    [FileGroup] int NOT NULL,
    [Description] varchar(50) NULL,
    [CreatedBy] varchar(50) NULL,
    [CreatedDate] datetime NULL,
    CONSTRAINT [PK_T_MsFileGroup] PRIMARY KEY ([FileGroup])
);

GO

CREATE TABLE [T_MsMenu] (
    [MenuId] int NOT NULL IDENTITY,
    [MenuCode] varchar(20) NOT NULL,
    [MenuName] varchar(50) NULL,
    [MenuURL] varchar(150) NULL,
    [MenuGroup] varchar(50) NULL,
    [MenuParent] varchar(20) NULL,
    [MenuTitle] varchar(100) NULL,
    [IsLink] bit NOT NULL,
    [IsActive] bit NOT NULL,
    [IsLoginRequired] bit NOT NULL,
    [CreatedBy] varchar(50) NULL,
    [CreatedDate] datetime NULL,
    [ModifyBy] varchar(50) NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_MsMenu] PRIMARY KEY ([MenuId])
);

GO

CREATE TABLE [T_MsMenuContent] (
    [Menu] varchar(50) NOT NULL,
    [MenuContent] varchar(max) NULL,
    [Active] bit NOT NULL,
    CONSTRAINT [PK_T_MsMenuContent] PRIMARY KEY ([Menu])
);

GO

CREATE TABLE [T_MsParameter] (
    [FlagId] int NOT NULL,
    [ParameterCode] varchar(50) NOT NULL,
    [Id] int NOT NULL IDENTITY,
    [Description] varchar(200) NOT NULL,
    [ParentCode] varchar(50) NULL,
    [CreatedBy] varchar(50) NOT NULL,
    [CreatedDate] datetime NOT NULL,
    [ModifyBy] varchar(50) NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_T_MsParameter] PRIMARY KEY ([FlagId], [ParameterCode])
);

GO

CREATE TABLE [T_MsParameter_Flag] (
    [FlagId] int NOT NULL IDENTITY,
    [Description] varchar(50) NOT NULL,
    CONSTRAINT [PK_T_MsParameter_Flag] PRIMARY KEY ([FlagId])
);

GO

CREATE TABLE [T_MsProject] (
    [ProjectCode] varchar(10) NOT NULL,
    [ProjectName] varchar(200) NOT NULL,
    [CreatedBy] varchar(50) NOT NULL,
    [CreatedDate] date NOT NULL,
    [ModifyBy] varchar(200) NULL,
    [ModifyDate] date NULL,
    CONSTRAINT [PK_T_MsProject] PRIMARY KEY ([ProjectCode])
);

GO

CREATE TABLE [T_MsSite] (
    [SiteCode] int NOT NULL IDENTITY,
    [SiteName] varchar(50) NULL,
    [Description] varchar(100) NULL,
    [NavURL] varchar(max) NULL,
    [Image] varchar(50) NULL,
    [IsShow] bit NOT NULL,
    CONSTRAINT [PK_SiteMenu] PRIMARY KEY ([SiteCode])
);

GO

CREATE TABLE [T_MsUser_Site] (
    [UserId] varchar(50) NOT NULL,
    [SiteCode] int NOT NULL,
    [Id] int NOT NULL IDENTITY,
    [SiteUserId] varchar(50) NOT NULL,
    [SitePassword] varchar(50) NOT NULL,
    CONSTRAINT [PK_T_MsUser_Site] PRIMARY KEY ([UserId], [SiteCode])
);

GO

CREATE TABLE [T_MsUserGroup] (
    [UserLevel] int NOT NULL,
    [Description] varchar(50) NOT NULL,
    CONSTRAINT [PK_UserGroup] PRIMARY KEY ([UserLevel])
);

GO

CREATE TABLE [T_MsUserGroup_Menu] (
    [Id] int NOT NULL IDENTITY,
    [MenuCode] varchar(20) NOT NULL,
    [UserLevel] int NOT NULL,
    CONSTRAINT [PK_T_MsUserGroup_Menu] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_MsUserGroup_Site] (
    [Id] int NOT NULL IDENTITY,
    [SiteCode] int NOT NULL,
    [UserLevel] int NOT NULL,
    CONSTRAINT [PK_T_MsUserGroup_Site] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_News] (
    [Id] int NOT NULL IDENTITY,
    [Judul] varchar(max) NOT NULL,
    [CategoryCode] int NOT NULL,
    [Image] varchar(max) NOT NULL,
    [ImagePath] varchar(max) NULL,
    [PostBy] varchar(50) NOT NULL,
    [Caption] varchar(max) NOT NULL,
    [Isi] nvarchar(max) NOT NULL,
    [CreatedBy] varchar(50) NOT NULL,
    [CreatedDate] datetime NOT NULL,
    [ModifyBy] varchar(50) NULL,
    [ModifyDate] datetime NULL,
    [Attach] varchar(500) NULL,
    [AttachPath] varchar(max) NULL,
    CONSTRAINT [PK_T_News] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_Question] (
    [Id] int NOT NULL IDENTITY,
    [Topic] varchar(200) NOT NULL,
    [Question] varchar(max) NOT NULL,
    [PostBy] varchar(50) NULL,
    [PostDate] datetime NULL,
    [IsClosed] bit NOT NULL,
    CONSTRAINT [PK_T_Question] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_QuestionDetail] (
    [Id] int NOT NULL IDENTITY,
    [QuestionId] int NOT NULL,
    [Answer] varchar(max) NOT NULL,
    [PostBy] varchar(50) NULL,
    [PostDate] datetime NULL,
    CONSTRAINT [PK_T_QuestionDetail] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_Recruitment] (
    [UserId] varchar(50) NOT NULL,
    [Name] varchar(100) NOT NULL,
    [BirthDate] date NOT NULL,
    [Email] varchar(100) NOT NULL,
    [Gender] varchar(1) NOT NULL DEFAULT (('M')),
    [Phone] varchar(20) NOT NULL,
    [Address] varchar(500) NOT NULL,
    [Country] varchar(50) NOT NULL,
    [City] varchar(50) NOT NULL,
    [PostalCode] varchar(50) NOT NULL,
    [Nationality] varchar(50) NOT NULL,
    [CreatedDate] datetime NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_T_Recrutment] PRIMARY KEY ([UserId])
);

GO

CREATE TABLE [T_Recruitment_Academy] (
    [Id] int NOT NULL IDENTITY,
    [UserId] varchar(50) NOT NULL,
    [Institute] varchar(100) NOT NULL,
    [Location] varchar(50) NOT NULL,
    [StudyFrom] varchar(4) NOT NULL,
    [StudyTo] varchar(4) NULL,
    [Qualification] varchar(50) NOT NULL,
    [Major] varchar(50) NOT NULL,
    [CertificateFileName] varchar(100) NULL,
    [CertificateFilePath] varchar(200) NULL,
    [CreatedDate] datetime NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_T_Recruitment_Academy] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_Recruitment_Academy_NonFormal] (
    [Id] int NOT NULL IDENTITY,
    [UserId] varchar(50) NOT NULL,
    [Education] varchar(200) NOT NULL,
    [Institute] varchar(100) NOT NULL,
    [Location] varchar(50) NOT NULL,
    [CertificateFileName] varchar(100) NULL,
    [CertificateFilePath] varchar(200) NULL,
    [CreatedDate] datetime NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_T_Recruitment_Academy_NonFormal] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_Recruitment_Document] (
    [Id] int NOT NULL IDENTITY,
    [UserId] varchar(50) NOT NULL,
    [Description] varchar(200) NOT NULL,
    [FileName] varchar(100) NOT NULL,
    [FilePath] varchar(200) NOT NULL,
    [Notes] varchar(500) NULL,
    [CreatedDate] datetime NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_T_Recruitment_Document] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_Recruitment_Experience] (
    [Id] int NOT NULL IDENTITY,
    [UserId] varchar(50) NOT NULL,
    [CompanyName] varchar(200) NOT NULL,
    [Location] varchar(50) NULL,
    [State] varchar(50) NULL,
    [PositionTitle] varchar(100) NOT NULL,
    [PositionLevel] varchar(50) NOT NULL,
    [MonthStart] varchar(3) NOT NULL,
    [YearStart] varchar(4) NOT NULL,
    [MonthEnd] varchar(3) NULL,
    [YearEnd] varchar(4) NULL,
    [StillPresent] bit NOT NULL DEFAULT (((1))),
    [Specialization] varchar(50) NOT NULL,
    [Industry] varchar(50) NOT NULL,
    [Salary] varchar(20) NULL,
    [SalaryCurrency] varchar(50) NULL,
    [ExperienceDescription] varchar(max) NULL,
    [CreatedDate] datetime NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_T_Recruitment_Experience] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_Recruitment_Family] (
    [Id] int NOT NULL IDENTITY,
    [UserId] varchar(50) NOT NULL,
    [FamilyName] varchar(100) NOT NULL,
    [Gender] varchar(1) NOT NULL DEFAULT (('M')),
    [Relation] varchar(50) NOT NULL,
    [CreatedDate] datetime NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_T_Recruitment_Family] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_Recruitment_Skill] (
    [Id] int NOT NULL IDENTITY,
    [UserId] varchar(50) NOT NULL,
    [Skill] varchar(100) NOT NULL,
    [Proficiency] varchar(1) NOT NULL,
    [CreatedDate] datetime NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_T_Recruitment_Skill] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_ResetPassword] (
    [Id] int NOT NULL IDENTITY,
    [Credential] varchar(200) NOT NULL,
    [Token] varchar(32) NOT NULL,
    [ExpiredDate] datetime NOT NULL,
    [IsUsed] bit NOT NULL,
    CONSTRAINT [PK_T_ResetPassword] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_Tutorial] (
    [TutorialCode] varchar(50) NOT NULL,
    [Id] int NULL,
    [TutorialTitle] varchar(200) NOT NULL,
    [TutorialDescription] varchar(max) NOT NULL,
    [VideoName] varchar(200) NULL,
    [VideoPath] varchar(max) NULL,
    [TutorialParent] varchar(50) NULL,
    [CreatedBy] varchar(50) NOT NULL,
    [CreatedDate] datetime NOT NULL,
    [ModifyBy] varchar(50) NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_T_Tutorial] PRIMARY KEY ([TutorialCode])
);

GO

CREATE TABLE [T_Vendor] (
    [Id] int NOT NULL,
    [UserId] varchar(50) NOT NULL,
    [VendorNumber] varchar(50) NULL,
    [CompanyName] varchar(200) NOT NULL,
    [BusinessForm] varchar(50) NOT NULL,
    [BusinessField] varchar(50) NOT NULL,
    [Country] varchar(50) NOT NULL,
    [City] varchar(50) NOT NULL,
    [Address] varchar(500) NOT NULL,
    [PostalCode] varchar(50) NOT NULL,
    [Email] varchar(100) NOT NULL,
    [Phone] varchar(20) NOT NULL,
    [Fax] varchar(20) NULL,
    [CompanyWeb] varchar(200) NULL,
    [ContactPerson] varchar(200) NOT NULL,
    [CPNumber] varchar(20) NOT NULL,
    [CPEmail] varchar(100) NOT NULL,
    [IsSubmitted] bit NOT NULL,
    [CreatedDate] datetime NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_T_Vendor] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_Vendor_Bank] (
    [Id] int NOT NULL IDENTITY,
    [UserId] varchar(50) NOT NULL,
    [Bank] varchar(50) NOT NULL,
    [Branch] varchar(200) NOT NULL,
    [Country] varchar(50) NOT NULL,
    [City] varchar(50) NOT NULL,
    [Currency] varchar(50) NOT NULL,
    [AccountNumber] varchar(20) NOT NULL,
    [AccountName] varchar(100) NOT NULL,
    [CreatedDate] datetime NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_T_Vendor_Bank] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_Vendor_Branches] (
    [Id] int NOT NULL IDENTITY,
    [UserId] varchar(50) NOT NULL,
    [Country] varchar(50) NOT NULL,
    [City] varchar(50) NOT NULL,
    [Address] varchar(500) NOT NULL,
    [PostalCode] varchar(50) NOT NULL,
    [Email] varchar(100) NOT NULL,
    [Phone] varchar(20) NOT NULL,
    [Fax] varchar(20) NULL,
    [ContactPerson] varchar(200) NOT NULL,
    [CPNumber] varchar(20) NOT NULL,
    [CPEmail] varchar(100) NOT NULL,
    [CreatedDate] datetime NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_T_Vendor_Branches] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_Vendor_Document] (
    [Id] int NOT NULL,
    [UserId] varchar(50) NOT NULL,
    [Description] varchar(200) NOT NULL,
    [FileName] varchar(100) NOT NULL,
    [FilePath] varchar(200) NOT NULL,
    [Notes] varchar(500) NULL,
    [CreatedDate] datetime NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_T_Vendor_Document] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_Vendor_Item] (
    [Id] int NOT NULL IDENTITY,
    [UserId] varchar(50) NOT NULL,
    [SEGMENT1] varchar(50) NULL,
    [DESCRIPTION] varchar(300) NOT NULL,
    [PRIMARY_UOM_CODE] varchar(50) NOT NULL,
    [INVOICEABLE_ITEM_FLAG] varchar(1) NULL DEFAULT (('Y')),
    [INVOICE_ENABLED_FLAG] varchar(1) NULL DEFAULT (('Y')),
    [ALLOW_ITEM_DESC_UPDATE_FLAG] varchar(1) NULL DEFAULT (('N')),
    [ORGANIZATION_CODE] varchar(50) NULL,
    [TRANSACTION_TYPE] varchar(50) NULL DEFAULT (('CREATE')),
    [PROCESS_FLAG] varchar(1) NULL DEFAULT (((1))),
    [INVENTORY_ITEM_FLAG] varchar(1) NULL DEFAULT (('Y')),
    [STOCK_ENABLED_FLAG] varchar(1) NULL DEFAULT (('Y')),
    [MTL_TRANSACTIONS_ENABLED_FLAG] varchar(1) NULL DEFAULT (('Y')),
    [RESERVEABLE_TYPE] varchar(10) NULL DEFAULT (((1))),
    [COSTING_ENABLED_FLAG] varchar(1) NULL DEFAULT (('Y')),
    [INVENTORY_ASSET_FLAG] varchar(1) NULL DEFAULT (('Y')),
    [PURCHASING_ITEM_FLAG] varchar(1) NULL DEFAULT (('Y')),
    [PURCHASING_ENABLED_FLAG] varchar(1) NULL DEFAULT (('Y')),
    [RECEIPT_REQUIRED_FLAG] varchar(1) NULL DEFAULT (('Y')),
    [MUST_USE_APPROVED_VENDOR_FLAG] varchar(1) NULL DEFAULT (('N')),
    [CUSTOMER_ORDER_ENABLED_FLAG] varchar(1) NULL DEFAULT (('N')),
    [CUSTOMER_ORDER_FLAG] varchar(1) NULL DEFAULT (('N')),
    [SHIPPABLE_ITEM_FLAG] varchar(1) NULL DEFAULT (('N')),
    [SO_TRANSACTIONS_FLAG] varchar(1) NULL DEFAULT (('N')),
    [RETURNABLE_FLAG] varchar(1) NULL DEFAULT (('N')),
    [Notes] varchar(max) NULL,
    [CreatedDate] datetime NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_T_Vendor_Item] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [T_Vendor_Legal] (
    [UserId] varchar(50) NOT NULL,
    [Id] int NOT NULL IDENTITY,
    [SIUP] varchar(50) NOT NULL,
    [SIUPDocumentName] varchar(100) NOT NULL,
    [SIUPDocumentPath] varchar(200) NOT NULL,
    [SIUPValidTo] date NOT NULL,
    [TDP] varchar(50) NOT NULL,
    [TDPDocumentName] varchar(100) NOT NULL,
    [TDPDocumentPath] varchar(200) NOT NULL,
    [TDPValidTo] date NOT NULL,
    [SKDP] varchar(50) NULL,
    [SKDPDocumentName] varchar(100) NULL,
    [SKDPDocumentPath] varchar(200) NULL,
    [SKDPValidTo] date NULL,
    [DeedOfIncorporation] varchar(50) NOT NULL,
    [DoIDocumentName] varchar(100) NOT NULL,
    [DoIDocumentPath] varchar(200) NOT NULL,
    [CertificateOfEstablishment] varchar(100) NULL,
    [CoEDocumentName] varchar(100) NULL,
    [CoEDocumentPath] varchar(200) NULL,
    [DeedOfChange] varchar(50) NULL,
    [DoCDocumentName] varchar(100) NULL,
    [DoCDocumentPath] varchar(200) NULL,
    [CertificateOfChange] varchar(100) NULL,
    [CoCDocumentName] varchar(100) NULL,
    [CoCDocumentPath] varchar(200) NULL,
    [CreatedDate] datetime NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_T_Vendor_Legal] PRIMARY KEY ([UserId])
);

GO

CREATE TABLE [T_Vendor_Tax] (
    [UserId] varchar(50) NOT NULL,
    [Id] int NOT NULL IDENTITY,
    [NPWP] varchar(50) NOT NULL,
    [NPWPAddress] varchar(200) NOT NULL,
    [NPWPDocumentName] varchar(100) NOT NULL,
    [NPWPDocumentPath] varchar(200) NOT NULL,
    [SKPKP] varchar(50) NULL,
    [SKPKPDocumentName] varchar(100) NULL,
    [SKPKPDocumentPath] varchar(200) NULL,
    [PPn] bit NOT NULL,
    [PPh] bit NOT NULL,
    [CreatedDate] datetime NULL,
    [ModifyDate] datetime NULL,
    CONSTRAINT [PK_T_Vendor_Tax] PRIMARY KEY ([UserId])
);

GO

CREATE TABLE [MasterDataBusinessFields] (
    [Id] int NOT NULL IDENTITY,
    [MasterDataBusinessFieldTypeId] int NOT NULL,
    [Name] varchar(100) NOT NULL,
    [KBLI] int NOT NULL,
    [CreatedDate] datetime NOT NULL,
    [CreatedBy] varchar(50) NOT NULL,
    [ModifiedDate] datetime NOT NULL,
    [ModifiyBy] varchar(50) NOT NULL,
    [IsDelete] bit NULL,
    CONSTRAINT [PK_MasterDataBusinessFields] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_MasterDataBusinessFieldsType_MasterDataBusinessFieldsType] FOREIGN KEY ([MasterDataBusinessFieldTypeId]) REFERENCES [MasterDataBusinessFieldsType] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [MasterDataRegion] (
    [Id] int NOT NULL IDENTITY,
    [Code] varchar(10) NULL,
    [Name] varchar(20) NOT NULL,
    [PhoneCode] varchar(5) NULL,
    [MasterDataCountryId] int NOT NULL,
    [CreatedDate] datetime NOT NULL,
    [CreatedBy] varchar(20) NOT NULL,
    [ModifiedDate] datetime NOT NULL,
    [ModifiyBy] varchar(20) NOT NULL,
    [IsDelete] bit NULL,
    CONSTRAINT [PK_MasterDataRegion] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_MasterDataCountry_MasterDataRegion] FOREIGN KEY ([MasterDataCountryId]) REFERENCES [MasterDataCountry] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [T_MsUser] (
    [UserId] varchar(50) NOT NULL,
    [UserName] varchar(50) NOT NULL,
    [Password] varchar(50) NULL,
    [ForgetPassword] varchar(50) NULL,
    [Region] varchar(50) NULL,
    [email] varchar(1000) NOT NULL,
    [SecondaryEmail] varchar(1000) NULL,
    [Jabatan] varchar(50) NULL,
    [Divisi] varchar(50) NULL,
    [ExtensionNumber] varchar(20) NULL,
    [LastLogin] datetime NULL,
    [LastLogout] datetime NULL,
    [Session] nvarchar(1000) NULL,
    [UserLevel] int NOT NULL,
    [CreatedBy] varchar(50) NULL,
    [CreatedDate] date NULL,
    [ModifyBy] varchar(50) NULL,
    [ModifyDate] date NULL,
    [Image] varchar(50) NULL,
    [Isactive] bit NOT NULL,
    [IP] varchar(50) NULL,
    [IsTemporary] bit NOT NULL,
    [StartAccess] date NULL,
    [EndAccess] date NULL,
    [IsUserAD] bit NOT NULL,
    [Domain] varchar(50) NULL,
    [LastAccessFAQ] datetime NULL,
    [LastAccessTutorial] datetime NULL,
    [LastAccessDocument] datetime NULL,
    [ActivateLink] varchar(32) NULL,
    CONSTRAINT [PK_MsUser] PRIMARY KEY ([UserId]),
    CONSTRAINT [FK_MsUser_UserGroup] FOREIGN KEY ([UserLevel]) REFERENCES [T_MsUserGroup] ([UserLevel]) ON DELETE NO ACTION
);

GO

CREATE TABLE [T_CommentNews] (
    [Id] int NOT NULL IDENTITY,
    [UserId] varchar(50) NOT NULL,
    [Email] varchar(50) NOT NULL,
    [Field] varchar(max) NOT NULL,
    [IdComment] int NOT NULL,
    [Date] date NOT NULL,
    CONSTRAINT [PK_T_CommentNews] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_CommentNews_News] FOREIGN KEY ([IdComment]) REFERENCES [T_News] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [MappingDataBusinessFieldPaymentMethods] (
    [Id] int NOT NULL IDENTITY,
    [MasterDataBusinessFieldId] int NOT NULL,
    [MasterDataPaymentMethodId] int NOT NULL,
    [CreatedDate] datetime NOT NULL,
    [CreatedBy] varchar(20) NOT NULL,
    [ModifiedDate] datetime NOT NULL,
    [ModifiyBy] varchar(20) NOT NULL,
    [IsDelete] bit NULL,
    CONSTRAINT [PK_MappingDataBusinessFieldPaymentMethods] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_MappingDataBusinessFieldPaymentMethods_MasterDataBusinessFields] FOREIGN KEY ([MasterDataBusinessFieldId]) REFERENCES [MasterDataBusinessFields] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_MappingDataBusinessFieldPaymentMethods_MasterDataPaymentMethod] FOREIGN KEY ([MasterDataPaymentMethodId]) REFERENCES [MasterDataPaymentMethod] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [MasterDataCity] (
    [Id] int NOT NULL IDENTITY,
    [MasterDataCountryId] int NOT NULL,
    [MasterDataRegionId] int NOT NULL,
    [Code] varchar(10) NULL,
    [Name] varchar(50) NOT NULL,
    [CreatedDate] datetime NOT NULL,
    [CreatedBy] varchar(20) NOT NULL,
    [ModifiedDate] datetime NOT NULL,
    [ModifyBy] varchar(50) NOT NULL,
    [IsDelete] bit NULL,
    CONSTRAINT [PK_MasterDataCity] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_MasterDataCity_MasterDataCountry] FOREIGN KEY ([MasterDataCountryId]) REFERENCES [MasterDataCountry] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_MasterDataCity_MasterDataRegion] FOREIGN KEY ([MasterDataRegionId]) REFERENCES [MasterDataRegion] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [MasterDataVendor] (
    [Id] int NOT NULL IDENTITY,
    [Name] varchar(100) NOT NULL,
    [MasterDataUserId] varchar(50) NOT NULL,
    [MasterDataBusinessTypeId] int NOT NULL,
    [MasterDataBusinessFieldsId] int NOT NULL,
    [MasterDataCountryId] int NOT NULL,
    [MasterDataRegionId] int NOT NULL,
    [MasterDataCityId] int NOT NULL,
    [Address] varchar(100) NOT NULL,
    [PostalCode] varchar(10) NOT NULL,
    [CompanyEmail] varchar(50) NOT NULL,
    [PIC] varchar(20) NOT NULL,
    [PICEmail] varchar(50) NOT NULL,
    [IdCardNumber] varchar(100) NOT NULL,
    [IntegrityPact] bit NULL,
    [MasterAgreement] bit NULL,
    [Logo] varchar(max) NULL,
    [CreatedDate] datetime NULL,
    [CreatedBy] varchar(20) NULL,
    [ModifiedDate] datetime NULL,
    [ModifiyBy] varchar(20) NULL,
    [IsDelete] nvarchar(10) NULL,
    CONSTRAINT [PK_MasterDataVendor] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_MasterDataVendor_MasterDataBusinessFields] FOREIGN KEY ([MasterDataBusinessFieldsId]) REFERENCES [MasterDataBusinessFields] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_MasterDataVendor_MasterDataBusinessType] FOREIGN KEY ([MasterDataBusinessTypeId]) REFERENCES [MasterDataBusinessType] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_MasterDataVendor_MasterDataCity] FOREIGN KEY ([MasterDataCityId]) REFERENCES [MasterDataCity] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_MasterDataVendor_MasterDataCountry] FOREIGN KEY ([MasterDataCountryId]) REFERENCES [MasterDataCountry] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_MasterDataVendor_MasterDataRegion] FOREIGN KEY ([MasterDataRegionId]) REFERENCES [MasterDataRegion] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_MasterDataVendor_MasterDataUser] FOREIGN KEY ([MasterDataUserId]) REFERENCES [T_MsUser] ([UserId]) ON DELETE NO ACTION
);

GO

CREATE TABLE [MappingDataVendorCurrencyBank] (
    [Id] int NOT NULL IDENTITY,
    [MasterDataVendorId] int NOT NULL,
    [MasterDataCurrencyId] int NOT NULL,
    [MasterDataBankId] int NOT NULL,
    [Area] varchar(20) NOT NULL,
    [City] varchar(20) NOT NULL,
    [AccountNumber] int NOT NULL,
    [Status] varchar(10) NULL,
    [AccountName] varchar(20) NOT NULL,
    [CreatedDate] datetime NOT NULL,
    [CreatedBy] varchar(20) NOT NULL,
    [ModifiedDate] datetime NOT NULL,
    [ModifiyBy] varchar(20) NOT NULL,
    [IsDelete] bit NULL,
    CONSTRAINT [PK_MappingDataVendorCurrencyBank] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_MappingDataVendorCurrencyBank_MasterDataBank] FOREIGN KEY ([MasterDataBankId]) REFERENCES [MasterDataBank] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_MappingDataVendorCurrencyBank_MasterDataCurrency] FOREIGN KEY ([MasterDataCurrencyId]) REFERENCES [MasterDataCurrency] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_MappingDataVendorCurrencyBank_MasterDataVendor] FOREIGN KEY ([MasterDataVendorId]) REFERENCES [MasterDataVendor] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [MappingDataVendorLegal] (
    [Id] int NOT NULL IDENTITY,
    [MasterDataVendorId] int NOT NULL,
    [NIB] varchar(20) NULL,
    [NIBAttachment] varchar(256) NULL,
    [BusinessPermit] varchar(20) NULL,
    [BusinessPermitAttachment] varchar(256) NULL,
    [SIUP] varchar(20) NULL,
    [SIUPExpiredDate] date NULL,
    [SIUPAttachment] varchar(256) NULL,
    [TDP] varchar(20) NULL,
    [TDPExpiredDate] date NULL,
    [TDPAttachment] varchar(256) NULL,
    [MemorandumOfAssociation] varchar(20) NULL,
    [MemorandumOfAssociationAttachment] varchar(256) NULL,
    [DecissionLetterMenkumham] varchar(20) NULL,
    [DecissionLetterMenkumhamAttachment] varchar(256) NULL,
    [MemorandumOfAssociationChanging] varchar(20) NULL,
    [MemorandumOfAssociationChangingAttachment] varchar(256) NULL,
    [DecissionLetterMenkumhamChanging] varchar(20) NULL,
    [DecissionLetterMenkumhamChangingAttachment] varchar(256) NULL,
    [ManagementStructure] varchar(256) NULL,
    [Signing] varchar(256) NULL,
    [CreatedDate] datetime NOT NULL,
    [CreatedBy] varchar(20) NOT NULL,
    [ModifiedDate] datetime NOT NULL,
    [ModifiyBy] varchar(20) NOT NULL,
    [IsDelete] nvarchar(10) NULL,
    CONSTRAINT [PK_MappingDataVendorLegal] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_MappingDataVendorLegal_MasterDataVendor] FOREIGN KEY ([MasterDataVendorId]) REFERENCES [MasterDataVendor] ([Id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_MappingDataBusinessFieldPaymentMethods_MasterDataBusinessFieldId] ON [MappingDataBusinessFieldPaymentMethods] ([MasterDataBusinessFieldId]);

GO

CREATE INDEX [IX_MappingDataBusinessFieldPaymentMethods_MasterDataPaymentMethodId] ON [MappingDataBusinessFieldPaymentMethods] ([MasterDataPaymentMethodId]);

GO

CREATE INDEX [IX_MappingDataVendorCurrencyBank_MasterDataBankId] ON [MappingDataVendorCurrencyBank] ([MasterDataBankId]);

GO

CREATE INDEX [IX_MappingDataVendorCurrencyBank_MasterDataCurrencyId] ON [MappingDataVendorCurrencyBank] ([MasterDataCurrencyId]);

GO

CREATE INDEX [IX_MappingDataVendorCurrencyBank_MasterDataVendorId] ON [MappingDataVendorCurrencyBank] ([MasterDataVendorId]);

GO

CREATE INDEX [IX_MappingDataVendorLegal_MasterDataVendorId] ON [MappingDataVendorLegal] ([MasterDataVendorId]);

GO

CREATE INDEX [IX_MasterDataBusinessFields_MasterDataBusinessFieldTypeId] ON [MasterDataBusinessFields] ([MasterDataBusinessFieldTypeId]);

GO

CREATE INDEX [IX_MasterDataCity_MasterDataCountryId] ON [MasterDataCity] ([MasterDataCountryId]);

GO

CREATE INDEX [IX_MasterDataCity_MasterDataRegionId] ON [MasterDataCity] ([MasterDataRegionId]);

GO

CREATE INDEX [IX_MasterDataRegion_MasterDataCountryId] ON [MasterDataRegion] ([MasterDataCountryId]);

GO

CREATE INDEX [IX_MasterDataVendor_MasterDataBusinessFieldsId] ON [MasterDataVendor] ([MasterDataBusinessFieldsId]);

GO

CREATE INDEX [IX_MasterDataVendor_MasterDataBusinessTypeId] ON [MasterDataVendor] ([MasterDataBusinessTypeId]);

GO

CREATE INDEX [IX_MasterDataVendor_MasterDataCityId] ON [MasterDataVendor] ([MasterDataCityId]);

GO

CREATE INDEX [IX_MasterDataVendor_MasterDataCountryId] ON [MasterDataVendor] ([MasterDataCountryId]);

GO

CREATE INDEX [IX_MasterDataVendor_MasterDataRegionId] ON [MasterDataVendor] ([MasterDataRegionId]);

GO

CREATE INDEX [IX_MasterDataVendor_MasterDataUserId] ON [MasterDataVendor] ([MasterDataUserId]);

GO

CREATE INDEX [IX_T_CommentNews_IdComment] ON [T_CommentNews] ([IdComment]);

GO

CREATE INDEX [IX_T_MsUser_UserLevel] ON [T_MsUser] ([UserLevel]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190714113038_14072019', N'2.2.4-servicing-10062');

GO

