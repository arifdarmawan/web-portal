﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPortal.Models.Models
{
    public class ModelVendorLegal
    {
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        public string Nib { get; set; }
        public string Nibattachment { get; set; }
        public string BusinessPermit { get; set; }
        public string BusinessPermitAttachment { get; set; }
        public string Siup { get; set; }
        public string SiupexpiredDate { get; set; }
        public string Siupattachment { get; set; }
        public string Tdp { get; set; }
        public string TdpexpiredDate { get; set; }
        public string Tdpattachment { get; set; }
        public string MemorandumOfAssociation { get; set; }
        public string MemorandumOfAssociationAttachment { get; set; }
        public string DecissionLetterMenkumham { get; set; }
        public string DecissionLetterMenkumhamAttachment { get; set; }
        public string MemorandumOfAssociationChanging { get; set; }
        public string MemorandumOfAssociationChangingAttachment { get; set; }
        public string DecissionLetterMenkumhamChanging { get; set; }
        public string DecissionLetterMenkumhamChangingAttachment { get; set; }
        public string Signing { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiyBy { get; set; }
        public bool? IsDelete { get; set; }
        public string ManagementStructure { get; set; }
        public int? SigningId { get; set; }
        public string SigningAttachment { get; set; }
    }
}
