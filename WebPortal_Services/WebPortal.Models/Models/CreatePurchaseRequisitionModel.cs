﻿using System;
using System.Collections.Generic;
using WebPortal.Models.Entities;

namespace WebPortal.Models.Models
{
    public class CreatePurchaseRequisitionModel
    {
        public int Id { get; set; }
        public string PRNumber { get; set; }
        public string RequestedFrom { get; set; }
        public int ConfigCompanyId { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyLocationCode { get; set; }
        public int ConfigDivisionId { get; set; }
        public string CompanyName { get; set; }
        public string DepartementCode { get; set; }
        public string DepartementName { get; set; }
        public int MasterItemId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int CurrencyId { get; set; }
        public int UomId { get; set; }
        public string Prstatus { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal GrandTotal { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public int MasterDataUserId { get; set; }
        public DateTime? DateRequired { get; set; }
        public List<CreatePurchaseRequisitionItemModel> ListItem { get; set; }
    }
}