﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WebPortal.Models.Models
{
    public class ImageBaseResponse : BaseResponse
    {
        public int filesCount { get; set; }
        public long fileSizes { get; set; }
        public List<string> fileNames { get; set; }
        public List<string> filePath { get; set; }
        public MemoryStream file { get; set; }
    }
}
