﻿using System;
using System.Collections.Generic;
using System.Text;
using WebPortal.Models.Entities;

namespace WebPortal.Models.Models
{
    public class CategoriesVendorModel : MappingDataVendorCategories
    {
        public List<int> MasterCategoriesIdArray { get; set; }
    }
}
