﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPortal.Models.Models
{
    public class BankAccountModel
    {
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        public int MasterDataCurrencyId { get; set; }
        public string Currency { get; set; }
        public int MasterDataBankId { get; set; }
        public string BankName { get; set; }
        public string Area { get; set; }
        public string City { get; set; }
        public string AccountNumber { get; set; }
        public string Status { get; set; }
        public string AccountName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiyBy { get; set; }
        public bool? IsDelete { get; set; }
        public string Attachment { get; set; }
    }
}
