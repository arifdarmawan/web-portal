﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPortal.Models.Models
{
    public class VendorItemCategoriesModel
    {
        public int MasterDataUserId { get; set; }
        public List<int> ListCategoryId { get; set; }
    }
}
