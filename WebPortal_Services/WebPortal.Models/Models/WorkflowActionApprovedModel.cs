﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPortal.Models.Models
{
    public class WorkflowActionApprovedModel
    {
        public int WorkflowProcessId { get; set; }
        public int MasterDataUserId { get; set; }
        public string Noted { get; set; }
    }
}
