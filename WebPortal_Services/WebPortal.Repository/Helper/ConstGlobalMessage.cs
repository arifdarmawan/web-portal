﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPortal.Repository.Helper
{
    public static class ConstGlobalMessage
    {
        public const string SubmittedMessage = "Submitted by ";
        public const string NotificationMessage = "Waiting Approval by ";
        public const string ApprovedMessage = "Approved by ";
        public const string CompletedMessage = "Completed by ";
        public const string RejectedMessage = "Rejected by ";
        public const string AutoRejectedMessage = "Rejected by System";
    }
}
