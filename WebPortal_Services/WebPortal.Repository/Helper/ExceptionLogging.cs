﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WebPortal.Repository.Helper
{
    public static class ExceptionLogging
    {  
        public static void SendErrorToText(Exception ex)
        {
            try
            {
                // Set the file path
                string filepath = Path.Combine(Directory.GetCurrentDirectory(), "ErrorLogFileUploads" + "\\" + DateTime.Now.Ticks.ToString() + ".txt");

                // Add the error message to the file
                System.IO.File.AppendAllText(filepath, $"{DateTime.Now.ToString("o")} [ERR] {ex.Message}" + Environment.NewLine);
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }
    }
}
