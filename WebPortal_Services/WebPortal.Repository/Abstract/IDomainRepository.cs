﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;

namespace WebPortal.Repository.Abstract
{
    public interface IDomainRepository
    {
        Task<IEnumerable<MasterDataDomain>> GetAllDomain();
    }
}
