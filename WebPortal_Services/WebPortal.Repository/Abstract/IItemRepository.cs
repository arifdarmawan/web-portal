﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Models;

namespace WebPortal.Repository.Abstract
{
    public interface IItemRepository
    {
        Task<BaseResponse> GetAllItem();
        Task<BaseResponse> GetAllItemLookup();
        Task<BaseResponse> SearchItemByKey(string key);
        Task<BaseResponse> GetItemByCode(string bimsCode);
        Task<BaseResponse> GetVendorItemPrice(int itemId);
    }
}
