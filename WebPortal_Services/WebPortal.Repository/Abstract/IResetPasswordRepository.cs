﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;

namespace WebPortal.Repository.Abstract
{
    public interface IResetPasswordRepository
    {
        Task<BaseResponse> ResetPassword(string email);
        Task<BaseResponse> GetValidResetPassword(string token);
        Task<BaseResponse> SetNewPassword(NewPasswordModel data);
        Task<BaseResponse> SetChangePassword(NewPasswordModel data);
    }
}
