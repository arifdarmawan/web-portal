﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Models;

namespace WebPortal.Repository.Abstract
{
    public interface IFileRepository
    {
        Task<ImageBaseResponse> BasicUpload(List<IFormFile> files);
        Task<ImageBaseResponse> UploadPhoto(List<IFormFile> files);
        Task<BaseResponse> DownloadFile(string fileName);
    }
}
