﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Models;

namespace WebPortal.Repository.Abstract
{
    public interface IWorkflowRepository
    {
        #region workflow process
        Task<BaseResponse> GetWorkflows(int masterDataUserId, int pageNumber, string processStatus);
        Task<BaseResponse> GetWorkflowById(int workflowId);
        Task<BaseResponse> CountWorkflows(int masterDataUserId, string status);
        #endregion

        #region workflow action approval
        Task<BaseResponse> WorkflowActionApproved(WorkflowActionApprovedModel data);
        Task<BaseResponse> WorkflowActionRejected(WorkflowActionApprovedModel data);
        #endregion

        #region workflow history
        Task<BaseResponse> WorkflowHistoryById(int workflowId);
        #endregion
    }
}
