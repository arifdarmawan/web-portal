﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;

namespace WebPortal.Repository.Abstract
{
    public interface IVendorRepository
    {
        Task<BaseResponse> InsertVendorBasicInfo(BasicInfoModel data);
        Task<BaseResponse> InsertVendor(MasterDataVendor data);
        Task<BaseResponse> GetVendorByUserId(int userId);
        Task<BaseResponse> GetVendorInfoByEmail(string email);
        Task<BaseResponse> GetAllVendor();
        Task<BaseResponse> GetVendorBasicInfo(int masterDataUserId);
        Task<BaseResponse> GetVendorDetail(int masterDataUserId);
        Task<BaseResponse> GetVendorBankAccount(int masterDataUserId);
        Task<BaseResponse> GetVendorBankAccountPrimary(int masterDataUserId);
        Task<BaseResponse> InsertVendorBankAccount(BankAccountModel data);
        Task<BaseResponse> UpdateVendorBankAccount(MappingDataVendorCurrencyBank data);
        Task<BaseResponse> UpdateStatusVendorBankAccount(int id, int masterDataUserId, string status, string modifyBy);
        Task<BaseResponse> DeleteVendorBankAccount(int id);
        Task<BaseResponse> GetVendorPaymentTypeInfo(int masterDataUserId);
        Task<BaseResponse> GetVendorPaymentMethod(int masterDataUserId);
        Task<BaseResponse> GetVendorLegal(int masterDataUserId);
        Task<BaseResponse> GetVendorTax(int masterDataUserId);
        Task<BaseResponse> InsertVendorBankPayment(MappingDataVendorPayment data);
        Task<BaseResponse> InsertVendorLegal(MappingDataVendorLegal data);
        Task<BaseResponse> InsertVendorTax(MappingDataVendorTax data);
        Task<BaseResponse> UpdateBusinessEthics(int masterDataUserId, bool businessEthics);
        Task<BaseResponse> UpdateMasterAgreement(int masterDataUserId, bool masterAgreement);
        Task<BaseResponse> UpdateVendorStatus(int masterDataUserId, string status, string createdBy);
        Task<BaseResponse> InsertVendorNotification(int masterDataUserId, string message, string notificationType, string createdBy);
        Task<BaseResponse> IsPersonalVendorBusinessType(int userId);
        Task<BaseResponse> DeleteVendorLegalAttachment(int userId, string type);
        Task<BaseResponse> UpdateUserPhoto(int userId, string Image);
        Task<BaseResponse> GetDataVendortoCheckVerifikasi(int masterDataUserId);
        Task<BaseResponse> InsertVendorCategories(VendorItemCategoriesModel model);
        Task<BaseResponse> GetVendorCategories(int masterDataUserId);
    }
}
