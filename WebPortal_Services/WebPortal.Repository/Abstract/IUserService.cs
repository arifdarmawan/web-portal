﻿using System;
using System.Collections.Generic;
using System.Text;
using WebPortal.Models.Models;

namespace WebPortal.Repository.Abstract
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
        IEnumerable<User> GetAll();
    }
}
