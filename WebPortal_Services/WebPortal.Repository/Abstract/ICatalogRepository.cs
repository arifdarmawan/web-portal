﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;

namespace WebPortal.Repository.Abstract
{
    public interface ICatalogRepository
    {
        Task<BaseResponse> GetAllCategories();
        Task<BaseResponse> GetCategoriesByGroupLevel();
    }
}
