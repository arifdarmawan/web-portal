﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;

namespace WebPortal.Repository.Abstract
{
    public interface IJobTitleRepository
    {
        Task<BaseResponse> GetJobTitles();
        Task<BaseResponse> InsertJobTitle(MasterDataJobTitle data);
    }
}
