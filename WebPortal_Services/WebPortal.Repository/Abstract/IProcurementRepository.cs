﻿using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;

namespace WebPortal.Repository.Abstract
{
    public interface IProcurementRepository
    {
        #region Purchase Requisition
        Task<BaseResponse> GetAccessPurchaseRequisition(int userid); 
        Task<BaseResponse> InsertPurchaseRequisition(CreatePurchaseRequisitionModel data);
        Task<BaseResponse> GetCheckVendorItem(int itemId);
        Task<BaseResponse> GetPpnVendorItemCategories(int itemId);
        Task<BaseResponse> GetPurchaseRequisition();
        Task<BaseResponse> GetRealizationBudgetItem(string itemcode, int year);
        Task<BaseResponse> GetBookingRealizationBudgetItem(string bimsCode, int year);
        Task<BaseResponse> getPurchaseRequisitionById(int id);
        Task<BaseResponse> getPurchaseRequisitionItemByVendorId(int id);
        Task<BaseResponse> GetPurchaseRequisitionItemOnProcess(string company, string bimsCode);
        Task<BaseResponse> GetRFQList();
        Task<BaseResponse> GetRFQDetail(int id, int masterItemId);
        Task<BaseResponse> GetQCFList();
        Task<BaseResponse> GetQCFDetail(int id, int masterItemId);
        Task<BaseResponse> BlastToVendor(RequestForQuotationEmployeeModel data);
        #endregion

        #region Request For Quotation
        Task<BaseResponse> GetRequestForQuotationPRId(int prId, int masterItemId);
        Task<BaseResponse> GetRequestForQuotationbyUserId(int userId);
        Task<BaseResponse> GetRequestForQuotationDetailByRfqId(int rfqId);
        Task<BaseResponse> ProcessRequestForQuotation(RequestForQuotationModel data);
        #endregion

        #region Quotation Comparation Form
        Task<BaseResponse> getQuotationComparationFormByPrId(int prid, int masterItemId);
        Task<BaseResponse> QuotationComparationFormHeader(int prId, int masterItemId);
        Task<BaseResponse> QuotationComparationFormItem(int prId, int masterItemId);
        Task<BaseResponse> ProcessQuotationComparationForm(int PrId, int masterDataVendorId, string notes, string modifyBy);
        #endregion
        
        #region Purchase Order
        Task<BaseResponse> GetPurchaseOrder();
        Task<BaseResponse> GetPurchaseOrderById(int id, int userId);
        Task<BaseResponse> GetPurchaseOrderByVendorId(int id);
        Task<BaseResponse> GetPurchaseOrderItemById(int id/*, int masterDataVendorId*/);
        Task<BaseResponse> getPurchaseOrderItemByVendorId(int id);
        Task<BaseResponse> ProcessPurchaseOrder(int id, string modifyBy);
        Task<BaseResponse> RejectPurchaseOrder(int id, string modifyBy);
        #endregion
    }
}