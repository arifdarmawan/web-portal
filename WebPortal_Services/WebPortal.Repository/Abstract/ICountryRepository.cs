﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Models;

namespace WebPortal.Repository.Abstract
{
    public interface ICountryRepository
    {
        Task<BaseResponse> GetCountries();
    }
}
