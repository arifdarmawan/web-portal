﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Linq;

namespace WebPortal.Repository.Concrete
{
    public class RegionRepository : IRegionRepository
    {
        readonly WebPortalContext _context;
        public RegionRepository(WebPortalContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> GetRegions()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await _context.MasterDataRegion.AsNoTracking().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetRegionsByCountryId(int masterCountryId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from region in _context.MasterDataRegion
                              where region.MasterDataCountryId == masterCountryId
                              select new
                              {
                                  Value = region.Id,
                                  Text = region.Name
                              }).AsNoTracking().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;
                response.Message = "Reload data success.";
                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message.ToString();             
            }

            return response;
        }
    }
}
