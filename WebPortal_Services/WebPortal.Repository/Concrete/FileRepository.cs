﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;
using WebPortal.Repository.Helper;

namespace WebPortal.Repository.Concrete
{
    public class FileRepository : IFileRepository
    {
        readonly WebPortalContext _context;
        public FileRepository(WebPortalContext context)
        {
            _context = context;
        }

        public async Task<ImageBaseResponse> BasicUpload(List<IFormFile> files)
        {
            ImageBaseResponse response = new ImageBaseResponse();

            try
            {
                long size = files.Sum(f => f.Length);

                var filePaths = new List<string>();
                var fileNames = new List<string>();

                foreach (var formFile in files)
                {
                    if (formFile.Length > 0)
                    {
                        // full path to file in temp location
                        string uniqueName = DateTime.UtcNow.Ticks + "__" + formFile.FileName;
                        var filePath = Path.Combine(Directory.GetCurrentDirectory(), "FileUploads", uniqueName);

                        fileNames.Add(uniqueName);
                        filePaths.Add(filePath);

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(stream);
                        }
                    }
                }

                // process uploaded files
                // Don't rely on or trust the FileName property without validation.
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;
                response.filesCount = files.Count;
                response.fileSizes = size;
                response.fileNames = fileNames;
                response.filePath = filePaths;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }
        public async Task<ImageBaseResponse> UploadPhoto(List<IFormFile> files)
        {
            ImageBaseResponse response = new ImageBaseResponse();

            try
            {
                long size = files.Sum(f => f.Length);

                var filePaths = new List<string>();
                var fileNames = new List<string>();

                foreach (var formFile in files)
                {
                    if (formFile.Length > 0)
                    {
                        // full path to file in temp location
                        string uniqueName = DateTime.UtcNow.Ticks + "__" + formFile.FileName;
                        var filePath = Path.Combine(Directory.GetCurrentDirectory(), "FileUploads", uniqueName);

                        fileNames.Add(uniqueName);
                        filePaths.Add(filePath);

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(stream);
                        }
                    }
                }

                // process uploaded files
                // Don't rely on or trust the FileName property without validation.
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;
                response.filesCount = files.Count;
                response.fileSizes = size;
                response.fileNames = fileNames;
                response.filePath = filePaths;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> DownloadFile(string fileName)
        {
            BaseResponse response = new BaseResponse();
            ImageBaseResponse imageResponse = new ImageBaseResponse();

            try
            {
                var path = Path.Combine(Directory.GetCurrentDirectory(), "FileUploads", fileName);

                var memory = new MemoryStream();
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;

                imageResponse.Status = HttpStatusCode.OK.ToString();
                imageResponse.Code = (int)HttpStatusCode.OK;
                imageResponse.file = memory;
                imageResponse.filePath.Add(GlobalHelper.GetContentType(path).Result);
                imageResponse.fileNames.Add(Path.GetFileName(path));

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;
                response.Message = "Retrieve File Success.";
                response.Data = imageResponse;
            }
            catch (Exception ex)
            {
                imageResponse.Status = HttpStatusCode.InternalServerError.ToString();
                imageResponse.Code = (int)HttpStatusCode.InternalServerError;
                imageResponse.Message = ex.ToString();

                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = "Failed to Retrieve File.";
            }

            return response;
        }
    }
}
