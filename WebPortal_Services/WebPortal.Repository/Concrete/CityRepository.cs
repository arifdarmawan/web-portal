﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Linq;

namespace WebPortal.Repository.Concrete
{
    public class CityRepository : ICityRepository
    {
        readonly WebPortalContext _context;

        public CityRepository(WebPortalContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> GetCities()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await _context.MasterDataCity.AsNoTracking().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetCitiesByregionid(int masterDataRegionId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from city in _context.MasterDataCity
                                    where city.MasterDataRegionId == masterDataRegionId
                                    select new
                                    {
                                        Value = city.Id,
                                        Text = city.Name
                                    }).AsNoTracking().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;
                response.Message = "Reload data success.";
                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message.ToString();
            }

            return response;
        }
    }
}
