﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using WebPortal.Models.Models;
using System.Net;
using System.DirectoryServices.AccountManagement;
using WebPortal.Repository.Helper;
using FirstResource.System.Library.EnumMaster;
using System.Globalization;

namespace WebPortal.Repository.Concrete
{
    public class UserRepository : IUserRepository
    {
        readonly WebPortalContext _context;
        public UserRepository(WebPortalContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> Insert(MasterDataUser data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    if (_context.MasterDataUser.Where(n => n.Email == data.Email).Count() == 0)
                    {
                        MasterDataUser newUser = new MasterDataUser
                        {
                            UserId = data.Email,
                            UserName = data.Email,
                            Password = data.Password,
                            Email = data.Email,
                            UserLevel = data.UserLevel,
                            CreatedBy = data.Email,
                            CreatedDate = DateTime.Now,
                            ModifyBy = data.Email,
                            ModifyDate = DateTime.Now,
                            IsActive = false,
                            ActivateLink = data.ActivateLink,
                            RegistrationNumber = "CTC" + String.Format(CultureInfo.InvariantCulture, "{0:0000000}", await _context.MasterDataUser.MaxAsync(n => n.Id) + 1)
                        };

                        await _context.MasterDataUser.AddAsync(newUser);
                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response = ResponseConstant.SAVE_SUCCESS;
                    }
                    else
                    {
                        response.Code = (int)HttpStatusCode.BadRequest;
                        response.Status = HttpStatusCode.BadRequest.ToString();
                        response.Message = "Email already exist.";

                        dbcxtransaction.Rollback();
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> GetUserByEmail(string email)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from user in _context.MasterDataUser
                                    where user.Email == email
                                    select user).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetUserByEmailOrUserId(string email)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from user in _context.MasterDataUser
                                    where user.UserId == email
                                    select user).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;
                response.Message = "User Found";

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetUserById(int id)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from user in _context.MasterDataUser
                                    where user.Id == id
                                    select user).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;
                response.Message = "User Found";

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> InsertUserFromAd(LoginViewModel model)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var domains = await _context.MasterDataDomain.ToListAsync();

                    foreach (var domain in domains)
                    {
                        try
                        {
                            var domainContext = new PrincipalContext(ContextType.Domain, domain.DomainLink, model.UserId, model.Password);

                            var userAD = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, model.UserId);

                            if (userAD != null)
                            {
                                MasterDataUser newUser = new MasterDataUser
                                {
                                    UserId = model.UserId,
                                    UserName = userAD.GivenName + " " + userAD.Surname,
                                    Email = userAD.EmailAddress,
                                    UserLevel = domain.DomainLink.Contains("first-resources") ? 11 : domain.DomainLink.Contains("fap") ? 12 : domain.DomainLink.Contains("borneo") ? 13 : 1,
                                    Region = await GlobalHelper.GetProperty(userAD, "physicalDeliveryOfficeName"),
                                    Jabatan = await GlobalHelper.GetProperty(userAD, "title"),
                                    Divisi = await GlobalHelper.GetProperty(userAD, "department"),
                                    ExtensionNumber = userAD.VoiceTelephoneNumber,
                                    IsUserAd = true,
                                    IsActive = true,
                                    Domain = domain.DomainLink
                                };

                                await _context.MasterDataUser.AddAsync(newUser);
                                await _context.SaveChangesAsync();

                                dbcxtransaction.Commit();

                                response.Code = (int)HttpStatusCode.OK;
                                response.Status = HttpStatusCode.OK.ToString();
                                response.Message = "User successfull inserted.";
                                response.Data = newUser;
                            }
                        }
                        catch (Exception ex)
                        {
                            response.Message = ex.Message;
                        }
                    }
                }
                catch (Exception ex)
                {
                    dbcxtransaction.Rollback();

                    if (ex.Message.Contains("The user name or password is incorrect."))
                    {
                        response.Code = (int)CustomHttpStatusCode.UserAdNotRegistered;
                        response.Message = "Username not registered. Please contact administrator.";
                    }
                    else
                    {
                        response.Message = ex.ToString();
                        response.Code = (int)HttpStatusCode.InternalServerError;
                        response.Status = HttpStatusCode.InternalServerError.ToString();
                    }
                }
            }

            return response;
        }

        public async Task<BaseResponse> ActivationUser(string activationCode)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var user = await _context.MasterDataUser.FirstOrDefaultAsync(n => n.ActivateLink == activationCode && !n.IsActive);

                    if (user != null)
                    {
                        user.IsActive = true;

                        _context.Entry(user).State = EntityState.Modified;

                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response = ResponseConstant.UPDATE_SUCCESS;
                        response.Data = user.UserId;
                    }
                    else
                    {
                        response.Code = (int)HttpStatusCode.OK;
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Message = "Not Found.";
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> UpdateUserFromAd(ActiveDirectoryUserModel data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var user = await _context.MasterDataUser.FirstOrDefaultAsync(n => n.Email == data.UserId || n.UserId == data.UserId);

                    using (var domainContext = new PrincipalContext(ContextType.Domain, user.Domain, user.UserId, data.Password))
                    {
                        using (var userAD = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, user.UserId))
                        {
                            if (userAD != null)
                            {
                                user.UserName = userAD.GivenName + " " + userAD.Surname;
                                user.Email = userAD.EmailAddress;
                                user.Region = await GlobalHelper.GetProperty(userAD, "physicalDeliveryOfficeName");
                                user.Jabatan = await GlobalHelper.GetProperty(userAD, "title");
                                user.Divisi = await GlobalHelper.GetProperty(userAD, "department");
                                user.ExtensionNumber = userAD.VoiceTelephoneNumber;

                                _context.Entry(user).State = EntityState.Modified;
                                await _context.SaveChangesAsync();

                                dbcxtransaction.Commit();

                                response.Code = (int)HttpStatusCode.OK;
                                response.Status = HttpStatusCode.OK.ToString();
                                response.Message = "User Exist.";
                                response.Data = user;
                            }
                            else
                            {
                                response = ResponseConstant.NO_RESULT;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    dbcxtransaction.Rollback();

                    if (ex.Message.Contains("The user name or password is incorrect."))
                    {
                        response.Code = (int)HttpStatusCode.BadRequest;
                        response.Status = HttpStatusCode.BadRequest.ToString();
                        response.Message = ex.Message;
                    }
                    else
                    {
                        response.Code = (int)HttpStatusCode.InternalServerError;
                        response.Status = HttpStatusCode.InternalServerError.ToString();
                        response.Message = ex.Message;
                    }
                }
            }

            return response;
        }
    }
}
