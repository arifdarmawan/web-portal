﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;

namespace WebPortal.Repository.Concrete
{
    public class JobTitleRepository : IJobTitleRepository
    {
        readonly WebPortalContext _context;
        public JobTitleRepository(WebPortalContext context)
        {
            _context = context;
        }
        public async Task<BaseResponse> GetJobTitles()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from vendorBank in _context.MasterDataJobTitle
                                    select new
                                    {
                                        vendorBank.Id,
                                        vendorBank.Title
                                    }).AsNoTracking().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> InsertJobTitle(MasterDataJobTitle data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    MasterDataJobTitle jobTitle = new MasterDataJobTitle
                    {
                        Title = data.Title,
                        CreatedDate = DateTime.Now,
                        CreatedBy = "Admin",
                        ModifiedDate = DateTime.Now,
                        ModifiedBy = "Admin",
                        IsDelete = false
                    };

                    await _context.MasterDataJobTitle.AddAsync(jobTitle);
                    await _context.SaveChangesAsync();

                    dbcxtransaction.Commit();

                    response = ResponseConstant.SAVE_SUCCESS;
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }
    }
}
