﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortal.Repository.Concrete
{
    public class ResetPasswordRepository : IResetPasswordRepository
    {
        readonly WebPortalContext _context;
        public ResetPasswordRepository(WebPortalContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> ResetPassword(string email)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    // update reset password data before create new one
                    #region update
                    var existingResetPass = await _context.MasterDataResetPassword.Where(n => n.Credential == email).ToListAsync();

                    existingResetPass.ForEach(n => n.IsUsed = true);
                    #endregion

                    #region insert
                    MasterDataResetPassword newReset = new MasterDataResetPassword
                    {
                        Credential = email,
                        IsUsed = false,
                        Token = Guid.NewGuid().ToString().Replace("-", ""),
                        ExpiredDate = DateTime.UtcNow.AddDays(1)
                    };

                    await _context.MasterDataResetPassword.AddAsync(newReset);
                    await _context.SaveChangesAsync();

                    dbcxtransaction.Commit();

                    response.Code = (int)HttpStatusCode.OK;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Data = newReset.Token;
                    #endregion
                }
                catch (Exception ex)
                {
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();
                    response.Message = ex.ToString();
                }
            }

            return response;
        }

        public async Task<BaseResponse> GetValidResetPassword(string token)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from single in _context.MasterDataResetPassword
                                    where single.Token == token
                                    && !single.IsUsed
                                    && single.ExpiredDate > DateTime.Now
                                    select single).FirstOrDefaultAsync();

                response.Code = (int)HttpStatusCode.OK;
                response.Status = HttpStatusCode.OK.ToString();
                response.Message = "Retrieve data success";
                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> SetNewPassword(NewPasswordModel data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var resetData = await (from single in _context.MasterDataResetPassword
                                        where single.Token == data.Token
                                        && !single.IsUsed
                                        && single.ExpiredDate > DateTime.Now
                                        select single).FirstOrDefaultAsync();

                    if (resetData.ExpiredDate < DateTime.Now)
                    {
                        response.Code = (int)HttpStatusCode.Accepted;
                        response.Status = HttpStatusCode.Accepted.ToString();
                        response.Message = "Token expired, Please request new token.";
                    } else
                    {
                        var user = await _context.MasterDataUser.FirstOrDefaultAsync(n => n.Email == resetData.Credential);

                        user.Password = data.NewPassword;

                        _context.Entry(user).State = EntityState.Modified;

                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response = ResponseConstant.UPDATE_SUCCESS;
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }
            return response;
        }

        public async Task<BaseResponse> SetChangePassword(NewPasswordModel data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var user = await _context.MasterDataUser.FirstOrDefaultAsync(n => n.UserId== data.UserId);

                    if (user != null)
                    {
                        user.Password = data.NewPassword;
                        _context.Entry(user).State = EntityState.Modified;
                        
                        await _context.SaveChangesAsync();
                        dbcxtransaction.Commit();
                        
                        response.Code = (int)HttpStatusCode.OK;
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Message = "Your Password has been changed!";
                    }
                    else
                    {
                        response.Code = (int)HttpStatusCode.NotFound;
                        response.Status = HttpStatusCode.NotFound.ToString();
                        response.Message = "Update Password Fail.!";
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
                return response;
            }
        }
    }
}
