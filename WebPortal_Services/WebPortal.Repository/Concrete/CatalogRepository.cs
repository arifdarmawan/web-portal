﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;
using System.Linq;

namespace WebPortal.Repository.Concrete
{
    public class CatalogRepository : ICatalogRepository
    {
        readonly WebPortalContext _context;
        public CatalogRepository(WebPortalContext context)
        {
            _context = context;
        }
        public async Task<BaseResponse> GetAllCategories()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from category in _context.MasterItemCategories
                              select new
                              {
                                  category.CategoryId,
                                  category.CategoryCode,
                                  category.CategoryName,
                                  category.ParentCategoryId
                              }).AsNoTracking().ToListAsync();

                response.Data = result;
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetCategoriesByGroupLevel()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from categories in _context.MasterItemCategories
                                    where categories.ParentCategoryId == 0
                                    select new
                                    {
                                        categories.CategoryId,
                                        categories.CategoryCode,
                                        categories.CategoryName,
                                        categories.ParentCategoryId,
                                        categoryField = (from vcategories in _context.MasterItemCategories
                                                         where vcategories.ParentCategoryId != 0
                                                         select new
                                                         {
                                                             vcategories.CategoryId,
                                                             vcategories.CategoryCode,
                                                             vcategories.CategoryName,
                                                         })
                                    }).AsNoTracking().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }
    }
}
