﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Repository.Abstract;

namespace WebPortal.Repository.Concrete
{
    public class DomainRepository : IDomainRepository
    {
        readonly WebPortalContext _context;
        public DomainRepository(WebPortalContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<MasterDataDomain>> GetAllDomain()
        {
            return await _context.MasterDataDomain.AsNoTracking().ToListAsync();
        }
    }
}
