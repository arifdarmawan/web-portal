﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Net;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;
using System.Linq;

namespace WebPortal.Repository.Concrete
{
    public class ItemRepository: IItemRepository
    {
        readonly WebPortalContext _context;
        public ItemRepository(WebPortalContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> GetAllItem()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await _context.MasterItems.AsNoTracking().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetAllItemLookup()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from item in _context.MasterItems
                                    select new
                                    {
                                        value = item.ItemName,
                                        id = item.ItemId,
                                        code = item.ItemCode
                                    }).AsNoTracking().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetItemByCode(string bimsCode)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from item in _context.MasterItems
                                    join itemdetail in _context.MasterItemDetails
                                        on item.ItemCode equals itemdetail.ItemCode
                                    join category in _context.MasterItemCategories
                                        on item.MasterItemCategoryId equals category.CategoryId
                                    join uom in _context.MasterUnitOfMeasure
                                        on item.Uomid equals uom.Uomid
                                    where item.BimsCode == bimsCode
                                    select new
                                    {
                                        item.ItemId,
                                        item.ItemCode,
                                        item.BimsCode,
                                        item.OracleCode,
                                        item.ItemName,
                                        itemdetail.ItemSpecification,
                                        category.CategoryCode,
                                        category.CategoryName,
                                        uom.Uomid,
                                        uom.Uomname,
                                        item.UnitPrice
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetVendorItemPrice(int itemId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from itemPrice in _context.MappingVendorItemPrice
                                    join curr in _context.MasterDataCurrency
                                        on itemPrice.MasterDataCurrencyId equals curr.Id
                                    where itemPrice.MasterDataItemId == itemId
                                    select new
                                    {
                                        CurrencyId = curr.Id,
                                        itemPrice.Price
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> SearchItemByKey(string key)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from item in _context.MasterItems
                                    where item.ItemName.ToLower().Contains(key.ToLower())
                                    select new
                                    {
                                        Value = item.ItemName,
                                        Id = item.ItemId,
                                        Code = item.BimsCode
                                    }).ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }
    }
}
