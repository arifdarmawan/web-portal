﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;
using WebPortal.Repository.Helper;
using FirstResource.System.Library.EnumMaster;

namespace WebPortal.Repository.Concrete
{
    public class ProcurementRepository : IProcurementRepository
    {
        readonly WebPortalContext _context;

        public ProcurementRepository(WebPortalContext context)
        {
            _context = context;
        }

        #region Purchase Requisition
        public async Task<BaseResponse> GetAccessPurchaseRequisition(int userid)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from mgroup in _context.WorkflowGroupMember
                                    join company in _context.ConfigCompany
                                        on mgroup.ConfigCompanyId equals company.Id
                                    join division in _context.ConfigDivision
                                        on mgroup.ConfigDivisionId equals division.Id
                                    where mgroup.MasterDataUserId == userid && mgroup.ApprovalLevel == 1
                                    select new
                                    {
                                        mgroup.ConfigCompanyId,
                                        company.CompanyCode,
                                        company.CompanyName,
                                        mgroup.ConfigDivisionId,
                                        division.DivisionCode,
                                        DivisionName = division.Description
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetCheckVendorItem(int itemId)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var result = await (from vendorItem in _context.MappingVendorItemPrice
                                        join vendor in _context.MasterDataVendor
                                            on vendorItem.MasterDataVendorId equals vendor.Id
                                        where vendorItem.MasterDataItemId == itemId && vendorItem.PriceValidDate.Value.Date >= DateTime.Now.Date
                                        select new
                                        {
                                            vendorItem.MasterDataVendorId,
                                            vendorName = vendor.Name
                                        }).FirstOrDefaultAsync();

                    response.Status = HttpStatusCode.OK.ToString();
                    response.Code = (int)HttpStatusCode.OK;

                    if (result != null)
                    {
                        response.Message = "Retrieve data success";
                    }
                    else
                    {
                        response.Message = "No result";
                    }

                    response.Data = result;
                }
                catch (Exception ex)
                {
                    response.Status = HttpStatusCode.InternalServerError.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Message = ex.ToString();
                }
            }

            return response;
        }

        public async Task<BaseResponse> GetPpnVendorItemCategories(int itemId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from mapVendorItemPrice in _context.MappingVendorItemPrice
                                    join vendor in _context.MasterDataVendor
                                        on mapVendorItemPrice.MasterDataVendorId equals vendor.Id
                                    join user in _context.MasterDataUser
                                        on vendor.MasterDataUserId equals user.Id
                                    join vendorTax in _context.MappingDataVendorTax
                                        on user.Id equals vendorTax.MasterDataUserId
                                    join items in _context.MasterItems
                                        on mapVendorItemPrice.MasterDataItemId equals items.ItemId
                                    where items.ItemId == itemId
                                    select new
                                    {
                                        VendorId = vendor.Id,
                                        items.ItemId,
                                        PPN = vendorTax.Ppn
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> InsertPurchaseRequisition(CreatePurchaseRequisitionModel data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    string workflowTypeCode;

                    #region MasterPurchaseRequisition
                    MasterPurchaseRequisition purchaseRequisition = new MasterPurchaseRequisition
                    {
                        Prnumber = await _context.MasterPurchaseRequisition.CountAsync() > 0 ? data.PRNumber + "-" + string.Format(CultureInfo.InvariantCulture, "{0:0000000}", await _context.MasterPurchaseRequisition.MaxAsync(n => n.Id) + 1) : data.PRNumber + "-" + string.Format(CultureInfo.InvariantCulture, "{0:0000000}", 1),
                        RequestedFrom = data.RequestedFrom,
                        CompanyCode = data.CompanyCode,
                        DepartementCode = data.DepartementCode,
                        CompanyLocationCode = data.CompanyLocationCode,
                        Prstatus = data.Prstatus,
                        GrandTotal = data.GrandTotal,
                        CreatedBy = data.CreatedBy,
                        CreatedDate = data.CreatedDate,
                        ModifiedBy = data.ModifiedBy,
                        ModifyDate = data.ModifiedDate,
                        IsDelete = false
                    };

                    await _context.MasterPurchaseRequisition.AddAsync(purchaseRequisition);
                    await _context.SaveChangesAsync();
                    #endregion

                    #region MasterPurchaseRequisitionItem
                    List<MasterPurchaseRequisitionItem> purchaseRequisitionItem = new List<MasterPurchaseRequisitionItem>();
                    List<MasterRequestForQuotation> requestForQuotation = new List<MasterRequestForQuotation>();

                    foreach (var item in data.ListItem)
                    {
                        if (item.CurrencyId == null)
                        {
                            item.CurrencyId = 76;
                        }

                        MasterPurchaseRequisitionItem requisitionItem = new MasterPurchaseRequisitionItem()
                        {
                            Specification = item.Specification,
                            PurposeDesc = item.PurposeDesc,
                            Qty = item.Qty,
                            Uomid = item.UomId,
                            CurrencyId = item.CurrencyId,
                            UnitPrice = item.UnitPrice,
                            TotalPrice = item.TotalPrice,
                            Ppn = item.PPN,
                            DateRequired = item.DateRequired,
                            BudgetStatus = item.BudgetStatus,
                            ItemVendorStatus = item.ItemVendorStatus,
                            MasterItemId = item.MasterItemId,
                            MasterPurchaseRequisitionId = purchaseRequisition.Id,
                            IsRequestVendor= item.IsRequestVendor,
                            CreatedDate = data.CreatedDate,
                            CreatedBy = data.CreatedBy,
                            ModifyDate = data.ModifiedDate,
                            ModifyBy = data.ModifiedBy
                        };
                        purchaseRequisitionItem.Add(requisitionItem);
                    }
                    await _context.MasterPurchaseRequisitionItem.AddRangeAsync(purchaseRequisitionItem);
                    await _context.SaveChangesAsync();
                    #endregion

                    // Pengecekan ke table master vendor, column => contract datem categories Id if count > 0 PR else PTA
                    if (purchaseRequisition.Prstatus == PurchaseRequisitionStatus.PTA.ToString())
                    {
                        workflowTypeCode = FirstResource.System.Library.EnumMaster.WorkflowType.PTA.ToString();
                    }
                    else
                    {
                        workflowTypeCode = FirstResource.System.Library.EnumMaster.WorkflowType.PR.ToString();
                    }

                    #region WorkflowProcess
                    WorkflowProcess workflowProcess = new WorkflowProcess
                    {
                        WorkflowTypeCode = workflowTypeCode,
                        MasterDataUserId = data.MasterDataUserId,
                        ProcessStatus = WorkflowProcessStatus.START.ToString(),
                        CreatedDate = data.CreatedDate,
                        CreatedBy = data.CreatedBy,
                        ModifiedDate = data.ModifiedDate,
                        ModifiedBy = data.ModifiedBy
                    };

                    await _context.WorkflowProcess.AddAsync(workflowProcess);
                    await _context.SaveChangesAsync();
                    #endregion

                    #region MappingWorkflowPurchaseRequisition
                    MappingWorkflowPurchaseRequisition workflowPurchaseRequisition = new MappingWorkflowPurchaseRequisition
                    {
                        ProcessId = workflowProcess.Id,
                        PurchaseRequisitionId = purchaseRequisition.Id
                    };

                    await _context.MappingWorkflowPurchaseRequisition.AddAsync(workflowPurchaseRequisition);
                    await _context.SaveChangesAsync();
                    #endregion

                    #region WorkflowRequest
                    WorkflowRequest workflowRequest = new WorkflowRequest
                    {
                        WorkflowProcessId = workflowProcess.Id,
                        Name = "Purchase Requisition - " + data.CreatedBy,
                        MasterDataUserId = data.MasterDataUserId,
                        CreatedDate = data.CreatedDate,
                        CreatedBy = data.CreatedBy,
                        ModifiedDate = data.ModifiedDate,
                        ModifiedBy = data.ModifiedBy
                    };

                    await _context.WorkflowRequest.AddAsync(workflowRequest);
                    await _context.SaveChangesAsync();
                    #endregion

                    #region WorkflowActionApproval
                    var wfGroupMemberList = await (from wfGroupMember in _context.WorkflowGroupMember
                                                   where wfGroupMember.ConfigCompanyId == data.ConfigCompanyId &&
                                                   wfGroupMember.ConfigDivisionId == data.ConfigDivisionId &&
                                                   wfGroupMember.WorkflowTypeCode == workflowTypeCode
                                                   orderby wfGroupMember.ApprovalLevel
                                                   select new
                                                   {
                                                       wfGroupMember.MasterDataUserId,
                                                       wfGroupMember.ApprovalLevel
                                                   }).ToListAsync();

                    List<WorkflowActionApproval> wfActionApprovalList = new List<WorkflowActionApproval>();
                    foreach (var member in wfGroupMemberList)
                    {
                        WorkflowActionApproval actionApproval = new WorkflowActionApproval()
                        {
                            WorkflowProcessId = workflowProcess.Id,
                            WorkflowRequestId = workflowRequest.Id,
                            MasterDataUserId = member.MasterDataUserId,
                            IsActive = member.ApprovalLevel == 1 ? false : true, // default isActive = false by requestor
                            IsComplete = member.ApprovalLevel == 1 ? true : false, // default isComplete = true by requestor
                            CreatedDate = data.CreatedDate,
                            CreatedBy = data.CreatedBy,
                            ModifiedDate = data.ModifiedDate,
                            ModifiedBy = data.ModifiedBy
                        };

                        wfActionApprovalList.Add(actionApproval);
                    }

                    await _context.WorkflowActionApproval.AddRangeAsync(wfActionApprovalList);
                    await _context.SaveChangesAsync();
                    #endregion

                    #region WorkflowActivity
                    var currentNextAction = new List<Tuple<int, string, string>>().Select(t => new { Id = t.Item1, Desc = t.Item2, Activity = t.Item3 }).ToList();
                    var currentAction = await (from wfActionApproval in _context.WorkflowActionApproval
                                               where wfActionApproval.WorkflowProcessId == workflowProcess.Id
                                               && wfActionApproval.WorkflowRequestId == workflowRequest.Id
                                               && wfActionApproval.IsActive == false // this logic as indicator submitter or requestor
                                               && wfActionApproval.IsComplete == true // this logic as indicator submitter or requestor
                                               select new
                                               {
                                                   wfActionApproval.Id,
                                                   Desc = ConstGlobalMessage.SubmittedMessage + _context.MasterDataUser.Where(n => n.Id == wfActionApproval.MasterDataUserId).Select(n => n.UserName).FirstOrDefault(),
                                                   Activity = ActivityStatus.APPROVED.ToString()
                                               }).FirstOrDefaultAsync();

                    var nextAction = await (from wfActionApproval in _context.WorkflowActionApproval
                                            where wfActionApproval.WorkflowProcessId == workflowProcess.Id
                                            && wfActionApproval.WorkflowRequestId == workflowRequest.Id
                                            && wfActionApproval.IsActive == true // this logic as indicator next approval
                                            && wfActionApproval.IsComplete == false // this logic as indicator next approval
                                            select new
                                            {
                                                wfActionApproval.Id,
                                                Desc = ConstGlobalMessage.NotificationMessage + _context.MasterDataUser.Where(n => n.Id == wfActionApproval.MasterDataUserId).Select(n => n.UserName).FirstOrDefault(),
                                                Activity = ActivityStatus.WAITING_APPROVAL.ToString()
                                            }).FirstOrDefaultAsync();

                    currentNextAction.Add(currentAction);
                    currentNextAction.Add(nextAction);

                    List<WorkflowActivity> workflowActivities = new List<WorkflowActivity>();
                    foreach (var action in currentNextAction)
                    {
                        WorkflowActivity workflowActivity = new WorkflowActivity
                        {
                            WorkflowActionApprovalId = action.Id,
                            Activity = action.Activity,
                            Desc = action.Desc,
                            CreatedDate = data.CreatedDate,
                            CreatedBy = data.CreatedBy,
                            ModifiedDate = data.ModifiedDate,
                            ModifiedBy = data.ModifiedBy
                        };

                        workflowActivities.Add(workflowActivity);
                    }

                    await _context.WorkflowActivity.AddRangeAsync(workflowActivities);
                    await _context.SaveChangesAsync();
                    #endregion

                    response.Code = (int)HttpStatusCode.OK;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Message = "Save Data Success";

                    dbcxtransaction.Commit();
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> GetPurchaseRequisition()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var result = await (from headerPR in _context.MasterPurchaseRequisition
                                    join itemPR in _context.MasterPurchaseRequisitionItem
                                        on headerPR.Id equals itemPR.MasterPurchaseRequisitionId
                                    //join curr in _context.MasterDataCurrency
                                    //    on itemPR.CurrencyId equals curr.Id
                                    //where headerPR.Prstatus != PurchaseRequisitionStatus.WAITING_RFQ.ToString()
                                    orderby headerPR.ModifyDate descending
                                    select new
                                    {
                                        headerPR.Id,
                                        headerPR.CompanyCode,
                                        headerPR.CompanyLocationCode,
                                        headerPR.DepartementCode,
                                        headerPR.Prnumber,
                                        headerPR.RequestedFrom,
                                        //Currency = curr.Code,
                                        headerPR.GrandTotal,
                                        headerPR.Prstatus,
                                        headerPR.ModifyDate
                                    }).AsNoTracking().Distinct().ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetPurchaseRequisitionByPRNo(string prNo)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from prHeader in _context.MasterPurchaseRequisition
                                    where prHeader.Prnumber == prNo
                                    select new
                                    {
                                        prHeader.Prnumber,
                                        prHeader.Prstatus,
                                        prHeader.CompanyCode,
                                        prHeader.DepartementCode,
                                        ListItem = (from prItem in _context.MasterPurchaseRequisitionItem
                                                    select new
                                                    {
                                                        Specification = prItem.Specification,
                                                        Qty = prItem.Qty,
                                                        Uom = prItem.Uomid,
                                                        Currency = prItem.Currency,
                                                        PurposeDesc = prItem.PurposeDesc,
                                                    })
                                    }).FirstOrDefaultAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;


                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> getPurchaseRequisitionById(int Id)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from prHeader in _context.MasterPurchaseRequisition
                                    join company in _context.ConfigCompany
                                        on prHeader.CompanyCode equals company.CompanyCode
                                    join division in _context.ConfigDivision
                                        on prHeader.DepartementCode equals division.DivisionCode
                                    join MappingwfPR in _context.MappingWorkflowPurchaseRequisition
                                        on prHeader.Id equals MappingwfPR.PurchaseRequisitionId
                                    join wfProcess in _context.WorkflowProcess
                                        on MappingwfPR.ProcessId equals wfProcess.Id
                                    where prHeader.Id == Id
                                    select new
                                    {
                                        prHeader.Prnumber,
                                        prHeader.Prstatus,
                                        prHeader.CompanyCode,
                                        company.CompanyName,
                                        prHeader.CompanyLocationCode,
                                        prHeader.DepartementCode,
                                        DepartementName = division.Description,
                                        prHeader.GrandTotal,
                                        prHeader.CreatedDate,
                                        ProcessId = wfProcess.Id,
                                        ListItem = (from pritem in _context.MasterPurchaseRequisitionItem
                                                    join items in _context.MasterItems
                                                        on pritem.MasterItemId equals items.ItemId
                                                    join uom in _context.MasterUnitOfMeasure
                                                        on pritem.Uomid equals uom.Uomid
                                                    where pritem.MasterPurchaseRequisitionId == Id
                                                    orderby pritem.Id descending
                                                    select new
                                                    {
                                                        pritem.Id,
                                                        pritem.Specification,
                                                        pritem.PurposeDesc,
                                                        Uom = uom.Uomcode,
                                                        Currency = (from curr in _context.MasterDataCurrency
                                                                    where curr.Id == pritem.CurrencyId
                                                                    select curr.Code).FirstOrDefault(),
                                                        pritem.Qty,
                                                        pritem.UnitPrice,
                                                        pritem.TotalPrice,
                                                        pritem.Ppn,
                                                        pritem.MasterItemId,
                                                        items.BimsCode,
                                                        items.ItemName,
                                                        pritem.ItemVendorStatus,
                                                        UnitPriceString = string.Format("{0:N2}", pritem.UnitPrice),
                                                        TotalPriceString = string.Format("{0:N2}", pritem.TotalPrice)
                                                    }).Distinct(),
                                    }).FirstOrDefaultAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> getPurchaseRequisitionItemByVendorId(int id)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from vendor in _context.MasterDataVendor
                                    join user in _context.MasterDataUser
                                        on vendor.MasterDataUserId equals user.Id
                                    join vendorTax in _context.MappingDataVendorTax
                                        on user.Id equals vendorTax.MasterDataUserId
                                    join vendorcategories in _context.MappingDataVendorCategories
                                        on vendor.Id equals vendorcategories.MasterDataVendorId
                                    join itemcategory in _context.MasterItemCategories
                                        on vendorcategories.CategoriesId equals itemcategory.CategoryId
                                    join items in _context.MasterItems
                                        on itemcategory.CategoryId equals items.MasterItemCategoryId
                                    join pritem in _context.MasterPurchaseRequisitionItem
                                        on items.ItemId equals pritem.MasterItemId
                                    join uom in _context.MasterUnitOfMeasure
                                        on pritem.Uomid equals uom.Uomid
                                    join curr in _context.MasterDataCurrency
                                        on pritem.CurrencyId equals curr.Id
                                    where pritem.MasterPurchaseRequisitionId == id
                                    select new
                                    {
                                        VendorName = vendor.Name,
                                        ItemCode = items.ItemCode,
                                        BimsCode = items.BimsCode,
                                        ItemName = items.ItemName,
                                        Specification = pritem.Specification.Replace("{", "").Replace("}", "").Replace("\"", ""),
                                        PurposeDesc = pritem.PurposeDesc,
                                        Qty = pritem.Qty,
                                        UomName = uom.Uomname,
                                        Currency = curr.Code,
                                        PPN = vendorTax.Ppn,
                                        UnitPrice = pritem.UnitPrice,
                                        TotalPrice = pritem.TotalPrice,
                                        UnitPriceString = string.Format("{0:N2}", pritem.UnitPrice),
                                        TotalPriceString = string.Format("{0:N2}", pritem.TotalPrice)
                                    }).AsNoTracking().Distinct().ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetRealizationBudgetItem(string bimsCode, int year)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from poItem in _context.MasterPurchaseOrderItem
                                    join masteritem in _context.MasterItems
                                        on poItem.MasterItemId equals masteritem.ItemId
                                    join po in _context.MasterPurchaseOrder
                                   on poItem.MasterPurchaseOrderId equals po.Id
                                    where masteritem.BimsCode == bimsCode && poItem.CreatedDate.Year == year && po.Postatus != FirstResource.System.Library.Extensions.EnumExtensions.GetDescription(PurchaseRequisitionStatus.REJECTED).Result
                                    select new
                                    {
                                        Qty = poItem.Qty,
                                        UnitPrice = poItem.UnitPrice,
                                        TotalPrice = poItem.TotalPrice
                                    }).ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetBookingRealizationBudgetItem(string itemCode, int year)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from masteritem in _context.MasterItems
                                    join prItem in _context.MasterPurchaseRequisitionItem
                                        on masteritem.ItemId equals prItem.MasterItemId
                                    where masteritem.ItemCode == itemCode && prItem.CreatedDate.Year == year
                                    select new
                                    {
                                        Qty = prItem.Qty,
                                        UnitPrice = prItem.UnitPrice,
                                        TotalPrice = prItem.TotalPrice
                                    }).ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetRFQList()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from prItem in _context.MasterPurchaseRequisitionItem
                                    join prHeader in _context.MasterPurchaseRequisition
                                        on prItem.MasterPurchaseRequisitionId equals prHeader.Id
                                    join items in _context.MasterItems
                                        on prItem.MasterItemId equals items.ItemId
                                    join category in _context.MasterItemCategories
                                        on items.MasterItemCategoryId equals category.CategoryId
                                    where prHeader.Prstatus == PurchaseRequisitionStatus.COMPLETED.ToString()
                                          ///&& prItem.ItemVendorStatus != PurchaseRequisitionStatus.VENDOR_IS_EXIST.ToString()
                                           && prItem.IsRequestVendor == true
                                    orderby prHeader.CreatedDate descending
                                    select new
                                    {
                                        prItem.MasterPurchaseRequisitionId,
                                        PRNumber = prHeader.Prnumber,
                                        prItem.MasterItemId,
                                        items.ItemName,
                                        prItem.Specification,
                                        prItem.Qty,
                                        prItem.Uomid,
                                        prItem.TotalPrice,
                                        prItem.PurposeDesc,
                                        prItem.ItemVendorStatus
                                    }).AsNoTracking().ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetRFQDetail(int id, int masterItemId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from prHeader in _context.MasterPurchaseRequisition
                                    join company in _context.ConfigCompany
                                        on prHeader.CompanyCode equals company.CompanyCode
                                    join division in _context.ConfigDivision
                                        on prHeader.DepartementCode equals division.DivisionCode
                                    join MappingwfPR in _context.MappingWorkflowPurchaseRequisition
                                        on prHeader.Id equals MappingwfPR.PurchaseRequisitionId
                                    join wfProcess in _context.WorkflowProcess
                                        on MappingwfPR.ProcessId equals wfProcess.Id
                                    join pritem in _context.MasterPurchaseRequisitionItem
                                        on prHeader.Id equals pritem.MasterPurchaseRequisitionId
                                    join items in _context.MasterItems
                                        on pritem.MasterItemId equals items.ItemId
                                    join uom in _context.MasterUnitOfMeasure
                                        on pritem.Uomid equals uom.Uomid
                                    where pritem.MasterPurchaseRequisitionId == id && pritem.MasterItemId == masterItemId
                                    select new
                                    {
                                        Prid = prHeader.Id,
                                        prHeader.Prnumber,
                                        prHeader.CompanyCode,
                                        company.CompanyName,
                                        prHeader.CompanyLocationCode,
                                        prHeader.DepartementCode,
                                        DepartementName = division.Description,
                                        prHeader.CreatedDate,
                                        ProcessId = wfProcess.Id,
                                        items.MasterItemCategoryId,
                                        pritem.MasterItemId,
                                        pritem.ItemVendorStatus,
                                        items.ItemCode,
                                        items.BimsCode,
                                        items.ItemName,
                                        Specification = pritem.Specification.Replace("{", "").Replace("}", "").Replace("\"", ""),
                                        pritem.PurposeDesc,
                                        pritem.Qty,
                                        UomName = uom.Uomname,
                                        pritem.UnitPrice,
                                        pritem.TotalPrice,
                                        UnitPriceString = string.Format("{0:N2}", pritem.UnitPrice),
                                        TotalPriceString = string.Format("{0:N2}", pritem.TotalPrice)
                                    }).FirstOrDefaultAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetQCFList()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from prItem in _context.MasterPurchaseRequisitionItem
                                    join prHeader in _context.MasterPurchaseRequisition
                                        on prItem.MasterPurchaseRequisitionId equals prHeader.Id
                                    join qcf in _context.MasterQuotationComparationForm
                                        on prHeader.Id equals qcf.Prid
                                    join items in _context.MasterItems
                                        on prItem.MasterItemId equals items.ItemId
                                    join category in _context.MasterItemCategories
                                        on items.MasterItemCategoryId equals category.CategoryId
                                    where prItem.IsRequestVendor == true
                                        && prItem.ItemVendorStatus != PurchaseRequisitionStatus.REQUEST_VENDOR.ToString()
                                        orderby prHeader.CreatedDate descending
                                    select new
                                    {
                                        prItem.Id,
                                        prItem.MasterPurchaseRequisitionId,
                                        PRNumber = prHeader.Prnumber,
                                        prItem.MasterItemId,
                                        items.ItemName,
                                        prItem.Specification,
                                        prItem.Qty,
                                        prItem.Uomid,
                                        prItem.TotalPrice,
                                        prItem.PurposeDesc,
                                        prItem.ItemVendorStatus
                                    }).AsNoTracking().Distinct().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetQCFDetail(int id, int masterItemId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from prHeader in _context.MasterPurchaseRequisition
                                    join company in _context.ConfigCompany
                                        on prHeader.CompanyCode equals company.CompanyCode
                                    join division in _context.ConfigDivision
                                        on prHeader.DepartementCode equals division.DivisionCode
                                    join MappingwfPR in _context.MappingWorkflowPurchaseRequisition
                                        on prHeader.Id equals MappingwfPR.PurchaseRequisitionId
                                    join wfProcess in _context.WorkflowProcess
                                        on MappingwfPR.ProcessId equals wfProcess.Id
                                    join pritem in _context.MasterPurchaseRequisitionItem
                                        on prHeader.Id equals pritem.MasterPurchaseRequisitionId
                                    join items in _context.MasterItems
                                        on pritem.MasterItemId equals items.ItemId
                                    join uom in _context.MasterUnitOfMeasure
                                        on pritem.Uomid equals uom.Uomid
                                    where pritem.MasterPurchaseRequisitionId == id && pritem.MasterItemId == masterItemId
                                    select new
                                    {
                                        Prid = prHeader.Id,
                                        prHeader.Prnumber,
                                        prHeader.Prstatus,
                                        prHeader.CompanyCode,
                                        company.CompanyName,
                                        prHeader.CompanyLocationCode,
                                        prHeader.DepartementCode,
                                        DepartementName = division.Description,
                                        prHeader.CreatedDate,
                                        ProcessId = wfProcess.Id,
                                        items.MasterItemCategoryId,
                                        pritem.MasterItemId,
                                        items.ItemCode,
                                        items.BimsCode,
                                        items.ItemName,
                                        Specification = pritem.Specification.Replace("{", "").Replace("}", "").Replace("\"", ""),
                                        pritem.PurposeDesc,
                                        pritem.Qty,
                                        UomName = uom.Uomname,
                                        pritem.UnitPrice,
                                        pritem.TotalPrice,
                                        UnitPriceString = string.Format("{0:N2}", pritem.UnitPrice),
                                        TotalPriceString = string.Format("{0:N2}", pritem.TotalPrice)
                                    }).FirstOrDefaultAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetPurchaseRequisitionapproval()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from purchase in _context.MasterPurchaseRequisition
                                    join workflowprocess in _context.MappingWorkflowPurchaseRequisition
                                    on purchase.Id equals workflowprocess.PurchaseRequisitionId
                                    select new
                                    {
                                        purchase.Prnumber
                                    }).AsNoTracking().Distinct().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                response.Data = result;
            }
            catch
            {
            }

            return response;
        }

        public async Task<BaseResponse> GetPurchaseRequisitionItemOnProcess(string company, string bimsCode)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from prHeader in _context.MasterPurchaseRequisition
                                    join prItem in _context.MasterPurchaseRequisitionItem
                                        on prHeader.Id equals prItem.MasterPurchaseRequisitionId
                                    join item in _context.MasterItems
                                        on prItem.MasterItemId equals item.ItemId
                                    where prHeader.CompanyCode == company
                                    && prHeader.Prstatus == PurchaseRequisitionStatus.WAITING_APPROVAL.ToString()
                                    && item.BimsCode == bimsCode
                                    select prHeader.Id).CountAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                response.Data = result;
            }
            catch
            {
            }

            return response;
        }

        public async Task<BaseResponse> BlastToVendor(RequestForQuotationEmployeeModel data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    int count = 1;

                    var getVendor = await (from categoryVendor in _context.MappingDataVendorCategories
                                           where categoryVendor.CategoriesId == data.MasterItemCategoryId
                                           select new
                                           {
                                               VendorId = categoryVendor.MasterDataVendorId,
                                               MasterItemCategoryId = categoryVendor.CategoriesId
                                           }).AsNoTracking().Distinct().ToListAsync();

                    #region MasterRequestForQuotation
                    List<MasterRequestForQuotation> masterRequestForQuotation = new List<MasterRequestForQuotation>();

                    foreach (var resVendor in getVendor)
                    {
                        MasterRequestForQuotation rfq = new MasterRequestForQuotation
                        {
                            Prid = data.Prid,
                            Rfqnumber = await _context.MasterRequestForQuotation.CountAsync() > 0 ? data.Rfqnumber + "-" + string.Format(CultureInfo.InvariantCulture, "{0:0000000}", await _context.MasterPurchaseRequisition.MaxAsync(n => n.Id) + count) : data.Rfqnumber + "-" + string.Format(CultureInfo.InvariantCulture, "{0:0000000}", count),
                            MasterDataVendorId = resVendor.VendorId,
                            CompanyCode = data.CompanyCode,
                            MasterItemCategoryId = data.MasterItemCategoryId,
                            MasterItemId = data.MasterItemId,
                            Qty = data.Qty,
                            UnitPrice = 0,
                            TotalPrice = 0,
                            CurrencyId = 76,
                            DeliveryTime = 0,
                            Rfqstatus = PurchaseRequisitionStatus.PENDING.ToString(),
                            CreatedBy = data.CreatedBy,
                            CreatedDate = data.CreatedDate,
                            ModifyBy = data.ModifyBy,
                            ModifyDate = data.ModifyDate,
                            IsDelete = false
                        };
                        masterRequestForQuotation.Add(rfq);
                        count++;
                    }
                    await _context.MasterRequestForQuotation.AddRangeAsync(masterRequestForQuotation);
                    await _context.SaveChangesAsync();
                    #endregion

                    #region Purchase Requisition Item
                    List<MasterPurchaseRequisitionItem> purchaseRequisitionitem = new List<MasterPurchaseRequisitionItem>();
                    
                    var getPRItem = await (from pritem in _context.MasterPurchaseRequisitionItem
                                                         where pritem.MasterItemId == data.MasterItemId 
                                                            && pritem.ItemVendorStatus == PurchaseRequisitionStatus.REQUEST_VENDOR.ToString()
                                                         select pritem).AsNoTracking().ToListAsync();
                    
                    foreach (var res in getPRItem) 
                    {
                        var listPRItem = await _context.MasterPurchaseRequisitionItem.Where(n => n.Id == res.Id).FirstOrDefaultAsync();

                        listPRItem.ItemVendorStatus = PurchaseRequisitionStatus.RFQ_ON_PROCESS.ToString();
                        listPRItem.ModifyBy = data.ModifyBy;
                        listPRItem.ModifyDate = data.ModifyDate;

                        _context.Entry(listPRItem).State = EntityState.Modified;
                        await _context.SaveChangesAsync();
                    }
                    #endregion

                    #region Quotation Comparation Form
                    MasterQuotationComparationForm QCF = new MasterQuotationComparationForm
                    {
                        Prid = data.Prid,
                        Qcfnumber = await _context.MasterPurchaseRequisition.CountAsync() > 0 ? data.Qcfnumber + "-" + string.Format(CultureInfo.InvariantCulture, "{0:0000000}", await _context.MasterPurchaseRequisition.MaxAsync(n => n.Id) + 1) : data.Qcfnumber + "-" + string.Format(CultureInfo.InvariantCulture, "{0:0000000}", 1),
                        CompanyCode = data.CompanyCode,
                        MasterDataVendorId = 0,
                        MasterItemCategoryId = data.MasterItemCategoryId,
                        MasterItemId = data.MasterItemId,
                        Qty = data.Qty,
                        UnitPrice = 0,
                        TotalPrice = 0,
                        CurrencyId = 76,
                        Qcfstatus = PurchaseRequisitionStatus.PENDING.ToString(),
                        CreatedBy = data.CreatedBy,
                        CreatedDate = data.CreatedDate,
                        ModifyBy = data.ModifyBy,
                        ModifyDate = data.ModifyDate
                    };

                    await _context.MasterQuotationComparationForm.AddAsync(QCF);
                    await _context.SaveChangesAsync();
                    #endregion

                    response.Code = (int)HttpStatusCode.OK;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Message = "Save data success.!";

                    dbcxtransaction.Commit();
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }
        #endregion

        #region Request For Quotation
        public async Task<BaseResponse> GetRequestForQuotationPRId(int prId, int masterItemId)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var result = await (from headerPR in _context.MasterPurchaseRequisition
                                     join rfq in _context.MasterRequestForQuotation
                                         on headerPR.Id equals rfq.Prid
                                     join items in _context.MasterItems
                                         on rfq.MasterItemId equals items.ItemId
                                     join vendor in _context.MasterDataVendor
                                         on rfq.MasterDataVendorId equals vendor.Id
                                     where rfq.Prid == prId
                                        && rfq.MasterItemId == masterItemId
                                        && rfq.UnitPrice != 0
                                     orderby rfq.TotalPrice ascending
                                     select new
                                     {
                                         headerPR.Id,
                                         RFQId = rfq.Id,
                                         rfq.Rfqnumber,
                                         MasterDataVendorId = vendor.Id,
                                         VendorName = vendor.Name,
                                         items.ItemId,
                                         items.ItemName,
                                         rfq.Qty,
                                         rfq.TotalPrice
                                     }).AsNoTracking().ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetRequestForQuotationbyUserId(int userId)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var result = await (from rfq in _context.MasterRequestForQuotation
                                    join items in _context.MasterItems
                                        on rfq.MasterItemId equals items.ItemId
                                    join company in _context.ConfigCompany
                                        on rfq.CompanyCode equals company.CompanyCode
                                    join vendor in _context.MasterDataVendor
                                        on rfq.MasterDataVendorId equals vendor.Id
                                    join user in _context.MasterDataUser
                                        on vendor.MasterDataUserId equals user.Id
                                    where user.Id == userId &&
                                        rfq.Rfqstatus != PurchaseRequisitionStatus.COMPLETED.ToString()
                                    select new
                                    {
                                        rfq.Id,
                                        rfq.Rfqnumber,
                                        rfq.CompanyCode,
                                        company.CompanyName,
                                        items.ItemName,
                                        rfq.Qty,
                                        rfq.Rfqstatus
                                    }).AsNoTracking().ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetRequestForQuotationDetailByRfqId(int rfqId)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var result = await (from rfq in _context.MasterRequestForQuotation
                                    join prHeader in _context.MasterPurchaseRequisition
                                       on rfq.Prid equals prHeader.Id
                                    join prItem in _context.MasterPurchaseRequisitionItem
                                    on prHeader.Id equals prItem.MasterPurchaseRequisitionId
                                    join items in _context.MasterItems
                                        on rfq.MasterItemId equals items.ItemId
                                    join company in _context.ConfigCompany
                                        on rfq.CompanyCode equals company.CompanyCode
                                    join vendor in _context.MasterDataVendor
                                        on rfq.MasterDataVendorId equals vendor.Id
                                    join user in _context.MasterDataUser
                                        on vendor.MasterDataUserId equals user.Id
                                    where rfq.Id == rfqId
                                    select new
                                    {
                                        rfq.Id,
                                        rfq.Prid,
                                        rfq.Rfqnumber,
                                        company.CompanyName,
                                        CompanyAddress = company.Address,
                                        company.Npwp,
                                        prItem.Specification,
                                        prItem.PurposeDesc,
                                        vendor.MasterDataUserId,
                                        rfq.MasterDataVendorId,
                                        rfq.MasterItemCategoryId,
                                        rfq.MasterItemId,
                                        items.ItemName,
                                        ItemCode = items.BimsCode,
                                        rfq.Qty,
                                        rfq.UnitPrice,
                                        rfq.TotalPrice,
                                        UnitPriceString = string.Format("{0:N2}", rfq.UnitPrice),
                                        TotalPriceString = string.Format("{0:N2}", rfq.TotalPrice),
                                        rfq.PriceValidDate,
                                        rfq.DeliveryTime,
                                        rfq.Rfqstatus,
                                        rfq.CreatedDate,
                                        rfq.Notes
                                    }).FirstOrDefaultAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> ProcessRequestForQuotation(RequestForQuotationModel data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var listRFQ = await _context.MasterRequestForQuotation.Where(n => n.Id == data.Id).FirstOrDefaultAsync();

                    listRFQ.UnitPrice = data.UnitPrice;
                    listRFQ.PriceValidDate = data.PriceValidDate;
                    listRFQ.DeliveryTime = data.DeliveryTime;
                    listRFQ.TotalPrice = data.TotalPrice;
                    listRFQ.Notes = data.Notes;
                    listRFQ.Rfqstatus = PurchaseRequisitionStatus.RFQ_ON_PROCESS.ToString();
                    listRFQ.ModifyBy = data.ModifyBy;
                    listRFQ.ModifyDate = DateTime.Now;

                    _context.Entry(listRFQ).State = EntityState.Modified;

                    await _context.SaveChangesAsync();

                    dbcxtransaction.Commit();

                    response.Code = (int)HttpStatusCode.OK;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Message = "Update Request For Quotation success.!";
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }
            return response;
        }
        #endregion

        #region Quotation Comparation Form
        public async Task<BaseResponse> getQuotationComparationFormByPrId(int prid, int masterItemId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from qcf in _context.MasterQuotationComparationForm
                                    where qcf.Prid == prid
                                        && qcf.MasterItemId == masterItemId
                                    select qcf).FirstOrDefaultAsync();

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;

        }
        public async Task<BaseResponse> QuotationComparationFormHeader(int prId, int masterItemId)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var result = await (from headerPR in _context.MasterPurchaseRequisition
                                    join qcf in _context.MasterQuotationComparationForm
                                        on headerPR.Id equals qcf.Prid
                                    join rfq in _context.MasterRequestForQuotation
                                         on headerPR.Id equals rfq.Prid
                                    join items in _context.MasterItems
                                        on rfq.MasterItemId equals items.ItemId
                                    join company in _context.ConfigCompany
                                        on rfq.CompanyCode equals company.CompanyCode
                                    where headerPR.Id == prId
                                        && qcf.MasterItemId == masterItemId
                                    select new
                                    {
                                        rfq.Prid,
                                        qcf.Qcfnumber,
                                        qcf.MasterDataVendorId,
                                        QCFDate = qcf.CreatedDate,
                                        headerPR.Prnumber,
                                        items.ItemName,
                                        PRDate = headerPR.CreatedDate,
                                        rfq.Qty,
                                        company.CompanyName
                                    }).FirstOrDefaultAsync();

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> QuotationComparationFormItem(int prId, int masterItemId)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var getData = await (from headerPR in _context.MasterPurchaseRequisition
                                    join rfq in _context.MasterRequestForQuotation
                                        on headerPR.Id equals rfq.Prid
                                    join items in _context.MasterItems
                                        on rfq.MasterItemId equals items.ItemId
                                    join vendor in _context.MasterDataVendor
                                        on rfq.MasterDataVendorId equals vendor.Id
                                    join user in _context.MasterDataUser
                                        on vendor.MasterDataUserId equals user.Id
                                    join vendorTax in _context.MappingDataVendorTax
                                        on user.Id equals vendorTax.MasterDataUserId
                                    where rfq.Prid == prId
                                        && rfq.MasterItemId == masterItemId
                                        && rfq.UnitPrice != 0
                                    orderby rfq.TotalPrice ascending
                                    select new
                                    {
                                        rfq.Prid,
                                        rfq.Rfqnumber,
                                        MasterDataVendorId = vendor.Id,
                                        VendorName = vendor.Name,
                                        vendor.Pic,
                                        vendor.Picphone,
                                        vendorTax.Ppn,
                                        items.ItemId,
                                        items.ItemName,
                                        rfq.Qty,
                                        rfq.UnitPrice,
                                        rfq.TotalPrice,
                                        rfq.DeliveryTime
                                    }).AsNoTracking().ToListAsync();

                var result = getData
                    .Take(3);

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> ProcessQuotationComparationForm(int PrId, int masterDataVendorId, string notes, string modifyBy)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var getRFQVendor = await (from rfq in _context.MasterRequestForQuotation
                                                join vendor in _context.MasterDataVendor
                                                    on rfq.MasterDataVendorId equals vendor.Id
                                                join user in _context.MasterDataUser
                                                    on vendor.MasterDataUserId equals user.Id
                                                join vendorTax in _context.MappingDataVendorTax
                                                    on user.Id equals vendorTax.MasterDataUserId
                                                where rfq.Prid == PrId && rfq.MasterDataVendorId == masterDataVendorId
                                                select new
                                                {
                                                    rfq.MasterItemCategoryId,
                                                    rfq.MasterItemId,
                                                    rfq.Qty,
                                                    rfq.CurrencyId,
                                                    rfq.UnitPrice,
                                                    rfq.TotalPrice,
                                                    vendorTax.Ppn
                                                }).FirstOrDefaultAsync();

                    #region Request For Quotation
                    var listRFQ = await _context.MasterRequestForQuotation.Where(n => n.Prid == PrId && n.MasterItemId == getRFQVendor.MasterItemId).FirstOrDefaultAsync();
                    listRFQ.Rfqstatus = PurchaseRequisitionStatus.CLOSED.ToString();
                    listRFQ.ModifyBy = modifyBy;
                    listRFQ.ModifyDate = DateTime.Now;

                    _context.Entry(listRFQ).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                    #endregion

                    #region Quotation Comparation Form
                    var listQCF = await _context.MasterQuotationComparationForm.Where(n => n.Prid == PrId && n.MasterItemId == getRFQVendor.MasterItemId).FirstOrDefaultAsync();

                    listQCF.MasterDataVendorId = masterDataVendorId;
                    listQCF.UnitPrice = getRFQVendor.UnitPrice;
                    listQCF.TotalPrice = getRFQVendor.TotalPrice;
                    listQCF.Notes = notes;
                    listQCF.Qcfstatus = PurchaseRequisitionStatus.COMPLETED.ToString();
                    listQCF.ModifyBy = modifyBy;
                    listQCF.ModifyDate = DateTime.Now;

                    _context.Entry(listQCF).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                    #endregion

                    #region Vendor Item Price
                    MappingVendorItemPrice vendorItemPrice = new MappingVendorItemPrice
                    {
                        MasterDataVendorId = masterDataVendorId,
                        MasterDataItemId = getRFQVendor.MasterItemId,
                        MasterDataCurrencyId = getRFQVendor.CurrencyId,
                        Price = getRFQVendor.UnitPrice,
                        PriceValidDate = DateTime.Now,
                        CreatedBy = modifyBy,
                        CreateDate = DateTime.Now,
                        ModifiedBy = modifyBy,
                        ModifiedDate = DateTime.Now,
                        IsDelete = false
                    };

                    await _context.MappingVendorItemPrice.AddAsync(vendorItemPrice);
                    await _context.SaveChangesAsync();
                    #endregion

                    #region MasterItem
                    var items = await _context.MasterItems.Where(n => n.ItemId == getRFQVendor.MasterItemId).FirstOrDefaultAsync();

                    items.UnitPrice = getRFQVendor.UnitPrice;
                    items.ModifyBy = modifyBy;
                    items.ModifiedDate = DateTime.Now;

                    _context.Entry(items).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                    #endregion

                    #region Purchase Requisition Item
                    List<MasterPurchaseRequisitionItem> purchaseRequisitionitem = new List<MasterPurchaseRequisitionItem>();

                    var getPRItem = await (from pritem in _context.MasterPurchaseRequisitionItem
                                           where pritem.MasterItemId == getRFQVendor.MasterItemId
                                              && pritem.ItemVendorStatus != PurchaseRequisitionStatus.VENDOR_IS_EXIST.ToString()
                                           select pritem).AsNoTracking().ToListAsync();

                    foreach (var res in getPRItem)
                    {
                        var listPRItem = await _context.MasterPurchaseRequisitionItem.Where(n => n.Id == res.Id).FirstOrDefaultAsync();

                        listPRItem.ItemVendorStatus = PurchaseRequisitionStatus.VENDOR_IS_EXIST.ToString();
                        
                        listPRItem.UnitPrice = getRFQVendor.UnitPrice;
                        listPRItem.TotalPrice = getRFQVendor.UnitPrice * listPRItem.Qty;
                        listPRItem.Ppn = getRFQVendor.Ppn;

                        listPRItem.ModifyBy = modifyBy;
                        listPRItem.ModifyDate = DateTime.Now;

                        _context.Entry(listPRItem).State = EntityState.Modified;
                        await _context.SaveChangesAsync();
                    }
                    #endregion

                    #region Generate PO
                    decimal? ppnPrice;
                    double grandTotal = 0;

                    List<MasterPurchaseRequisition> purchaserequisition = new List<MasterPurchaseRequisition>();

                    foreach (var pritems in getPRItem)
                    {
                        var getDataPurchaseRequisition = await (from prHeader in _context.MasterPurchaseRequisition
                                                                join prItem in _context.MasterPurchaseRequisitionItem
                                                                    on prHeader.Id equals prItem.MasterPurchaseRequisitionId
                                                                where prHeader.Id == pritems.MasterPurchaseRequisitionId
                                                                select prHeader).FirstOrDefaultAsync();

                        var getPonumber = "PO-" + DateTime.Now.Year + "-" + getDataPurchaseRequisition.CompanyCode + "-" + getDataPurchaseRequisition.DepartementCode;

                        ppnPrice = pritems.TotalPrice * getRFQVendor.Ppn / 100;

                        var double_ppn = Convert.ToDouble(ppnPrice);

                        var calc = Convert.ToDouble(pritems.TotalPrice) + double_ppn;

                        grandTotal += calc;

                        MasterPurchaseOrder purchaseOrder = new MasterPurchaseOrder
                        {
                            Ponumber = await _context.MasterPurchaseOrder.CountAsync() > 0 ? getPonumber + "-" + string.Format(CultureInfo.InvariantCulture, "{0:0000000}", await _context.MasterPurchaseOrder.MaxAsync(n => n.Id) + 1) : getPonumber + "-" + string.Format(CultureInfo.InvariantCulture, "{0:0000000}", 1),
                            MasterPrid = getDataPurchaseRequisition.Id,
                            MasterDataVendorId = masterDataVendorId,
                            Postatus = "WAITING",
                            GrandTotal = getDataPurchaseRequisition.GrandTotal,
                            CreatedBy = modifyBy,
                            CreatedDate = DateTime.Now,
                            ModifiedBy = modifyBy,
                            ModifyDate = DateTime.Now,
                            IsDelete = false
                        };
                        purchaseOrder.GrandTotalAfterTax = Convert.ToDecimal(grandTotal);

                        await _context.MasterPurchaseOrder.AddAsync(purchaseOrder);
                        await _context.SaveChangesAsync();

                        MasterPurchaseOrderItem orderItem = new MasterPurchaseOrderItem()
                        {
                            Specification = pritems.Specification,
                            PurposeDesc = pritems.PurposeDesc,
                            Qty = pritems.Qty,
                            Uomid = pritems.Uomid,
                            CurrencyId = pritems.CurrencyId,
                            UnitPrice = getRFQVendor.UnitPrice,
                            TotalPrice = getRFQVendor.TotalPrice,
                            CreatedDate = DateTime.Now,
                            CreatedBy = modifyBy,
                            ModifyDate = DateTime.Now,
                            ModifyBy = modifyBy,
                            MasterItemId = pritems.MasterItemId,
                            MasterPurchaseOrderId = purchaseOrder.Id,
                            Ppn = getRFQVendor.Ppn
                        };
                        await _context.MasterPurchaseOrderItem.AddRangeAsync(orderItem);
                        await _context.SaveChangesAsync();
                    }                   
                    #endregion

                    response.Code = (int)HttpStatusCode.OK;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Message = "Update Quotation Comparation Form success.";

                    dbcxtransaction.Commit();
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }
            return response;
        }
        #endregion

        #region Purchase Order
        public async Task<BaseResponse> GetPurchaseOrderById(int id, int userId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from poHeader in _context.MasterPurchaseOrder
                                    join prheader in _context.MasterPurchaseRequisition
                                        on poHeader.MasterPrid equals prheader.Id
                                    join vendor in _context.MasterDataVendor
                                        on poHeader.MasterDataVendorId equals vendor.Id
                                    join company in _context.ConfigCompany
                                        on prheader.CompanyCode equals company.CompanyCode
                                    where poHeader.Id == id && vendor.MasterDataUserId == userId
                                    select new
                                    {
                                        poHeader.Id,
                                        CompanyName = company.CompanyName,
                                        CompanyAddress = company.Address,
                                        Npwp = company.Npwp,
                                        PONumber = poHeader.Ponumber,
                                        CreatedDate = poHeader.CreatedDate,
                                        PaymentStatus = poHeader.PaymentStatus,
                                        POStatus = poHeader.Postatus,
                                        GrandTotal = (from prItem in _context.MasterPurchaseRequisitionItem
                                                      join vendorItem in _context.MappingVendorItemPrice
                                                          on prItem.MasterItemId equals vendorItem.MasterDataItemId
                                                      where prItem.MasterPurchaseRequisitionId == prheader.Id && vendorItem.MasterDataVendorId == vendor.Id
                                                      select new
                                                      {
                                                          prItem.TotalPrice
                                                      }).Sum(n => n.TotalPrice),
                                        ListItem = (from poItem in _context.MasterPurchaseOrderItem
                                                    join items in _context.MasterItems
                                                        on poItem.MasterItemId equals items.ItemId
                                                    join curr in _context.MasterDataCurrency
                                                        on poItem.CurrencyId equals curr.Id
                                                    join vendorItem in _context.MappingVendorItemPrice
                                                        on poItem.MasterItemId equals vendorItem.MasterDataItemId
                                                    where poItem.MasterPurchaseOrderId == id && vendorItem.MasterDataVendorId == vendor.Id
                                                    select new
                                                    {
                                                        ItemCode = items.ItemCode,
                                                        ItemName = items.ItemName,
                                                        Specification = poItem.Specification.Replace("{", "").Replace("}", "").Replace("\"", ""),
                                                        PurposeDesc = poItem.PurposeDesc,
                                                        Qty = poItem.Qty,
                                                        Uom = poItem.Uomid,
                                                        Currency = curr.Code,
                                                        UnitPrice = poItem.UnitPrice,
                                                        TotalPrice = poItem.TotalPrice
                                                    }).ToList()
                                    }).FirstOrDefaultAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetPurchaseOrderByVendorId(int userId)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var result = await (from headerPO in _context.MasterPurchaseOrder
                                    join headerPR in _context.MasterPurchaseRequisition
                                        on headerPO.MasterPrid equals headerPR.Id
                                    join company in _context.ConfigCompany
                                        on headerPR.CompanyCode equals company.CompanyCode
                                    join vendor in _context.MasterDataVendor
                                        on headerPO.MasterDataVendorId equals vendor.Id
                                    where vendor.MasterDataUserId == userId
                                    orderby headerPO.ModifyDate descending
                                    select new
                                    {
                                        headerPO.Id,
                                        PONumber = headerPO.Ponumber,
                                        PODate = string.Format("{0:dd-MM-yyyy}", headerPO.CreatedDate),
                                        company.CompanyName,
                                        Currency = (from currency in _context.MasterDataCurrency
                                                    join itemPo in _context.MasterPurchaseOrderItem
                                                        on currency.Id equals itemPo.CurrencyId
                                                    where itemPo.MasterPurchaseOrderId == headerPO.Id
                                                    select currency.Code).FirstOrDefault(),
                                        PoStatus = headerPO.Postatus,
                                        GrandTotal = (from prItem in _context.MasterPurchaseRequisitionItem
                                                      join vendorItem in _context.MappingVendorItemPrice
                                                          on prItem.MasterItemId equals vendorItem.MasterDataItemId
                                                      where prItem.MasterPurchaseRequisitionId == headerPR.Id && vendorItem.MasterDataVendorId == vendor.Id
                                                      select new
                                                      {
                                                          prItem.TotalPrice
                                                      }).Sum(n => n.TotalPrice),
                                        PPN = (from ppn in _context.MappingDataVendorTax
                                               where ppn.MasterDataUserId == userId
                                               select ppn.Ppn).FirstOrDefault()
                                    }).AsNoTracking().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetPurchaseOrder()
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var result = await (from headerPO in _context.MasterPurchaseOrder
                                    join headerPR in _context.MasterPurchaseRequisition
                                        on headerPO.MasterPrid equals headerPR.Id
                                    join company in _context.ConfigCompany
                                        on headerPR.CompanyCode equals company.CompanyCode
                                    join itemPO in _context.MasterPurchaseOrderItem
                                        on headerPO.Id equals itemPO.MasterPurchaseOrderId
                                    join currency in _context.MasterDataCurrency
                                        on itemPO.CurrencyId equals currency.Id
                                    orderby headerPO.ModifyDate descending
                                    select new
                                    {
                                        headerPO.Id,
                                        PONumber = headerPO.Ponumber,
                                        company.CompanyName,
                                        Currency = currency.Code,
                                        headerPO.GrandTotal,
                                        PoStatus = headerPO.Postatus,
                                        headerPO.CreatedDate
                                    }).AsNoTracking().Distinct().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> ProcessPurchaseOrder(int id, string modifyBy)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var listPurchaseOrder = await _context.MasterPurchaseOrder.Where(n => n.Id == id).FirstOrDefaultAsync();

                    listPurchaseOrder.Postatus = "PROCESS";
                    listPurchaseOrder.ModifiedBy = modifyBy;
                    listPurchaseOrder.ModifyDate = DateTime.Now;

                    _context.Entry(listPurchaseOrder).State = EntityState.Modified;

                    await _context.SaveChangesAsync();

                    dbcxtransaction.Commit();

                    response.Code = (int)HttpStatusCode.OK;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Message = "Update Purchase Order success.";
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> RejectPurchaseOrder(int id, string modifyBy)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var listPurchaseOrder = await _context.MasterPurchaseOrder.Where(n => n.Id == id).FirstOrDefaultAsync();

                    listPurchaseOrder.Postatus = "CANCEL";
                    listPurchaseOrder.ModifiedBy = modifyBy;
                    listPurchaseOrder.ModifyDate = DateTime.Now;

                    _context.Entry(listPurchaseOrder).State = EntityState.Modified;

                    await _context.SaveChangesAsync();
                    dbcxtransaction.Commit();

                    var getPurchaseOrderbyPRId = await _context.MasterPurchaseOrder.Where(n => n.MasterPrid == listPurchaseOrder.MasterPrid).AsNoTracking().ToListAsync();

                    var checkStatus = getPurchaseOrderbyPRId.All(a => a.Postatus == "CANCEL");

                    if (checkStatus == true)
                    {
                        var PurchaseRequisition = await _context.MasterPurchaseRequisition.FirstOrDefaultAsync(n => n.Id == listPurchaseOrder.MasterPrid);
                        PurchaseRequisition.Prstatus = PurchaseRequisitionStatus.REJECTED.ToString();
                        PurchaseRequisition.ModifiedBy = "System";
                        PurchaseRequisition.ModifyDate = DateTime.Now;

                        _context.Entry(PurchaseRequisition).State = EntityState.Modified;

                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();
                    }

                    response.Code = (int)HttpStatusCode.OK;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Message = "Update Purchase Order success.";
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> GetPurchaseOrderItemById(int id/*, int masterDataVendorId*/)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from poHeader in _context.MasterPurchaseOrder
                                    join poItem in _context.MasterPurchaseOrderItem
                                        on poHeader.Id equals poItem.MasterPurchaseOrderId
                                    join prheader in _context.MasterPurchaseRequisition
                                        on poHeader.MasterPrid equals prheader.Id
                                    join company in _context.ConfigCompany
                                        on prheader.CompanyCode equals company.CompanyCode
                                    join departement in _context.ConfigDivision
                                        on prheader.DepartementCode equals departement.DivisionCode
                                    where poHeader.Id == id
                                    select new
                                    {
                                        poHeader.Id,
                                        company.CompanyName,
                                        CompanyAddress = company.Address,
                                        company.Npwp,
                                        PONumber = poHeader.Ponumber,
                                        DepartementName = departement.Description,
                                        poHeader.CreatedDate,
                                        poHeader.PaymentStatus,
                                        POStatus = poHeader.Postatus,
                                        poHeader.GrandTotal,
                                        poHeader.GrandTotalAfterTax,
                                        ListItem = (from poitem in _context.MasterPurchaseOrderItem
                                                    join poheader in _context.MasterPurchaseOrder
                                                        on poitem.MasterPurchaseOrderId equals poheader.Id
                                                    join items in _context.MasterItems
                                                        on poitem.MasterItemId equals items.ItemId
                                                    join uom in _context.MasterUnitOfMeasure
                                                        on poitem.Uomid equals uom.Uomid
                                                    join curr in _context.MasterDataCurrency
                                                        on poitem.CurrencyId equals curr.Id
                                                    where poitem.MasterPurchaseOrderId == id
                                                    select new
                                                    {
                                                        poHeader.Id,
                                                        items.ItemCode,
                                                        items.ItemName,
                                                        Specification = poitem.Specification.Replace("{", "").Replace("}", "").Replace("\"", ""),
                                                        poitem.PurposeDesc,
                                                        poitem.Qty,
                                                        UomName = uom.Uomname,
                                                        Currency = curr.Code,
                                                        PPN = poitem.Ppn,
                                                        poitem.UnitPrice,
                                                        poitem.TotalPrice,
                                                        UnitPriceString = string.Format("{0:N2}", poitem.UnitPrice),
                                                        TotalPriceString = string.Format("{0:N2}", poitem.TotalPrice)
                                                    }).Distinct() //.AsNoTracking().Distinct().ToListAsync();
                                    }).FirstOrDefaultAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> getPurchaseOrderItemByVendorId(int id)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from vendor in _context.MasterDataVendor
                                    join user in _context.MasterDataUser
                                        on vendor.MasterDataUserId equals user.Id
                                    join vendorTax in _context.MappingDataVendorTax
                                        on user.Id equals vendorTax.MasterDataUserId
                                    join vendorcategories in _context.MappingDataVendorCategories
                                        on vendor.Id equals vendorcategories.MasterDataVendorId
                                    join itemcategory in _context.MasterItemCategories
                                        on vendorcategories.CategoriesId equals itemcategory.CategoryId
                                    join items in _context.MasterItems
                                        on itemcategory.CategoryId equals items.MasterItemCategoryId
                                    join poitem in _context.MasterPurchaseOrderItem
                                        on items.ItemId equals poitem.MasterItemId
                                    join uom in _context.MasterUnitOfMeasure
                                        on poitem.Uomid equals uom.Uomid
                                    join curr in _context.MasterDataCurrency
                                        on poitem.CurrencyId equals curr.Id
                                    where poitem.MasterPurchaseOrderId == id
                                    select new
                                    {
                                        VendorName = vendor.Name,
                                        ItemCode = items.ItemCode,
                                        ItemName = items.ItemName,
                                        Specification = poitem.Specification.Replace("{", "").Replace("}", "").Replace("\"", ""),
                                        PurposeDesc = poitem.PurposeDesc,
                                        Qty = poitem.Qty,
                                        UomName = uom.Uomname,
                                        Currency = curr.Code,
                                        PPN = vendorTax.Ppn,
                                        UnitPrice = poitem.UnitPrice,
                                        TotalPrice = poitem.TotalPrice,
                                        UnitPriceString = string.Format("{0:N2}", poitem.UnitPrice),
                                        TotalPriceString = string.Format("{0:N2}", poitem.TotalPrice)
                                    }).AsNoTracking().Distinct().ToListAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        #endregion
    }
}