﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;

namespace WebPortal.Repository.Concrete
{
    public class VendorRepository : IVendorRepository
    {
        readonly WebPortalContext _context;
        public VendorRepository(WebPortalContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> InsertVendor(MasterDataVendor data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    if (_context.MasterDataVendor.Where(n => n.MasterDataUserId == data.MasterDataUserId).Count() == 0)
                    {
                        MasterDataVendor vendor = new MasterDataVendor
                        {
                            Name = data.Name,
                            MasterDataUserId = data.MasterDataUserId,
                            MasterDataBusinessTypeId = data.MasterDataBusinessTypeId,
                            MasterDataCountryId = data.MasterDataCountryId,
                            MasterDataRegionId = data.MasterDataRegionId,
                            MasterDataCityId = data.MasterDataCityId,
                            Address = data.Address,
                            PostalCode = data.PostalCode,
                            CompanyEmail = data.CompanyEmail,
                            Pic = data.Pic,
                            Picemail = data.Picemail,
                            IdCardNumber = data.IdCardNumber,
                            IntegrityPact = data.IntegrityPact,
                            MasterAgreement = data.MasterAgreement,
                            Logo = data.Logo,
                            CreatedDate = DateTime.Now,
                            CreatedBy = data.Name,
                            ModifiedDate = DateTime.Now,
                            ModifiyBy = data.Name,
                            IsDelete = data.IsDelete
                        };

                        await _context.MasterDataVendor.AddAsync(vendor);
                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response = ResponseConstant.SAVE_SUCCESS;
                    }
                    else
                    {
                        response.Code = (int)HttpStatusCode.BadRequest;
                        response.Status = HttpStatusCode.BadRequest.ToString();
                        response.Message = "Email already exist.";

                        dbcxtransaction.Rollback();
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> InsertVendorBasicInfo(BasicInfoModel data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    if (_context.MasterDataVendor.Where(n => n.MasterDataUserId == data.MasterDataUserId).Count() == 0)
                    {
                        MasterDataVendor vendor = new MasterDataVendor
                        {
                            Name = data.Name,
                            MasterDataUserId = data.MasterDataUserId,
                            MasterDataBusinessTypeId = data.MasterDataBusinessTypeId,
                            MasterDataCountryId = data.MasterDataCountryId,
                            MasterDataRegionId = data.MasterDataRegionId,
                            MasterDataCityId = data.MasterDataCityId,
                            Address = data.Address,
                            MailingAddress = data.MailingAddress,
                            PostalCode = data.PostalCode,
                            CompanyEmail = data.CompanyEmail,
                            CompanyPhone = data.CompanyPhone,
                            Pic = data.Pic,
                            Picemail = data.Picemail,
                            IdCardNumber = data.IdCardNumber,
                            Picphone = data.Picphone,
                            IntegrityPact = data.IntegrityPact != null ? data.IntegrityPact : false,
                            MasterAgreement = data.MasterAgreement != null ? data.MasterAgreement : false,
                            Logo = data.Logo,
                            CreatedDate = DateTime.Now,
                            CreatedBy = "VENDOR",
                            ModifiedDate = DateTime.Now,
                            ModifiyBy = "VENDOR",
                            IsDelete = data.IsDelete
                        };

                        await _context.MasterDataVendor.AddAsync(vendor);
                        await _context.SaveChangesAsync();

                        // mapping master data vendor with businessfield
                        for (var i = 0; i < data.MasterBusinessFieldsIdArray.Count; i++)
                        {
                            MappingDataVendorBusinessFields mvbf = new MappingDataVendorBusinessFields
                            {
                                MasterDataVendorId = vendor.Id,
                                MasterDataBusinessFieldId = data.MasterBusinessFieldsIdArray[i],
                                CreatedDate = DateTime.Now,
                                CreatedBy = "VENDOR",
                                ModifiedDate = DateTime.Now,
                                ModifyBy = "VENDOR"
                            };

                            await _context.MappingDataVendorBusinessFields.AddAsync(mvbf);
                            await _context.SaveChangesAsync();
                        }

                        dbcxtransaction.Commit();

                        response.Code = (int)HttpStatusCode.OK;
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Message = "Save Data Success";
                        response.Data = vendor.Id;

                    }
                    else if (_context.MasterDataVendor.Where(n => n.MasterDataUserId == data.MasterDataUserId).Count() != 0)
                    {
                        var vendor = await _context.MasterDataVendor.FirstOrDefaultAsync(n => n.MasterDataUserId == data.MasterDataUserId);

                        vendor.Name = data.Name;
                        vendor.MasterDataUserId = data.MasterDataUserId;
                        vendor.MasterDataBusinessTypeId = data.MasterDataBusinessTypeId;
                        vendor.MasterDataCountryId = data.MasterDataCountryId;
                        vendor.MasterDataRegionId = data.MasterDataRegionId;
                        vendor.MasterDataCityId = data.MasterDataCityId;
                        vendor.Address = data.Address;
                        vendor.MailingAddress = data.MailingAddress;
                        vendor.PostalCode = data.PostalCode;
                        vendor.CompanyEmail = data.CompanyEmail;
                        vendor.CompanyPhone = data.CompanyPhone;
                        vendor.Pic = data.Pic;
                        vendor.Picemail = data.Picemail;
                        vendor.IdCardNumber = data.IdCardNumber;
                        vendor.Picphone = data.Picphone;
                        vendor.Logo = data.Logo;
                        vendor.ModifiedDate = DateTime.Now;
                        vendor.ModifiyBy = "VENDOR";

                        _context.Entry(vendor).State = EntityState.Modified;

                        await _context.SaveChangesAsync();

                        // remove before edit
                        var businessFields = await _context.MappingDataVendorBusinessFields.Where(n => n.MasterDataVendorId == vendor.Id).ToListAsync();
                        _context.MappingDataVendorBusinessFields.RemoveRange(businessFields);

                        await _context.SaveChangesAsync();

                        // mapping master data vendor with businessfield
                        for (var i = 0; i < data.MasterBusinessFieldsIdArray.Count; i++)
                        {
                            MappingDataVendorBusinessFields mvbf = new MappingDataVendorBusinessFields
                            {
                                MasterDataVendorId = vendor.Id,
                                MasterDataBusinessFieldId = data.MasterBusinessFieldsIdArray[i],
                                CreatedDate = DateTime.Now,
                                CreatedBy = "VENDOR",
                                ModifiedDate = DateTime.Now,
                                ModifyBy = "VENDOR"
                            };

                            await _context.MappingDataVendorBusinessFields.AddAsync(mvbf);
                            await _context.SaveChangesAsync();
                        }

                        dbcxtransaction.Commit();

                        response.Code = (int)HttpStatusCode.OK;
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Message = "Update data success.";
                        response.Data = vendor.Id;
                    }
                    else
                    {
                        response.Code = (int)HttpStatusCode.BadRequest;
                        response.Status = HttpStatusCode.BadRequest.ToString();
                        response.Message = "Email already exist.";

                        dbcxtransaction.Rollback();
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> GetVendorByUserId(int userId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from vendor in _context.MasterDataVendor
                                    join user in _context.MasterDataUser
                                    on vendor.MasterDataUserId equals user.Id
                                    where vendor.MasterDataUserId == userId
                                    select new
                                    {
                                        vendor.Id,
                                        vendor.MasterDataUserId,
                                        vendor.MasterDataBusinessTypeId,
                                        BusinessTypeName = vendor.MasterDataBusinessType.Name,
                                        vendor.Name,
                                        vendor.Address,
                                        vendor.MailingAddress,
                                        vendor.MasterDataCountryId,
                                        Country = vendor.MasterDataCountry.Name,
                                        vendor.MasterDataRegionId,
                                        Region = vendor.MasterDataRegion.Name,
                                        vendor.MasterDataCityId,
                                        City = vendor.MasterDataCity.Name,
                                        vendor.PostalCode,
                                        vendor.CompanyEmail,
                                        vendor.CompanyPhone,
                                        vendor.Pic,
                                        vendor.Picemail,
                                        vendor.IdCardNumber,
                                        vendor.Picphone,
                                        vendor.IntegrityPact,
                                        vendor.MasterAgreement,
                                        vendor.Status,
                                        user.RegistrationNumber,
                                        MasterBusinessFieldsIdArray = (from mvbf in _context.MappingDataVendorBusinessFields
                                                                       where mvbf.MasterDataVendorId == vendor.Id
                                                                       select mvbf.MasterDataBusinessFieldId).ToList(),
                                        BusinessFields = (from bf in _context.MappingDataVendorBusinessFields
                                                          where bf.MasterDataVendorId == vendor.Id
                                                          select new
                                                          {
                                                              bf.MasterDataBusinessField.Kbli,
                                                              bf.MasterDataBusinessField.Name
                                                          })
                                    }).FirstOrDefaultAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetAllVendor()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from vendor in _context.MasterDataVendor
                                        orderby vendor.CreatedDate descending
                                    select new
                                    {
                                        vendor.Id,
                                        vendor.MasterDataUserId,
                                        BusinessTypeName = vendor.MasterDataBusinessType.Name,
                                        vendor.Name,
                                        vendor.Address,
                                        vendor.CompanyPhone,
                                        vendor.Status,
                                        vendor.CreatedDate, 
                                        BusinessFields = (from bf in _context.MappingDataVendorBusinessFields
                                                          where bf.MasterDataVendorId == vendor.Id
                                                          select new
                                                          {
                                                              bf.MasterDataBusinessField.Name
                                                          })
                                    }).AsNoTracking().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetVendorBasicInfo(int masterDataUserId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from basicInfo in _context.MasterDataVendor
                                    where basicInfo.MasterDataUserId == masterDataUserId
                                    select new
                                    {
                                        BusinessType = basicInfo.MasterDataBusinessType.Name,
                                        CompanyName = basicInfo.Name,
                                        BusinessFields = (from bf in _context.MappingDataVendorBusinessFields
                                                          where bf.MasterDataVendorId == basicInfo.Id
                                                          select new
                                                          {
                                                              bf.MasterDataBusinessField.Name
                                                          }),
                                        basicInfo.Address,
                                        basicInfo.MailingAddress,
                                        Country = basicInfo.MasterDataCountry.Name,
                                        Province = basicInfo.MasterDataRegion.Name,
                                        City = basicInfo.MasterDataCity.Name,
                                        basicInfo.PostalCode,
                                        basicInfo.CompanyEmail,
                                        basicInfo.CompanyPhone,
                                        basicInfo.Pic,
                                        basicInfo.IdCardNumber,
                                        basicInfo.Picemail,
                                        basicInfo.Picphone
                                    }).FirstOrDefaultAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetVendorDetail(int masterDataUserId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from basicInfo in _context.MasterDataVendor
                                    where basicInfo.MasterDataUserId == masterDataUserId
                                    select new
                                    {
                                        BusinessType = basicInfo.MasterDataBusinessType.Name,
                                        CompanyName = basicInfo.Name,
                                        BusinessFields = (from bf in _context.MappingDataVendorBusinessFields
                                                          where bf.MasterDataVendorId == basicInfo.Id
                                                          select new
                                                          {
                                                              bf.MasterDataBusinessField.Name
                                                          }),
                                        basicInfo.Address,
                                        basicInfo.MailingAddress,
                                        Country = basicInfo.MasterDataCountry.Name,
                                        Province = basicInfo.MasterDataRegion.Name,
                                        City = basicInfo.MasterDataCity.Name,
                                        basicInfo.PostalCode,
                                        basicInfo.CompanyEmail,
                                        basicInfo.CompanyPhone,
                                        basicInfo.Pic,
                                        basicInfo.IdCardNumber,
                                        basicInfo.Picemail,
                                        basicInfo.Picphone,
                                        BankAccount = (from ba in _context.MappingDataVendorCurrencyBank
                                                       where ba.MasterDataUserId == masterDataUserId && ba.Status == "Main"
                                                       select new
                                                       {
                                                           Currency = ba.MasterDataCurrency.Name,
                                                           BankName = ba.MasterDataBank.Name,
                                                           ba.Area,
                                                           ba.City,
                                                           ba.AccountNumber,
                                                           ba.AccountName
                                                       }).FirstOrDefault(),
                                        BankPayment = (from vendorPayment in _context.MappingDataVendorPayment
                                                       join topConfig in _context.MappingDataBusinessFieldTopconfig
                                                       on vendorPayment.MappingDataBusinessFieldTopConfigId equals topConfig.Id
                                                       join paymentConfig in _context.MappingDataBusinessFieldPaymentConfig
                                                       on vendorPayment.MappingDataBusinessFieldPaymentConfigId equals paymentConfig.Id
                                                       where vendorPayment.MasterDataUserId == masterDataUserId
                                                       select new
                                                       {
                                                           topConfig.MasterDataTop.Top,
                                                           paymentConfig.DownPayment,
                                                           paymentConfig.Progressive,
                                                           paymentConfig.FullPayment,
                                                           paymentConfig.Retention
                                                       }).FirstOrDefault()
                                    }).FirstOrDefaultAsync();
                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetVendorBankAccount(int masterDatauserId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from vendorBank in _context.MappingDataVendorCurrencyBank
                                    where vendorBank.MasterDataUserId == masterDatauserId
                                    && vendorBank.IsDelete == false
                                    select new
                                    {
                                        vendorBank.Id,
                                        CurrencyId = vendorBank.MasterDataCurrencyId,
                                        Currency = vendorBank.MasterDataCurrency.Code,
                                        BankId = vendorBank.MasterDataBankId,
                                        BankName = vendorBank.MasterDataBank.Name,
                                        Area = vendorBank.Area,
                                        vendorBank.City,
                                        vendorBank.AccountNumber,
                                        vendorBank.AccountName,
                                        vendorBank.Status,
                                        vendorBank.Attachment
                                    }).ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetVendorBankAccountPrimary(int masterDataUserId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from vendorBank in _context.MappingDataVendorCurrencyBank
                                    where vendorBank.MasterDataUserId == masterDataUserId
                                    select new
                                    {
                                        vendorBank.Id,
                                        Currency = vendorBank.MasterDataCurrency.Code,
                                        BankName = vendorBank.MasterDataBank.Name,
                                        vendorBank.Area,
                                        vendorBank.City,
                                        vendorBank.AccountNumber,
                                        vendorBank.AccountName,
                                        vendorBank.Status
                                    }).AsNoTracking().ToListAsync();

                if (result.Count > 1)
                {
                    if (result.Any(n => n.Status.ToLower() == "Main".ToLower()))
                    {
                        response.Data = result.FirstOrDefault(n => n.Status.ToLower() == "Main".ToLower());
                    }
                    else
                    {
                        response.Data = result.FirstOrDefault();
                    }

                    response.Status = HttpStatusCode.OK.ToString();
                    response.Code = (int)HttpStatusCode.OK;
                    response.Message = "Retrieve data success";
                }
                else if (result.Count == 1)
                {
                    response.Data = result.FirstOrDefault();
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Code = (int)HttpStatusCode.OK;
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Code = (int)HttpStatusCode.OK;
                    response.Data = result.FirstOrDefault();
                    response.Message = "No result";
                }
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> InsertVendorBankAccount(BankAccountModel data)
        {
            BaseResponse response = new BaseResponse();

            var result = await (from vBank in _context.MappingDataVendorCurrencyBank
                                where vBank.MasterDataUserId == data.MasterDataUserId
                                select new
                                {
                                    vBank.Id,
                                    Currency = vBank.MasterDataCurrency.Code,
                                    BankName = vBank.MasterDataBank.Name,
                                    vBank.Area,
                                    vBank.City,
                                    vBank.AccountNumber,
                                    vBank.AccountName,
                                    vBank.Status
                                }).ToListAsync();

            if (result.Count < 1)
            {
                data.Status = "Main";
            }

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {

                    MappingDataVendorCurrencyBank vendorBank = new MappingDataVendorCurrencyBank
                    {
                        MasterDataUserId = data.MasterDataUserId,
                        MasterDataCurrencyId = data.MasterDataCurrencyId,
                        MasterDataBankId = data.MasterDataBankId,
                        Area = data.Area,
                        City = data.City,
                        AccountNumber = data.AccountNumber,
                        Status = data.Status,
                        AccountName = data.AccountName,
                        CreatedDate = DateTime.Now,
                        CreatedBy = data.CreatedBy,
                        ModifiedDate = DateTime.Now,
                        ModifiyBy = data.ModifiyBy,
                        IsDelete = false,
                        Attachment = data.Attachment
                    };

                    await _context.MappingDataVendorCurrencyBank.AddAsync(vendorBank);
                    await _context.SaveChangesAsync();

                    dbcxtransaction.Commit();

                    response = ResponseConstant.SAVE_SUCCESS;
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> UpdateVendorBankAccount(MappingDataVendorCurrencyBank data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var vendorBankAccount = await _context.MappingDataVendorCurrencyBank.FirstOrDefaultAsync(n => n.Id == data.Id);

                    vendorBankAccount.MasterDataBankId = data.MasterDataBankId;
                    vendorBankAccount.Area = data.Area;
                    vendorBankAccount.MasterDataCurrencyId = data.MasterDataCurrencyId;
                    vendorBankAccount.City = data.City;
                    vendorBankAccount.AccountNumber = data.AccountNumber;
                    vendorBankAccount.AccountName = data.AccountName;
                    vendorBankAccount.ModifiyBy = data.ModifiyBy;
                    vendorBankAccount.ModifiedDate = DateTime.Now;
                    vendorBankAccount.Attachment = data.Attachment;

                    _context.Entry(vendorBankAccount).State = EntityState.Modified;

                    await _context.SaveChangesAsync();

                    dbcxtransaction.Commit();

                    response.Code = (int)HttpStatusCode.OK;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Message = "Update data success.";
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> UpdateStatusVendorBankAccount(int id, int masterDataUserId, string status, string modifyBy)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var listBankAccount = await _context.MappingDataVendorCurrencyBank.Where(n => n.MasterDataUserId == masterDataUserId).ToListAsync();

                    foreach (var bankAccount in listBankAccount)
                    {
                        if (bankAccount.Id == id)
                            bankAccount.Status = status;
                        else
                            bankAccount.Status = "Other";

                        bankAccount.ModifiyBy = modifyBy;
                        bankAccount.ModifiedDate = DateTime.Now;

                        _context.Entry(bankAccount).State = EntityState.Modified;
                        await _context.SaveChangesAsync();
                    }

                    dbcxtransaction.Commit();

                    response.Code = (int)HttpStatusCode.OK;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Message = "Update Status Bank Success.";
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> DeleteVendorBankAccount(int id)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var bankAccount = await _context.MappingDataVendorCurrencyBank.FindAsync(id);
                    bankAccount.IsDelete = true;

                    _context.Entry(bankAccount).State = EntityState.Modified;
                    await _context.SaveChangesAsync();

                    dbcxtransaction.Commit();

                    response.Code = (int)HttpStatusCode.OK;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Message = "Delete Bank Success.";
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> GetVendorPaymentTypeInfo(int masterDataUserId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var vendor = await _context.MasterDataVendor.FirstOrDefaultAsync(n => n.MasterDataUserId == masterDataUserId);

                if (vendor != null)
                {
                    var result = await (from vendorPayment in _context.MappingDataVendorPayment
                                        join topConfig in _context.MappingDataBusinessFieldTopconfig
                                        on vendorPayment.MappingDataBusinessFieldTopConfigId equals topConfig.Id
                                        join paymentConfig in _context.MappingDataBusinessFieldPaymentConfig
                                        on vendorPayment.MappingDataBusinessFieldPaymentConfigId equals paymentConfig.Id
                                        where vendorPayment.MasterDataUserId == masterDataUserId
                                        select new
                                        {
                                            vendorPayment.Id,
                                            vendorPayment.MappingDataBusinessFieldTopConfigId,
                                            vendorPayment.MappingDataBusinessFieldPaymentConfigId,
                                            topConfig.MasterDataTop.Top,
                                            paymentConfig.DownPayment,
                                            paymentConfig.Progressive,
                                            paymentConfig.FullPayment,
                                            paymentConfig.Retention
                                        }).FirstOrDefaultAsync();

                    response.Status = HttpStatusCode.OK.ToString();
                    response.Code = (int)HttpStatusCode.OK;

                    if (result != null)
                    {
                        response.Message = "Retrieve data success";
                    }
                    else
                    {
                        response.Message = "No result";
                    }

                    response.Data = result;
                }
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> InsertVendorBankPayment(MappingDataVendorPayment data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var vendorPayment = await _context.MappingDataVendorPayment.FirstOrDefaultAsync(n => n.MasterDataUserId == data.MasterDataUserId);

                    if (vendorPayment == null)
                    {
                        MappingDataVendorPayment newVendorPayment = new MappingDataVendorPayment
                        {
                            MasterDataUserId = data.MasterDataUserId,
                            MappingDataBusinessFieldTopConfigId = data.MappingDataBusinessFieldTopConfigId,
                            MappingDataBusinessFieldPaymentConfigId = data.MappingDataBusinessFieldPaymentConfigId,
                            CreatedBy = String.IsNullOrEmpty(data.CreatedBy) ? data.CreatedBy : "Vendor",
                            CreatedDate = DateTime.Now,
                            ModifiedBy = String.IsNullOrEmpty(data.CreatedBy) ? data.CreatedBy : "Vendor",
                            ModifiedDate = DateTime.Now,
                            IsDelete = false
                        };

                        await _context.MappingDataVendorPayment.AddAsync(newVendorPayment);
                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response = ResponseConstant.SAVE_SUCCESS;
                    }
                    else
                    {
                        vendorPayment.MappingDataBusinessFieldTopConfigId = data.MappingDataBusinessFieldTopConfigId;
                        vendorPayment.MappingDataBusinessFieldPaymentConfigId = data.MappingDataBusinessFieldPaymentConfigId;
                        vendorPayment.ModifiedBy = String.IsNullOrEmpty(data.CreatedBy) ? data.CreatedBy : "Vendor";
                        vendorPayment.ModifiedDate = DateTime.Now;

                        _context.Entry(vendorPayment).State = EntityState.Modified;

                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response = ResponseConstant.UPDATE_SUCCESS;
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> GetVendorPaymentMethod(int masterDataUserId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from vendorPayment in _context.MappingDataVendorPayment
                                    where vendorPayment.MasterDataUserId == masterDataUserId
                                    select new
                                    {
                                        vendorPayment.MappingDataBusinessFieldTopConfigId,
                                        vendorPayment.MappingDataBusinessFieldPaymentConfigId
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Status = HttpStatusCode.InternalServerError.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> InsertVendorLegal(MappingDataVendorLegal data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var checkVendorLegal = await _context.MappingDataVendorLegal.FirstOrDefaultAsync(n => n.MasterDataUserId == data.MasterDataUserId);

                    if (checkVendorLegal == null)
                    {
                        MappingDataVendorLegal vendorLegal = new MappingDataVendorLegal
                        {
                            MasterDataUserId = data.MasterDataUserId,
                            Nib = data.Nib,
                            Nibattachment = data.Nibattachment,
                            BusinessPermit = data.BusinessPermit,
                            BusinessPermitAttachment = data.BusinessPermitAttachment,
                            Siup = data.Siup,
                            SiupexpiredDate = data.SiupexpiredDate,
                            Siupattachment = data.Siupattachment,
                            Tdp = data.Tdp,
                            TdpexpiredDate = data.TdpexpiredDate,
                            Tdpattachment = data.Tdpattachment,
                            MemorandumOfAssociation = data.MemorandumOfAssociation,
                            MemorandumOfAssociationAttachment = data.MemorandumOfAssociationAttachment,
                            DecissionLetterMenkumham = data.DecissionLetterMenkumham,
                            DecissionLetterMenkumhamAttachment = data.DecissionLetterMenkumhamAttachment,
                            MemorandumOfAssociationChanging = data.MemorandumOfAssociationChanging,
                            MemorandumOfAssociationChangingAttachment = data.MemorandumOfAssociationChangingAttachment,
                            DecissionLetterMenkumhamChanging = data.DecissionLetterMenkumhamChanging,
                            DecissionLetterMenkumhamChangingAttachment = data.DecissionLetterMenkumhamChangingAttachment,
                            ManagementStructure = data.ManagementStructure,
                            Signing = data.Signing,
                            SigningId = data.SigningId,
                            SigningAttachment = data.SigningAttachment,
                            DocumentOther = data.DocumentOther,
                            OtherAttachment = data.OtherAttachment,
                            CreatedDate = DateTime.Now,
                            CreatedBy = data.CreatedBy,
                            ModifiedDate = DateTime.Now,
                            ModifiyBy = data.ModifiyBy,
                            IsDelete = false
                        };

                        await _context.MappingDataVendorLegal.AddAsync(vendorLegal);
                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response.Status = HttpStatusCode.OK.ToString();
                        response.Code = (int)HttpStatusCode.OK;
                        response.Message = "Save Data Success";
                        response.Data = vendorLegal;
                    }
                    else
                    {
                        checkVendorLegal.Nib = data.Nib;
                        checkVendorLegal.Nibattachment = data.Nibattachment;
                        checkVendorLegal.BusinessPermit = data.BusinessPermit;
                        checkVendorLegal.BusinessPermitAttachment = data.BusinessPermitAttachment;
                        checkVendorLegal.Siup = data.Siup;
                        checkVendorLegal.SiupexpiredDate = data.SiupexpiredDate;
                        checkVendorLegal.Siupattachment = data.Siupattachment;
                        checkVendorLegal.Tdp = data.Tdp;
                        checkVendorLegal.TdpexpiredDate = data.TdpexpiredDate;
                        checkVendorLegal.Tdpattachment = data.Tdpattachment;
                        checkVendorLegal.MemorandumOfAssociation = data.MemorandumOfAssociation;
                        checkVendorLegal.MemorandumOfAssociationAttachment = data.MemorandumOfAssociationAttachment;
                        checkVendorLegal.DecissionLetterMenkumham = data.DecissionLetterMenkumham;
                        checkVendorLegal.DecissionLetterMenkumhamAttachment = data.DecissionLetterMenkumhamAttachment;
                        checkVendorLegal.MemorandumOfAssociationChanging = data.MemorandumOfAssociationChanging;
                        checkVendorLegal.MemorandumOfAssociationChangingAttachment = data.MemorandumOfAssociationChangingAttachment;
                        checkVendorLegal.DecissionLetterMenkumhamChanging = data.DecissionLetterMenkumhamChanging;
                        checkVendorLegal.DecissionLetterMenkumhamChangingAttachment = data.DecissionLetterMenkumhamChangingAttachment;
                        checkVendorLegal.ManagementStructure = data.ManagementStructure;
                        checkVendorLegal.Signing = data.Signing;
                        checkVendorLegal.SigningId = data.SigningId;
                        checkVendorLegal.SigningAttachment = data.SigningAttachment;
                        checkVendorLegal.DocumentOther = data.DocumentOther;
                        checkVendorLegal.OtherAttachment = data.OtherAttachment;
                        checkVendorLegal.ModifiedDate = DateTime.Now;
                        checkVendorLegal.ModifiyBy = data.ModifiyBy;

                        _context.Entry(checkVendorLegal).State = EntityState.Modified;

                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response.Status = HttpStatusCode.OK.ToString();
                        response.Code = (int)HttpStatusCode.OK;
                        response.Message = "Update Data Success";
                        response.Data = checkVendorLegal;
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> GetVendorLegal(int masterDataUserId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from vendorLegal in _context.MappingDataVendorLegal
                                    where vendorLegal.MasterDataUserId == masterDataUserId
                                    select new
                                    {
                                        vendorLegal.Id,
                                        vendorLegal.MasterDataUserId,
                                        vendorLegal.Nib,
                                        vendorLegal.Nibattachment,
                                        vendorLegal.BusinessPermit,
                                        vendorLegal.BusinessPermitAttachment,
                                        vendorLegal.Siup,
                                        vendorLegal.SiupexpiredDate,
                                        vendorLegal.Siupattachment,
                                        vendorLegal.Tdp,
                                        vendorLegal.TdpexpiredDate,
                                        vendorLegal.Tdpattachment,
                                        vendorLegal.MemorandumOfAssociation,
                                        vendorLegal.MemorandumOfAssociationAttachment,
                                        vendorLegal.DecissionLetterMenkumham,
                                        vendorLegal.DecissionLetterMenkumhamAttachment,
                                        vendorLegal.MemorandumOfAssociationChanging,
                                        vendorLegal.MemorandumOfAssociationChangingAttachment,
                                        vendorLegal.DecissionLetterMenkumhamChanging,
                                        vendorLegal.DecissionLetterMenkumhamChangingAttachment,
                                        vendorLegal.Signing,
                                        vendorLegal.ManagementStructure,
                                        vendorLegal.SigningId,
                                        vendorLegal.SigningAttachment,
                                        vendorLegal.DocumentOther,
                                        vendorLegal.OtherAttachment,
                                        JobTitle = (from job in _context.MasterDataJobTitle
                                                    where job.Id == vendorLegal.SigningId
                                                    select job.Title)
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> InsertVendorTax(MappingDataVendorTax data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var vendorTax = await _context.MappingDataVendorTax.FirstOrDefaultAsync(n => n.MasterDataUserId == data.MasterDataUserId);

                    if (vendorTax == null)
                    {
                        MappingDataVendorTax newVendorTax = new MappingDataVendorTax
                        {
                            MasterDataUserId = data.MasterDataUserId,
                            TypeNpwp = data.TypeNpwp,
                            Npwp = data.Npwp,
                            TypePkp = data.TypePkp,
                            Pkp = data.Pkp,
                            TypeBkp = data.TypeBkp,
                            Ppn = data.Ppn,
                            MasterDataBkpCategoryId = data.MasterDataBkpCategoryId,
                            CreatedBy = String.IsNullOrEmpty(data.CreatedBy) ? data.CreatedBy : "Vendor",
                            CreatedDate = DateTime.Now,
                            ModifiedBy = String.IsNullOrEmpty(data.CreatedBy) ? data.CreatedBy : "Vendor",
                            ModifiedDate = DateTime.Now,
                            IsDelete = false
                        };

                        await _context.MappingDataVendorTax.AddAsync(newVendorTax);
                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response = ResponseConstant.SAVE_SUCCESS;
                    }
                    else
                    {
                        vendorTax.TypeNpwp = data.TypeNpwp;
                        vendorTax.Npwp = data.Npwp;
                        vendorTax.TypePkp = data.TypePkp;
                        vendorTax.Pkp = data.Pkp;
                        vendorTax.TypeBkp = data.TypeBkp;
                        vendorTax.Ppn = data.Ppn;
                        vendorTax.MasterDataBkpCategoryId = data.MasterDataBkpCategoryId;
                        vendorTax.ModifiedBy = String.IsNullOrEmpty(data.CreatedBy) ? data.CreatedBy : "Vendor";
                        vendorTax.ModifiedDate = DateTime.Now;

                        _context.Entry(vendorTax).State = EntityState.Modified;

                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response = ResponseConstant.UPDATE_SUCCESS;
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> GetVendorTax(int masterDataUserId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from vendorTax in _context.MappingDataVendorTax
                                    where vendorTax.MasterDataUserId == masterDataUserId
                                    select new
                                    {
                                        vendorTax.Id,
                                        vendorTax.TypeNpwp,
                                        vendorTax.Npwp,
                                        vendorTax.TypePkp,
                                        vendorTax.Pkp,
                                        vendorTax.TypeBkp,
                                        vendorTax.Ppn,
                                        vendorTax.MasterDataBkpCategoryId,
                                        BkpDesc = vendorTax.MasterDataBkpCategory.Desc
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> UpdateBusinessEthics(int masterDataUserId, bool businessEthics)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var vendorBussEthics = await _context.MasterDataVendor.FirstOrDefaultAsync(n => n.MasterDataUserId == masterDataUserId);

                    if (vendorBussEthics != null)
                    {
                        vendorBussEthics.IntegrityPact = businessEthics;

                        _context.Entry(vendorBussEthics).State = EntityState.Modified;

                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response = ResponseConstant.UPDATE_SUCCESS;
                    }
                    else
                    {
                        response = ResponseConstant.NO_RESULT;
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> UpdateMasterAgreement(int masterDataUserId, bool masterAgreement)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var vendorBussEthics = await _context.MasterDataVendor.FirstOrDefaultAsync(n => n.MasterDataUserId == masterDataUserId);

                    if (vendorBussEthics != null)
                    {
                        vendorBussEthics.MasterAgreement = masterAgreement;

                        _context.Entry(vendorBussEthics).State = EntityState.Modified;

                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response = ResponseConstant.UPDATE_SUCCESS;
                    }
                    else
                    {
                        response = ResponseConstant.NO_RESULT;
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> UpdateVendorStatus(int masterDataUserId, string status, string createdBy)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var vendor = await _context.MasterDataVendor.FirstOrDefaultAsync(n => n.MasterDataUserId == masterDataUserId);

                    if (vendor != null)
                    {
                        vendor.Status = status;
                        vendor.ModifiedDate = DateTime.Now;
                        vendor.ModifiyBy = createdBy;

                        _context.Entry(vendor).State = EntityState.Modified;

                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response.Code = (int)HttpStatusCode.OK;
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Message = "Feedback has been sent to " + vendor.Name;
                    }
                    else
                    {
                        response = ResponseConstant.NO_RESULT;
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> InsertVendorNotification(int masterDataUserId, string message, string notificationType, string createdBy)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    MasterDataVendorNotification notification = new MasterDataVendorNotification
                    {
                        MasterDataUserId = masterDataUserId,
                        NotificationType = notificationType,
                        Message = message,
                        IsRead = false,
                        CreatedDate = DateTime.Now,
                        CreatedBy = createdBy,
                        ModifiedDate = DateTime.Now,
                        ModifiedBy = createdBy,
                        IsDelete = false
                    };

                    await _context.MasterDataVendorNotification.AddAsync(notification);
                    await _context.SaveChangesAsync();

                    dbcxtransaction.Commit();

                    response = ResponseConstant.SAVE_SUCCESS;

                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> IsPersonalVendorBusinessType(int userId)
        {
            BaseResponse response = new BaseResponse();
            bool isPersonalType = false;

            try
            {
                var result = await (from vendor in _context.MasterDataVendor
                                    where vendor.MasterDataUserId == userId
                                    select new
                                    {
                                        BusinessType = vendor.MasterDataBusinessType.Code
                                    }).FirstOrDefaultAsync();

                if (result.BusinessType.Equals("PR", StringComparison.OrdinalIgnoreCase))
                {
                    isPersonalType = true;
                }

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;
                response.Data = isPersonalType;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Status = HttpStatusCode.InternalServerError.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetVendorInfoByEmail(string email)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from vendor in _context.MasterDataVendor
                                    where vendor.MasterDataUser.Email == email
                                    select new
                                    {
                                        BusinessTypeName = vendor.MasterDataBusinessType.Name,
                                        vendor.Name,
                                        vendor.Pic
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;
                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Status = HttpStatusCode.InternalServerError.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> DeleteVendorLegalAttachment(int userId, string type)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var vendor = await _context.MappingDataVendorLegal.FirstOrDefaultAsync(n => n.MasterDataUserId == userId);

                    if (vendor != null)
                    {
                        switch (type.ToLower())
                        {
                            case "nibattachment":
                                vendor.Nibattachment = null;
                                break;
                            case "businesspermitattachment":
                                vendor.BusinessPermitAttachment = null;
                                break;
                            case "siupattachment":
                                vendor.Siupattachment = null;
                                break;
                            case "tdpattachment":
                                vendor.Tdpattachment = null;
                                break;
                            case "memorandumofassociationattachment":
                                vendor.MemorandumOfAssociationAttachment = null;
                                break;
                            case "decissionlettermenkumhamattachment":
                                vendor.DecissionLetterMenkumhamAttachment = null;
                                break;
                            case "memorandumofassociationchangingattachment":
                                vendor.MemorandumOfAssociationChangingAttachment = null;
                                break;
                            case "decissionlettermenkumhamchangingattachment":
                                vendor.DecissionLetterMenkumhamChangingAttachment = null;
                                break;
                            case "signingattachment":
                                vendor.SigningAttachment = null;
                                break;
                            case "otherattachment":
                                vendor.OtherAttachment = null;
                                break;
                            default:
                                break;
                        }

                        vendor.ModifiedDate = DateTime.Now;

                        _context.Entry(vendor).State = EntityState.Modified;

                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response.Code = (int)HttpStatusCode.OK;
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Message = "Delete Success.";
                    }
                    else
                    {
                        response = ResponseConstant.NO_RESULT;
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> UpdateUserPhoto(int userId, string Image)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var dataUser = await _context.MasterDataUser.FirstOrDefaultAsync(n => n.Id == userId);

                    if (dataUser != null)
                    {
                        dataUser.Image = Image;
                        dataUser.ModifyDate = DateTime.Now;

                        _context.Entry(dataUser).State = EntityState.Modified;

                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response.Code = (int)HttpStatusCode.OK;
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Message = "Feedback has been sent to " + dataUser.UserName;
                        response.Data = dataUser.Image;
                    }
                    else
                    {
                        response = ResponseConstant.NO_RESULT;
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> GetDataVendortoCheckVerifikasi(int masterDataUserId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from vendor in _context.MasterDataVendor
                                    join vendorbank in _context.MappingDataVendorCurrencyBank
                                    on vendor.MasterDataUserId equals vendorbank.MasterDataUserId
                                    join vendorpayment in _context.MappingDataVendorPayment
                                    on vendor.MasterDataUserId equals vendorpayment.MasterDataUserId
                                    join vendorlegal in _context.MappingDataVendorLegal
                                    on vendor.MasterDataUserId equals vendorlegal.MasterDataUserId
                                    join vendortax in _context.MappingDataVendorTax
                                    on vendor.MasterDataUserId equals vendortax.MasterDataUserId
                                    where vendor.MasterDataUserId == masterDataUserId
                                    select new
                                    {
                                        vendor.MasterDataUserId,
                                        vendor.Name,
                                        vendor.Status,
                                        vendorlegal.Siup,
                                        vendorlegal.SiupexpiredDate,
                                        vendorlegal.Tdp,
                                        vendorlegal.TdpexpiredDate,
                                        vendortax.Npwp
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> InsertVendorCategories(VendorItemCategoriesModel data)
        {
            BaseResponse response = new BaseResponse();
            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    List<MappingDataVendorCategories> listVendorCategories = new List<MappingDataVendorCategories>();
                    var vendor = await (from a in _context.MasterDataVendor
                                        where a.MasterDataUserId == data.MasterDataUserId
                                        select new
                                        {
                                            VendorId = a.Id,
                                            UserName = a.MasterDataUser.UserName
                                        }).FirstOrDefaultAsync();

                    // Delete before save
                    var tempVendorCategories = await _context.MappingDataVendorCategories.Where(n => n.MasterDataVendorId == vendor.VendorId).ToListAsync();

                    _context.MappingDataVendorCategories.RemoveRange(tempVendorCategories);
                    await _context.SaveChangesAsync();

                    // Save after delete
                    foreach (var categoryId in data.ListCategoryId)
                    {
                        MappingDataVendorCategories vendorCategory = new MappingDataVendorCategories
                        {
                            MasterDataVendorId = vendor.VendorId,
                            CategoriesId = categoryId,
                            CreatedDate = DateTime.Now,
                            CreatedBy = vendor.UserName,
                            ModifiedDate = DateTime.Now,
                            ModifiedBy = vendor.UserName
                        };

                        listVendorCategories.Add(vendorCategory);
                    }

                    await _context.MappingDataVendorCategories.AddRangeAsync(listVendorCategories);
                    await _context.SaveChangesAsync();

                    dbcxtransaction.Commit();

                    response = ResponseConstant.SAVE_SUCCESS;
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> GetVendorCategories(int masterDataUserId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from vendor in _context.MasterDataVendor
                                    join vendorCat in _context.MappingDataVendorCategories
                                    on vendor.Id equals vendorCat.MasterDataVendorId
                                    where vendor.MasterDataUserId == masterDataUserId
                                    select vendorCat.CategoriesId).ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }
    }
}
