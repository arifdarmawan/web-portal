﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;

namespace WebPortal.Repository.Concrete
{
    public class BkpCategoryRepository : IBkpCategoryRepository
    {
        readonly WebPortalContext _context;
        public BkpCategoryRepository(WebPortalContext context)
        {
            _context = context;
        }
        public async Task<BaseResponse> GetBkpCategories()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from bkp in _context.MasterDataBkpCategory
                                    select new {
                                        bkp.Id,
                                        bkp.Desc
                                    }).AsNoTracking().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> InsertBkpCategory(MasterDataBkpCategory data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    MasterDataBkpCategory newData = new MasterDataBkpCategory
                    {
                        Desc = data.Desc,
                        CreatedBy = "Admin",
                        CreatedDate = DateTime.Now,
                        ModifiedBy = "Admin",
                        ModifiedDate = DateTime.Now,
                        IsDelete = false
                    };

                    await _context.MasterDataBkpCategory.AddAsync(newData);
                    await _context.SaveChangesAsync();

                    dbcxtransaction.Commit();

                    response = ResponseConstant.SAVE_SUCCESS;
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }
    }
}
