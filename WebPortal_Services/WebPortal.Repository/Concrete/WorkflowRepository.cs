﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using FirstResource.System.Library.EnumMaster;
using System.Globalization;
using WebPortal.Repository.Helper;

namespace WebPortal.Repository.Concrete
{
    public class WorkflowRepository : IWorkflowRepository
    {
        readonly WebPortalContext _context;

        public WorkflowRepository(WebPortalContext context)
        {
            _context = context;
        }

        #region workflow process
        public async Task<BaseResponse> GetWorkflows(int masterDataUserId, int pageNumber, string status)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                int page = 5;

                var result = new List<Tuple<string, string, string, DateTime, string, string, int, string>>().Select(t => new
                {
                    WorkflowType = t.Item1, // Property Global
                    ItemName = t.Item2, // Property Global
                    PRNumber = t.Item3,
                    RequestedDate = t.Item4, // Property Global
                    GrandTotal = t.Item5,
                    Status = t.Item6, // Property Global
                    WorkflowId = t.Item7, // Property Global
                    ActivityStatus = "", // Property Global
                    LastApproverId = 0,
                    CreatedDate = DateTime.Now
                }).ToList();

                if (status == WorkflowProcessStatus.START.ToString()) // Filter Action Pending
                {
                    var listActivityStatusByUserId = await (from masterPR in _context.MasterPurchaseRequisition
                                                            join mappingWFPR in _context.MappingWorkflowPurchaseRequisition on masterPR.Id equals mappingWFPR.PurchaseRequisitionId
                                                            join workflowProc in _context.WorkflowProcess on mappingWFPR.ProcessId equals workflowProc.Id
                                                            join wfActionApproval in _context.WorkflowActionApproval on workflowProc.Id equals wfActionApproval.WorkflowProcessId
                                                            join wfActivity in _context.WorkflowActivity on wfActionApproval.Id equals wfActivity.WorkflowActionApprovalId
                                                            where (wfActionApproval.MasterDataUserId == masterDataUserId)
                                                                && (workflowProc.ProcessStatus == status)
                                                                && (workflowProc.MasterDataUserId != masterDataUserId) // Filter for only approver user id exclude requestor user id
                                                            orderby workflowProc.ModifiedDate descending
                                                            select new
                                                            {
                                                                WorkflowType = workflowProc.WorkflowTypeCode,
                                                                WorkflowTitle = FirstResource.System.Library.Extensions.EnumExtensions.GetDescription((FirstResource.System.Library.EnumMaster.WorkflowType)Enum.Parse(typeof(FirstResource.System.Library.EnumMaster.WorkflowType), workflowProc.WorkflowTypeCode)).Result,
                                                                PRNumber = masterPR.Prnumber,
                                                                RequestedDate = masterPR.CreatedDate,
                                                                GrandTotal = masterPR.GrandTotal,
                                                                Status = FirstResource.System.Library.Extensions.EnumExtensions.GetDescription((PurchaseRequisitionStatus)Enum.Parse(typeof(PurchaseRequisitionStatus), masterPR.Prstatus)).Result,
                                                                WorkflowId = workflowProc.Id,
                                                                ActivityStatus = wfActivity.Activity,
                                                                LastApproverId = (from a in _context.WorkflowActionApproval
                                                                                  where a.WorkflowProcessId == workflowProc.Id
                                                                                  join b in _context.WorkflowGroupMember on a.MasterDataUserId equals b.MasterDataUserId
                                                                                  orderby b.ApprovalLevel descending
                                                                                  select a.MasterDataUserId).FirstOrDefault(),
                                                                ActivityCreatedDate = wfActivity.CreatedDate
                                                            }).AsNoTracking().ToListAsync();

                    var prActionPending = listActivityStatusByUserId.GroupBy(c => c.PRNumber)
                        .Where(grp => grp.Count() == 1)
                        .Select(grp => grp.FirstOrDefault())
                        .Skip(page * pageNumber)
                        .Take(page);

                    response.Data = prActionPending;
                }
                else if (status == WorkflowProcessStatus.END.ToString()) // Filter Action Approved
                {
                    var listActivityStatusByUserId = await (from masterPR in _context.MasterPurchaseRequisition
                                                            join mappingWFPR in _context.MappingWorkflowPurchaseRequisition on masterPR.Id equals mappingWFPR.PurchaseRequisitionId
                                                            join workflowProc in _context.WorkflowProcess on mappingWFPR.ProcessId equals workflowProc.Id
                                                            join wfActionApproval in _context.WorkflowActionApproval on workflowProc.Id equals wfActionApproval.WorkflowProcessId
                                                            join wfActivity in _context.WorkflowActivity on wfActionApproval.Id equals wfActivity.WorkflowActionApprovalId
                                                            where (wfActionApproval.MasterDataUserId == masterDataUserId)
                                                                && (workflowProc.MasterDataUserId != masterDataUserId) // Filter for only approver user id exclude requestor user id
                                                            orderby wfActivity.CreatedDate descending                                                          
                                                            select new
                                                            {
                                                                WorkflowType = workflowProc.WorkflowTypeCode,
                                                                WorkflowTitle = FirstResource.System.Library.Extensions.EnumExtensions.GetDescription((FirstResource.System.Library.EnumMaster.WorkflowType)Enum.Parse(typeof(FirstResource.System.Library.EnumMaster.WorkflowType), workflowProc.WorkflowTypeCode)).Result,
                                                                PRNumber = masterPR.Prnumber,
                                                                RequestedDate = masterPR.CreatedDate,
                                                                GrandTotal = masterPR.GrandTotal,
                                                                Status = FirstResource.System.Library.Extensions.EnumExtensions.GetDescription((PurchaseRequisitionStatus)Enum.Parse(typeof(PurchaseRequisitionStatus), masterPR.Prstatus)).Result,
                                                                WorkflowId = workflowProc.Id,
                                                                ActivityStatus = wfActivity.Activity,
                                                                LastApproverId = (from a in _context.WorkflowActionApproval
                                                                                  where a.WorkflowProcessId == workflowProc.Id
                                                                                  join b in _context.WorkflowGroupMember on a.MasterDataUserId equals b.MasterDataUserId
                                                                                  orderby b.ApprovalLevel descending
                                                                                  select a.MasterDataUserId).FirstOrDefault(),
                                                                ActivityCreatedDate = wfActivity.CreatedDate
                                                            }).AsNoTracking().ToListAsync();

                    var prActionApprovedTemp = listActivityStatusByUserId.GroupBy(c => c.PRNumber)
                        .Where(grp => grp.Count() > 1)
                        .Select(grp => grp.FirstOrDefault())
                        .Where(n => n != null);

                    var isActionLastApprovalTemp = prActionApprovedTemp.Where(n => n.LastApproverId == masterDataUserId);

                    // cast to list
                    var prActionApproved = new List<object>();                   

                    if (isActionLastApprovalTemp.Count() > 0)
                    {
                        // remove if PR Status End for last approver
                        var isActionLastApproval = isActionLastApprovalTemp.Where(n => n.Status == FirstResource.System.Library.Extensions.EnumExtensions.GetDescription(PurchaseRequisitionStatus.REJECTED).Result).Cast<object>().ToList();
                        prActionApproved = prActionApprovedTemp.Except(isActionLastApproval).ToList();
                    } else
                    {
                        // remove item 
                        prActionApproved = prActionApprovedTemp.Where(n => n.ActivityStatus != FirstResource.System.Library.EnumMaster.ActivityStatus.REJECTED.ToString()).Cast<object>().ToList();
                    }

                    var prResult = prActionApproved.Skip(page * pageNumber).Take(page).ToList();

                    response.Data = prResult;
                }
                else // Filter Action Rejected
                {
                    var listActivityStatusByUserId = await (from masterPR in _context.MasterPurchaseRequisition
                                                            join mappingWFPR in _context.MappingWorkflowPurchaseRequisition on masterPR.Id equals mappingWFPR.PurchaseRequisitionId
                                                            join workflowProc in _context.WorkflowProcess on mappingWFPR.ProcessId equals workflowProc.Id
                                                            join wfActionApproval in _context.WorkflowActionApproval on workflowProc.Id equals wfActionApproval.WorkflowProcessId
                                                            join wfActivity in _context.WorkflowActivity on wfActionApproval.Id equals wfActivity.WorkflowActionApprovalId
                                                            where (wfActionApproval.MasterDataUserId == masterDataUserId)
                                                                && (workflowProc.ProcessStatus == status)
                                                                && (workflowProc.MasterDataUserId != masterDataUserId) // Filter for only approver user id exclude requestor user id
                                                            orderby workflowProc.ModifiedDate descending
                                                            select new
                                                            {
                                                                WorkflowType = workflowProc.WorkflowTypeCode,
                                                                WorkflowTitle = FirstResource.System.Library.Extensions.EnumExtensions.GetDescription((FirstResource.System.Library.EnumMaster.WorkflowType)Enum.Parse(typeof(FirstResource.System.Library.EnumMaster.WorkflowType), workflowProc.WorkflowTypeCode)).Result,
                                                                PRNumber = masterPR.Prnumber,
                                                                RequestedDate = masterPR.CreatedDate,
                                                                GrandTotal = masterPR.GrandTotal,
                                                                Status = FirstResource.System.Library.Extensions.EnumExtensions.GetDescription((PurchaseRequisitionStatus)Enum.Parse(typeof(PurchaseRequisitionStatus), masterPR.Prstatus)).Result,
                                                                WorkflowId = workflowProc.Id,
                                                                ActivityStatus = wfActivity.Activity,
                                                                LastApproverId = (from a in _context.WorkflowActionApproval
                                                                                  where a.WorkflowProcessId == workflowProc.Id
                                                                                  join b in _context.WorkflowGroupMember on a.MasterDataUserId equals b.MasterDataUserId
                                                                                  orderby b.ApprovalLevel descending
                                                                                  select a.MasterDataUserId).FirstOrDefault(),
                                                                ActivityCreatedDate = wfActivity.CreatedDate
                                                            }).AsNoTracking().Distinct().ToListAsync();

                    var prActionRejected = listActivityStatusByUserId.GroupBy(n => n.PRNumber)
                        .Select(n => n.FirstOrDefault(x => x.ActivityStatus == ActivityStatus.REJECTED.ToString()))
                        .Where(n => n != null)
                        .Skip(page * pageNumber)
                        .Take(page);

                    response.Data = prActionRejected;
                }

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

               
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetWorkflowById(int workflowId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from wfProcess in _context.WorkflowProcess
                                    where wfProcess.Id == workflowId
                                    select new
                                    {
                                        WorkflowId = wfProcess.Id,
                                        wfProcess.WorkflowTypeCode,
                                        WorkflowTitle = FirstResource.System.Library.Extensions.EnumExtensions.GetDescription((FirstResource.System.Library.EnumMaster.WorkflowType)Enum.Parse(typeof(FirstResource.System.Library.EnumMaster.WorkflowType), wfProcess.WorkflowTypeCode)).Result,
                                        WorkflowPurchaseRequsitionContext = wfProcess.WorkflowTypeCode == wfProcess.WorkflowTypeCode ?
                                        (from mappingWFPR in _context.MappingWorkflowPurchaseRequisition
                                         where mappingWFPR.ProcessId == wfProcess.Id
                                         join masterPR in _context.MasterPurchaseRequisition on mappingWFPR.PurchaseRequisitionId equals masterPR.Id
                                         select new
                                         {
                                             PRNumber = masterPR.Prnumber,
                                             RequestedDate = masterPR.CreatedDate,
                                             masterPR.GrandTotal,
                                             Status = FirstResource.System.Library.Extensions.EnumExtensions.GetDescription((PurchaseRequisitionStatus)Enum.Parse(typeof(PurchaseRequisitionStatus), masterPR.Prstatus)).Result,
                                             ListItem = (from masterPrItem in _context.MasterPurchaseRequisitionItem
                                                         where masterPrItem.MasterPurchaseRequisitionId == masterPR.Id
                                                         select new
                                                         {
                                                             masterPrItem.MasterItem.ItemName,
                                                             Quantity = masterPrItem.Qty,
                                                             Currency = masterPrItem.Currency.Code,
                                                             masterPrItem.UnitPrice,
                                                             masterPrItem.TotalPrice,
                                                             PPN = masterPrItem.Ppn,
                                                             masterPrItem.MasterItem.ItemCode,
                                                             masterPrItem.MasterItem.BimsCode,
                                                             TotalPriceReal = masterPrItem.TotalPrice,
                                                         }).ToList(),
                                             LocationCode = masterPR.CompanyLocationCode,
                                             CompanyCode = (from a in _context.WorkflowProcess
                                                            where a.Id == workflowId
                                                            join b in _context.WorkflowGroupMember on a.MasterDataUserId equals b.MasterDataUserId
                                                            join c in _context.ConfigCompany on b.ConfigCompanyId equals c.Id
                                                            select c.CompanyCode).FirstOrDefault(),
                                             Year = masterPR.CreatedDate.Year
                                         }).FirstOrDefault() : null
                                    }).AsNoTracking().FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> CountWorkflows(int masterDataUserId, string status)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                if (status == WorkflowProcessStatus.START.ToString())
                {
                    var listActivityStatusByUserId = await (from masterPR in _context.MasterPurchaseRequisition
                                                            join mappingWFPR in _context.MappingWorkflowPurchaseRequisition on masterPR.Id equals mappingWFPR.PurchaseRequisitionId
                                                            join workflowProc in _context.WorkflowProcess on mappingWFPR.ProcessId equals workflowProc.Id
                                                            join wfActionApproval in _context.WorkflowActionApproval on workflowProc.Id equals wfActionApproval.WorkflowProcessId
                                                            join wfActivity in _context.WorkflowActivity on wfActionApproval.Id equals wfActivity.WorkflowActionApprovalId
                                                            where (wfActionApproval.MasterDataUserId == masterDataUserId)
                                                                && (workflowProc.ProcessStatus == status)
                                                                && (workflowProc.MasterDataUserId != masterDataUserId) // Filter for only approver user id exclude requestor user id
                                                            orderby workflowProc.ModifiedDate descending
                                                            select new
                                                            {
                                                                PRNumber = masterPR.Prnumber,
                                                                Status = FirstResource.System.Library.Extensions.EnumExtensions.GetDescription((PurchaseRequisitionStatus)Enum.Parse(typeof(PurchaseRequisitionStatus), masterPR.Prstatus)).Result,
                                                                ActivityStatus = wfActivity.Activity
                                                            }).AsNoTracking().ToListAsync();

                    var countPrActionPending = listActivityStatusByUserId.GroupBy(c => c.PRNumber)
                        .Where(grp => grp.Count() == 1)
                        .Select(grp => grp.FirstOrDefault()).Count();

                    response.Data = countPrActionPending;

                }
                else if (status == WorkflowProcessStatus.END.ToString())
                {
                    var listActivityStatusByUserId = await (from masterPR in _context.MasterPurchaseRequisition
                                                            join mappingWFPR in _context.MappingWorkflowPurchaseRequisition on masterPR.Id equals mappingWFPR.PurchaseRequisitionId
                                                            join workflowProc in _context.WorkflowProcess on mappingWFPR.ProcessId equals workflowProc.Id
                                                            join wfActionApproval in _context.WorkflowActionApproval on workflowProc.Id equals wfActionApproval.WorkflowProcessId
                                                            join wfActivity in _context.WorkflowActivity on wfActionApproval.Id equals wfActivity.WorkflowActionApprovalId
                                                            where (wfActionApproval.MasterDataUserId == masterDataUserId)
                                                                && (workflowProc.MasterDataUserId != masterDataUserId) // Filter for only approver user id exclude requestor user id
                                                            orderby wfActivity.CreatedDate descending
                                                            select new
                                                            {
                                                                PRNumber = masterPR.Prnumber,
                                                                Status = FirstResource.System.Library.Extensions.EnumExtensions.GetDescription((PurchaseRequisitionStatus)Enum.Parse(typeof(PurchaseRequisitionStatus), masterPR.Prstatus)).Result,
                                                                ActivityStatus = wfActivity.Activity,
                                                                LastApproverId = (from a in _context.WorkflowActionApproval
                                                                                  where a.WorkflowProcessId == workflowProc.Id
                                                                                  join b in _context.WorkflowGroupMember on a.MasterDataUserId equals b.MasterDataUserId
                                                                                  orderby b.ApprovalLevel descending
                                                                                  select a.MasterDataUserId).FirstOrDefault()
                                                            }).AsNoTracking().ToListAsync();


                    var prActionApprovedTemp = listActivityStatusByUserId.GroupBy(c => c.PRNumber)
                         .Where(grp => grp.Count() > 1)
                         .Select(grp => grp.FirstOrDefault())
                         .Where(n => n != null);

                    var isActionLastApprovalTemp = prActionApprovedTemp.Where(n => n.LastApproverId == masterDataUserId);

                    // cast to list
                    var prActionApproved = new List<object>();

                    if (isActionLastApprovalTemp.Count() > 0)
                    {
                        // remove if PR Status End for last approver
                        var isActionLastApproval = isActionLastApprovalTemp.Where(n => n.Status == FirstResource.System.Library.Extensions.EnumExtensions.GetDescription(PurchaseRequisitionStatus.REJECTED).Result).Cast<object>().ToList();
                        prActionApproved = prActionApprovedTemp.Except(isActionLastApproval).ToList();
                    }
                    else
                    {
                        // remove item 
                        prActionApproved = prActionApprovedTemp.Where(n => n.ActivityStatus != FirstResource.System.Library.EnumMaster.ActivityStatus.REJECTED.ToString()).Cast<object>().ToList();
                    }

                    var countPrActionApproved = prActionApproved.Count();

                    response.Data = countPrActionApproved;
                }
                else
                {
                    var listActivityStatusByUserId = await (from masterPR in _context.MasterPurchaseRequisition
                                                            join mappingWFPR in _context.MappingWorkflowPurchaseRequisition on masterPR.Id equals mappingWFPR.PurchaseRequisitionId
                                                            join workflowProc in _context.WorkflowProcess on mappingWFPR.ProcessId equals workflowProc.Id
                                                            join wfActionApproval in _context.WorkflowActionApproval on workflowProc.Id equals wfActionApproval.WorkflowProcessId
                                                            join wfActivity in _context.WorkflowActivity on wfActionApproval.Id equals wfActivity.WorkflowActionApprovalId
                                                            where (wfActionApproval.MasterDataUserId == masterDataUserId)
                                                                && (workflowProc.ProcessStatus == status)
                                                                && (workflowProc.MasterDataUserId != masterDataUserId) // Filter for only approver user id exclude requestor user id
                                                            orderby workflowProc.ModifiedDate descending
                                                            select new
                                                            {
                                                                PRNumber = masterPR.Prnumber,
                                                                Status = FirstResource.System.Library.Extensions.EnumExtensions.GetDescription((PurchaseRequisitionStatus)Enum.Parse(typeof(PurchaseRequisitionStatus), masterPR.Prstatus)).Result,
                                                                ActivityStatus = wfActivity.Activity
                                                            }).AsNoTracking().Distinct().ToListAsync();

                    var countPrActionRejected = listActivityStatusByUserId
                        .GroupBy(n => n.PRNumber)
                        .Select(n => n.FirstOrDefault(x => x.ActivityStatus == ActivityStatus.REJECTED.ToString()))
                        .Where(n => n != null)
                        .Count();

                    response.Data = countPrActionRejected;
                }

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;
                response.Message = "Retrieve data success";
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        #endregion

        #region workflow action approval
        public async Task<BaseResponse> WorkflowActionApproved(WorkflowActionApprovedModel data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    #region WorkflowActionApproval
                    MasterDataUser userCreated = await _context.MasterDataUser.FirstOrDefaultAsync(n => n.Id == data.MasterDataUserId);
                    WorkflowActionApproval workflowActionApproval = await _context.WorkflowActionApproval.FirstOrDefaultAsync(n => n.WorkflowProcessId == data.WorkflowProcessId && n.MasterDataUserId == data.MasterDataUserId);

                    workflowActionApproval.IsActive = false;
                    workflowActionApproval.IsComplete = true;
                    workflowActionApproval.ModifiedBy = userCreated.UserName;
                    workflowActionApproval.ModifiedDate = DateTime.Now;

                    _context.Entry(workflowActionApproval).State = EntityState.Modified;

                    await _context.SaveChangesAsync();
                    #endregion

                    #region WorkflowActivity
                    var currentNextAction = new List<Tuple<int, string, string, string>>().Select(t => new { Id = t.Item1, Desc = t.Item2, Activity = t.Item3, Noted = "" }).ToList();

                    var currentAction = new
                    {
                        Id = workflowActionApproval.Id,
                        Desc = ConstGlobalMessage.ApprovedMessage + _context.MasterDataUser.Where(n => n.Id == data.MasterDataUserId).Select(n => n.UserName).FirstOrDefault(),
                        Activity = FirstResource.System.Library.EnumMaster.ActivityStatus.APPROVED.ToString(),
                        Noted = data.Noted
                    };

                    var nextAction = await (from wfActionApproval in _context.WorkflowActionApproval
                                            where wfActionApproval.WorkflowProcessId == data.WorkflowProcessId
                                            && wfActionApproval.IsActive == true // this logic as indicator next approval
                                            && wfActionApproval.IsComplete == false // this logic as indicator next approval
                                            select new
                                            {
                                                wfActionApproval.Id,
                                                Desc = ConstGlobalMessage.NotificationMessage + _context.MasterDataUser.Where(n => n.Id == wfActionApproval.MasterDataUserId).Select(n => n.UserName).FirstOrDefault(),
                                                Activity = FirstResource.System.Library.EnumMaster.ActivityStatus.WAITING_APPROVAL.ToString(),
                                                Noted = ""
                                            }).FirstOrDefaultAsync();

                    if (nextAction == null) // if last approver set message to complete & set workflow process status to END
                    {
                        currentNextAction.Add(currentAction);

                        #region WorkflowProcess

                        WorkflowProcess workflowProcess = await _context.WorkflowProcess.FindAsync(data.WorkflowProcessId);

                        workflowProcess.ProcessStatus = FirstResource.System.Library.EnumMaster.WorkflowProcessStatus.END.ToString();
                        _context.Entry(workflowActionApproval).State = EntityState.Modified;

                        await _context.SaveChangesAsync();

                        #endregion
                    }
                    else
                    {
                        currentNextAction.Add(currentAction);
                        currentNextAction.Add(nextAction);
                    }

                    List<WorkflowActivity> workflowActivities = new List<WorkflowActivity>();
                    foreach (var action in currentNextAction)
                    {
                        WorkflowActivity workflowActivity = new WorkflowActivity
                        {
                            WorkflowActionApprovalId = action.Id,
                            Activity = action.Activity,
                            Desc = action.Desc,
                            Noted = action.Noted,
                            CreatedDate = DateTime.Now,
                            CreatedBy = userCreated.UserName,
                            ModifiedDate = DateTime.Now,
                            ModifiedBy = userCreated.UserName
                        };

                        workflowActivities.Add(workflowActivity);
                    }

                    await _context.WorkflowActivity.AddRangeAsync(workflowActivities);
                    await _context.SaveChangesAsync();

                    #endregion

                    #region Update Purchase Requisition Status
                    var masterPurchaseRequisition = await (from masterPR in _context.MasterPurchaseRequisition
                                                            join mappingPR in _context.MappingWorkflowPurchaseRequisition on masterPR.Id equals mappingPR.PurchaseRequisitionId
                                                            where mappingPR.ProcessId == data.WorkflowProcessId
                                                            select masterPR).FirstOrDefaultAsync();

                    if (nextAction == null) // if last approver set status to complete
                    {
                        masterPurchaseRequisition.Prstatus = FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.COMPLETED.ToString();
                        masterPurchaseRequisition.ModifyDate = DateTime.Now;
                        masterPurchaseRequisition.ModifiedBy = userCreated.UserName;

                        await _context.SaveChangesAsync();

                        #region Generate PO
                        if (masterPurchaseRequisition.Prstatus == PurchaseRequisitionStatus.COMPLETED.ToString())
                        {
                            var getDataPurchaseRequisition = await (from mapWFPurchaseRequest in _context.MappingWorkflowPurchaseRequisition
                                                                    join wfProcess in _context.WorkflowProcess
                                                                        on mapWFPurchaseRequest.ProcessId equals wfProcess.Id
                                                                    join purchaseRequest in _context.MasterPurchaseRequisition
                                                                        on mapWFPurchaseRequest.PurchaseRequisitionId equals purchaseRequest.Id
                                                                    join prItems in _context.MasterPurchaseRequisitionItem
                                                                        on purchaseRequest.Id equals prItems.MasterPurchaseRequisitionId
                                                                    where mapWFPurchaseRequest.ProcessId == data.WorkflowProcessId
                                                                    select purchaseRequest).FirstOrDefaultAsync();

                            var getVendor = await (from prItem in _context.MasterPurchaseRequisitionItem
                                                   join items in _context.MasterItems
                                                        on prItem.MasterItemId equals items.ItemId
                                                   join Categories in _context.MasterItemCategories
                                                        on items.MasterItemCategoryId equals Categories.CategoryId
                                                   join mappingVendorItemPrice in _context.MappingVendorItemPrice
                                                        on prItem.MasterItemId equals mappingVendorItemPrice.MasterDataItemId
                                                   join vendor in _context.MasterDataVendor
                                                        on mappingVendorItemPrice.MasterDataVendorId equals vendor.Id
                                                   where prItem.MasterPurchaseRequisitionId == getDataPurchaseRequisition.Id
                                                        && mappingVendorItemPrice.PriceValidDate >= DateTime.Now.Date
                                                   select new
                                                   {
                                                       VendorId = vendor.Id,
                                                       VendorName = vendor.Name,
                                                   }).AsNoTracking().Distinct().ToListAsync();

                            foreach (var listVendorItem in getVendor) 
                            {
                                var getDataPurchaseRequisitionItem = await (from prItem in _context.MasterPurchaseRequisitionItem
                                                           join items in _context.MasterItems
                                                                on prItem.MasterItemId equals items.ItemId
                                                            join Categories in _context.MasterItemCategories
                                                                on items.MasterItemCategoryId equals Categories.CategoryId
                                                            join MappingVendorCategories in _context.MappingDataVendorCategories
                                                                on Categories.CategoryId equals MappingVendorCategories.CategoriesId
                                                            join vendor in _context.MasterDataVendor
                                                                on MappingVendorCategories.MasterDataVendorId equals vendor.Id
                                                            where prItem.MasterPurchaseRequisitionId == getDataPurchaseRequisition.Id
                                                                && vendor.Id == listVendorItem.VendorId
                                                           select prItem).ToListAsync();

                                var getPonumber = "PO-" + DateTime.Now.Year + "-" + getDataPurchaseRequisition.CompanyCode + "-" + getDataPurchaseRequisition.DepartementCode;

                                var GrandTotal = getDataPurchaseRequisitionItem.Sum(d => d.TotalPrice);

                                var getPpn = getDataPurchaseRequisitionItem.FirstOrDefault();

                                decimal ppnPrice = Convert.ToDecimal(GrandTotal) * Convert.ToDecimal(getPpn.Ppn) / 100;

                                decimal GrandTotalAfterTax = Convert.ToDecimal(GrandTotal) + ppnPrice;

                                MasterPurchaseOrder purchaseOrder = new MasterPurchaseOrder
                                {
                                    Ponumber = await _context.MasterPurchaseOrder.CountAsync() > 0 ? getPonumber + "-" + string.Format(CultureInfo.InvariantCulture, "{0:0000000}", await _context.MasterPurchaseOrder.MaxAsync(n => n.Id) + 1) : getPonumber + "-" + string.Format(CultureInfo.InvariantCulture, "{0:0000000}", 1),
                                    MasterPrid = getDataPurchaseRequisition.Id,
                                    MasterDataVendorId = listVendorItem.VendorId,
                                    Postatus = "WAITING",
                                    GrandTotal = GrandTotal,
                                    GrandTotalAfterTax = GrandTotalAfterTax,
                                    CreatedBy = getDataPurchaseRequisition.CreatedBy,
                                    CreatedDate = DateTime.Now,
                                    ModifiedBy = getDataPurchaseRequisition.CreatedBy,
                                    ModifyDate = DateTime.Now,
                                    IsDelete = false
                                };

                                await _context.MasterPurchaseOrder.AddAsync(purchaseOrder);
                                await _context.SaveChangesAsync();

                                List<MasterPurchaseOrderItem> purchaseOrderItem = new List<MasterPurchaseOrderItem>();

                                foreach (var item in getDataPurchaseRequisitionItem)
                                {
                                    MasterPurchaseOrderItem orderItem = new MasterPurchaseOrderItem()
                                    {
                                        Specification = item.Specification,
                                        PurposeDesc = item.PurposeDesc,
                                        Qty = item.Qty,
                                        Uomid = item.Uomid,
                                        CurrencyId = item.CurrencyId,
                                        UnitPrice = item.UnitPrice,
                                        TotalPrice = item.TotalPrice,
                                        CreatedDate = item.CreatedDate,
                                        CreatedBy = item.CreatedBy,
                                        ModifyDate = item.ModifyDate,
                                        ModifyBy = item.ModifyBy,
                                        MasterItemId = item.MasterItemId,
                                        MasterPurchaseOrderId = purchaseOrder.Id,
                                        Ppn = item.Ppn
                                    };
                                    purchaseOrderItem.Add(orderItem);
                                }

                                await _context.MasterPurchaseOrderItem.AddRangeAsync(purchaseOrderItem);
                                await _context.SaveChangesAsync();
                            }
                        }

                        #endregion
                    }
                    #endregion

                    dbcxtransaction.Commit();

                    response.Code = (int)HttpStatusCode.OK;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Message = "Update data success";
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> WorkflowActionRejected(WorkflowActionApprovedModel data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    #region WorkflowActionApproval
                    MasterDataUser userCreated = await _context.MasterDataUser.FirstOrDefaultAsync(n => n.Id == data.MasterDataUserId);
                    WorkflowActionApproval workflowActionApproval = await _context.WorkflowActionApproval.FirstOrDefaultAsync(n => n.WorkflowProcessId == data.WorkflowProcessId && n.MasterDataUserId == data.MasterDataUserId);

                    workflowActionApproval.IsActive = false;
                    workflowActionApproval.IsComplete = true;
                    workflowActionApproval.ModifiedBy = userCreated.UserName;
                    workflowActionApproval.ModifiedDate = DateTime.Now;

                    _context.Entry(workflowActionApproval).State = EntityState.Modified;

                    await _context.SaveChangesAsync();
                    #endregion

                    #region WorkflowActivity                   
                    var currentNextAction = new List<Tuple<int, string, string, string>>().Select(t => new { Id = t.Item1, Desc = t.Item2, Activity = "", Noted = "" }).ToList();

                    var currentAction = await (from wfActionApproval in _context.WorkflowActionApproval
                                               where wfActionApproval.WorkflowProcessId == data.WorkflowProcessId
                                               && wfActionApproval.MasterDataUserId == data.MasterDataUserId
                                               select new
                                               {
                                                   wfActionApproval.Id,
                                                   Desc = ConstGlobalMessage.RejectedMessage + _context.MasterDataUser.Where(n => n.Id == wfActionApproval.MasterDataUserId).Select(n => n.UserName).FirstOrDefault(),
                                                   Activity = FirstResource.System.Library.EnumMaster.ActivityStatus.REJECTED.ToString(),
                                                   Noted = data.Noted
                                               }).FirstOrDefaultAsync();

                    var nextAction = await (from wfActionApproval in _context.WorkflowActionApproval
                                            where wfActionApproval.WorkflowProcessId == data.WorkflowProcessId
                                            && wfActionApproval.IsActive == true
                                            && wfActionApproval.IsComplete == false
                                            select new
                                            {
                                                wfActionApproval.Id,
                                                Desc = ConstGlobalMessage.RejectedMessage + _context.MasterDataUser.Where(n => n.Id == wfActionApproval.MasterDataUserId).Select(n => n.UserName).FirstOrDefault(),
                                                Activity = FirstResource.System.Library.EnumMaster.ActivityStatus.REJECTED.ToString(),
                                                Noted = ""
                                            }).ToListAsync();

                    currentNextAction.Add(currentAction);
                    currentNextAction.AddRange(nextAction);

                    List<WorkflowActivity> workflowActivities = new List<WorkflowActivity>();
                    foreach (var action in currentNextAction)
                    {
                        WorkflowActivity workflowActivity = new WorkflowActivity
                        {
                            WorkflowActionApprovalId = action.Id,
                            Activity = action.Activity,
                            Desc = action.Desc,
                            Noted = action.Noted,
                            CreatedDate = DateTime.Now,
                            CreatedBy = userCreated.UserName,
                            ModifiedDate = DateTime.Now,
                            ModifiedBy = userCreated.UserName
                        };

                        workflowActivities.Add(workflowActivity);
                    }

                    await _context.WorkflowActivity.AddRangeAsync(workflowActivities);
                    await _context.SaveChangesAsync();
                    #endregion

                    #region WorkflowProcess
                    WorkflowProcess workflowProcess = await _context.WorkflowProcess.FindAsync(data.WorkflowProcessId);

                    workflowProcess.ProcessStatus = FirstResource.System.Library.EnumMaster.WorkflowProcessStatus.CANCEL.ToString();
                    _context.Entry(workflowActionApproval).State = EntityState.Modified;

                    await _context.SaveChangesAsync();
                    #endregion

                    #region Update Purchase Requisition Status
                    var masterPurchaseRequisition = await (from masterPR in _context.MasterPurchaseRequisition
                                                           join mappingPR in _context.MappingWorkflowPurchaseRequisition on masterPR.Id equals mappingPR.PurchaseRequisitionId
                                                           where mappingPR.ProcessId == data.WorkflowProcessId
                                                           select masterPR).FirstOrDefaultAsync();

                    masterPurchaseRequisition.Prstatus = FirstResource.System.Library.EnumMaster.PurchaseRequisitionStatus.REJECTED.ToString();
                    masterPurchaseRequisition.ModifyDate = DateTime.Now;
                    masterPurchaseRequisition.ModifiedBy = userCreated.UserName;

                    await _context.SaveChangesAsync();

                    #endregion

                    dbcxtransaction.Commit();

                    response.Code = (int)HttpStatusCode.OK;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Message = "Update data success.";
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }
        #endregion

        #region workflow history
        public async Task<BaseResponse> WorkflowHistoryById(int workflowId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from wfProcess in _context.WorkflowProcess
                                    where wfProcess.Id == workflowId
                                    select new
                                    {
                                        requestorActionId = wfProcess.MasterDataUserId,
                                        requestorActionName = wfProcess.MasterDataUser.UserName,
                                        createdDate = wfProcess.CreatedDate,
                                        userApproval = (from wfActionApproval in _context.WorkflowActionApproval
                                                        where wfActionApproval.WorkflowProcessId == workflowId && wfActionApproval.MasterDataUserId != wfProcess.MasterDataUserId
                                                        orderby wfActionApproval.Id
                                                        select new
                                                        {
                                                            userActionId = wfActionApproval.MasterDataUserId,
                                                            userActionName = (from user in _context.MasterDataUser
                                                                              where user.Id == wfActionApproval.MasterDataUserId
                                                                              select user.UserName).FirstOrDefault(),
                                                            activity = (from wfActivity in _context.WorkflowActivity
                                                                        where wfActivity.WorkflowActionApprovalId == wfActionApproval.Id
                                                                        orderby wfActivity.CreatedDate descending
                                                                        select new
                                                                        {
                                                                            activity = FirstResource.System.Library.Extensions.EnumExtensions.GetDescription((ActivityStatus)Enum.Parse(typeof(ActivityStatus), wfActivity.Activity)).Result,
                                                                            noted = wfActivity.Noted,
                                                                            createdDate = wfActivity.CreatedDate
                                                                        }).FirstOrDefault()                                                          
                                                        }).ToList(),
                                        workflowType = wfProcess.WorkflowTypeCode
                                    }).AsNoTracking().FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }
        #endregion
    }
}
