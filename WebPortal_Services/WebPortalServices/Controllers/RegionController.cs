﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegionController : ControllerBase
    {
        IRegionRepository _RegionRepository;
        public RegionController(IRegionRepository regionRepository)
        {
            _RegionRepository = regionRepository;
        }

        [HttpGet("Get")]
        public async Task<BaseResponse> Get()
        {
            return await _RegionRepository.GetRegions();
        }

        [HttpGet("GetRegionsByCountryId")]
        public async Task<BaseResponse> GetRegionsByCountryId(int masterDataCountryId)
        {
            return await _RegionRepository.GetRegionsByCountryId(masterDataCountryId);
        }
    }
}