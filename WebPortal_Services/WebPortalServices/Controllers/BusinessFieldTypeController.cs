﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BusinessFieldTypeController : ControllerBase
    {
        IBusinessFieldsTypeRepository _BusinessFieldsTypeRepository;
        public BusinessFieldTypeController(IBusinessFieldsTypeRepository businessFieldsTypeRepository)
        {
            _BusinessFieldsTypeRepository = businessFieldsTypeRepository;
        }

        [HttpGet("GetBusinessFieldsByGroupType")]
        public async Task<BaseResponse> GetBusinessFieldsByGroupType()
        {
            return await _BusinessFieldsTypeRepository.GetBusinessFieldsByGroupType();
        }

        [HttpGet("GetBusinessFieldTOP")]
        public async Task<BaseResponse> GetBusinessFieldsTOP([FromQuery] int paramBusinessFieldId)
        {
            return await _BusinessFieldsTypeRepository.GetBusinessFieldTOP(paramBusinessFieldId);
        }

        [HttpGet("GetBusinessFieldsPayment")]
        public async Task<BaseResponse> GetBusinessFieldsPayment([FromQuery] int paramBusinessFieldId)
        {
            return await _BusinessFieldsTypeRepository.GetBusinessFieldPayment(paramBusinessFieldId);
        }
    }
}
