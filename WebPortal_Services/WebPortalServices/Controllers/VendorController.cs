﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendorController : ControllerBase
    {
        IVendorRepository _VendorRepository;
        public VendorController(IVendorRepository vendorRepository)
        {
            _VendorRepository = vendorRepository;
        }
        
        [HttpPost("InsertVendor")]
        public async Task<BaseResponse> InsertVendor([FromBody] MasterDataVendor data)
        {
            return await _VendorRepository.InsertVendor(data);
        }

        [HttpPost("InsertVendorBasicInfo")]
        public async Task<BaseResponse> InsertVendorBasicInfo([FromBody] BasicInfoModel data)
        {
            return await _VendorRepository.InsertVendorBasicInfo(data);
        }

        [HttpGet("GetVendorByUserId")]
        public async Task<BaseResponse> GetVendorByUserId(int userId)
        {
            return await _VendorRepository.GetVendorByUserId(userId);
        }

        [HttpGet("GetAllVendor")]
        public async Task<BaseResponse> GetAllVendor()
        {
            return await _VendorRepository.GetAllVendor();
        }

        [HttpGet("GetVendorDetails")]
        public async Task<BaseResponse> GetVendorDetails([FromQuery] int userId)
        {
            return await _VendorRepository.GetVendorDetail(userId);
        }

        [HttpGet("GetVendorBankAccount")]
        public async Task<BaseResponse> GetVendorBankAccount(int masterDataUserId)
        {
            return await _VendorRepository.GetVendorBankAccount(masterDataUserId);
        }

        [HttpGet("GetVendorBankAccountPrimary")]
        public async Task<BaseResponse> GetVendorBankAccountPrimary(int userId)
        {
            return await _VendorRepository.GetVendorBankAccountPrimary(userId);
        }

        [HttpPost("InsertVendorBankAccount")]
        public async Task<BaseResponse> InsertVendorBankAccount([FromBody] BankAccountModel data)
        {
            return await _VendorRepository.InsertVendorBankAccount(data);
        }

        [HttpPost("UpdateVendorBankAccount")]
        public async Task<BaseResponse> UpdateVendorBankAccount([FromBody] MappingDataVendorCurrencyBank data)
        {
            return await _VendorRepository.UpdateVendorBankAccount(data);
        }

        [HttpPost("UpdateStatusVendorBankAccount")]
        public async Task<BaseResponse> UpdateStatusVendorBankAccount([FromQuery] int id, int masterDataUserId, string status, string modifyBy)
        {
            return await _VendorRepository.UpdateStatusVendorBankAccount(id, masterDataUserId, status, modifyBy);
        }

        [HttpPost("DeleteVendorBankAccount")]
        public async Task<BaseResponse> DeleteVendorBankAccount([FromQuery] int id)
        {
            return await _VendorRepository.DeleteVendorBankAccount(id);
        }

        [HttpGet("GetVendorPaymentTypeInfo")]
        public async Task<BaseResponse> GetVendorPaymentTypeInfo([FromQuery] int masterDataUserId)
        {
            return await _VendorRepository.GetVendorPaymentTypeInfo(masterDataUserId);
        }

        [HttpGet("GetVendorInfoByEmail")]
        public async Task<BaseResponse> GetVendorInfoByEmail([FromQuery] string email)
        {
            return await _VendorRepository.GetVendorInfoByEmail(email);
        }

        [HttpGet("GetVendorPaymentMethod")]
        public async Task<BaseResponse> GetVendorPaymentMethod([FromQuery] int masterDataUserId)
        {
            return await _VendorRepository.GetVendorPaymentMethod(masterDataUserId);
        }

        [HttpGet("GetVendorLegal")]
        public async Task<BaseResponse> GetVendorLegal(int masterDataUserId)
        {
            return await _VendorRepository.GetVendorLegal(masterDataUserId);
        }

        [HttpGet("GetVendorTax")]
        public async Task<BaseResponse> GetVendorTax(int masterDataUserId)
        {
            return await _VendorRepository.GetVendorTax(masterDataUserId);
        }

        [HttpPost("InsertVendorPayment")]
        public async Task<BaseResponse> InsertVendorPayment([FromBody] MappingDataVendorPayment data)
        {
            return await _VendorRepository.InsertVendorBankPayment(data);
        }

        [HttpPost("InsertVendorLegal")]
        public async Task<BaseResponse> InsertVendorLegal([FromBody] MappingDataVendorLegal data)
        {
            return await _VendorRepository.InsertVendorLegal(data);
        }

        [HttpPost("InsertVendorTax")]
        public async Task<BaseResponse> InsertVendorTax([FromBody] MappingDataVendorTax data)
        {
            return await _VendorRepository.InsertVendorTax(data);
        }

        [HttpPost("UpdateBusinessEthicsStatus")]
        public async Task<BaseResponse> UpdateBusinessEthicsStatus([FromQuery] int masterDataUserId, bool businessEthics)
        {
            return await _VendorRepository.UpdateBusinessEthics(masterDataUserId, businessEthics);
        }

        [HttpPost("UpdateMasterAgreementStatus")]
        public async Task<BaseResponse> UpdateMasterAgreementStatus([FromQuery] int masterDataUserId, bool masterAgreement)
        {
            return await _VendorRepository.UpdateMasterAgreement(masterDataUserId, masterAgreement);
        }

        [HttpPut("UpdateVendorStatus")]
        public async Task<BaseResponse> UpdateVendorStatus([FromQuery] int masterDataUserId, string status, string createdBy)
        {
            return await _VendorRepository.UpdateVendorStatus(masterDataUserId, status, createdBy);
        }

        [HttpPost("InsertVendorNotification")]
        public async Task<BaseResponse> InsertVendorNotification([FromQuery] int masterDataUserId, string messages, string notificationType, string createdBy)
        {
            return await _VendorRepository.InsertVendorNotification(masterDataUserId, messages, notificationType, createdBy);
        }

        [HttpGet("IsPersonalVendorBusinessType")]
        public async Task<BaseResponse> IsPersonalVendorBusinessType([FromQuery] int userId)
        {
            return await _VendorRepository.IsPersonalVendorBusinessType(userId);
        }

        [HttpPost("DeleteVendorLegalAttachment")]
        public async Task<BaseResponse> DeleteVendorLegalAttachment([FromQuery] int userId, string type)
        {
            return await _VendorRepository.DeleteVendorLegalAttachment(userId, type);
        }

        [HttpPut("UpdateUserPhoto")]
        public async Task<BaseResponse> UpdateUserPhoto([FromQuery] int userId, string Image)
        {
            return await _VendorRepository.UpdateUserPhoto(userId, Image);
        }

        [HttpGet("GetDataVendortoCheckVerifikasi")]
        public async Task<BaseResponse> GetDataVendortoCheckVerifikasi([FromQuery] int masterDataUserId)
        {
            return await _VendorRepository.GetDataVendortoCheckVerifikasi(masterDataUserId);
        }

        [HttpPost("InsertVendorCategories")]
        public async Task<BaseResponse> InsertVendorCategories([FromBody] VendorItemCategoriesModel model)
        {
            return await _VendorRepository.InsertVendorCategories(model);
        }

        [HttpGet("GetVendorCategories")]
        public async Task<BaseResponse> GetVendorCategories([FromQuery] int masterDataUserId)
        {
            return await _VendorRepository.GetVendorCategories(masterDataUserId);
        }
    }
}