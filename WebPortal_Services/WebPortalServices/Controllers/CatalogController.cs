﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogController : ControllerBase
    {
        readonly ICatalogRepository _catalogRepository;
        public CatalogController(ICatalogRepository catalogRepository)
        {
            _catalogRepository = catalogRepository;
        }

        [HttpGet("GetAllCategories")]
        public async Task<BaseResponse> GetAllCategories()
        {
            return await _catalogRepository.GetAllCategories();
        }
        [HttpGet("GetCategoriesByGroupLevel")]
        public async Task<BaseResponse> GetCategoriesByGroupLevel()
        {
            return await _catalogRepository.GetCategoriesByGroupLevel();
        }
    }
}