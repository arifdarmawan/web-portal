﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobTitleController : ControllerBase
    {
        readonly IJobTitleRepository _jobTitleRepository;
        public JobTitleController(IJobTitleRepository jobTitleRepository)
        {
            _jobTitleRepository = jobTitleRepository;
        }

        [HttpGet("GetJobTitles")]
        public async Task<BaseResponse> GetJobTitles()
        {
            return await _jobTitleRepository.GetJobTitles();
        }

        [HttpPost("InsertJobTitles")]
        public async Task<BaseResponse> InsertJobTitles([FromBody] MasterDataJobTitle data)
        {
            return await _jobTitleRepository.InsertJobTitle(data);
        }
    }
}