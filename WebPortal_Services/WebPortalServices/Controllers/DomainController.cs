﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Entities;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DomainController : ControllerBase
    {
        private readonly IDomainRepository _MasterDataDomainRepository;
        public DomainController(IDomainRepository MasterDataDomainRepository)
        {
            _MasterDataDomainRepository = MasterDataDomainRepository;
        }
        
        [HttpGet]
        public async Task<IEnumerable<MasterDataDomain>> GetAllDomain()
        {
            return await _MasterDataDomainRepository.GetAllDomain();
        }
    }
}