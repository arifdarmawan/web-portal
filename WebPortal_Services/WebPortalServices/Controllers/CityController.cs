﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class CityController : ControllerBase
    {
        ICityRepository _CityRepository;
        public CityController(ICityRepository cityRepository)
        {
            _CityRepository = cityRepository;
        }

        [HttpGet("Get")]
        public async Task<BaseResponse> Get()
        {
            return await _CityRepository.GetCities();
        }

        [HttpGet("GetCitiesByRegionId")]
        public async Task<BaseResponse> GetCitiesByRegionId(int masterDataRegionId)
        {
            return await _CityRepository.GetCitiesByregionid(masterDataRegionId);
        }
    }
}