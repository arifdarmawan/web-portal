﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkflowController : ControllerBase
    {
        IWorkflowRepository _workflowRepository;
        public WorkflowController(IWorkflowRepository workflowRepository)
        {
            _workflowRepository = workflowRepository;
        }

        [HttpGet("GetWorkflows")]
        public async Task<BaseResponse> GetWorkflows([FromQuery] int userId, int pageNumber, string processStatus)
        {
            return await _workflowRepository.GetWorkflows(userId, pageNumber, processStatus);
        }

        [HttpGet("GetWorkflowById")]
        public async Task<BaseResponse> GetWorkflowById([FromQuery] int workflowId)
        {
            return await _workflowRepository.GetWorkflowById(workflowId);
        }

        [HttpGet("CountWorkflows")]
        public async Task<BaseResponse> CountWorkflows([FromQuery] int userId, string status)
        {
            return await _workflowRepository.CountWorkflows(userId, status);
        }

        [HttpPost("WorkflowActionApproved")]
        public async Task<BaseResponse> WorkflowActionApproved([FromBody] WorkflowActionApprovedModel data)
        {
            return await _workflowRepository.WorkflowActionApproved(data);
        }

        [HttpPost("WorkflowActionRejected")]
        public async Task<BaseResponse> WorkflowActionRejected([FromBody] WorkflowActionApprovedModel data)
        {
            return await _workflowRepository.WorkflowActionRejected(data);
        }

        [HttpGet("GetWorkflowHistoryById")]
        public async Task<BaseResponse> GetWorkflowHistoryById([FromQuery] int workflowId)
        {
            return await _workflowRepository.WorkflowHistoryById(workflowId);
        }
    }
}