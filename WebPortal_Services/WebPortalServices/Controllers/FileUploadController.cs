﻿using System;
using System.Web;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;
using WebPortal.Repository.Helper;
using Microsoft.AspNetCore.Hosting;
using System.Net;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileUploadController : Controller
    {
        readonly IFileRepository _fileRepository;
        readonly IHostingEnvironment _hostingEnvironment;
        public FileUploadController(IFileRepository fileRepository, IHostingEnvironment hostingEnvironment)
        {
            this._fileRepository = fileRepository;
            this._hostingEnvironment = hostingEnvironment;
        }

        [HttpPost("BasicUpload")]
        public async Task<ImageBaseResponse> BasicUpload(List<IFormFile> files)
        {
            return await _fileRepository.BasicUpload(files);
        }

        [HttpPost("UploadPhoto")]
        public async Task<ImageBaseResponse> UploadPhoto(List<IFormFile> files)
        {
            return await _fileRepository.UploadPhoto(files);
        }

        [HttpGet("BasicDownload")]
        [EnableCors("_webPortalAllowCORS")]      
        public async Task<IActionResult> BasicDowload(string fileName)
        {
            try
            {
                if (fileName == null)
                    return Content("filename not present");

                var path = Path.Combine(Directory.GetCurrentDirectory(), "FileUploads", fileName);

                var memory = new MemoryStream();
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;

                return File(memory, GlobalHelper.GetContentType(path).Result, Path.GetFileName(path));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                throw;
            }           
        }
        
        [HttpGet("DownloadPhoto")]
        [EnableCors("_webPortalAllowCORS")]
        public async Task<IActionResult> DownloadPhoto(string fileName)
        {
            if (fileName == null)
                return Content("filename not present");

            var path = Path.Combine(Directory.GetCurrentDirectory(), "FileUploads", fileName);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;

            return File(memory, GlobalHelper.GetContentType(path).Result, Path.GetFileName(path));
        }

        [HttpGet("DownloadFile")]
        public async Task<BaseResponse> DownloadFile(string fileName)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var path = "";
                var extFile = Path.GetExtension(fileName);
                var result = extFile.ToLower();

                if (result != ".pdf")
                {
                    path = Path.Combine(Directory.GetCurrentDirectory(), "FileUploads", fileName);
                }
                else 
                {
                    path = Path.Combine(Directory.GetCurrentDirectory(), "FileUploads", fileName);
                }

                string FileDir = path;
                byte[] bytes = System.IO.File.ReadAllBytes(FileDir);

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;
                response.Message = "Retrieve File Success.";
                response.Data = bytes;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = "Failed to Retrieve File.";
            }

            return response;

        }
    }
}