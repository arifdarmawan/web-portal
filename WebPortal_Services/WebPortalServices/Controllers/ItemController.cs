﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        IItemRepository _ItemRepository;
        public ItemController(IItemRepository itemRepository) 
        {
            _ItemRepository = itemRepository;
        }
        
        [HttpGet("Get")]
        public async Task<BaseResponse> get() 
        {
            return await _ItemRepository.GetAllItem();
        }

        [HttpGet("GetAllItemLookup")]
        public async Task<BaseResponse> GetAllItemLookup()
        {
            return await _ItemRepository.GetAllItemLookup();
        }

        [HttpGet("SearchItemByKey")]
        public async Task<BaseResponse> SearchItemByKey(string key)
        {
            return await _ItemRepository.SearchItemByKey(key);
        }

        [HttpGet("GetItemByCode")]
        public async Task<BaseResponse> GetItemByCode(string bimsCode)
        {
            return await _ItemRepository.GetItemByCode(bimsCode);
        }

        [HttpGet("GetVendorItemPrice")]
        public async Task<BaseResponse> GetVendorItemPrice(int itemId)
        {
            return await _ItemRepository.GetVendorItemPrice(itemId);
        }
    }
}