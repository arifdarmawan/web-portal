﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryController : ControllerBase
    {
        ICountryRepository _CountryRepository;
        public CountryController(ICountryRepository countryRepository)
        {
            _CountryRepository = countryRepository;
        }

        [HttpGet("Get")]
        public async Task<BaseResponse> Get()
        {
            return await _CountryRepository.GetCountries();
        }
    }
}