﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UnitOfMeasureController : ControllerBase
    {
        IUnitofMeasureRepository _unitofmeasure;
        public UnitOfMeasureController(IUnitofMeasureRepository unitofMeasure)
        {
            _unitofmeasure = unitofMeasure;
        }

        [HttpGet("Get")]
        public async Task<BaseResponse> Get()
        {
            return await _unitofmeasure.GetUOM();
        }
    }
}