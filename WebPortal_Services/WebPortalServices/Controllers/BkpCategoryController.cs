﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Entities;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BkpCategoryController : ControllerBase
    {
        readonly IBkpCategoryRepository _bkpCategoryRepository;
        public BkpCategoryController(IBkpCategoryRepository bkpCategoryRepository)
        {
            _bkpCategoryRepository = bkpCategoryRepository;
        }

        [HttpGet("GetBkpCategories")]
        public async Task<BaseResponse> GetBkpCategories()
        {
            return await _bkpCategoryRepository.GetBkpCategories();
        }

        [HttpPost("InsertBkpCategory")]
        public async Task<BaseResponse> InsertBkpCategory(MasterDataBkpCategory data)
        {
            return await _bkpCategoryRepository.InsertBkpCategory(data);
        }
    }
}