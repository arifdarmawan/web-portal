﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProcurementController : ControllerBase
    {
        IProcurementRepository _ProcurementRepository;
        public ProcurementController(IProcurementRepository procurementRepository)
        {
            _ProcurementRepository = procurementRepository;
        }

        #region Purchase Requisition
        [HttpGet("GetAccessPurchaseRequisition")]
        public async Task<BaseResponse> GetAccessPurchaseRequisition([FromQuery] int userid)
        {
            return await _ProcurementRepository.GetAccessPurchaseRequisition(userid);
        }

        [HttpGet("GetCheckVendorItem")]
        public async Task<BaseResponse> GetCheckVendorItem([FromQuery] int itemId)
        {
            return await _ProcurementRepository.GetCheckVendorItem(itemId);
        }

        [HttpGet("GetPpnVendorItemCategories")]
        public async Task<BaseResponse> GetPpnVendorItemCategories([FromQuery] int itemId)
        {
            return await _ProcurementRepository.GetPpnVendorItemCategories(itemId);
        }

        [HttpPost("InsertPurchaseRequisition")]
        public async Task<BaseResponse> InsertPurchaseRequisition([FromBody] CreatePurchaseRequisitionModel data)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                response = await _ProcurementRepository.InsertPurchaseRequisition(data);

                if (response.Status == HttpStatusCode.OK.ToString())
                {
                    // Sending Email

                }
            }
            catch (System.Exception)
            {

                throw;
            }

            return response;
        }

        [HttpGet("GetPurchaseRequisition")]
        public async Task<BaseResponse> GetPurchaseRequisition()
        {
            return await _ProcurementRepository.GetPurchaseRequisition();
        }

        [HttpGet("GetPurchaseRequisitionById")]
        public async Task<BaseResponse> GetPurchaseRequisitionById([FromQuery] int id)
        {
            return await _ProcurementRepository.getPurchaseRequisitionById(id);
        }

        [HttpGet("getPurchaseRequisitionItemByVendorId")]
        public async Task<BaseResponse> getPurchaseRequisitionItemByVendorId([FromQuery] int id)
        {
            return await _ProcurementRepository.getPurchaseRequisitionItemByVendorId(id);
        }

        [HttpGet("GetRealizationBudgetItem")]
        public async Task<BaseResponse> GetRealizationBudgetItem([FromQuery] string bimsCode, int year)
        {
            return await _ProcurementRepository.GetRealizationBudgetItem(bimsCode, year);
        }

        [HttpGet("GetBookingRealizationBudgetItem")]
        public async Task<BaseResponse> GetBookingRealizationBudgetItem([FromQuery] string itemCode, int year)
        {
            return await _ProcurementRepository.GetBookingRealizationBudgetItem(itemCode, year);
        }

        [HttpGet("GetPurchaseRequisitionItemOnProcess")]
        public async Task<BaseResponse> GetPurchaseRequisitionItemOnProcess(string company, string bimsCode)
        {
            return await _ProcurementRepository.GetPurchaseRequisitionItemOnProcess(company, bimsCode);
        }

        [HttpGet("GetRFQList")]
        public async Task<BaseResponse> GetRFQList()
        {
            return await _ProcurementRepository.GetRFQList();
        }

        [HttpGet("GetRFQDetail")]
        public async Task<BaseResponse> GetRFQDetail(int id, int masterItemId)
        {
            return await _ProcurementRepository.GetRFQDetail(id, masterItemId);
        }

        [HttpGet("GetQCFList")]
        public async Task<BaseResponse> GetQCFList()
        {
            return await _ProcurementRepository.GetQCFList();
        }

        [HttpGet("GetQCFDetail")]
        public async Task<BaseResponse> GetQCFDetail(int id, int masterItemId)
        {
            return await _ProcurementRepository.GetQCFDetail(id, masterItemId);
        }

        [HttpPost("BlastToVendor")]
        public async Task<BaseResponse> BlastToVendor([FromBody] RequestForQuotationEmployeeModel data)
        {
            return await _ProcurementRepository.BlastToVendor(data);
        }

        [HttpGet("GetRequestForQuotationPRId")]
        public async Task<BaseResponse> GetRequestForQuotationPRId(int prId, int masterItemId)
        {
            return await _ProcurementRepository.GetRequestForQuotationPRId(prId, masterItemId);
        }

        [HttpGet("GetRequestForQuotationbyUserId")]
        public async Task<BaseResponse> GetRequestForQuotationbyUserId(int userId)
        {
            return await _ProcurementRepository.GetRequestForQuotationbyUserId(userId);
        }

        [HttpGet("GetRequestForQuotationDetailByRfqId")]
        public async Task<BaseResponse> GetRequestForQuotationDetailByRfqId(int rfqId)
        {
            return await _ProcurementRepository.GetRequestForQuotationDetailByRfqId(rfqId);
        }

        [HttpPost("ProcessRequestForQuotation")]
        public async Task<BaseResponse> ProcessRequestForQuotation([FromBody] RequestForQuotationModel data)
        {
            return await _ProcurementRepository.ProcessRequestForQuotation(data);
        }

        [HttpGet("getQuotationComparationFormByPrId")]
        public async Task<BaseResponse> getQuotationComparationFormByPrId(int prid, int masterItemId)
        {
            return await _ProcurementRepository.getQuotationComparationFormByPrId(prid, masterItemId);
        }

        [HttpGet("QuotationComparationFormHeader")]
        public async Task<BaseResponse> QuotationComparationFormHeader(int prId, int masterItemId)
        {
            return await _ProcurementRepository.QuotationComparationFormHeader(prId, masterItemId);
        }

        [HttpGet("QuotationComparationFormItem")]
        public async Task<BaseResponse> QuotationComparationFormItem(int prId, int masterItemId)
        {
            return await _ProcurementRepository.QuotationComparationFormItem(prId, masterItemId);
        }

        [HttpPut("ProcessQuotationComparationForm")]
        public async Task<BaseResponse> ProcessQuotationComparationForm([FromQuery] int PrId, int masterDataVendorId, string notes, string modifyBy)
        {
            return await _ProcurementRepository.ProcessQuotationComparationForm(PrId, masterDataVendorId, notes, modifyBy);
        }

        #endregion

        #region Purchase Order
        [HttpGet("GetPurchaseOrder")]
        public async Task<BaseResponse> GetPurchaseOrder()
        {
            return await _ProcurementRepository.GetPurchaseOrder();
        }

        [HttpGet("GetPurchaseOrderById")]
        public async Task<BaseResponse> GetPurchaseOrderById(int id, int userId)
        {
            return await _ProcurementRepository.GetPurchaseOrderById(id, userId);
        }

        [HttpGet("GetPurchaseOrderByVendorId")]
        public async Task<BaseResponse> GetPurchaseOrderByVendorId(int userId)
        {
            return await _ProcurementRepository.GetPurchaseOrderByVendorId(userId);
        }

        [HttpGet("GetPurchaseOrderItemById")]
        public async Task<BaseResponse> GetPurchaseOrderItemById(int id/*, int masterDataVendorId*/)
        {
            return await _ProcurementRepository.GetPurchaseOrderItemById(id/*, masterDataVendorId*/);
        }

        [HttpGet("getPurchaseOrderItemByVendorId")]
        public async Task<BaseResponse> getPurchaseOrderItemByVendorId(int id)
        {
            return await _ProcurementRepository.getPurchaseOrderItemByVendorId(id);
        }

        [HttpPut("ProcessPurchaseOrder")]
        public async Task<BaseResponse> ProcessPurchaseOrder([FromQuery] int id, string modifyBy)
        {
            return await _ProcurementRepository.ProcessPurchaseOrder(id, modifyBy);
        }

        [HttpPut("RejectPurchaseOrder")]
        public async Task<BaseResponse> RejectPurchaseOrder([FromQuery] int id, string modifyBy)
        {
            return await _ProcurementRepository.RejectPurchaseOrder(id, modifyBy);
        }
        #endregion
    }
}