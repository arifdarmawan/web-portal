﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models.Models;
using WebPortal.Repository.Abstract;

namespace WebPortalServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BankController : ControllerBase
    {
        IBankRepository _BankRepository;
        public BankController(IBankRepository bankRepository)
        {
            _BankRepository = bankRepository;
        }

        [HttpGet("GetAllBank")]
        public async Task<BaseResponse> GetAllBank()
        {
            return await _BankRepository.GetAllBank();
        }
    }
}