﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebPortal.Models.Entities;
using WebPortal.Repository.Abstract;
using WebPortal.Repository.Concrete;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using WebPortal.Models.Models;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using WebPortal.Repository.Helper;
using Swashbuckle.AspNetCore.Swagger;

namespace WebPortalServices
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        readonly string WebPortalAllowCORS = "_webPortalAllowCORS";
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string[] domains = new string[] {
                Configuration.GetValue<string>("Settings:WebPortalDomain"),
                Configuration.GetValue<string>("Settings:EmployeePortalDomain")
            };

            services.AddCors(options =>
            {
                options.AddPolicy(WebPortalAllowCORS, builder =>
                {
                    builder.WithOrigins(domains);
                });
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            RegisterDependencyInjection(services);

            var connection = Configuration.GetValue<string>("Settings:ConnectionString");
            // SQL Server
            //services.AddDbContext<WebPortalContext>(options => options.UseSqlServer(connection));
            // PostgreSQL
            if (!string.IsNullOrWhiteSpace(connection))
            {
                services.AddEntityFrameworkNpgsql().AddDbContext<WebPortalContext>(opts =>
                {
                    opts.UseNpgsql(connection);
                });
            }

            // JWT Authentication
            // configure strongly typed settings objects
            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Web Portal API", Version = "v1" });

                var security = new Dictionary<string, IEnumerable<string>>
                {
                    { "Bearer", new string[0] }
                };

                c.AddSecurityDefinition(name: "Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the bearer scheme",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
            });
        }

        private void RegisterDependencyInjection(IServiceCollection services)
        {
            services.AddSingleton(Configuration);

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IDomainRepository, DomainRepository>();
            services.AddScoped<IResetPasswordRepository, ResetPasswordRepository>();
            services.AddScoped<IBusinessTypeRepository, BusinessTypeRepository>();
            services.AddScoped<IBusinessFieldsTypeRepository, BusinessFieldsTypeRepository>();
            services.AddScoped<ICountryRepository, CountryRepository>();
            services.AddScoped<IRegionRepository, RegionRepository>();
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<ICurrencyRepository, CurrencyRepository>();
            services.AddScoped<IVendorRepository, VendorRepository>();
            services.AddScoped<IBankRepository, BankRepository>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IJobTitleRepository, JobTitleRepository>();
            services.AddScoped<IBkpCategoryRepository, BkpCategoryRepository>();
            services.AddScoped<ICatalogRepository, CatalogRepository>();
            services.AddScoped<IProcurementRepository, ProcurementRepository>();
            services.AddScoped<IUnitofMeasureRepository, UnitofMeasureRepository>();
            services.AddScoped<IItemRepository, ItemRepository>();
            services.AddScoped<IWorkflowRepository, WorkflowRepository>();
            services.AddScoped<IUserService, UserService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Web Portal API V1");
                c.DefaultModelExpandDepth(0);
                c.DefaultModelsExpandDepth(-1);
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //app.UseCors(WebPortalAllowCORS);
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseHttpsRedirection();

            var option = new RewriteOptions();
            option.AddRedirect("^$", "swagger");
            app.UseRewriter(option);
            app.UseStaticFiles();
            app.UseMvc();
        }
    }
}
