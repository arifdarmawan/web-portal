﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPortal.Model
{
    class MetadataPartial
    {
    }

    [MetadataType(typeof(T_FilesMetaData))]
    public partial class T_Files { }

    [MetadataType(typeof(T_CommentNewsMetaData))]
    public partial class T_CommentNews { }

    [MetadataType(typeof(T_NewsMetaData))]
    public partial class T_News { }

    [MetadataType(typeof(T_TutorialMetaData))]
    public partial class T_Tutorial { }

    [MetadataType(typeof(T_MsUserMetaData))]
    public partial class T_MsUser { }

    [MetadataType(typeof(T_MsProjectMetaData))]
    public partial class T_MsProject { }

    [MetadataType(typeof(T_FAQMetaData))]
    public partial class T_FAQ { }
}
