//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebPortal.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_Vendor_Legal
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string SIUP { get; set; }
        public string SIUPDocumentName { get; set; }
        public string SIUPDocumentPath { get; set; }
        public System.DateTime SIUPValidTo { get; set; }
        public string TDP { get; set; }
        public string TDPDocumentName { get; set; }
        public string TDPDocumentPath { get; set; }
        public System.DateTime TDPValidTo { get; set; }
        public string SKDP { get; set; }
        public string SKDPDocumentName { get; set; }
        public string SKDPDocumentPath { get; set; }
        public Nullable<System.DateTime> SKDPValidTo { get; set; }
        public string DeedOfIncorporation { get; set; }
        public string DoIDocumentName { get; set; }
        public string DoIDocumentPath { get; set; }
        public string CertificateOfEstablishment { get; set; }
        public string CoEDocumentName { get; set; }
        public string CoEDocumentPath { get; set; }
        public string DeedOfChange { get; set; }
        public string DoCDocumentName { get; set; }
        public string DoCDocumentPath { get; set; }
        public string CertificateOfChange { get; set; }
        public string CoCDocumentName { get; set; }
        public string CoCDocumentPath { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
    }
}
