﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using WebPortal.Model;
using WebPortal.Models;

namespace WebPortal.GeneralClass
{
    public class oGeneral
    {
        static string passPhrase = "Pas5pr@se123467890bcdf";        // can be any string
        static string saltValue = "s@1tValue";        // can be any string
        static string hashAlgorithm = "SHA1";             // can be "MD5"
        static int passwordIterations = 2;                  // can be any number
        static string initVector = "@1B2c3D4e5F6g7H8"; // must be 16 bytes
        static int keySize = 256;

        FrInternallEntities db = new FrInternallEntities();

        public string CreateSession(string UserId)
        {
            DateTime now = DateTime.UtcNow; if (now.Year < 2018)
            {
                //Session created
                string DecryptedSession = UserId + DateTime.UtcNow.ToString();
                string Session1 = Encrypt(DecryptedSession);
                return Session1;
            }
            return "";
        }


        public string Encrypt(string Decrypted)
        {
            DateTime now = DateTime.UtcNow;
            string Result = RijndaelSimple.Encrypt(Decrypted, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
            return Result;
        }

        public string Decrypt(string Encypted)
        {
            string Result = RijndaelSimple.Decrypt(Encypted, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
            return Result;
        }

        public void clearSession(HttpSessionStateBase Session)
        {
            Session.Remove("LogedUserID");
            Session.Remove("LogedUserNameFull");
            Session.Abandon();
        }

        public string Logout(string user)
        {
            DateTime now = DateTime.UtcNow;
            FrInternallEntities db = new FrInternallEntities();
            FrInternallEntities msuser = new FrInternallEntities();
            var v = db.T_MsUser.Where(a => a.UserId.Equals(user)).FirstOrDefault();
            v.LastLogout = now;
            //v.Isactive = false;
            db.Entry(v).State = EntityState.Modified;
            db.SaveChanges();
            return "";
        }

        //public string upload(HttpPostedFileBase file)
        //{
        //    string path1 = "";
        //    var filename = Path.GetFileName(file.FileName);
        //    var path = Path.Combine(Server.MapPath("~/Download/"), filename);
        //    file.SaveAs(path);
        //    path1 = path;
        //    return "";
        //}


        //public string SendEmail(string mail, string UserId)
        //{
        //    BasicPage basicpage = new BasicPage();
        //    string code = basicpage.Code(mail, UserId);
        //    var body = "<p><b>Hi " + UserId + "</b></p><div>Your Password code is <b>" + code + "</b></div>";
        //    var message = new MailMessage();
        //    message.To.Add(new MailAddress(mail));
        //    message.From = new MailAddress("do-not-reply@first-resources.com");
        //    message.Subject = "Forget your Account";
        //    message.Body = body;
        //    message.IsBodyHtml = true;
        //    //string H = "U44sr7+hV01wL7338WYDxQ==";
        //    //string M = Decrypt(H);
        //    SmtpClient smtp = new SmtpClient();
        //    smtp.Host = "10.96.2.13";
        //    smtp.Port = 25;
        //    smtp.Send(message);
        //    TempData["Message"] = "Email Sudah Terkirim...";
        //    return code;
        //}

        //public string SendEmailSaran(string mail, string name, string messageFrom)
        //{
        //    BasicPage basicpage = new BasicPage();
        //    var body = "<p class=MsoPlainText><b style='mso-bidi-font-weight:normal'>Hai Helpdesk,<o:p></o:p></b></p>" +
        //                            "<p class=MsoPlainText>Ada saran masuk dari  <b style='mso-bidi-font-weight:normal'><i style='mso-bidi-font-style:normal'>" + name + "</i></b> yaitu <b style='mso-bidi-font-weight:normal'><i style='mso-bidi-font-style:normal'>" + messageFrom + " , </i></b>mohon di tanggapi.<o:p></o:p></p>" +
        //                            "<p class=MsoNormal><a name='_MailAutoSig'><span style='font-size:12.0pt;mso-fareast-font-family:'Times New Roman';mso-fareast-theme-font:minor-fareast;color:black;mso-themecolor:text1;mso-no-proof:yes'>Salam,<o:p></o:p></span></a></p>" +
        //                            "<p class=MsoNormal><span style='mso-bookmark:_MailAutoSig'><span style='font-size:12.0pt;mso-fareast-font-family:'Times New Roman';mso-fareast-theme-font:minor-fareast;color:black;mso-themecolor:text1;mso-no-proof:yes'><o:p>&nbsp;</o:p></span></span></p>" +
        //                            "<p class=MsoNormal><span style='mso-bookmark:_MailAutoSig'><b style='mso-bidi-font-weight:normal'><i style='mso-bidi-font-style:normal'><span style='font-size:12.0pt;mso-fareast-font-family:'Times New Roman';mso-fareast-theme-font:minor-fareast;color:black;mso-themecolor:text1;mso-no-proof:yes'>Team IT Developer<o:p></o:p></span></i></b></span></p>";
        //    var message = new MailMessage();
        //    message.To.Add(new MailAddress(mail));
        //    message.From = new MailAddress("do-not-reply@first-resources.com");
        //    message.Subject = "[WebPortal]- Saran";
        //    message.Body = body;
        //    message.IsBodyHtml = true;
        //    SmtpClient smtp = new SmtpClient();
        //    smtp.Host = "10.96.2.13";
        //    smtp.Port = 25;
        //    smtp.Send(message);
        //    TempData["Message"] = "Email Sudah Terkirim...";
        //    return "";
        //}

        public PagingAndGroup getPaging(PagingAndGroup mod)
        {
            int startPage = mod.currentPage - 5, endPage = mod.currentPage + 4;
            bool toFirst = true, toLast = true;

            if (startPage <= 0)
            {
                endPage -= (startPage - 1);
                startPage = 1;
                toFirst = false;
            }

            if (endPage > mod.totalPages)
            {
                endPage = mod.totalPages;
                toLast = false;

                if (endPage > 10)
                {
                    startPage = endPage - 9;
                }
            }

            mod.startPage = startPage;
            mod.endPage = endPage;
            mod.toFirst = toFirst;
            mod.toLast = toLast;

            return mod;
        }

        public string randomString(int length)
        {
            Random random = new Random();
            const string pool = "abcdefghijklmnopqrstuvwxyz0123456789";
            var builder = new StringBuilder();

            for (var i = 0; i < length; i++)
            {
                var c = pool[random.Next(0, pool.Length)];
                builder.Append(c);
            }

            return builder.ToString();
        }

        public string execQuery(string query)
        {
            string result = "";

            try
            {
                db.Database.ExecuteSqlCommand(query);
                db.SaveChanges();

                result = "Success";
            }
            catch
            {
                result = "Execute Query Error, Please Try Again";    
            }

            return result;
        }
    }
}