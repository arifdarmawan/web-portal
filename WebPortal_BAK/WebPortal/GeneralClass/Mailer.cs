﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using WebPortal.Models;

namespace WebPortal.GeneralClass
{
    public class Mailer
    {
        string _sender = "";
        string _password = "";

        public Mailer(string sender, string password)
        {
            _sender = sender;
            _password = password;
        }

        public void SettingEmail(string recipient, string subject, string message)
        {
            SmtpClient client = new SmtpClient("smtp.gmail.com");

            client.Port = 587;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential(_sender, _password);
            client.EnableSsl = true;
            client.Credentials = credentials;

            try
            {
                var mail = new MailMessage(_sender.Trim(), recipient.Trim());
                mail.Subject = subject;
                mail.Body = message;
                mail.IsBodyHtml = true;
                client.Send(mail);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        public static void SendMail(FormatEmailModel formatEmailModel)
        {
            var sender = new Mailer(formatEmailModel.EmailHost, formatEmailModel.EmailHostPassword);

            sender.SettingEmail(formatEmailModel.EmailRecipient, formatEmailModel.Subject, ConstructEmailBody(formatEmailModel));
        }

        static string ConstructEmailBody(FormatEmailModel formatEmailModel)
        {
            string body = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/EmailBody.htm"));

            body = body.Replace("#logoUrl#", formatEmailModel.LogoUrl);
            body = body.Replace("#link#", formatEmailModel.Link);
            body = body.Replace("#linkMessage#", formatEmailModel.LinkMessage);
            body = body.Replace("#message1#", formatEmailModel.Message1);
            body = body.Replace("#message2#", formatEmailModel.Message2);

            return body;
        }
    }
}