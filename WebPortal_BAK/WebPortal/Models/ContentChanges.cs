﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPortal.Models
{
    public class ContentChanges
    {
        public int Id { get; set; }
        public string Modul { get; set; }
        public string Type { get; set; }
        public int? NewsId { get; set; }
        public string Judul { get; set; }
        public string Caption { get; set; }
        public string Isi { get; set; }
        public int? CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public string Image { get; set; }
        public string ImagePath { get; set; }
        public string Attach { get; set; }
        public string AttachPath { get; set; }
        public int? FileId { get; set; }
        public string FileName { get; set; }
        public int? IdFileGroup { get; set; }
        public string FileGroup { get; set; }
        public string Description { get; set; }
        public string LocationFile { get; set; }
        public string TutorialCode { get; set; }
        public string TutorialTitle { get; set; }
        public string TutorialDescription { get; set; }
        public string VideoName { get; set; }
        public string VideoPath { get; set; }
        public string TutorialParent { get; set; }
        public string TutorialParentTitle { get; set; }
        public int? FAQId { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectName { get; set; }
        public int? TypeFAQ { get; set; }
        public string GroupFAQ { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string PostBy { get; set; }
        public DateTime PostDate { get; set; }
        public bool IsResponded { get; set; }
        public string Response { get; set; }
        public DateTime? RespondDate { get; set; }
    }
}