﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebPortal.Model;

namespace WebPortal.Models
{
    public class SharedModel
    {
    }

    public class NavigationBar
    {
        public List<T_MsMenu> listMenu { get; set; }
        public bool newFAQ { get; set; }
        public bool newTutorial { get; set; }
        public bool newDocument { get; set; }
        public bool newChanges { get; set; }
    }

    public class UserAccount
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public bool IsUserAD { get; set; }



        #region RecruitmentPersonal
        [Required(ErrorMessage = "This field is required")]
        public string Personal_Name { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public System.DateTime Personal_BirthDate { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        public string Personal_Email { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Personal_Gender { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Personal_Phone { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Personal_Address { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Personal_Country { get; set; }
        public string Personal_CountryName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Personal_City { get; set; }
        public string Personal_CityName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Personal_PostalCode { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Personal_Nationality { get; set; }
        public string Personal_NationalityName { get; set; }
        #endregion

        public List<Recruitment_Family> recruitmentFamily { get; set; }

        #region FormRecruitmentFamily
        public int? Family_Id { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Family_FamilyName { get; set; }

        public string Family_Gender { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Family_Relation { get; set; }        
        #endregion

        public List<Recruitment_Academy> recruitmentAcademy { get; set; }

        #region FormRecruitmentAcademy
        public int? Academy_Id { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Academy_Institute { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Academy_Location { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Academy_StudyFrom { get; set; }

        public string Academy_StudyTo { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Academy_Qualification { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Academy_Major { get; set; }

        public string Academy_CertificateFileName { get; set; }

        public string Academy_CertificateFilePath { get; set; }
        #endregion

        public List<Recruitment_Academy_NonFormal> recruitmentAcademy_NonFormal { get; set; }

        #region FormRecruitmentAcademy_NonFormal
        public int? Academy_NonFormal_Id { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Academy_NonFormal_Education { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Academy_NonFormal_Institute { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Academy_NonFormal_Location { get; set; }

        public string Academy_NonFormal_CertificateFileName { get; set; }

        public string Academy_NonFormal_CertificateFilePath { get; set; }
        #endregion

        public List<Recruitment_Experience> recruitmentExperience { get; set; }

        #region FormRecruitmentExperience
        public int? Experience_Id { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Experience_CompanyName { get; set; }

        public string Experience_Location { get; set; }

        public string Experience_State { get; set; }
        
        [Required(ErrorMessage = "This field is required")]
        public string Experience_PositionTitle { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Experience_PositionLevel { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Experience_MonthStart { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Experience_YearStart { get; set; }

        public string Experience_MonthEnd { get; set; }

        public string Experience_YearEnd { get; set; }

        public bool Experience_StillPresent { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Experience_Specialization { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Experience_Industry { get; set; }
        
        public string Experience_Salary { get; set; }

        public string Experience_SalaryCurrency { get; set; }
        
        public string Experience_ExperienceDescription { get; set; }
        #endregion

        public List<Recruitment_Skill> recruitmentSkill { get; set; }

        #region FormRecruitmentSkill
        public List<string> ListSkill { get; set; }
        #endregion

        public List<Recruitment_Document> recruitmentDocument { get; set; }

        #region FormRecruitmentDocument
        public int? Recruitment_Document_Id { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Recruitment_Document_Description { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Recruitment_Document_FileName { get; set; }
        
        public string Recruitment_Document_FilePath { get; set; }

        public string Recruitment_Document_Notes { get; set; }
        #endregion



        #region VendorCompany
        public int? CompanyId { get; set; }
        
        public string VendorNumber { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_CompanyName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_BusinessForm { get; set; }
        public string Vendor_BusinessFormDescription { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_BusinessField { get; set; }
        public string Vendor_BusinessFieldDescription { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Country { get; set; }
        public string Vendor_CountryName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_City { get; set; }
        public string Vendor_CityName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Address { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_PostalCode { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        public string Vendor_Email { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Phone { get; set; }

        public string Vendor_Fax { get; set; }

        public string Vendor_CompanyWeb { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_ContactPerson { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_CPNumber { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        public string Vendor_CPEmail { get; set; }
        #endregion

        public List<Vendor_Bank> vendorBank { get; set; }

        #region FormVendorBank
        public int? Bank_Id { get; set; }
        
        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Bank { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Branch { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_BankCountry { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_BankCity { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Currency { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_AccountNumber { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_AccountName { get; set; }
        #endregion

        #region VendorLegality
        [Required(ErrorMessage = "This field is required")]
        public string Vendor_SIUP { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Vendor_SIUPValidTo { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression(".*.pdf$", ErrorMessage = "File PDF only")]
        public string Vendor_SIUPDocumentName { get; set; }

        public string Vendor_SIUPDocumentPath { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_TDP { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Vendor_TDPValidTo { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression(".*.pdf$", ErrorMessage = "File PDF only")]
        public string Vendor_TDPDocumentName { get; set; }

        public string Vendor_TDPDocumentPath { get; set; }

        public string Vendor_SKDP { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> Vendor_SKDPValidTo { get; set; }

        [RegularExpression(".*.pdf$", ErrorMessage = "File PDF only")]
        public string Vendor_SKDPDocumentName { get; set; }

        public string Vendor_SKDPDocumentPath { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_DeedOfIncorporation { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression(".*.pdf$", ErrorMessage = "File PDF only")]
        public string Vendor_DoIDocumentName { get; set; }

        public string Vendor_DoIDocumentPath { get; set; }

        public string Vendor_CertificateOfEstablishment { get; set; }

        [RegularExpression(".*.pdf$", ErrorMessage = "File PDF only")]
        public string Vendor_CoEDocumentName { get; set; }

        public string Vendor_CoEDocumentPath { get; set; }

        public string Vendor_DeedOfChange { get; set; }

        [RegularExpression(".*.pdf$", ErrorMessage = "File PDF only")]
        public string Vendor_DoCDocumentName { get; set; }

        public string Vendor_DoCDocumentPath { get; set; }

        public string Vendor_CertificateOfChange { get; set; }

        [RegularExpression(".*.pdf$", ErrorMessage = "File PDF only")]
        public string Vendor_CoCDocumentName { get; set; }

        public string Vendor_CoCDocumentPath { get; set; }
        #endregion

        #region VendorTax
        [Required(ErrorMessage = "This field is required")]
        public string Vendor_NPWP { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_NPWPAddress { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression(".*.pdf$", ErrorMessage = "File PDF only")]
        public string Vendor_NPWPDocumentName { get; set; }

        public string Vendor_NPWPDocumentPath { get; set; }

        public string Vendor_SKPKP { get; set; }

        public string Vendor_SKPKPDocumentName { get; set; }

        public string Vendor_SKPKPDocumentPath { get; set; }

        public bool Vendor_PPn { get; set; }

        public bool Vendor_PPh { get; set; }
        #endregion

        public List<Vendor_Branches> vendorBranches { get; set; }

        #region FormVendorBranches
        public int? Branches_Id { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Branches_Country { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Branches_City { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Branches_Address { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Branches_PostalCode { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        public string Vendor_Branches_Email { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Branches_Phone { get; set; }

        public string Vendor_Branches_Fax { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Branches_ContactPerson { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Branches_CPNumber { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        public string Vendor_Branches_CPEmail { get; set; }
        #endregion

        public List<Vendor_Document> vendorDocument { get; set; }

        #region FormVendorDocument
        public int? Vendor_Document_Id { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Document_Description { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Document_FileName { get; set; }
        
        public string Vendor_Document_FilePath { get; set; }

        public string Vendor_Document_Notes { get; set; }
        #endregion

        public Boolean isSubmitted { get; set; }

        public string SubmitStatus { get; set; }



        public List<Vendor_Item> vendorItem { get; set; }

        #region FormVendorItem
        public int? Item_Id { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Item_Description { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Vendor_Item_UOM { get; set; }

        public string Vendor_Item_Notes { get; set; }
        #endregion
    }

    public class UserProfile
    {
        public T_MsUser userProfile { get; set; }
        public List<ListUserSiteMapping> listUserSite { get; set; }
    }

    public class ListUserSiteMapping
    {
        public int SiteCode { get; set; }
        public string SiteName { get; set; }
        public string SiteUserId { get; set; }
        public string SitePassword { get; set; }
    }
}