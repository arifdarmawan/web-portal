﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPortal.Models
{
    public class MenuMapping
    {
        public int UserLevel { get; set; }
        public string Description { get; set; }
        public List<ListMenuMapping> listMenu { get; set; }
    }

    public class ListMenuMapping
    {
        public string MenuCode { get; set; }
        public string MenuName { get; set; }
        public bool Checker { get; set; }
    }

    public class SubmitMenuMapping
    {
        public int UserLevel { get; set; }
        public string[] CheckedMenu { get; set; }
        public string[] UnCheckedMenu { get; set; }
    }

    public class SubmitMenu
    {
        public string MenuCode { get; set; }
    }
}