﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPortal.Model;

namespace WebPortal.Models
{
    public class ChangesView
    {
        public NewsWithCategory publishedNews { get; set; }
        public FilesWithGroup publishedFile { get; set; }
        public TutorialWithParent publishedTutorial { get; set; }
        public FAQProjectContent publishedFAQ { get; set; }
        public ContentChanges changes { get; set; }
    }
}