﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPortal.Models
{
    public class SearchModel
    {
        public List<FilesWithGroup> listFile { get; set; }
        public List<NewsWithCategory> listNews { get; set; }
        public List<TutorialWithParent> listTutorial { get; set; }
        public string stringSearch { get; set; }
    }
}