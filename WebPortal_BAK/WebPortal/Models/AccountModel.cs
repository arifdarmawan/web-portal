﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebPortal.Models
{
    public class AccountModel
    {

    }

    public class Recruitment_Personal
    {
        public string UserId { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public System.DateTime BirthDate { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        public string Email { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Address { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Country { get; set; }
        public string CountryName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string City { get; set; }
        public string CityName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Nationality { get; set; }
        public string NationalityName { get; set; }

        public Nullable<System.DateTime> CreatedDate { get; set; }

        public Nullable<System.DateTime> ModifyDate { get; set; }
    }

    public class Recruitment_Family
    {
        public int? FamilyId { get; set; }

        public string UserId { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Family_FamilyName { get; set; }

        public string Family_Gender { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Family_Relation { get; set; }
        public string Family_RelationDescription { get; set; }

        public Nullable<System.DateTime> CreatedDate { get; set; }

        public Nullable<System.DateTime> ModifyDate { get; set; }
    }

    public class Recruitment_Academy
    {
        public int? AcademyId { get; set; }

        public string UserId { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Institute { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Location { get; set; }
        public string LocationName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string StudyFrom { get; set; }

        public string StudyTo { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Qualification { get; set; }
        public string QualificationDescription { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Major { get; set; }
        public string MajorDescription { get; set; }

        public string CertificateFileName { get; set; }

        public string CertificateFilePath { get; set; }

        public Nullable<System.DateTime> CreatedDate { get; set; }

        public Nullable<System.DateTime> ModifyDate { get; set; }
    }
    
    public class Recruitment_Academy_NonFormal
    {
        public int? AcademyId { get; set; }

        public string UserId { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Education { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Institute { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Location { get; set; }
        public string LocationName { get; set; }

        public string CertificateFileName { get; set; }

        public string CertificateFilePath { get; set; }

        public Nullable<System.DateTime> CreatedDate { get; set; }

        public Nullable<System.DateTime> ModifyDate { get; set; }
    }

    public class Recruitment_Experience
    {
        public int ExperienceId { get; set; }

        public string UserId { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string PositionTitle { get; set; }

        public string Location { get; set; }
        public string LocationName { get; set; }

        public string State { get; set; }
        public string StateName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string PositionLevel { get; set; }
        public string PositionLevelDescription { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string MonthStart { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string YearStart { get; set; }
        
        public string MonthEnd { get; set; }
        
        public string YearEnd { get; set; }
        
        public bool StillPresent { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Specialization { get; set; }
        public string SpecializationDescription { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Industry { get; set; }
        public string IndustryDescription { get; set; }
        
        public string Salary { get; set; }

        public string SalaryCurrency { get; set; }
        public string SalaryCurrencyDescription { get; set; }
        
        public string ExperienceDescription { get; set; }
        
        public Nullable<System.DateTime> CreatedDate { get; set; }
        
        public Nullable<System.DateTime> ModifyDate { get; set; }
    }

    public class Recruitment_Skill
    {
        public int? SkillId { get; set; }
        
        public string UserId { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Skill { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Proficiency { get; set; }

        public bool isDelete { get; set; }
        
        public Nullable<System.DateTime> CreatedDate { get; set; }
        
        public Nullable<System.DateTime> ModifyDate { get; set; }
    }

    public class Recruitment_Document
    {
        public int? DocumentId { get; set; }

        public string UserId { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Description { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string FileName { get; set; }

        public string FilePath { get; set; }

        public string Notes { get; set; }
    }

    public class Vendor_Company
    {
        public int? CompanyId { get; set; }
        
        public string UserId { get; set; }
        
        public string VendorNumber { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string BusinessForm { get; set; }
        public string BusinessFormDescription { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string BusinessField { get; set; }
        public string BusinessFieldDescription { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Country { get; set; }
        public string CountryName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string City { get; set; }
        public string CityName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Address { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        public string Email { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Phone { get; set; }
        
        public string Fax { get; set; }
        
        public string CompanyWeb { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string ContactPerson { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string CPNumber { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        public string CPEmail { get; set; }
    }

    public class Vendor_Bank
    {
        public int? BankId { get; set; }
        
        public string UserId { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Bank { get; set; }
        public string BankName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Branch { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Country { get; set; }
        public string CountryName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string City { get; set; }
        public string CityName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Currency { get; set; }
        public string CurrencyDescription { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string AccountNumber { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string AccountName { get; set; }
    }

    public class Vendor_Legality
    {
        public int? CompanyId { get; set; }

        public string UserId { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string SIUP { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime SIUPValidTo { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string SIUPDocumentName { get; set; }

        public string SIUPDocumentPath { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string TDP { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime TDPValidTo { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string TDPDocumentName { get; set; }

        public string TDPDocumentPath { get; set; }

        public string SKDP { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> SKDPValidTo { get; set; }

        public string SKDPDocumentName { get; set; }

        public string SKDPDocumentPath { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string DeedOfIncorporation { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string DoIDocumentName { get; set; }

        public string DoIDocumentPath { get; set; }

        public string CertificateOfEstablishment { get; set; }

        public string CoEDocumentName { get; set; }

        public string CoEDocumentPath { get; set; }

        public string DeedOfChange { get; set; }

        public string DoCDocumentName { get; set; }

        public string DoCDocumentPath { get; set; }

        public string CertificateOfChange { get; set; }

        public string CoCDocumentName { get; set; }

        public string CoCDocumentPath { get; set; }
    }

    public class Vendor_Tax
    {
        public int? CompanyId { get; set; }

        public string UserId { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string NPWP { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string NPWPAddress { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string NPWPDocumentName { get; set; }

        public string NPWPDocumentPath { get; set; }

        public string SKPKP { get; set; }

        public string SKPKPDocumentName { get; set; }

        public string SKPKPDocumentPath { get; set; }

        public bool PPn { get; set; }

        public bool PPh { get; set; }
    }

    public class Vendor_Branches
    {
        public int? BranchesId { get; set; }
        
        public string UserId { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Country { get; set; }
        public string CountryName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string City { get; set; }
        public string CityName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Address { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        public string Email { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Phone { get; set; }
        
        public string Fax { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string ContactPerson { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string CPNumber { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        public string CPEmail { get; set; }
    }

    public class Vendor_Item
    {
        public int? ItemId { get; set; }
        
        public string UserId { get; set; }
        
        //public string SEGMENT1 { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string DESCRIPTION { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string PRIMARY_UOM_CODE { get; set; }
        public string PRIMARY_UOM_CODE_DESCRIPTION { get; set; }
        
        //public string INVOICEABLE_ITEM_FLAG { get; set; }
        //public string INVOICE_ENABLED_FLAG { get; set; }
        //public string ALLOW_ITEM_DESC_UPDATE_FLAG { get; set; }
        //public string ORGANIZATION_CODE { get; set; }
        //public string TRANSACTION_TYPE { get; set; }
        //public string PROCESS_FLAG { get; set; }
        //public string INVENTORY_ITEM_FLAG { get; set; }
        //public string STOCK_ENABLED_FLAG { get; set; }
        //public string MTL_TRANSACTIONS_ENABLED_FLAG { get; set; }
        //public string RESERVEABLE_TYPE { get; set; }
        //public string COSTING_ENABLED_FLAG { get; set; }
        //public string INVENTORY_ASSET_FLAG { get; set; }
        //public string PURCHASING_ITEM_FLAG { get; set; }
        //public string PURCHASING_ENABLED_FLAG { get; set; }
        //public string RECEIPT_REQUIRED_FLAG { get; set; }
        //public string MUST_USE_APPROVED_VENDOR_FLAG { get; set; }
        //public string CUSTOMER_ORDER_ENABLED_FLAG { get; set; }
        //public string CUSTOMER_ORDER_FLAG { get; set; }
        //public string SHIPPABLE_ITEM_FLAG { get; set; }
        //public string SO_TRANSACTIONS_FLAG { get; set; }
        //public string RETURNABLE_FLAG { get; set; }

        public string Notes { get; set; }
    }

    public class Vendor_Document
    {
        public int? DocumentId { get; set; }
        
        public string UserId { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Description { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string FileName { get; set; }
        
        public string FilePath { get; set; }
        
        public string Notes { get; set; }
    }

    public class Vendor_Submit
    {
        public string UserId { get; set; }

        public Boolean IsSubmitted { get; set; }

        public string Status { get; set; }
    }
}