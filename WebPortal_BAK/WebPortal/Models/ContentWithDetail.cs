﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebPortal.Model;

namespace WebPortal.Models
{
    public class ContentWithDetail
    {
    }
    
    public class FilesWithGroup
    {
        public int Id { get; set; }
        public int IdFileGroup { get; set; }
        public string FileGroup { get; set; }
        public string FileName { get; set; }
        public string Description { get; set; }
        public string FileLocation { get; set; }
        public bool IsChanged { get; set; }
    }

    public class NewsWithCategory
    {
        public int Id { get; set; }
        public string Judul { get; set; }
        public int CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public string Caption { get; set; }
        public string Isi { get; set; }
        public string Image { get; set; }
        public string ImagePath { get; set; }
        public string Attach { get; set; }
        public string AttachPath { get; set; }
        public string PostBy { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public bool IsChanged { get; set; }
    }

    public class TutorialWithParent
    {
        public int? Id { get; set; }
        public string TutorialCode { get; set; }
        public string TutorialTitle { get; set; }
        public string TutorialDescription { get; set; }
        public string VideoName { get; set; }
        public string VideoPath { get; set; }
        public string TutorialParent { get; set; }
        public string TutorialParentTitle { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public bool IsChanged { get; set; }
    }

    public class UserWithLevel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public int UserLevel { get; set; }
        public string Description { get; set; }
        public string Password { get; set; }
        public string Divisi { get; set; }
        public string Jabatan { get; set; }
        public string Region { get; set; }
        public bool IsTemporary { get; set; }
        public DateTime StartAccess { get; set; }
        public DateTime EndAccess { get; set; }
    }

    public class HelpQuestion
    {
        public int HelpId { get; set; }
        public string Topic { get; set; }
        public string Question { get; set; }
        public string PostBy { get; set; }
        public DateTime PostDate { get; set; }
        public List<HelpAnswer> listAnswer { get; set; }
        public bool IsClosed { get; set; }
    }

    public class HelpAnswer
    {
        public int Id { get; set; }
        public int HelpId { get; set; }
        public string Answer { get; set; }
        public string PostBy { get; set; }
        public DateTime PostDate { get; set; }
    }

    public class FeedbackTopic
    {
        public int ContactUsId { get; set; }
        public string Topic { get; set; }
        public string Feedback { get; set; }
        public string PostBy { get; set; }
        public DateTime PostDate { get; set; }
        public List<FeedbackReply> listReply { get; set; }
    }

    public class FeedbackReply
    {
        public int Id { get; set; }
        public int ContactUsId { get; set; }
        public string Reply { get; set; }
        public string PostBy { get; set; }
        public DateTime PostDate { get; set; }
    }

    public class FAQProject
    {
        public string ProjectCode { get; set; }
        public string ProjectName { get; set; }
        public string GroupFAQ { get; set; }
        public string URL { get; set; }
        public List<FAQProjectContent> listProjectContent { get; set; }
        public bool IsChanged { get; set; }
    }

    public class FAQProjectContent
    {
        public int Id { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectName { get; set; }
        public string GroupFAQ { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string PostedBy { get; set; }
        public DateTime PostedDate { get; set; }
        public bool IsChanged { get; set; }
    }

    public class FAQGroup
    {
        public string ProjectCode { get; set; }
        public string ProjectName { get; set; }
        public List<FAQGroupContent> listGroup { get; set; }
    }

    public class FAQGroupContent
    {
        public string GroupFAQ { get; set; }
        public string URL { get; set; }
    }

    public class MsVendor
    {
        public string UserId { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string NamaPerusahaan { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Alamat { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string AlamatSurat { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string SIUP { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? SIUPValidTo { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string TDP { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? TDPValidTo { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string SKDP { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? SKDPValidTo { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string PhoneNumber { get; set; }

        public string Fax { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        public string Email { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string NamaPJ { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string NomorKTPPJ { get; set; }

        public int? JumlahKaryawan { get; set; }

        public string BidangUsaha { get; set; }
        public string NamaBidangUsaha { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string AktaPendirian { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string SKPendirian { get; set; }

        public string AktaPerubahan { get; set; }

        public string SKPerubahan { get; set; }

        //[Required(ErrorMessage = "This field is required")]
        public string MasaPembayaran { get; set; }
        public string LamaMasaPembayaran { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string JenisUsaha { get; set; }
        public string NamaJenisUsaha { get; set; }

        public bool PPn { get; set; }

        public bool PPh { get; set; }

        public string NPWP { get; set; }

        public string SKPKP { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Bank { get; set; }
        public string NamaBank { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Cabang { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Kota { get; set; }
        public string NamaKota { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Negara { get; set; }
        public string NamaNegara { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string MataUang { get; set; }
        public string NamaMataUang { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Rekening { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string PemegangRekening { get; set; }

        public string Bank2 { get; set; }
        public string NamaBank2 { get; set; }

        public string Cabang2 { get; set; }

        public string Kota2 { get; set; }
        public string NamaKota2 { get; set; }

        public string Negara2 { get; set; }
        public string NamaNegara2 { get; set; }

        public string MataUang2 { get; set; }
        public string NamaMataUang2 { get; set; }

        public string Rekening2 { get; set; }

        public string PemegangRekening2 { get; set; }
    }

    public class MsVendor_Data
    {
        [Required(ErrorMessage = "Mohon Isi Nama Perusahaan")]
        public string NamaPerusahaan { get; set; }

        [Required(ErrorMessage = "Mohon Isi Alamat")]
        public string Alamat { get; set; }

        [Required(ErrorMessage = "Mohon Isi Alamat Surat")]
        public string AlamatSurat { get; set; }

        [Required(ErrorMessage = "Mohon Isi Nomor SIUP")]
        public string SIUP { get; set; }

        [Required(ErrorMessage = "Mohon Isi ")]
        public DateTime SIUPValidTo { get; set; }

        [Required(ErrorMessage = "Mohon Isi Nomor TDP")]
        public string TDP { get; set; }

        [Required(ErrorMessage = "Mohon Isi ")]
        public DateTime TDPValidTo { get; set; }

        [Required(ErrorMessage = "Mohon Isi Nomor SKDP")]
        public string SKDP { get; set; }

        [Required(ErrorMessage = "Mohon Isi ")]
        public DateTime SKDPValidTo { get; set; }

        [Required(ErrorMessage = "Mohon Isi Nomor Telepon")]
        public string PhoneNumber { get; set; }

        public string Fax { get; set; }

        [Required(ErrorMessage = "Mohon Isi Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Mohon Isi Nama Penanggung Jawab")]
        public string NamaPJ { get; set; }

        [Required(ErrorMessage = "Mohon Isi Nomor KTP")]
        public string NomorKTPPJ { get; set; }

        public int? JumlahKaryawan { get; set; }

        public string BidangUsaha { get; set; }

        [Required(ErrorMessage = "Mohon Isi Akta Pendirian")]
        public string AktaPendirian { get; set; }

        [Required(ErrorMessage = "Mohon Isi Nomor SK Pendirian")]
        public string SKPendirian { get; set; }

        public string AktaPerubahan { get; set; }

        public string SKPerubahan { get; set; }

        [Required(ErrorMessage = "Mohon Pilih Masa Pembayaran")]
        public string MasaPembayaran { get; set; }

        [Required(ErrorMessage = "Mohon Pilih Jenis Usaha")]
        public string JenisUsaha { get; set; }

        public bool PPn { get; set; }

        public bool PPh { get; set; }

        public string NPWP { get; set; }

        public string SKPKP { get; set; }
    }

    public class MsVendor_Bank
    {
        [Required(ErrorMessage = "Mohon Pilih Bank")]
        public string Bank { get; set; }

        [Required(ErrorMessage = "Mohon Isi Bank Cabang")]
        public string Cabang { get; set; }

        [Required(ErrorMessage = "Mohon Pilih Kota")]
        public string Kota { get; set; }

        [Required(ErrorMessage = "Mohon Pilih Negara")]
        public string Negara { get; set; }

        [Required(ErrorMessage = "Mohon Pilih Mata Uang")]
        public string MataUang { get; set; }

        [Required(ErrorMessage = "Mohon Isi Nomor Rekening")]
        public string Rekening { get; set; }

        [Required(ErrorMessage = "Mohon Isi Nama Pemegang Rekening")]
        public string PemegangRekening { get; set; }

        public string Bank2 { get; set; }

        public string Cabang2 { get; set; }
        
        public string Kota2 { get; set; }
        
        public string Negara2 { get; set; }
        
        public string MataUang2 { get; set; }
        
        public string Rekening2 { get; set; }
        
        public string PemegangRekening2 { get; set; }
    }
}