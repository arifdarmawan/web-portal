﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPortal.Models
{
    public class SiteMapping
    {
        public int UserLevel { get; set; }
        public string Description { get; set; }
        public List<ListSiteMapping> listSite { get; set; }
    }

    public class ListSiteMapping
    {
        public int SiteCode { get; set; }
        public string SiteName { get; set; }
        public bool Checker { get; set; }
    }

    public class SubmitSiteMapping
    {
        public int UserLevel { get; set; }
        public int[] CheckedSite { get; set; }
        public int[] UnCheckedSite { get; set; }
    }

    public class SubmitSite
    {
        public int SiteCode { get; set; }
    }
}