﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPortal.Model;

namespace WebPortal.Models
{
    public class PagingAndGroup
    {
        public List<T_MsSite> listSiteMenu { get; set; }

        public List<NewsWithCategory> listNews { get; set; }
        public List<T_News> listLatestNews { get; set; }
        public List<T_CommentNews> listComment { get; set; }
        public List<Grouping> listCategory { get; set; }
        public T_News news { get; set; }
        public T_CommentNews comment { get; set; }

        public List<TutorialWithParent> listTutorial { get; set; }
        public List<T_Tutorial> listLatestTutorial { get; set; }
        public List<Grouping> listTutorialProject { get; set; }

        public List<UserWithLevel> listUser { get; set; }

        public List<T_MsUserGroup> listUserGroup { get; set; }

        public List<T_MsProject> listProject { get; set; }

        public List<FeedbackTopic> listFeedback { get; set; }
        public T_ContactUs contactUs { get; set; }

        public List<HelpQuestion> listQuestion { get; set; }
        public List<T_Question> listLatestQuestion { get; set; }
        public HelpQuestion question { get; set; }
        public HelpAnswer questionDetail { get; set; }

        public int currentPage { get; set; }
        public int totalItem { get; set; }
        public int totalPages { get; set; }
        public int startPage { get; set; }
        public int endPage { get; set; }
        public bool toFirst { get; set; }
        public bool toLast { get; set; }
        public int startIndexItem { get; set; }

        public string stringSearch { get; set; }
    }
}