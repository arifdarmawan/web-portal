﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WebPortal.Models
{
    public class FormatEmailModel
    {
        public string EmailHost
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailHost"];
            }
        }
        public string EmailHostPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailHostPassword"];
            }
        }
        public string LogoUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["LogoUrl"];
            }
        }
        public string Subject { get; set; }
        public string EmailRecipient { get; set; }
        public string Message1 { get; set; }
        public string Message2 { get; set; }
        public string Link { get; set; }
        public string LinkMessage { get; set; }
    }
}