﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPortal.Models
{
    public class DropdownModel
    {
        public string stringValue { get; set; }
        public int intValue { get; set; }
        public string Text { get; set; }
    }
}