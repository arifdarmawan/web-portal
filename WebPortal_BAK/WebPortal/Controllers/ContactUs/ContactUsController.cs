﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPortal.GeneralClass;
using WebPortal.Model;
using WebPortal.Models;

namespace WebPortal.Controllers.Feedback
{
    [NoDirectAccess]
    [SessionTimeout]
    public class ContactUsController : Controller
    {
        //
        // GET: /Feedback/
        oGeneral ogeneral = new oGeneral();
        FrInternallEntities db = new FrInternallEntities();

        public ActionResult Index(int? Page)
        {
            int startIndex = 0;
            int loadPage = 0;

            if (Page != null)
            {
                loadPage = Page.HasValue ? Page.Value - 1 : 0;
                startIndex = 10 * loadPage;
            }

            string userId = Session["LogedUserId"].ToString();

            List<FeedbackTopic> listFeedback = (from feedback in db.T_ContactUs.Where(w => w.PostBy == userId)
                                                join user in db.T_MsUser on feedback.PostBy equals user.UserId
                                                select new FeedbackTopic
                                                {
                                                    ContactUsId = feedback.ContactUsId,
                                                    Topic = feedback.Topic,
                                                    Feedback = feedback.Feedback,
                                                    PostBy = user.UserName,
                                                    PostDate = feedback.PostDate.Value
                                                }).OrderByDescending(o => o.PostDate).ToList();

            int totalFeedback = listFeedback.Count;
            int totalPages = (totalFeedback / 10) + (totalFeedback % 10 > 0 ? 1 : 0);

            listFeedback = listFeedback.Skip(startIndex).Take(10).ToList();

            PagingAndGroup mod = new PagingAndGroup();
            mod.listFeedback = listFeedback;
            mod.stringSearch = "";
            mod.currentPage = loadPage + 1;
            mod.totalItem = totalFeedback;
            mod.totalPages = totalPages;

            mod = ogeneral.getPaging(mod);

            return View(mod);
        }

        public ActionResult SubmitFeedback(PagingAndGroup feed)
        {
            TempData["Message"] = "Feedback Submitted";

            feed.contactUs.PostBy = Session["LogedUserID"].ToString();
            feed.contactUs.PostDate = DateTime.Now;

            db.T_ContactUs.Add(feed.contactUs);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

    }
}
