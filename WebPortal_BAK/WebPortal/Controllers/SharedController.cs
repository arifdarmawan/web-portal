﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPortal.Model;
using WebPortal.Models;

namespace WebPortal.Controllers
{
    public class SharedController : Controller
    {
        //
        // GET: /Shared/
        FrInternallEntities db = new FrInternallEntities();

        public ActionResult TopMenuBar()
        {
            UserProfile profile = new UserProfile();

            if (Session["LogedUserId"] != null)
            {
                string userId = Session["LogedUserId"].ToString();

                profile.userProfile = db.T_MsUser.Where(w => w.UserId == userId).First();
                profile.listUserSite = (from UGSite in db.T_MsUserGroup_Site.Where(w => w.UserLevel == profile.userProfile.UserLevel)
                                        join site in db.T_MsSite on UGSite.SiteCode equals site.SiteCode
                                        join userSite in db.T_MsUser_Site.Where(w => w.UserId == userId) on site.SiteCode equals userSite.SiteCode into t
                                        from rt in t.DefaultIfEmpty()
                                        select new ListUserSiteMapping
                                        {
                                            SiteCode = UGSite.SiteCode,
                                            SiteName = site.SiteName,
                                            SiteUserId = rt.SiteUserId,
                                            SitePassword = rt.SitePassword
                                        }).ToList();
                //profile.userSite = db.T_MsUser_Site.Where(w => w.UserId == userId).ToList();
            }

            return PartialView(profile);
        }

        public ActionResult MenuBar()
        {
            int userLevel = (Session["UserLevel"] != null ? (int)Session["UserLevel"] : 100);

            List<string> listUserMenu = db.T_MsUserGroup_Menu.Where(ss => ss.UserLevel == userLevel).Select(ss => ss.MenuCode).ToList();
            List<T_MsMenu> listMenu = db.T_MsMenu.Where(ss => ss.IsActive && !ss.IsLoginRequired).Union(db.T_MsMenu.Where(ss => ss.IsActive && ss.IsLoginRequired && listUserMenu.Contains(ss.MenuCode))).OrderBy(o => o.MenuCode).ToList();

            NavigationBar mod = new NavigationBar();
            mod.listMenu = listMenu;
            mod.newFAQ = false;
            mod.newTutorial = false;
            mod.newDocument = false;
            mod.newChanges = false;

            if (Session["LogedUserId"] != null)
            {
                string userId = Session["LogedUserId"].ToString();

                T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();

                DateTime? lastAccessFAQ = dbUser.LastAccessFAQ;
                DateTime? lastAccessTutorial = dbUser.LastAccessTutorial;
                DateTime? lastAccessDocument = dbUser.LastAccessDocument;

                if (lastAccessFAQ != null)
                {
                    int countFAQ = db.T_FAQ.Where(w => w.CreatedDate > lastAccessFAQ || w.ModifyDate > lastAccessFAQ).Count();

                    if (countFAQ > 0)
                        mod.newFAQ = true;
                }
                else
                    mod.newFAQ = true;

                if (lastAccessTutorial != null)
                {
                    int countTutorial = db.T_Tutorial.Where(w => w.CreatedDate > lastAccessTutorial || w.ModifyDate > lastAccessTutorial).Count();

                    if (countTutorial > 0)
                        mod.newTutorial = true;
                }
                else
                    mod.newTutorial = true;

                if (lastAccessDocument != null)
                {
                    int countDocument = db.T_Files.Where(w => w.CreatedDate > lastAccessDocument).Count();

                    if (countDocument > 0)
                        mod.newDocument = true;
                }
                else
                    mod.newDocument = true;

                int countChanges = db.T_ContentChanges.Where(w => !w.IsResponded).Count();

                if (countChanges > 0)
                    mod.newChanges = true;
            }

            return PartialView(mod);
        }

        public ActionResult Footer()
        {
            PagingAndGroup mod = new PagingAndGroup();

            if (Session["UserLevel"] != null)
            {
                int userLevel = int.Parse(Session["UserLevel"].ToString());

                List<T_MsSite> listSite = (from userSite in db.T_MsUserGroup_Site.Where(w => w.UserLevel == userLevel)
                                           join msSite in db.T_MsSite.Where(w => w.IsShow) on userSite.SiteCode equals msSite.SiteCode
                                           select msSite).ToList();

                mod.listSiteMenu = listSite;
            }

            return PartialView(mod);
        }

    }
}
