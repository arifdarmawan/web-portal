﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPortal.GeneralClass;
using WebPortal.Model;
using WebPortal.Models;

namespace WebPortal.Controllers.Knowledge
{
    [NoDirectAccess]
    [SessionTimeout]
    public class HelpController : Controller
    {
        //
        // GET: /Help/
        oGeneral ogeneral = new oGeneral();
        FrInternallEntities db = new FrInternallEntities();

        public ActionResult Index(string stringSearch, int? Page)
        {
            int startIndex = 0;
            int loadPage = 0;

            if (Page != null)
            {
                loadPage = Page.HasValue ? Page.Value - 1 : 0;
                startIndex = 10 * loadPage;
            }

            List<HelpQuestion> listQuestion = (from help in db.T_Question
                                               join user in db.T_MsUser on help.PostBy equals user.UserId
                                               select new HelpQuestion
                                               {
                                                   HelpId = help.Id,
                                                   Topic = help.Topic,
                                                   Question = help.Question,
                                                   PostBy = user.UserName,
                                                   PostDate = help.PostDate.Value,
                                                   IsClosed = help.IsClosed
                                               }).OrderByDescending(o => o.PostDate).ToList();

            if (stringSearch != null)
            {
                listQuestion = listQuestion.Where(w => w.Question.ToLower().Contains(stringSearch.ToLower()) || w.Topic.ToLower().Contains(stringSearch.ToLower()) || w.PostBy.ToLower().Contains(stringSearch.ToLower())).ToList();
            }

            int totalQuestion = listQuestion.ToList().Count;
            int totalPages = (totalQuestion / 10) + (totalQuestion % 10 > 0 ? 1 : 0);

            listQuestion = listQuestion.Skip(startIndex).Take(10).ToList();

            List<T_Question> latestQuestion = db.T_Question.OrderByDescending(o => o.PostDate).Take(3).ToList();

            PagingAndGroup mod = new PagingAndGroup();
            mod.listQuestion = listQuestion;
            mod.listLatestQuestion = latestQuestion;
            mod.stringSearch = "";
            mod.currentPage = loadPage + 1;
            mod.totalItem = totalQuestion;
            mod.totalPages = totalPages;

            mod = ogeneral.getPaging(mod);

            return View(mod);
        }

        [HttpGet]
        public ActionResult ViewFAQ(int Id)
        {
            HelpQuestion question = (from help in db.T_Question.Where(w => w.Id == Id)
                                     join user in db.T_MsUser on help.PostBy equals user.UserId
                                     select new HelpQuestion
                                     {
                                         HelpId = help.Id,
                                         Topic = help.Topic,
                                         Question = help.Question,
                                         PostBy = user.UserName,
                                         PostDate = help.PostDate.Value,
                                         IsClosed = help.IsClosed
                                     }).First();

            List<HelpAnswer> listQuestionDetail = (from answer in db.T_QuestionDetail.Where(w => w.QuestionId == Id)
                                                   join user in db.T_MsUser on answer.PostBy equals user.UserId
                                                   select new HelpAnswer
                                                   {
                                                       Id = answer.Id,
                                                       HelpId = answer.QuestionId,
                                                       Answer = answer.Answer,
                                                       PostBy = user.UserName,
                                                       PostDate = answer.PostDate.Value
                                                   }).OrderBy(o => o.PostDate).ToList();
                                                     
            question.listAnswer = listQuestionDetail;

            PagingAndGroup mod = new PagingAndGroup();
            mod.question = question;

            return View(mod);
        }

        [HttpPost]
        public ActionResult Answer(PagingAndGroup ans)
        {
            T_QuestionDetail questionDetail = new T_QuestionDetail();
            questionDetail.QuestionId = ans.questionDetail.HelpId;
            questionDetail.Answer = ans.questionDetail.Answer.Replace(System.Environment.NewLine, ";");
            questionDetail.PostBy = Session["LogedUserId"].ToString();
            questionDetail.PostDate = DateTime.Now;

            db.T_QuestionDetail.Add(questionDetail);
            db.SaveChanges();

            return RedirectToAction("ViewFAQ", new { Id = ans.questionDetail.HelpId });
        }

    }
}
