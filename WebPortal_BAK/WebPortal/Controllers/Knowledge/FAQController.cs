﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPortal.GeneralClass;
using WebPortal.Model;
using WebPortal.Models;

namespace WebPortal.Controllers.Knowledge
{
    [NoDirectAccess]
    [SessionTimeout]
    public class FAQController : Controller
    {
        //
        // GET: /FAQ/
        FrInternallEntities db = new FrInternallEntities();

        public ActionResult Index()
        {
            List<T_MsProject> listProject = (from project in db.T_MsProject
                                             join faq in db.T_FAQ on project.ProjectCode equals faq.ProjectCode
                                             select project).Distinct().ToList();

            string userId = Session["LogedUserId"].ToString();
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();
            dbUser.LastAccessFAQ = DateTime.Now;
            db.SaveChanges();

            return View(listProject);
        }

        public ActionResult ProjectIndex(string projectCode)
        {
            List<FAQGroupContent> listFAQGroupContent = (from faq in db.T_FAQ.Where(w => w.ProjectCode == projectCode)
                                                         select new FAQGroupContent
                                                         {
                                                             GroupFAQ = faq.GroupFAQ,
                                                             URL = projectCode + "%2B" + faq.GroupFAQ
                                                         }).Distinct().ToList();

            FAQGroup listFAQGroup = new FAQGroup();
            listFAQGroup.ProjectCode = projectCode;
            listFAQGroup.ProjectName = db.T_MsProject.Where(w => w.ProjectCode == projectCode).Select(s => s.ProjectName).First();
            listFAQGroup.listGroup = listFAQGroupContent;

            return View(listFAQGroup);
        }

        public ActionResult ProjectFAQContent(string faqCode)
        {
            string projectCode = faqCode.Substring(0, faqCode.IndexOf("+"));
            string groupFAQ = faqCode.Substring(faqCode.IndexOf("+") + 1);

            List<FAQProjectContent> listFAQContent = (from faq in db.T_FAQ.Where(w => w.ProjectCode == projectCode && w.GroupFAQ == groupFAQ)
                                                      select new FAQProjectContent
                                                      {
                                                          Id = faq.Id,
                                                          ProjectCode = faq.ProjectCode,
                                                          GroupFAQ = faq.GroupFAQ,
                                                          Question = faq.Q,
                                                          Answer = faq.A,
                                                          PostedBy = faq.ModifyBy == null ? faq.CreatedBy : faq.ModifyBy,
                                                          PostedDate = faq.ModifyDate.Value == null ? faq.CreatedDate : faq.ModifyDate.Value
                                                      }).OrderBy(o => o.Id).ToList();

            FAQProject listFAQ = new FAQProject();
            listFAQ.ProjectCode = projectCode;
            listFAQ.ProjectName = db.T_MsProject.Where(w => w.ProjectCode == projectCode).Select(s => s.ProjectName).First();
            listFAQ.GroupFAQ = groupFAQ;
            listFAQ.listProjectContent = listFAQContent;

            return View(listFAQ);
        }

        public ActionResult ListFAQ()
        {
            List<FAQProject> listFAQ = (from faq in db.T_FAQ
                                        join project in db.T_MsProject on faq.ProjectCode equals project.ProjectCode
                                        join changes in db.T_ContentChanges.Where(w => w.FAQId != null && !w.IsResponded) on faq.Id equals changes.FAQId into t
                                        from rt in t.DefaultIfEmpty()
                                        select new FAQProject
                                        {
                                            ProjectCode = faq.ProjectCode,
                                            ProjectName = project.ProjectName,
                                            GroupFAQ = faq.GroupFAQ,
                                            URL = faq.ProjectCode + "%2B" + faq.GroupFAQ,
                                            IsChanged = rt.FAQId == null ? false : true
                                        }).Where(w => !w.IsChanged).Distinct().OrderBy(o => o.ProjectName).ToList();

            return View(listFAQ);
        }

        public ActionResult ListFAQContent(string faqCode)
        {
            if (faqCode != null)
            {
                Session["faqCode"] = faqCode;
            }
            else
            {
                if (Session["faqCode"] != null)
                {
                    faqCode = Session["faqCode"].ToString();
                }
                else
                {
                    return RedirectToAction("ListFAQ");
                }
            }

            string projectCode = faqCode.Substring(0, faqCode.IndexOf("+"));
            string groupFAQ = faqCode.Substring(faqCode.IndexOf("+") + 1);

            List<FAQProjectContent> listFAQContent = (from faq in db.T_FAQ.Where(w => w.ProjectCode == projectCode && w.GroupFAQ == groupFAQ)
                                                      join changes in db.T_ContentChanges.Where(w => w.FAQId != null && !w.IsResponded) on faq.Id equals changes.FAQId into t
                                                      from rt in t.DefaultIfEmpty()
                                                      select new FAQProjectContent
                                                      {
                                                          Id = faq.Id,
                                                          ProjectCode = faq.ProjectCode,
                                                          GroupFAQ = faq.GroupFAQ,
                                                          Question = faq.Q,
                                                          Answer = faq.A,
                                                          PostedBy = faq.ModifyBy == null ? faq.CreatedBy : faq.ModifyBy,
                                                          PostedDate = faq.ModifyDate.Value == null ? faq.CreatedDate : faq.ModifyDate.Value,
                                                          IsChanged = rt.FAQId == null ? false : true
                                                      }).Where(w => !w.IsChanged).OrderBy(o => o.Id).ToList();

            FAQProject listFAQ = new FAQProject();
            listFAQ.ProjectCode = projectCode;
            listFAQ.ProjectName = db.T_MsProject.Where(w => w.ProjectCode == projectCode).Select(s => s.ProjectName).First();
            listFAQ.GroupFAQ = groupFAQ;
            listFAQ.listProjectContent = listFAQContent;

            return View(listFAQ);
        }

        [HttpGet]
        public ActionResult EditFAQContent(int Id)
        {
            T_FAQ dbFAQ = db.T_FAQ.Where(w => w.Id == Id).First();

            List<DropdownModel> ProjectList = (from project in db.T_MsProject
                                               select new DropdownModel
                                               {
                                                   stringValue = project.ProjectCode,
                                                   Text = project.ProjectName
                                               }).ToList();
            ViewBag.ProjectList = new SelectList(ProjectList, "stringValue", "Text");

            List<DropdownModel> GroupList = new List<DropdownModel>();
            GroupList.Add(new DropdownModel { stringValue = "Master", Text = "Master" });
            GroupList.Add(new DropdownModel { stringValue = "Transaction", Text = "Transaction" });
            GroupList.Add(new DropdownModel { stringValue = "Process", Text = "Process" });
            GroupList.Add(new DropdownModel { stringValue = "Report", Text = "Report" });
            GroupList.Add(new DropdownModel { stringValue = "Other", Text = "Other" });
            ViewBag.GroupList = new SelectList(GroupList, "stringValue", "Text");

            return View(dbFAQ);
        }

        [HttpPost]
        public ActionResult EditFAQContent(T_FAQ faqContent)
        {
            if (ModelState.IsValid)
            {
                List<T_MsApproval> listApproval = db.T_MsApproval.ToList();

                int userLevel = int.Parse(Session["UserLevel"].ToString());

                if (listApproval.Any(a => a.UserLevel == userLevel))
                {
                    T_FAQ dbFAQ = db.T_FAQ.Where(w => w.Id == faqContent.Id).First();

                    dbFAQ.ProjectCode = faqContent.ProjectCode;
                    dbFAQ.GroupFAQ = faqContent.GroupFAQ;
                    dbFAQ.Q = faqContent.Q;
                    dbFAQ.A = faqContent.A;
                    dbFAQ.ModifyBy = Session["LogedUserId"].ToString();
                    dbFAQ.ModifyDate = DateTime.Now;

                    TempData["Message"] = "FAQ Updated";
                }
                else
                {
                    T_ContentChanges changes = new T_ContentChanges();
                    changes.Modul = "FAQ";
                    changes.Type = "Update";
                    changes.FAQId = faqContent.Id;
                    changes.ProjectCode = faqContent.ProjectCode;
                    changes.GroupFAQ = faqContent.GroupFAQ;
                    changes.Question = faqContent.Q;
                    changes.Answer = faqContent.A;
                    changes.PostBy = Session["LogedUserID"].ToString();
                    changes.PostDate = DateTime.Now;

                    TempData["Message"] = "FAQ Updated, Waiting for Approval By SuperAdmin";
                    db.T_ContentChanges.Add(changes);
                }

                db.SaveChanges();

                return RedirectToAction("ListFAQContent");
            }

            return View(faqContent);
        }

        [HttpGet]
        public ActionResult AddFAQContent()
        {
            List<DropdownModel> ProjectList = (from project in db.T_MsProject
                                               select new DropdownModel
                                               {
                                                   stringValue = project.ProjectCode,
                                                   Text = project.ProjectName
                                               }).ToList();
            ViewBag.ProjectList = new SelectList(ProjectList, "stringValue", "Text");

            List<DropdownModel> GroupList = new List<DropdownModel>();
            GroupList.Add(new DropdownModel { stringValue = "Master", Text = "Master" });
            GroupList.Add(new DropdownModel { stringValue = "Transaction", Text = "Transaction" });
            GroupList.Add(new DropdownModel { stringValue = "Process", Text = "Process" });
            GroupList.Add(new DropdownModel { stringValue = "Report", Text = "Report" });
            GroupList.Add(new DropdownModel { stringValue = "Other", Text = "Other" });
            ViewBag.GroupList = new SelectList(GroupList, "stringValue", "Text");

            return View();
        }

        [HttpPost]
        public ActionResult AddFAQContent(T_FAQ faqContent)
        {
            if (ModelState.IsValid)
            {
                List<T_MsApproval> listApproval = db.T_MsApproval.ToList();

                int userLevel = int.Parse(Session["UserLevel"].ToString());

                if (listApproval.Any(a => a.UserLevel == userLevel))
                {
                    T_FAQ newFAQ = new T_FAQ();

                    newFAQ.ProjectCode = faqContent.ProjectCode;
                    newFAQ.GroupFAQ = faqContent.GroupFAQ;
                    newFAQ.Q = faqContent.Q;
                    newFAQ.A = faqContent.A;
                    newFAQ.CreatedBy = Session["LogedUserId"].ToString();
                    newFAQ.CreatedDate = DateTime.Now;

                    db.T_FAQ.Add(newFAQ);
                    TempData["Message"] = "FAQ Added";
                }
                else
                {
                    T_ContentChanges changes = new T_ContentChanges();
                    changes.Modul = "FAQ";
                    changes.Type = "Add";
                    changes.ProjectCode = faqContent.ProjectCode;
                    changes.GroupFAQ = faqContent.GroupFAQ;
                    changes.Question = faqContent.Q;
                    changes.Answer = faqContent.A;
                    changes.PostBy = Session["LogedUserID"].ToString();
                    changes.PostDate = DateTime.Now;

                    TempData["Message"] = "FAQ Added, Waiting for Approval By SuperAdmin";
                    db.T_ContentChanges.Add(changes);
                }
                
                db.SaveChanges();

                return RedirectToAction("ListFAQ");
            }

            return View(faqContent);
        }

        public ActionResult DeleteFAQContent(int Id)
        {
            var dbFAQ = db.T_FAQ.Where(w => w.Id == Id).First();
            List<T_MsApproval> listApproval = db.T_MsApproval.ToList();

            int userLevel = int.Parse(Session["UserLevel"].ToString());

            if (listApproval.Any(a => a.UserLevel == userLevel))
            {
                db.T_FAQ.Remove(dbFAQ);
                TempData["Message"] = "FAQ Deleted";
            }
            else
            {
                T_ContentChanges changes = new T_ContentChanges();
                changes.Modul = "FAQ";
                changes.Type = "Delete";
                changes.FAQId = dbFAQ.Id;
                changes.ProjectCode = dbFAQ.ProjectCode;
                changes.GroupFAQ = dbFAQ.GroupFAQ;
                changes.Question = dbFAQ.Q;
                changes.Answer = dbFAQ.A;
                changes.PostBy = Session["LogedUserID"].ToString();
                changes.PostDate = DateTime.Now;

                TempData["Message"] = "FAQ Deleted, Waiting for Approval By SuperAdmin";
                db.T_ContentChanges.Add(changes);
            }

            db.SaveChanges();
            
            return RedirectToAction("ListFAQContent");
        }

        [HttpGet]
        public ActionResult CreateProject()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateProject(T_MsProject project)
        {
            if (ModelState.IsValid)
            {
                T_MsProject newProject = new T_MsProject();
                newProject.ProjectCode = project.ProjectCode;
                newProject.ProjectName = project.ProjectName;
                newProject.CreatedBy = Session["LogedUserId"].ToString();
                newProject.CreatedDate = DateTime.Now;

                db.T_MsProject.Add(newProject);
                db.SaveChanges();

                TempData["Message"] = "Project Added";
                return RedirectToAction("ListFAQ");
            }

            return View(project);
        }
    }
}
