﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPortal.Model;

namespace WebPortal.Controllers.SOP
{
    public class HumanResourceController : Controller
    {
        //
        // GET: /HumanResource/
        FrInternallEntities db = new FrInternallEntities();

        public ActionResult Index()
        {
            List<T_MsMenuContent> listMenuContent = db.T_MsMenuContent.ToList();

            return View(listMenuContent);
        }

    }
}
