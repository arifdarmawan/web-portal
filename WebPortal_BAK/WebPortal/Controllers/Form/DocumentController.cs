﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using WebPortal.GeneralClass;
using WebPortal.Model;
using WebPortal.Models;

namespace WebPortal.Controllers.Form
{
    [NoDirectAccess]
    [SessionTimeout]
    public class DocumentController : Controller
    {
        //
        // GET: /Document/
        FrInternallEntities db = new FrInternallEntities();

        public ActionResult Index(string stringSearch)
        {
            List<FilesWithGroup> listFile = (from file in db.T_Files
                                                    join fileGroup in db.T_MsFileGroup on file.FileGroup equals fileGroup.FileGroup
                                                    select new FilesWithGroup
                                                    {
                                                        Id = file.ID,
                                                        IdFileGroup = fileGroup.FileGroup,
                                                        FileGroup = fileGroup.Description,
                                                        FileName = file.NameFile,
                                                        Description = file.Description,
                                                        FileLocation = file.LocationFile
                                                    }).ToList();

            if (stringSearch != null)
            {
                listFile = listFile.Where(w => w.FileName.ToLower().Contains(stringSearch.ToLower()) || w.Description.ToLower().Contains(stringSearch.ToLower())).ToList();
            }

            string userId = Session["LogedUserId"].ToString();
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();
            dbUser.LastAccessDocument = DateTime.Now;
            db.SaveChanges();

            SearchModel mod = new SearchModel();
            mod.listFile = listFile;
            mod.stringSearch = "";

            return View(mod);
        }

        public ActionResult ListDocument(string stringSearch)
        {
            List<FilesWithGroup> listFile = (from file in db.T_Files
                                                    join fileGroup in db.T_MsFileGroup on file.FileGroup equals fileGroup.FileGroup
                                                    join changes in db.T_ContentChanges.Where(w => w.FileId != null && !w.IsResponded) on file.ID equals changes.FileId into t
                                                    from rt in t.DefaultIfEmpty()
                                                    select new FilesWithGroup
                                                    {
                                                        Id = file.ID,
                                                        IdFileGroup = fileGroup.FileGroup,
                                                        FileGroup = fileGroup.Description,
                                                        FileName = file.NameFile,
                                                        Description = file.Description,
                                                        FileLocation = file.LocationFile,
                                                        IsChanged = rt.FileId == null ? false : true
                                                    }).Where(w => !w.IsChanged).ToList();

            if (stringSearch != null)
            {
                listFile = listFile.Where(w => w.FileName.ToLower().Contains(stringSearch.ToLower()) || w.Description.ToLower().Contains(stringSearch.ToLower())).ToList();
            }

            SearchModel mod = new SearchModel();
            mod.listFile = listFile;
            mod.stringSearch = "";

            return View(mod);
        }

        public ActionResult DownloadFile(string urlPath)
        {
            int lastIndex = urlPath.LastIndexOf("/") + 1;
            string fileName = urlPath.Substring(lastIndex, urlPath.Length - lastIndex);
            string fileBytes = urlPath;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult DeleteFile(int FileId)
        {
            var file = db.T_Files.Where(a => a.ID.Equals(FileId)).FirstOrDefault();
            List<T_MsApproval> listApproval = db.T_MsApproval.ToList();

            int userLevel = int.Parse(Session["UserLevel"].ToString());

            if (listApproval.Any(a => a.UserLevel == userLevel))
            {
                System.IO.File.Delete(WebConfigurationManager.AppSettings["DocumentPathFolder"] + file.LocationFile.Substring(15, 16) + file.NameFile);
                Directory.Delete(WebConfigurationManager.AppSettings["DocumentPathFolder"] + file.LocationFile.Substring(15, 15));

                db.T_Files.Remove(file);
                TempData["Message"] = "File Deleted";
            }
            else
            {
                T_ContentChanges changes = new T_ContentChanges();
                changes.Modul = "Document";
                changes.Type = "Delete";
                changes.FileId = file.ID;
                changes.FileName = file.NameFile;
                changes.FileGroup = file.FileGroup;
                changes.Description = file.Description;
                changes.LocationFile = file.LocationFile;
                changes.PostBy = Session["LogedUserID"].ToString();
                changes.PostDate = DateTime.Now;

                db.T_ContentChanges.Add(changes);
                TempData["Message"] = "File Deleted, Waiting for Approval By SuperAdmin";
            }

            db.SaveChanges();
            return RedirectToAction("ListDocument");
        }

        [HttpGet]
        public ActionResult Upload()
        {
            List<DropdownModel> GroupList = (from filegroup in db.T_MsFileGroup
                                              select new DropdownModel
                                              {
                                                  intValue = filegroup.FileGroup,
                                                  Text = filegroup.Description
                                              }).ToList();
            ViewBag.GroupList = new SelectList(GroupList, "intValue", "Text");

            return View();
        }

        [HttpPost]
        public ActionResult Upload(T_Files file, HttpPostedFileBase uploadFile)
        {
            if (file != null && uploadFile != null)
            {
                if (uploadFile.ContentLength > 0 && uploadFile.ContentLength <= 10000000)
                {
                    string Year = DateTime.Now.Year.ToString(), Month = "0" + DateTime.Now.Month.ToString(), Day = "0" + DateTime.Now.Day.ToString(), Hour = "0" + DateTime.Now.Hour.ToString(), Minute = "0" + DateTime.Now.Minute.ToString(), Second = "0" + DateTime.Now.Second.ToString();
                    string Time = Year + Month.Substring(Month.Length - 2) + Day.Substring(Day.Length - 2) + "_" + Hour.Substring(Hour.Length - 2) + Minute.Substring(Minute.Length - 2) + Second.Substring(Second.Length - 2);

                    Directory.CreateDirectory(WebConfigurationManager.AppSettings["DocumentPathFolder"] + Time);
                    List<T_MsApproval> listApproval = db.T_MsApproval.ToList();

                    int userLevel = int.Parse(Session["UserLevel"].ToString());

                    if (listApproval.Any(a => a.UserLevel == userLevel))
                    {
                        var filename = Path.GetFileName(uploadFile.FileName);
                        var path = Path.Combine(Server.MapPath("~/Content/files/" + Time), filename);
                        uploadFile.SaveAs(path);

                        T_Files files = new T_Files();
                        files.NameFile = file.NameFile;
                        files.FileGroup = file.FileGroup;
                        files.Description = file.Description;
                        files.LocationFile = "/Content/files/" + Time + "/" + filename;
                        files.CreatedBy = Session["LogedUserID"].ToString();
                        files.CreatedDate = DateTime.Now;
                        db.T_Files.Add(files);
                        TempData["Message"] = "File Uploaded";
                    }
                    else
                    {
                        var filename = Path.GetFileName(uploadFile.FileName);
                        var path = Path.Combine(Server.MapPath("~/Content/files/" + Time), filename);
                        uploadFile.SaveAs(path);

                        T_ContentChanges files = new T_ContentChanges();
                        files.Modul = "Document";
                        files.Type = "Add";
                        files.FileName = file.NameFile;
                        files.FileGroup = file.FileGroup;
                        files.Description = file.Description;
                        files.LocationFile = "/Content/files/" + Time + "/" + filename;
                        files.PostBy = Session["LogedUserID"].ToString();
                        files.PostDate = DateTime.Now;
                        db.T_ContentChanges.Add(files);
                        TempData["Message"] = "File Uploaded, Waiting for Approval By SuperAdmin";
                    }

                    db.SaveChanges();
                }
                else
                {
                    ModelState.AddModelError("NameFile", "Maximal file size is 10mb");

                    List<DropdownModel> GroupList = (from filegroup in db.T_MsFileGroup
                                                     select new DropdownModel
                                                     {
                                                         intValue = filegroup.FileGroup,
                                                         Text = filegroup.Description
                                                     }).ToList();
                    ViewBag.GroupList = new SelectList(GroupList, "intValue", "Text");

                    return View(file);
                }
            }

            return RedirectToAction("ListDocument");
        }

    }
}
