﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using WebPortal.GeneralClass;
using WebPortal.Model;
using WebPortal.Models;

namespace WebPortal.Controllers
{
    //[NoDirectAccess]
    [SessionTimeout]
    public class MyAccountController : Controller
    {
        //
        // GET: /MyAccount/
        FrInternallEntities db = new FrInternallEntities();

        public ActionResult Index()
        {
            if (Session["LogedUserId"] == null)
            {
                return RedirectToAction("LoginPage");
            }

            UserProfile profile = new UserProfile();
            string userId = Session["LogedUserId"].ToString();

            profile.userProfile = db.T_MsUser.Where(w => w.UserId == userId).First();

            return View(profile);
        }

        public ActionResult SideBar()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            account.IsUserAD = db.T_MsUser.Where(w => w.UserId == userId).Select(s => s.IsUserAD).First();

            return PartialView(account);
        }



        public ActionResult EmployeeData()
        {
            UserProfile profile = new UserProfile();
            string userId = Session["LogedUserId"].ToString();

            profile.userProfile = db.T_MsUser.Where(w => w.UserId == userId).First();
            profile.listUserSite = (from UGSite in db.T_MsUserGroup_Site.Where(w => w.UserLevel == profile.userProfile.UserLevel)
                                    join site in db.T_MsSite on UGSite.SiteCode equals site.SiteCode
                                    join userSite in db.T_MsUser_Site.Where(w => w.UserId == userId) on site.SiteCode equals userSite.SiteCode into t
                                    from rt in t.DefaultIfEmpty()
                                    select new ListUserSiteMapping
                                    {
                                        SiteCode = UGSite.SiteCode,
                                        SiteName = site.SiteName,
                                        SiteUserId = rt.SiteUserId,
                                        SitePassword = rt.SitePassword
                                    }).ToList();

            return View(profile);
        }



        public ActionResult ApplicantPersonal()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();
            Recruitment_Personal recruitmentData = (from recruitment in db.T_Recruitment.Where(w => w.UserId == userId)
                                                    join country in db.T_MsParameter.Where(w => w.FlagId == 1) on recruitment.Country equals country.ParameterCode
                                                    join city in db.T_MsParameter.Where(w => w.FlagId == 2) on new { JoinProperty1 = recruitment.Country, JoinProperty2 = recruitment.City } equals new { JoinProperty1 = city.ParentCode, JoinProperty2 = city.ParameterCode }
                                                    join nationality in db.T_MsParameter.Where(w => w.FlagId == 1) on recruitment.Nationality equals nationality.ParameterCode
                                                    select new Recruitment_Personal
                                                    {
                                                        Name = recruitment.Name,
                                                        BirthDate = recruitment.BirthDate,
                                                        Email = recruitment.Email,
                                                        Gender = recruitment.Gender,
                                                        Phone = recruitment.Phone,
                                                        Address = recruitment.Address,
                                                        Country = recruitment.Country,
                                                        CountryName = country.Description,
                                                        City = recruitment.City,
                                                        CityName = city.Description,
                                                        PostalCode = recruitment.PostalCode,
                                                        Nationality = recruitment.Nationality,
                                                        NationalityName = nationality.Description
                                                    }).FirstOrDefault();

            account.Username = dbUser.UserName;
            account.Email = dbUser.email;

            if (recruitmentData != null)
            {
                account.Personal_Name = recruitmentData.Name;
                account.Personal_BirthDate = recruitmentData.BirthDate;
                account.Personal_Email = recruitmentData.Email;
                account.Personal_Gender = recruitmentData.Gender;
                account.Personal_Phone = recruitmentData.Phone;
                account.Personal_Address = recruitmentData.Address;
                account.Personal_Country = recruitmentData.Country;
                account.Personal_CountryName = recruitmentData.CountryName;
                account.Personal_City = recruitmentData.City;
                account.Personal_CityName = recruitmentData.CityName;
                account.Personal_PostalCode = recruitmentData.PostalCode;
                account.Personal_Nationality = recruitmentData.Nationality;
                account.Personal_NationalityName = recruitmentData.NationalityName;
            }

            List<DropdownModel> ListCountry = (from param in db.T_MsParameter.Where(w => w.FlagId == 1)
                                               select new DropdownModel
                                               {
                                                   stringValue = param.ParameterCode,
                                                   Text = param.Description
                                               }).OrderBy(o => o.Text).ToList();
            ViewBag.ListCountry = new SelectList(ListCountry, "stringValue", "Text");

            List<DropdownModel> ListCity = new List<DropdownModel>();
            if (account.Personal_Country != null)
            {
                ListCity = (from param in db.T_MsParameter.Where(w => w.FlagId == 2 && w.ParentCode == account.Personal_Country)
                            select new DropdownModel
                            {
                                stringValue = param.ParameterCode,
                                Text = param.Description
                            }).OrderBy(o => o.Text).ToList();
            }
            ViewBag.ListCity = new SelectList(ListCity, "stringValue", "Text");

            return View(account);
        }

        public ActionResult SaveChanges_ApplicantPersonal(UserAccount account)
        {
            string userId = Session["LogedUserId"].ToString();
            T_Recruitment dbRecruitment = db.T_Recruitment.Where(w => w.UserId == userId).FirstOrDefault();

            if (dbRecruitment != null)
            {
                dbRecruitment.Name = account.Personal_Name;
                dbRecruitment.BirthDate = account.Personal_BirthDate;
                dbRecruitment.Email = account.Personal_Email;
                dbRecruitment.Gender = account.Personal_Gender;
                dbRecruitment.Phone = account.Personal_Phone;
                dbRecruitment.Address = account.Personal_Address;
                dbRecruitment.Country = account.Personal_Country;
                dbRecruitment.City = account.Personal_City;
                dbRecruitment.PostalCode = account.Personal_PostalCode;
                dbRecruitment.Nationality = account.Personal_Nationality;
                dbRecruitment.ModifyDate = DateTime.Now;

                db.SaveChanges();
            }
            else
            {
                T_Recruitment newRecruitment = new T_Recruitment();
                newRecruitment.UserId = Session["LogedUserId"].ToString();
                newRecruitment.Name = account.Personal_Name;
                newRecruitment.BirthDate = account.Personal_BirthDate;
                newRecruitment.Email = account.Personal_Email;
                newRecruitment.Gender = account.Personal_Gender;
                newRecruitment.Phone = account.Personal_Phone;
                newRecruitment.Address = account.Personal_Address;
                newRecruitment.Country = account.Personal_Country;
                newRecruitment.City = account.Personal_City;
                newRecruitment.PostalCode = account.Personal_PostalCode;
                newRecruitment.Nationality = account.Personal_Nationality;
                newRecruitment.CreatedDate = DateTime.Now;

                db.T_Recruitment.Add(newRecruitment);
                db.SaveChanges();
            }

            TempData["Message"] = "Data Updated";
            return RedirectToAction("ApplicantPersonal");
        }

        public ActionResult ApplicantFamily()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();

            account.Username = dbUser.UserName;
            account.Email = dbUser.email;
            account.recruitmentFamily = (from recruitment in db.T_Recruitment_Family.Where(w => w.UserId == userId)
                                         join relation in db.T_MsParameter.Where(w => w.FlagId == 13) on recruitment.Relation equals relation.ParameterCode
                                         select new Recruitment_Family
                                         {
                                             FamilyId = recruitment.Id,
                                             Family_FamilyName = recruitment.FamilyName,
                                             Family_Gender = recruitment.Gender == "M" ? "Male" : "Female",
                                             Family_Relation = recruitment.Relation,
                                             Family_RelationDescription = relation.Description
                                         }).OrderBy(o => o.FamilyId).ToList();

            List<DropdownModel> ListRelation = (from param in db.T_MsParameter.Where(w => w.FlagId == 13).OrderBy(o => o.Id)
                                                select new DropdownModel
                                                {
                                                    stringValue = param.ParameterCode,
                                                    Text = param.Description
                                                }).ToList();
            ViewBag.ListRelation = new SelectList(ListRelation, "stringValue", "Text");

            return View(account);
        }

        public ActionResult SaveChanges_ApplicantFamily(UserAccount account)
        {
            string userId = Session["LogedUserId"].ToString();

            if (account.Family_Id.ToString() != "")
            {
                T_Recruitment_Family dbRecruitment_Family = db.T_Recruitment_Family.Where(w => w.UserId == userId && w.Id == account.Family_Id).FirstOrDefault();

                dbRecruitment_Family.FamilyName = account.Family_FamilyName;
                dbRecruitment_Family.Gender = account.Family_Gender;
                dbRecruitment_Family.Relation = account.Family_Relation;
                dbRecruitment_Family.ModifyDate = DateTime.Now;

                db.SaveChanges();
            }
            else
            {
                T_Recruitment_Family newRecruitment_Family = new T_Recruitment_Family();

                newRecruitment_Family.UserId = Session["LogedUserId"].ToString();
                newRecruitment_Family.FamilyName = account.Family_FamilyName;
                newRecruitment_Family.Gender = account.Family_Gender;
                newRecruitment_Family.Relation = account.Family_Relation;
                newRecruitment_Family.CreatedDate = DateTime.Now;

                db.T_Recruitment_Family.Add(newRecruitment_Family);
                db.SaveChanges();
            }

            TempData["Message"] = "Data Updated";
            return RedirectToAction("ApplicantFamily");
        }

        public JsonResult ApplicantFamily_Edit(int FamilyId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Recruitment_Family dbFamily = db.T_Recruitment_Family.Where(w => w.Id == FamilyId).FirstOrDefault();
            result.recruitmentFamily = dbFamily;
            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ApplicantFamily_Delete(int FamilyId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Recruitment_Family dbFamily = db.T_Recruitment_Family.Where(w => w.Id == FamilyId).FirstOrDefault();
            db.T_Recruitment_Family.Remove(dbFamily);
            db.SaveChanges();

            result.Message = "Data family deleted";
            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApplicantAcademy()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();

            account.Username = dbUser.UserName;
            account.Email = dbUser.email;
            account.recruitmentAcademy = (from recruitment in db.T_Recruitment_Academy.Where(w => w.UserId == userId)
                                          join location in db.T_MsParameter.Where(w => w.FlagId == 1) on recruitment.Location equals location.ParameterCode
                                          join degree in db.T_MsParameter.Where(w => w.FlagId == 8) on recruitment.Qualification equals degree.ParameterCode
                                          join fos in db.T_MsParameter.Where(w => w.FlagId == 9) on recruitment.Major equals fos.ParameterCode
                                          select new Recruitment_Academy
                                          {
                                              AcademyId = recruitment.Id,
                                              Institute = recruitment.Institute,
                                              Location = recruitment.Location,
                                              LocationName = location.Description,
                                              StudyFrom = recruitment.StudyFrom,
                                              StudyTo = recruitment.StudyTo,
                                              Qualification = recruitment.Qualification,
                                              QualificationDescription = degree.Description,
                                              Major = recruitment.Major,
                                              MajorDescription = fos.Description,
                                              CertificateFileName = recruitment.CertificateFileName,
                                              CertificateFilePath = recruitment.CertificateFilePath
                                          }).OrderBy(o => o.StudyFrom).ToList();

            List<DropdownModel> ListCountry = (from param in db.T_MsParameter.Where(w => w.FlagId == 1)
                                               select new DropdownModel
                                               {
                                                   stringValue = param.ParameterCode,
                                                   Text = param.Description
                                               }).OrderBy(o => o.Text).ToList();
            ViewBag.ListCountry = new SelectList(ListCountry, "stringValue", "Text");

            List<DropdownModel> ListQualification = (from param in db.T_MsParameter.Where(w => w.FlagId == 8).OrderBy(o => o.Id)
                                                     select new DropdownModel
                                                     {
                                                         stringValue = param.ParameterCode,
                                                         Text = param.Description
                                                     }).ToList();
            ViewBag.ListQualification = new SelectList(ListQualification, "stringValue", "Text");

            List<DropdownModel> ListFOS = (from param in db.T_MsParameter.Where(w => w.FlagId == 9)
                                           select new DropdownModel
                                           {
                                               stringValue = param.ParameterCode,
                                               Text = param.Description
                                           }).OrderBy(o => o.Text).ToList();
            ViewBag.ListFOS = new SelectList(ListFOS, "stringValue", "Text");

            List<DropdownModel> ListYear = new List<DropdownModel>();
            for (int a = DateTime.Now.Year; a > DateTime.Now.Year - 100; a--)
            {
                ListYear.Add(new DropdownModel { intValue = a, Text = a.ToString() });
            }
            ViewBag.ListYear = new SelectList(ListYear, "intValue", "Text");

            return View(account);
        }

        public ActionResult SaveChanges_ApplicantAcademy(UserAccount account, HttpPostedFileBase uploadFile)
        {
            string userId = Session["LogedUserId"].ToString();

            if (account.Academy_Id.ToString() != "")
            {
                T_Recruitment_Academy dbRecruitment_Academy = db.T_Recruitment_Academy.Where(w => w.UserId == userId && w.Id == account.Academy_Id).FirstOrDefault();

                dbRecruitment_Academy.Institute = account.Academy_Institute;
                dbRecruitment_Academy.Location = account.Academy_Location;
                dbRecruitment_Academy.StudyFrom = account.Academy_StudyFrom;
                dbRecruitment_Academy.StudyTo = account.Academy_StudyTo;
                dbRecruitment_Academy.Qualification = account.Academy_Qualification;
                dbRecruitment_Academy.Major = account.Academy_Major;
                dbRecruitment_Academy.ModifyDate = DateTime.Now;

                if (uploadFile != null)
                {
                    if (uploadFile.ContentLength > 0 && uploadFile.ContentLength <= 2000000)
                    {
                        string Year = DateTime.Now.Year.ToString(), Month = "0" + DateTime.Now.Month.ToString(), Day = "0" + DateTime.Now.Day.ToString(), Hour = "0" + DateTime.Now.Hour.ToString(), Minute = "0" + DateTime.Now.Minute.ToString(), Second = "0" + DateTime.Now.Second.ToString();
                        string Time = Year + Month.Substring(Month.Length - 2) + Day.Substring(Day.Length - 2) + "_" + Hour.Substring(Hour.Length - 2) + Minute.Substring(Minute.Length - 2) + Second.Substring(Second.Length - 2);

                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + Time);

                        if (dbRecruitment_Academy.CertificateFilePath != null)
                        {
                            System.IO.File.Delete(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + dbRecruitment_Academy.CertificateFilePath.Substring(26, 16) + dbRecruitment_Academy.CertificateFileName);
                            Directory.Delete(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + dbRecruitment_Academy.CertificateFilePath.Substring(26, 15));
                        }

                        var filename = Path.GetFileName(uploadFile.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/recruitmentfiles/" + Time), filename);
                        uploadFile.SaveAs(filepath);

                        dbRecruitment_Academy.CertificateFileName = account.Academy_CertificateFileName;
                        dbRecruitment_Academy.CertificateFilePath = "/Content/recruitmentfiles/" + Time + "/" + filename;
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("ApplicantAcademy");
                    }
                }

                db.SaveChanges();
            }
            else
            {
                T_Recruitment_Academy newRecruitment_Academy = new T_Recruitment_Academy();

                newRecruitment_Academy.UserId = Session["LogedUserId"].ToString();
                newRecruitment_Academy.Institute = account.Academy_Institute;
                newRecruitment_Academy.Location = account.Academy_Location;
                newRecruitment_Academy.StudyFrom = account.Academy_StudyFrom;
                newRecruitment_Academy.StudyTo = account.Academy_StudyTo;
                newRecruitment_Academy.Qualification = account.Academy_Qualification;
                newRecruitment_Academy.Major = account.Academy_Major;
                newRecruitment_Academy.CreatedDate = DateTime.Now;

                if (uploadFile != null)
                {
                    if (uploadFile.ContentLength > 0 && uploadFile.ContentLength <= 2000000)
                    {
                        string Year = DateTime.Now.Year.ToString(), Month = "0" + DateTime.Now.Month.ToString(), Day = "0" + DateTime.Now.Day.ToString(), Hour = "0" + DateTime.Now.Hour.ToString(), Minute = "0" + DateTime.Now.Minute.ToString(), Second = "0" + DateTime.Now.Second.ToString();
                        string Time = Year + Month.Substring(Month.Length - 2) + Day.Substring(Day.Length - 2) + "_" + Hour.Substring(Hour.Length - 2) + Minute.Substring(Minute.Length - 2) + Second.Substring(Second.Length - 2);

                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + Time);

                        if (newRecruitment_Academy.CertificateFilePath != null)
                        {
                            System.IO.File.Delete(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + newRecruitment_Academy.CertificateFilePath.Substring(26, 16) + newRecruitment_Academy.CertificateFileName);
                            Directory.Delete(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + newRecruitment_Academy.CertificateFilePath.Substring(26, 15));
                        }

                        var filename = Path.GetFileName(uploadFile.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/recruitmentfiles/" + Time), filename);
                        uploadFile.SaveAs(filepath);

                        newRecruitment_Academy.CertificateFileName = account.Academy_CertificateFileName;
                        newRecruitment_Academy.CertificateFilePath = "/Content/recruitmentfiles/" + Time + "/" + filename;
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("ApplicantAcademy");
                    }
                }

                db.T_Recruitment_Academy.Add(newRecruitment_Academy);
                db.SaveChanges();
            }

            TempData["Message"] = "Data Updated";
            return RedirectToAction("ApplicantAcademy");
        }

        public JsonResult ApplicantAcademy_Edit(int AcademyId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Recruitment_Academy dbAcademy = db.T_Recruitment_Academy.Where(w => w.Id == AcademyId).FirstOrDefault();
            result.recruitmentAcademy = dbAcademy;
            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ApplicantAcademy_Delete(int AcademyId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Recruitment_Academy dbAcademy = db.T_Recruitment_Academy.Where(w => w.Id == AcademyId).FirstOrDefault();

            if (dbAcademy.CertificateFilePath != null)
            {
                System.IO.File.Delete(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + dbAcademy.CertificateFilePath.Substring(26, 16) + dbAcademy.CertificateFileName);
                Directory.Delete(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + dbAcademy.CertificateFilePath.Substring(26, 15));
            }

            db.T_Recruitment_Academy.Remove(dbAcademy);
            db.SaveChanges();

            result.Message = "Data academy deleted";
            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApplicantAcademy_NonFormal()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();

            account.Username = dbUser.UserName;
            account.Email = dbUser.email;
            account.recruitmentAcademy_NonFormal = (from recruitment in db.T_Recruitment_Academy_NonFormal.Where(w => w.UserId == userId)
                                                    join location in db.T_MsParameter.Where(w => w.FlagId == 1) on recruitment.Location equals location.ParameterCode
                                                    select new Recruitment_Academy_NonFormal
                                                    {
                                                        AcademyId = recruitment.Id,
                                                        Education = recruitment.Education,
                                                        Institute = recruitment.Institute,
                                                        Location = recruitment.Location,
                                                        LocationName = location.Description,
                                                        CertificateFileName = recruitment.CertificateFileName,
                                                        CertificateFilePath = recruitment.CertificateFilePath
                                                    }).OrderBy(o => o.Education).ToList();

            List<DropdownModel> ListCountry = (from param in db.T_MsParameter.Where(w => w.FlagId == 1)
                                               select new DropdownModel
                                               {
                                                   stringValue = param.ParameterCode,
                                                   Text = param.Description
                                               }).OrderBy(o => o.Text).ToList();
            ViewBag.ListCountry = new SelectList(ListCountry, "stringValue", "Text");

            return View(account);
        }

        public ActionResult SaveChanges_ApplicantAcademy_NonFormal(UserAccount account, HttpPostedFileBase uploadFile)
        {
            string userId = Session["LogedUserId"].ToString();

            if (account.Academy_NonFormal_Id.ToString() != "")
            {
                T_Recruitment_Academy_NonFormal dbRecruitment_Academy = db.T_Recruitment_Academy_NonFormal.Where(w => w.UserId == userId && w.Id == account.Academy_NonFormal_Id).FirstOrDefault();

                dbRecruitment_Academy.Education = account.Academy_NonFormal_Education;
                dbRecruitment_Academy.Institute = account.Academy_NonFormal_Institute;
                dbRecruitment_Academy.Location = account.Academy_NonFormal_Location;
                dbRecruitment_Academy.ModifyDate = DateTime.Now;

                if (uploadFile != null)
                {
                    if (uploadFile.ContentLength > 0 && uploadFile.ContentLength <= 2000000)
                    {
                        string Year = DateTime.Now.Year.ToString(), Month = "0" + DateTime.Now.Month.ToString(), Day = "0" + DateTime.Now.Day.ToString(), Hour = "0" + DateTime.Now.Hour.ToString(), Minute = "0" + DateTime.Now.Minute.ToString(), Second = "0" + DateTime.Now.Second.ToString();
                        string Time = Year + Month.Substring(Month.Length - 2) + Day.Substring(Day.Length - 2) + "_" + Hour.Substring(Hour.Length - 2) + Minute.Substring(Minute.Length - 2) + Second.Substring(Second.Length - 2);

                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + Time);

                        if (dbRecruitment_Academy.CertificateFilePath != null)
                        {
                            System.IO.File.Delete(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + dbRecruitment_Academy.CertificateFilePath.Substring(26, 16) + dbRecruitment_Academy.CertificateFileName);
                            Directory.Delete(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + dbRecruitment_Academy.CertificateFilePath.Substring(26, 15));
                        }

                        var filename = Path.GetFileName(uploadFile.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/recruitmentfiles/" + Time), filename);
                        uploadFile.SaveAs(filepath);

                        dbRecruitment_Academy.CertificateFileName = account.Academy_NonFormal_CertificateFileName;
                        dbRecruitment_Academy.CertificateFilePath = "/Content/recruitmentfiles/" + Time + "/" + filename;
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("ApplicantAcademy");
                    }
                }

                db.SaveChanges();
            }
            else
            {
                T_Recruitment_Academy_NonFormal newRecruitment_Academy = new T_Recruitment_Academy_NonFormal();

                newRecruitment_Academy.UserId = Session["LogedUserId"].ToString();
                newRecruitment_Academy.Education = account.Academy_NonFormal_Education;
                newRecruitment_Academy.Institute = account.Academy_NonFormal_Institute;
                newRecruitment_Academy.Location = account.Academy_NonFormal_Location;
                newRecruitment_Academy.CreatedDate = DateTime.Now;

                if (uploadFile != null)
                {
                    if (uploadFile.ContentLength > 0 && uploadFile.ContentLength <= 2000000)
                    {
                        string Year = DateTime.Now.Year.ToString(), Month = "0" + DateTime.Now.Month.ToString(), Day = "0" + DateTime.Now.Day.ToString(), Hour = "0" + DateTime.Now.Hour.ToString(), Minute = "0" + DateTime.Now.Minute.ToString(), Second = "0" + DateTime.Now.Second.ToString();
                        string Time = Year + Month.Substring(Month.Length - 2) + Day.Substring(Day.Length - 2) + "_" + Hour.Substring(Hour.Length - 2) + Minute.Substring(Minute.Length - 2) + Second.Substring(Second.Length - 2);

                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + Time);

                        var filename = Path.GetFileName(uploadFile.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/recruitmentfiles/" + Time), filename);
                        uploadFile.SaveAs(filepath);

                        newRecruitment_Academy.CertificateFileName = account.Academy_NonFormal_CertificateFileName;
                        newRecruitment_Academy.CertificateFilePath = "/Content/recruitmentfiles/" + Time + "/" + filename;
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("ApplicantAcademy");
                    }
                }

                db.T_Recruitment_Academy_NonFormal.Add(newRecruitment_Academy);
                db.SaveChanges();
            }

            TempData["Message"] = "Data Updated";
            return RedirectToAction("ApplicantAcademy_NonFormal");
        }

        public JsonResult ApplicantAcademy_NonFormal_Edit(int AcademyId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Recruitment_Academy_NonFormal dbAcademy = db.T_Recruitment_Academy_NonFormal.Where(w => w.Id == AcademyId).FirstOrDefault();
            result.recruitmentAcademy_NonFormal = dbAcademy;
            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ApplicantAcademy_NonFormal_Delete(int AcademyId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Recruitment_Academy_NonFormal dbAcademy = db.T_Recruitment_Academy_NonFormal.Where(w => w.Id == AcademyId).FirstOrDefault();

            if (dbAcademy.CertificateFilePath != null)
            {
                System.IO.File.Delete(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + dbAcademy.CertificateFilePath.Substring(26, 16) + dbAcademy.CertificateFileName);
                Directory.Delete(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + dbAcademy.CertificateFilePath.Substring(26, 15));
            }


            db.T_Recruitment_Academy_NonFormal.Remove(dbAcademy);
            db.SaveChanges();

            result.Message = "Data academy deleted";
            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApplicantExperience()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();

            account.Username = dbUser.UserName;
            account.Email = dbUser.email;
            account.recruitmentExperience = (from recruitment in db.T_Recruitment_Experience.Where(w => w.UserId == userId)
                                             join location in db.T_MsParameter.Where(w => w.FlagId == 1) on recruitment.Location equals location.ParameterCode into t
                                             from rt in t.DefaultIfEmpty()
                                             join state in db.T_MsParameter.Where(w => w.FlagId == 2) on recruitment.State equals state.ParameterCode into t2
                                             from rt2 in t2.DefaultIfEmpty()
                                             join specialization in db.T_MsParameter.Where(w => w.FlagId == 10) on recruitment.Specialization equals specialization.ParameterCode
                                             join industry in db.T_MsParameter.Where(w => w.FlagId == 11) on recruitment.Industry equals industry.ParameterCode
                                             join level in db.T_MsParameter.Where(w => w.FlagId == 12) on recruitment.PositionLevel equals level.ParameterCode
                                             join currency in db.T_MsParameter.Where(w => w.FlagId == 7) on recruitment.SalaryCurrency equals currency.ParameterCode into t3
                                             from rt3 in t3.DefaultIfEmpty()
                                             select new Recruitment_Experience
                                             {
                                                 ExperienceId = recruitment.Id,
                                                 CompanyName = recruitment.CompanyName,
                                                 PositionTitle = recruitment.PositionTitle,
                                                 Location = recruitment.Location,
                                                 LocationName = rt.Description,
                                                 State = recruitment.State,
                                                 StateName = rt2.Description,
                                                 PositionLevel = recruitment.PositionLevel,
                                                 PositionLevelDescription = level.Description,
                                                 MonthStart = recruitment.MonthStart,
                                                 YearStart = recruitment.YearStart,
                                                 MonthEnd = recruitment.MonthEnd,
                                                 YearEnd = recruitment.YearEnd,
                                                 StillPresent = recruitment.StillPresent,
                                                 Specialization = recruitment.Specialization,
                                                 SpecializationDescription = specialization.Description,
                                                 Industry = recruitment.Industry,
                                                 IndustryDescription = industry.Description,
                                                 Salary = recruitment.Salary,
                                                 SalaryCurrency = recruitment.SalaryCurrency,
                                                 SalaryCurrencyDescription = rt3.Description,
                                                 ExperienceDescription = recruitment.ExperienceDescription
                                             }).OrderByDescending(o => o.YearStart + o.MonthStart).ToList();

            List<DropdownModel> ListSpecialization = (from param in db.T_MsParameter.Where(w => w.FlagId == 10).OrderBy(o => o.Description)
                                                      select new DropdownModel
                                                      {
                                                          stringValue = param.ParameterCode,
                                                          Text = param.Description
                                                      }).ToList();
            ViewBag.ListSpecialization = new SelectList(ListSpecialization, "stringValue", "Text");

            List<DropdownModel> ListIndustry = (from param in db.T_MsParameter.Where(w => w.FlagId == 11).OrderBy(o => o.Description)
                                                select new DropdownModel
                                                {
                                                    stringValue = param.ParameterCode,
                                                    Text = param.Description
                                                }).ToList();
            ViewBag.ListIndustry = new SelectList(ListIndustry, "stringValue", "Text");

            List<DropdownModel> ListPositionLevel = (from param in db.T_MsParameter.Where(w => w.FlagId == 12).OrderBy(o => o.Id)
                                                     select new DropdownModel
                                                     {
                                                         stringValue = param.ParameterCode,
                                                         Text = param.Description
                                                     }).ToList();
            ViewBag.ListPositionLevel = new SelectList(ListPositionLevel, "stringValue", "Text");

            List<DropdownModel> ListCountry = (from param in db.T_MsParameter.Where(w => w.FlagId == 1)
                                               select new DropdownModel
                                               {
                                                   stringValue = param.ParameterCode,
                                                   Text = param.Description
                                               }).OrderBy(o => o.Text).ToList();
            ViewBag.ListCountry = new SelectList(ListCountry, "stringValue", "Text");

            List<DropdownModel> ListCity = new List<DropdownModel>();
            ViewBag.ListCity = new SelectList(ListCity, "stringValue", "Text");

            List<DropdownModel> ListYear = new List<DropdownModel>();
            for (int a = DateTime.Now.Year; a > DateTime.Now.Year - 100; a--)
            {
                ListYear.Add(new DropdownModel { intValue = a, Text = a.ToString() });
            }
            ViewBag.ListYear = new SelectList(ListYear, "intValue", "Text");

            List<DropdownModel> ListMonth = new List<DropdownModel>();
            ListMonth.Add(new DropdownModel { stringValue = "Jan", Text = "Jan" });
            ListMonth.Add(new DropdownModel { stringValue = "Feb", Text = "Feb" });
            ListMonth.Add(new DropdownModel { stringValue = "Mar", Text = "Mar" });
            ListMonth.Add(new DropdownModel { stringValue = "Apr", Text = "Apr" });
            ListMonth.Add(new DropdownModel { stringValue = "May", Text = "May" });
            ListMonth.Add(new DropdownModel { stringValue = "Jun", Text = "Jun" });
            ListMonth.Add(new DropdownModel { stringValue = "Jul", Text = "Jul" });
            ListMonth.Add(new DropdownModel { stringValue = "Aug", Text = "Aug" });
            ListMonth.Add(new DropdownModel { stringValue = "Sep", Text = "Sep" });
            ListMonth.Add(new DropdownModel { stringValue = "Oct", Text = "Oct" });
            ListMonth.Add(new DropdownModel { stringValue = "Nov", Text = "Nov" });
            ListMonth.Add(new DropdownModel { stringValue = "Dec", Text = "Dec" });
            ViewBag.ListMonth = new SelectList(ListMonth, "stringValue", "Text");

            List<DropdownModel> ListMataUang = (from param in db.T_MsParameter.Where(w => w.FlagId == 7)
                                                select new DropdownModel
                                                {
                                                    stringValue = param.ParameterCode,
                                                    Text = param.Description
                                                }).OrderBy(o => o.Text).ToList();
            ViewBag.ListMataUang = new SelectList(ListMataUang, "stringValue", "Text");

            return View(account);
        }

        public ActionResult SaveChanges_ApplicantExperience(UserAccount account)
        {
            string userId = Session["LogedUserId"].ToString();

            if (account.Experience_Id.ToString() != "")
            {
                T_Recruitment_Experience dbRecruitment_Experience = db.T_Recruitment_Experience.Where(w => w.UserId == userId && w.Id == account.Experience_Id).FirstOrDefault();

                dbRecruitment_Experience.CompanyName = account.Experience_CompanyName;
                dbRecruitment_Experience.Location = account.Experience_Location;
                dbRecruitment_Experience.State = account.Experience_State;
                dbRecruitment_Experience.PositionTitle = account.Experience_PositionTitle;
                dbRecruitment_Experience.PositionLevel = account.Experience_PositionLevel;
                dbRecruitment_Experience.MonthStart = account.Experience_MonthStart;
                dbRecruitment_Experience.YearStart = account.Experience_YearStart;
                dbRecruitment_Experience.MonthEnd = account.Experience_MonthEnd;
                dbRecruitment_Experience.YearEnd = account.Experience_YearEnd;
                dbRecruitment_Experience.StillPresent = account.Experience_YearEnd == null ? true : false;
                dbRecruitment_Experience.Specialization = account.Experience_Specialization;
                dbRecruitment_Experience.Industry = account.Experience_Industry;
                dbRecruitment_Experience.Salary = account.Experience_Salary;
                dbRecruitment_Experience.SalaryCurrency = account.Experience_SalaryCurrency;
                dbRecruitment_Experience.ExperienceDescription = account.Experience_ExperienceDescription;
                dbRecruitment_Experience.ModifyDate = DateTime.Now;

                db.SaveChanges();
            }
            else
            {
                T_Recruitment_Experience newRecruitment_Experience = new T_Recruitment_Experience();

                newRecruitment_Experience.UserId = userId;
                newRecruitment_Experience.CompanyName = account.Experience_CompanyName;
                newRecruitment_Experience.Location = account.Experience_Location;
                newRecruitment_Experience.State = account.Experience_State;
                newRecruitment_Experience.PositionTitle = account.Experience_PositionTitle;
                newRecruitment_Experience.PositionLevel = account.Experience_PositionLevel;
                newRecruitment_Experience.MonthStart = account.Experience_MonthStart;
                newRecruitment_Experience.YearStart = account.Experience_YearStart;
                newRecruitment_Experience.MonthEnd = account.Experience_MonthEnd;
                newRecruitment_Experience.YearEnd = account.Experience_YearEnd;
                newRecruitment_Experience.StillPresent = account.Experience_YearEnd == null ? true : false;
                newRecruitment_Experience.Specialization = account.Experience_Specialization;
                newRecruitment_Experience.Industry = account.Experience_Industry;
                newRecruitment_Experience.Salary = account.Experience_Salary;
                newRecruitment_Experience.SalaryCurrency = account.Experience_SalaryCurrency;
                newRecruitment_Experience.ExperienceDescription = account.Experience_ExperienceDescription;
                newRecruitment_Experience.CreatedDate = DateTime.Now;

                db.T_Recruitment_Experience.Add(newRecruitment_Experience);
                db.SaveChanges();
            }

            TempData["Message"] = "Data Updated";
            return RedirectToAction("ApplicantExperience");
        }

        public JsonResult ApplicantExperience_Edit(int ExperienceId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Recruitment_Experience dbExperience = db.T_Recruitment_Experience.Where(w => w.Id == ExperienceId).FirstOrDefault();
            result.recruitmentExperience = dbExperience;
            result.Status = true;

            if (dbExperience.Location != null)
            {
                List<DropdownModel> ListCity = (from param in db.T_MsParameter.Where(w => w.FlagId == 2 && w.ParentCode == dbExperience.Location)
                                                select new DropdownModel
                                                {
                                                    stringValue = param.ParameterCode,
                                                    Text = param.Description
                                                }).OrderBy(o => o.Text).ToList();
                ViewBag.ListCity = new SelectList(ListCity, "stringValue", "Text");

                result.listDropdownData = ListCity;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ApplicantExperience_Delete(int ExperienceId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Recruitment_Experience dbExperience = db.T_Recruitment_Experience.Where(w => w.Id == ExperienceId).FirstOrDefault();
            db.T_Recruitment_Experience.Remove(dbExperience);
            db.SaveChanges();

            result.Message = "Data experience deleted";
            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApplicantSkills()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();

            account.Username = dbUser.UserName;
            account.Email = dbUser.email;
            account.recruitmentSkill = (from recruitment in db.T_Recruitment_Skill.Where(w => w.UserId == userId)
                                        select new Recruitment_Skill
                                        {
                                            SkillId = recruitment.Id,
                                            Skill = recruitment.Skill,
                                            Proficiency = recruitment.Proficiency,
                                            isDelete = false
                                        }).OrderBy(o => o.SkillId).ToList();

            List<DropdownModel> ListProficiency = new List<DropdownModel>();
            ListProficiency.Add(new DropdownModel { stringValue = "A", Text = "Advanced" });
            ListProficiency.Add(new DropdownModel { stringValue = "I", Text = "Intermediate" });
            ListProficiency.Add(new DropdownModel { stringValue = "B", Text = "Beginner" });
            //ViewBag.ListProficiency = new SelectList(ListProficiency, "stringValue", "Text");
            ViewBag.ListProficiency = ListProficiency.ToList();

            return View(account);
        }

        public ActionResult ApplicantSkills_Form()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();

            account.Username = dbUser.UserName;
            account.Email = dbUser.email;
            account.recruitmentSkill = (from recruitment in db.T_Recruitment_Skill.Where(w => w.UserId == userId)
                                        select new Recruitment_Skill
                                        {
                                            SkillId = recruitment.Id,
                                            Skill = recruitment.Skill,
                                            Proficiency = recruitment.Proficiency,
                                            isDelete = false
                                        }).OrderBy(o => o.SkillId).ToList();

            List<DropdownModel> ListProficiency = new List<DropdownModel>();
            ListProficiency.Add(new DropdownModel { stringValue = "A", Text = "Advanced" });
            ListProficiency.Add(new DropdownModel { stringValue = "I", Text = "Intermediate" });
            ListProficiency.Add(new DropdownModel { stringValue = "B", Text = "Beginner" });
            //ViewBag.ListProficiency = new SelectList(ListProficiency, "stringValue", "Text");
            ViewBag.ListProficiency = ListProficiency.ToList();

            return View(account);
        }

        public ActionResult SaveChanges_ApplicantSkill(UserAccount account)
        {
            string userId = Session["LogedUserId"].ToString();

            if (account.recruitmentSkill.Count > 0)
            {
                for (int i = 0; i < account.recruitmentSkill.Count; i++)
                {
                    int? SkillId = account.recruitmentSkill[i].SkillId;
                    string Skill = account.recruitmentSkill[i].Skill;
                    string Proficiency = account.recruitmentSkill[i].Proficiency;
                    bool isDelete = account.recruitmentSkill[i].isDelete;

                    if (SkillId != null)
                    {
                        T_Recruitment_Skill dbSkill = db.T_Recruitment_Skill.Where(w => w.Id == SkillId.Value).FirstOrDefault();

                        if (isDelete)
                        {
                            db.T_Recruitment_Skill.Remove(dbSkill);
                        }
                        else
                        {
                            dbSkill.Skill = Skill;
                            dbSkill.Proficiency = Proficiency;
                            dbSkill.ModifyDate = DateTime.Now;
                        }
                    }
                    else
                    {
                        if (!isDelete)
                        {
                            T_Recruitment_Skill newSkill = new T_Recruitment_Skill();
                            newSkill.UserId = userId;
                            newSkill.Skill = Skill;
                            newSkill.Proficiency = Proficiency;
                            newSkill.CreatedDate = DateTime.Now;
                            db.T_Recruitment_Skill.Add(newSkill);
                        }
                    }

                    db.SaveChanges();
                }
            }

            return RedirectToAction("ApplicantSkills");
        }

        public ActionResult ApplicantDocument()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();

            account.Username = dbUser.UserName;
            account.Email = dbUser.email;
            account.recruitmentDocument = (from recruitment in db.T_Recruitment_Document.Where(w => w.UserId == userId)
                                           select new Recruitment_Document
                                           {
                                               DocumentId = recruitment.Id,
                                               Description = recruitment.Description,
                                               FileName = recruitment.FileName,
                                               FilePath = recruitment.FilePath,
                                               Notes = recruitment.Notes
                                           }).OrderBy(o => o.DocumentId).ToList();

            return View(account);
        }

        public ActionResult SaveChanges_ApplicantDocument(UserAccount account, HttpPostedFileBase uploadFile)
        {
            string userId = Session["LogedUserId"].ToString();

            if (account.Recruitment_Document_Id.ToString() != "")
            {
                T_Recruitment_Document dbRecruitment_Document = db.T_Recruitment_Document.Where(w => w.UserId == userId && w.Id == account.Recruitment_Document_Id).FirstOrDefault();

                dbRecruitment_Document.Description = account.Recruitment_Document_Description;

                if (uploadFile != null)
                {
                    if (uploadFile.ContentLength > 0 && uploadFile.ContentLength <= 2000000)
                    {
                        string Year = DateTime.Now.Year.ToString(), Month = "0" + DateTime.Now.Month.ToString(), Day = "0" + DateTime.Now.Day.ToString(), Hour = "0" + DateTime.Now.Hour.ToString(), Minute = "0" + DateTime.Now.Minute.ToString(), Second = "0" + DateTime.Now.Second.ToString();
                        string Time = Year + Month.Substring(Month.Length - 2) + Day.Substring(Day.Length - 2) + "_" + Hour.Substring(Hour.Length - 2) + Minute.Substring(Minute.Length - 2) + Second.Substring(Second.Length - 2);

                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + Time);

                        if (dbRecruitment_Document.FilePath != null)
                        {
                            System.IO.File.Delete(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + dbRecruitment_Document.FilePath.Substring(26, 16) + dbRecruitment_Document.FileName);
                            Directory.Delete(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + dbRecruitment_Document.FilePath.Substring(26, 15));
                        }

                        var filename = Path.GetFileName(uploadFile.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/recruitmentfiles/" + Time), filename);
                        uploadFile.SaveAs(filepath);

                        dbRecruitment_Document.FileName = account.Recruitment_Document_FileName;
                        dbRecruitment_Document.FilePath = "/Content/recruitmentfiles/" + Time + "/" + filename;
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("ApplicantDocument");
                    }
                }

                dbRecruitment_Document.Notes = account.Recruitment_Document_Notes;
                dbRecruitment_Document.ModifyDate = DateTime.Now;

                db.SaveChanges();
            }
            else
            {
                if (uploadFile.ContentLength > 0 && uploadFile.ContentLength <= 2000000)
                {
                    string Year = DateTime.Now.Year.ToString(), Month = "0" + DateTime.Now.Month.ToString(), Day = "0" + DateTime.Now.Day.ToString(), Hour = "0" + DateTime.Now.Hour.ToString(), Minute = "0" + DateTime.Now.Minute.ToString(), Second = "0" + DateTime.Now.Second.ToString();
                    string Time = Year + Month.Substring(Month.Length - 2) + Day.Substring(Day.Length - 2) + "_" + Hour.Substring(Hour.Length - 2) + Minute.Substring(Minute.Length - 2) + Second.Substring(Second.Length - 2);

                    T_Recruitment_Document newRecruitment_Document = new T_Recruitment_Document();

                    newRecruitment_Document.UserId = Session["LogedUserId"].ToString();
                    newRecruitment_Document.Description = account.Recruitment_Document_Description;

                    Directory.CreateDirectory(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + Time);

                    var filename = Path.GetFileName(uploadFile.FileName);
                    var filepath = Path.Combine(Server.MapPath("~/Content/recruitmentfiles/" + Time), filename);
                    uploadFile.SaveAs(filepath);

                    newRecruitment_Document.FileName = filename;
                    newRecruitment_Document.FilePath = "/Content/recruitmentfiles/" + Time + "/" + filename;

                    newRecruitment_Document.Notes = account.Recruitment_Document_Notes;
                    newRecruitment_Document.CreatedDate = DateTime.Now;

                    db.T_Recruitment_Document.Add(newRecruitment_Document);
                    db.SaveChanges();
                }
                else
                {
                    TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                    return RedirectToAction("ApplicantDocument");
                }
            }

            TempData["Message"] = "Data Updated";
            return RedirectToAction("ApplicantDocument");
        }

        public JsonResult ApplicantDocument_Edit(int DocumentId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Recruitment_Document dbDocument = db.T_Recruitment_Document.Where(w => w.Id == DocumentId).FirstOrDefault();
            result.recruitmentDocument = dbDocument;
            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ApplicantDocument_Delete(int DocumentId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Recruitment_Document dbDocument = db.T_Recruitment_Document.Where(w => w.Id == DocumentId).FirstOrDefault();

            if (dbDocument.FilePath != null)
            {
                System.IO.File.Delete(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + dbDocument.FilePath.Substring(26, 16) + dbDocument.FileName);
                Directory.Delete(WebConfigurationManager.AppSettings["RecruitmentDocumentPathFolder"] + dbDocument.FilePath.Substring(26, 15));
            }

            db.T_Recruitment_Document.Remove(dbDocument);
            db.SaveChanges();

            result.Message = "Document deleted";
            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }



        public ActionResult VendorCompany()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();
            Vendor_Company vendorData = (from vendor in db.T_Vendor.Where(w => w.UserId == userId)
                                         join form in db.T_MsParameter.Where(w => w.FlagId == 4) on vendor.BusinessForm equals form.ParameterCode
                                         join field in db.T_MsParameter.Where(w => w.FlagId == 3) on vendor.BusinessField equals field.ParameterCode
                                         join country in db.T_MsParameter.Where(w => w.FlagId == 1) on vendor.Country equals country.ParameterCode
                                         join city in db.T_MsParameter.Where(w => w.FlagId == 2) on new { JoinProperty1 = vendor.Country, JoinProperty2 = vendor.City } equals new { JoinProperty1 = city.ParentCode, JoinProperty2 = city.ParameterCode }
                                         select new Vendor_Company
                                         {
                                             CompanyName = vendor.CompanyName,
                                             BusinessForm = vendor.BusinessForm,
                                             BusinessFormDescription = form.Description,
                                             BusinessField = vendor.BusinessField,
                                             BusinessFieldDescription = field.Description,
                                             Country = vendor.Country,
                                             CountryName = country.Description,
                                             City = vendor.City,
                                             CityName = city.Description,
                                             Address = vendor.Address,
                                             PostalCode = vendor.PostalCode,
                                             Email = vendor.Email,
                                             Phone = vendor.Phone,
                                             Fax = vendor.Fax,
                                             CompanyWeb = vendor.CompanyWeb,
                                             ContactPerson = vendor.ContactPerson,
                                             CPNumber = vendor.CPNumber,
                                             CPEmail = vendor.CPEmail
                                         }).FirstOrDefault();

            account.Username = dbUser.UserName;
            account.Email = dbUser.email;

            if (vendorData != null)
            {
                account.Vendor_CompanyName = vendorData.CompanyName;
                account.Vendor_BusinessForm = vendorData.BusinessForm;
                account.Vendor_BusinessFormDescription = vendorData.BusinessFormDescription;
                account.Vendor_BusinessField = vendorData.BusinessField;
                account.Vendor_BusinessFieldDescription = vendorData.BusinessFieldDescription;
                account.Vendor_Country = vendorData.Country;
                account.Vendor_CountryName = vendorData.CountryName;
                account.Vendor_City = vendorData.City;
                account.Vendor_CityName = vendorData.CityName;
                account.Vendor_Address = vendorData.Address;
                account.Vendor_PostalCode = vendorData.PostalCode;
                account.Vendor_Email = vendorData.Email;
                account.Vendor_Phone = vendorData.Phone;
                account.Vendor_Fax = vendorData.Fax;
                account.Vendor_CompanyWeb = vendorData.CompanyWeb;
                account.Vendor_ContactPerson = vendorData.ContactPerson;
                account.Vendor_CPNumber = vendorData.CPNumber;
                account.Vendor_CPEmail = vendorData.CPEmail;
            }

            List<DropdownModel> ListBusinessForm = (from param in db.T_MsParameter.Where(w => w.FlagId == 4)
                                                    select new DropdownModel
                                                    {
                                                        stringValue = param.ParameterCode,
                                                        Text = param.Description
                                                    }).OrderBy(o => o.Text).ToList();
            ViewBag.ListBusinessForm = new SelectList(ListBusinessForm, "stringValue", "Text");

            List<DropdownModel> ListBusinessField = (from param in db.T_MsParameter.Where(w => w.FlagId == 3)
                                                     select new DropdownModel
                                                     {
                                                         stringValue = param.ParameterCode,
                                                         Text = param.Description
                                                     }).OrderBy(o => o.Text).ToList();
            ViewBag.ListBusinessField = new SelectList(ListBusinessField, "stringValue", "Text");

            List<DropdownModel> ListCountry = (from param in db.T_MsParameter.Where(w => w.FlagId == 1)
                                               select new DropdownModel
                                               {
                                                   stringValue = param.ParameterCode,
                                                   Text = param.Description
                                               }).OrderBy(o => o.Text).ToList();
            ViewBag.ListCountry = new SelectList(ListCountry, "stringValue", "Text");

            List<DropdownModel> ListCity = new List<DropdownModel>();
            if (account.Vendor_Country != null)
            {
                ListCity = (from param in db.T_MsParameter.Where(w => w.FlagId == 2 && w.ParentCode == account.Vendor_Country)
                            select new DropdownModel
                            {
                                stringValue = param.ParameterCode,
                                Text = param.Description
                            }).OrderBy(o => o.Text).ToList();
            }
            ViewBag.ListCity = new SelectList(ListCity, "stringValue", "Text");

            return View(account);
        }

        public ActionResult SaveChanges_VendorCompany(UserAccount account)
        {
            string userId = Session["LogedUserId"].ToString();
            T_Vendor dbVendor = db.T_Vendor.Where(w => w.UserId == userId).FirstOrDefault();

            if (dbVendor != null)
            {
                dbVendor.CompanyName = account.Vendor_CompanyName;
                dbVendor.BusinessForm = account.Vendor_BusinessForm;
                dbVendor.BusinessField = account.Vendor_BusinessField;
                dbVendor.Country = account.Vendor_Country;
                dbVendor.City = account.Vendor_City;
                dbVendor.Address = account.Vendor_Address;
                dbVendor.PostalCode = account.Vendor_PostalCode;
                dbVendor.Email = account.Vendor_Email;
                dbVendor.Phone = account.Vendor_Phone;
                dbVendor.Fax = account.Vendor_Fax;
                dbVendor.CompanyWeb = account.Vendor_CompanyWeb;
                dbVendor.ContactPerson = account.Vendor_ContactPerson;
                dbVendor.CPNumber = account.Vendor_CPNumber;
                dbVendor.CPEmail = account.Vendor_CPEmail;
                dbVendor.ModifyDate = DateTime.Now;
                dbVendor.IsSubmitted = false;

                db.SaveChanges();
            }
            else
            {
                T_Vendor newVendor = new T_Vendor();
                newVendor.UserId = userId;
                newVendor.CompanyName = account.Vendor_CompanyName;
                newVendor.BusinessForm = account.Vendor_BusinessForm;
                newVendor.BusinessField = account.Vendor_BusinessField;
                newVendor.Country = account.Vendor_Country;
                newVendor.City = account.Vendor_City;
                newVendor.Address = account.Vendor_Address;
                newVendor.PostalCode = account.Vendor_PostalCode;
                newVendor.Email = account.Vendor_Email;
                newVendor.Phone = account.Vendor_Phone;
                newVendor.Fax = account.Vendor_Fax;
                newVendor.CompanyWeb = account.Vendor_CompanyWeb;
                newVendor.ContactPerson = account.Vendor_ContactPerson;
                newVendor.CPNumber = account.Vendor_CPNumber;
                newVendor.CPEmail = account.Vendor_CPEmail;
                newVendor.CreatedDate = DateTime.Now;
                newVendor.IsSubmitted = false;

                db.T_Vendor.Add(newVendor);
                db.SaveChanges();
            }

            TempData["Message"] = "Data Updated";
            return RedirectToAction("VendorCompany");
        }

        public ActionResult VendorBank()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();

            account.Username = dbUser.UserName;
            account.Email = dbUser.email;
            account.vendorBank = (from vendor in db.T_Vendor_Bank.Where(w => w.UserId == userId)
                                  join bank in db.T_MsParameter.Where(w => w.FlagId == 6) on vendor.Bank equals bank.ParameterCode
                                  join currency in db.T_MsParameter.Where(w => w.FlagId == 7) on vendor.Currency equals currency.ParameterCode
                                  join country in db.T_MsParameter.Where(w => w.FlagId == 1) on vendor.Country equals country.ParameterCode
                                  join city in db.T_MsParameter.Where(w => w.FlagId == 2) on new { JoinProperty1 = vendor.Country, JoinProperty2 = vendor.City } equals new { JoinProperty1 = city.ParentCode, JoinProperty2 = city.ParameterCode }
                                  select new Vendor_Bank
                                  {
                                      BankId = vendor.Id,
                                      Bank = vendor.Bank,
                                      BankName = bank.Description,
                                      Branch = vendor.Branch,
                                      Country = vendor.Country,
                                      CountryName = country.Description,
                                      City = vendor.City,
                                      CityName = city.Description,
                                      Currency = vendor.Currency,
                                      CurrencyDescription = currency.Description,
                                      AccountNumber = vendor.AccountNumber,
                                      AccountName = vendor.AccountName
                                  }).OrderBy(o => o.BankName).ToList();

            List<DropdownModel> ListBank = (from param in db.T_MsParameter.Where(w => w.FlagId == 6)
                                            select new DropdownModel
                                            {
                                                stringValue = param.ParameterCode,
                                                Text = param.Description
                                            }).OrderBy(o => o.Text).ToList();
            ViewBag.ListBank = new SelectList(ListBank, "stringValue", "Text");

            List<DropdownModel> ListCurrency = (from param in db.T_MsParameter.Where(w => w.FlagId == 7)
                                                select new DropdownModel
                                                {
                                                    stringValue = param.ParameterCode,
                                                    Text = param.Description
                                                }).OrderBy(o => o.Text).ToList();
            ViewBag.ListCurrency = new SelectList(ListCurrency, "stringValue", "Text");

            List<DropdownModel> ListCountry = (from param in db.T_MsParameter.Where(w => w.FlagId == 1)
                                               select new DropdownModel
                                               {
                                                   stringValue = param.ParameterCode,
                                                   Text = param.Description
                                               }).OrderBy(o => o.Text).ToList();
            ViewBag.ListCountry = new SelectList(ListCountry, "stringValue", "Text");

            List<DropdownModel> ListCity = new List<DropdownModel>();
            ViewBag.ListCity = new SelectList(ListCity, "stringValue", "Text");

            return View(account);
        }

        public ActionResult SaveChanges_VendorBank(UserAccount account)
        {
            string userId = Session["LogedUserId"].ToString();
            T_Vendor dbVendor = db.T_Vendor.Where(w => w.UserId == userId).FirstOrDefault();

            if (dbVendor != null)
                dbVendor.IsSubmitted = false;

            if (account.Bank_Id.ToString() != "")
            {
                T_Vendor_Bank dbVendor_Bank = db.T_Vendor_Bank.Where(w => w.UserId == userId && w.Id == account.Bank_Id).FirstOrDefault();

                dbVendor_Bank.Bank = account.Vendor_Bank;
                dbVendor_Bank.Branch = account.Vendor_Branch;
                dbVendor_Bank.Country = account.Vendor_BankCountry;
                dbVendor_Bank.City = account.Vendor_BankCity;
                dbVendor_Bank.Currency = account.Vendor_Currency;
                dbVendor_Bank.AccountNumber = account.Vendor_AccountNumber;
                dbVendor_Bank.AccountName = account.Vendor_AccountName;
                dbVendor_Bank.ModifyDate = DateTime.Now;

                db.SaveChanges();
            }
            else
            {
                T_Vendor_Bank newVendor_Bank = new T_Vendor_Bank();

                newVendor_Bank.UserId = Session["LogedUserId"].ToString();
                newVendor_Bank.Bank = account.Vendor_Bank;
                newVendor_Bank.Branch = account.Vendor_Branch;
                newVendor_Bank.Country = account.Vendor_BankCountry;
                newVendor_Bank.City = account.Vendor_BankCity;
                newVendor_Bank.Currency = account.Vendor_Currency;
                newVendor_Bank.AccountNumber = account.Vendor_AccountNumber;
                newVendor_Bank.AccountName = account.Vendor_AccountName;
                newVendor_Bank.CreatedDate = DateTime.Now;

                db.T_Vendor_Bank.Add(newVendor_Bank);
                db.SaveChanges();
            }

            TempData["Message"] = "Data Updated";
            return RedirectToAction("VendorBank");
        }

        public JsonResult VendorBank_Edit(int BankId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Vendor_Bank dbBank = db.T_Vendor_Bank.Where(w => w.Id == BankId).FirstOrDefault();
            result.vendorBank = dbBank;
            result.Status = true;

            if (dbBank.Country != null)
            {
                List<DropdownModel> ListCity = (from param in db.T_MsParameter.Where(w => w.FlagId == 2 && w.ParentCode == dbBank.Country)
                                                select new DropdownModel
                                                {
                                                    stringValue = param.ParameterCode,
                                                    Text = param.Description
                                                }).OrderBy(o => o.Text).ToList();
                ViewBag.ListCity = new SelectList(ListCity, "stringValue", "Text");

                result.listDropdownData = ListCity;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult VendorBank_Delete(int BankId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Vendor_Bank dbBank = db.T_Vendor_Bank.Where(w => w.Id == BankId).FirstOrDefault();
            db.T_Vendor_Bank.Remove(dbBank);
            db.SaveChanges();

            result.Message = "Data bank deleted";
            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult VendorLegal()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();
            Vendor_Legality vendorData = (from vendor in db.T_Vendor_Legal.Where(w => w.UserId == userId)
                                          select new Vendor_Legality
                                          {
                                              SIUP = vendor.SIUP,
                                              SIUPValidTo = vendor.SIUPValidTo,
                                              SIUPDocumentName = vendor.SIUPDocumentName,
                                              SIUPDocumentPath = vendor.SIUPDocumentPath,
                                              TDP = vendor.TDP,
                                              TDPValidTo = vendor.TDPValidTo,
                                              TDPDocumentName = vendor.TDPDocumentName,
                                              TDPDocumentPath = vendor.TDPDocumentPath,
                                              SKDP = vendor.SKDP,
                                              SKDPValidTo = vendor.SKDPValidTo,
                                              SKDPDocumentName = vendor.SKDPDocumentName,
                                              SKDPDocumentPath = vendor.SKDPDocumentPath,
                                              DeedOfIncorporation = vendor.DeedOfIncorporation,
                                              DoIDocumentName = vendor.DoIDocumentName,
                                              DoIDocumentPath = vendor.DoIDocumentPath,
                                              CertificateOfEstablishment = vendor.CertificateOfEstablishment,
                                              CoEDocumentName = vendor.CoEDocumentName,
                                              CoEDocumentPath = vendor.CoEDocumentPath,
                                              DeedOfChange = vendor.DeedOfChange,
                                              DoCDocumentName = vendor.DoCDocumentName,
                                              DoCDocumentPath = vendor.DoCDocumentPath,
                                              CertificateOfChange = vendor.CertificateOfChange,
                                              CoCDocumentName = vendor.CoCDocumentName,
                                              CoCDocumentPath = vendor.CoCDocumentPath
                                          }).FirstOrDefault();

            account.Username = dbUser.UserName;
            account.Email = dbUser.email;

            if (vendorData != null)
            {
                account.Vendor_SIUP = vendorData.SIUP;
                account.Vendor_SIUPValidTo = vendorData.SIUPValidTo;
                account.Vendor_SIUPDocumentName = vendorData.SIUPDocumentName;
                account.Vendor_SIUPDocumentPath = vendorData.SIUPDocumentPath;
                account.Vendor_TDP = vendorData.TDP;
                account.Vendor_TDPValidTo = vendorData.TDPValidTo;
                account.Vendor_TDPDocumentName = vendorData.TDPDocumentName;
                account.Vendor_TDPDocumentPath = vendorData.TDPDocumentPath;
                account.Vendor_SKDP = vendorData.SKDP;
                account.Vendor_SKDPValidTo = vendorData.SKDPValidTo;
                account.Vendor_SKDPDocumentName = vendorData.SKDPDocumentName;
                account.Vendor_SKDPDocumentPath = vendorData.SKDPDocumentPath;
                account.Vendor_DeedOfIncorporation = vendorData.DeedOfIncorporation;
                account.Vendor_DoIDocumentName = vendorData.DoIDocumentName;
                account.Vendor_DoIDocumentPath = vendorData.DoIDocumentPath;
                account.Vendor_CertificateOfEstablishment = vendorData.CertificateOfEstablishment;
                account.Vendor_CoEDocumentName = vendorData.CoEDocumentName;
                account.Vendor_CoEDocumentPath = vendorData.CoEDocumentPath;
                account.Vendor_DeedOfChange = vendorData.DeedOfChange;
                account.Vendor_DoCDocumentName = vendorData.DoCDocumentName;
                account.Vendor_DoCDocumentPath = vendorData.DoCDocumentPath;
                account.Vendor_CertificateOfChange = vendorData.CertificateOfChange;
                account.Vendor_CoCDocumentName = vendorData.CoCDocumentName;
                account.Vendor_CoCDocumentPath = vendorData.CoCDocumentPath;
            }

            return View(account);
        }

        public ActionResult SaveChanges_VendorLegal(UserAccount account, HttpPostedFileBase uploadFileSIUP, HttpPostedFileBase uploadFileTDP, HttpPostedFileBase uploadFileSKDP, HttpPostedFileBase uploadFileDoI, HttpPostedFileBase uploadFileCoE, HttpPostedFileBase uploadFileDoC, HttpPostedFileBase uploadFileCoC)
        {
            string userId = Session["LogedUserId"].ToString();
            T_Vendor_Legal dbVendor_Legal = db.T_Vendor_Legal.Where(w => w.UserId == userId).FirstOrDefault();
            T_Vendor dbVendor = db.T_Vendor.Where(w => w.UserId == userId).FirstOrDefault();

            if (dbVendor != null)
                dbVendor.IsSubmitted = false;

            if (dbVendor_Legal != null)
            {
                dbVendor_Legal.SIUP = account.Vendor_SIUP;
                dbVendor_Legal.SIUPValidTo = account.Vendor_SIUPValidTo;
                dbVendor_Legal.TDP = account.Vendor_TDP;
                dbVendor_Legal.TDPValidTo = account.Vendor_TDPValidTo;
                dbVendor_Legal.SKDP = account.Vendor_SKDP;
                dbVendor_Legal.SKDPValidTo = account.Vendor_SKDPValidTo;
                dbVendor_Legal.DeedOfIncorporation = account.Vendor_DeedOfIncorporation;
                dbVendor_Legal.CertificateOfEstablishment = account.Vendor_CertificateOfEstablishment;
                dbVendor_Legal.DeedOfChange = account.Vendor_DeedOfChange;
                dbVendor_Legal.CertificateOfChange = account.Vendor_CertificateOfChange;
                dbVendor_Legal.ModifyDate = DateTime.Now;

                if (uploadFileSIUP != null)
                {
                    if (uploadFileSIUP.ContentLength > 0 && uploadFileSIUP.ContentLength <= 2000000)
                    {
                        if (dbVendor_Legal.SIUPDocumentPath != null)
                        {
                            System.IO.File.Delete(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId + "\\" + dbVendor_Legal.SIUPDocumentName);
                        }

                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                        //var filename = Path.GetFileName(uploadFileSIUP.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "SIUP.pdf");
                        uploadFileSIUP.SaveAs(filepath);

                        dbVendor_Legal.SIUPDocumentName = "SIUP.pdf";
                        dbVendor_Legal.SIUPDocumentPath = "/Content/vendorfiles/" + userId + "/" + "SIUP.pdf";
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("VendorLegal");
                    }
                }

                if (uploadFileTDP != null)
                {
                    if (uploadFileTDP.ContentLength > 0 && uploadFileTDP.ContentLength <= 2000000)
                    {
                        if (dbVendor_Legal.TDPDocumentPath != null)
                        {
                            System.IO.File.Delete(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId + "\\" + dbVendor_Legal.TDPDocumentName);
                        }

                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                        //var filename = Path.GetFileName(uploadFileTDP.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "TDP.pdf");
                        uploadFileTDP.SaveAs(filepath);

                        dbVendor_Legal.TDPDocumentName = "TDP.pdf";
                        dbVendor_Legal.TDPDocumentPath = "/Content/vendorfiles/" + userId + "/" + "TDP.pdf";
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("VendorLegal");
                    }
                }

                if (uploadFileSKDP != null)
                {
                    if (uploadFileSKDP.ContentLength > 0 && uploadFileSKDP.ContentLength <= 2000000)
                    {
                        if (dbVendor_Legal.SKDPDocumentPath != null)
                        {
                            System.IO.File.Delete(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId + "\\" + dbVendor_Legal.SKDPDocumentName);
                        }

                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                        //var filename = Path.GetFileName(uploadFileSKDP.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "SKDP.pdf");
                        uploadFileSKDP.SaveAs(filepath);

                        dbVendor_Legal.SKDPDocumentName = "SKDP.pdf";
                        dbVendor_Legal.SKDPDocumentPath = "/Content/vendorfiles/" + userId + "/" + "SKDP.pdf";
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("VendorLegal");
                    }
                }

                if (uploadFileDoI != null)
                {
                    if (uploadFileDoI.ContentLength > 0 && uploadFileDoI.ContentLength <= 2000000)
                    {
                        if (dbVendor_Legal.DoIDocumentPath != null)
                        {
                            System.IO.File.Delete(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId + "\\" + dbVendor_Legal.DoIDocumentName);
                        }

                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                        //var filename = Path.GetFileName(uploadFileDoI.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "Deed of Incorporation.pdf");
                        uploadFileDoI.SaveAs(filepath);

                        dbVendor_Legal.DoIDocumentName = "Deed of Incorporation.pdf";
                        dbVendor_Legal.DoIDocumentPath = "/Content/vendorfiles/" + userId + "/" + "Deed of Incorporation.pdf";
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("VendorLegal");
                    }
                }

                if (uploadFileCoE != null)
                {
                    if (uploadFileCoE.ContentLength > 0 && uploadFileCoE.ContentLength <= 2000000)
                    {
                        if (dbVendor_Legal.CoEDocumentPath != null)
                        {
                            System.IO.File.Delete(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId + "\\" + dbVendor_Legal.CoEDocumentName);
                        }

                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                        //var filename = Path.GetFileName(uploadFileCoE.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "Certificate of Establishment.pdf");
                        uploadFileCoE.SaveAs(filepath);

                        dbVendor_Legal.CoEDocumentName = "Certificate of Establishment.pdf";
                        dbVendor_Legal.CoEDocumentPath = "/Content/vendorfiles/" + userId + "/" + "Certificate of Establishment.pdf";
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("VendorLegal");
                    }
                }

                if (uploadFileDoC != null)
                {
                    if (uploadFileDoC.ContentLength > 0 && uploadFileDoC.ContentLength <= 2000000)
                    {
                        if (dbVendor_Legal.DoCDocumentPath != null)
                        {
                            System.IO.File.Delete(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId + "\\" + dbVendor_Legal.DoCDocumentName);
                        }

                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                        //var filename = Path.GetFileName(uploadFileDoC.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "Deed of Change.pdf");
                        uploadFileDoC.SaveAs(filepath);

                        dbVendor_Legal.DoCDocumentName = "Deed of Change.pdf";
                        dbVendor_Legal.DoCDocumentPath = "/Content/vendorfiles/" + userId + "/" + "Deed of Change.pdf";
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("VendorLegal");
                    }
                }

                if (uploadFileCoC != null)
                {
                    if (uploadFileCoC.ContentLength > 0 && uploadFileCoC.ContentLength <= 2000000)
                    {
                        if (dbVendor_Legal.CoCDocumentPath != null)
                        {
                            System.IO.File.Delete(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId + "\\" + dbVendor_Legal.CoCDocumentName);
                        }

                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                        //var filename = Path.GetFileName(uploadFileCoC.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "Certificate of Change.pdf");
                        uploadFileCoC.SaveAs(filepath);

                        dbVendor_Legal.CoCDocumentName = "Certificate of Change.pdf";
                        dbVendor_Legal.CoCDocumentPath = "/Content/vendorfiles/" + userId + "/" + "Certificate of Change.pdf";
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("VendorLegal");
                    }
                }

                db.SaveChanges();
            }
            else
            {
                T_Vendor_Legal newVendor = new T_Vendor_Legal();
                newVendor.UserId = userId;
                newVendor.SIUP = account.Vendor_SIUP;
                newVendor.SIUPValidTo = account.Vendor_SIUPValidTo;
                newVendor.TDP = account.Vendor_TDP;
                newVendor.TDPValidTo = account.Vendor_TDPValidTo;
                newVendor.SKDP = account.Vendor_SKDP;
                newVendor.SKDPValidTo = account.Vendor_SKDPValidTo;
                newVendor.DeedOfIncorporation = account.Vendor_DeedOfIncorporation;
                newVendor.CertificateOfEstablishment = account.Vendor_CertificateOfEstablishment;
                newVendor.DeedOfChange = account.Vendor_DeedOfChange;
                newVendor.CertificateOfChange = account.Vendor_CertificateOfChange;
                newVendor.CreatedDate = DateTime.Now;

                if (uploadFileSIUP.ContentLength > 0 && uploadFileSIUP.ContentLength <= 2000000)
                {
                    Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                    //var filename = Path.GetFileName(uploadFileSIUP.FileName);
                    var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "SIUP.pdf");
                    uploadFileSIUP.SaveAs(filepath);

                    newVendor.SIUPDocumentName = "SIUP.pdf";
                    newVendor.SIUPDocumentPath = "/Content/vendorfiles/" + userId + "/" + "SIUP.pdf";
                }
                else
                {
                    TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                    return RedirectToAction("VendorLegal");
                }

                if (uploadFileTDP.ContentLength > 0 && uploadFileTDP.ContentLength <= 2000000)
                {
                    Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                    //var filename = Path.GetFileName(uploadFileTDP.FileName);
                    var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "TDP.pdf");
                    uploadFileTDP.SaveAs(filepath);

                    newVendor.TDPDocumentName = "TDP.pdf";
                    newVendor.TDPDocumentPath = "/Content/vendorfiles/" + userId + "/" + "TDP.pdf";
                }
                else
                {
                    TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                    return RedirectToAction("VendorLegal");
                }

                if (uploadFileSKDP != null)
                {
                    if (uploadFileSKDP.ContentLength > 0 && uploadFileSKDP.ContentLength <= 2000000)
                    {
                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                        //var filename = Path.GetFileName(uploadFileSKDP.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "SKDP.pdf");
                        uploadFileSKDP.SaveAs(filepath);

                        newVendor.SKDPDocumentName = "SKDP.pdf";
                        newVendor.SKDPDocumentPath = "/Content/vendorfiles/" + userId + "/" + "SKDP.pdf";
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("VendorLegal");
                    }
                }

                if (uploadFileDoI.ContentLength > 0 && uploadFileDoI.ContentLength <= 2000000)
                {
                    Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                    //var filename = Path.GetFileName(uploadFileDoI.FileName);
                    var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "Deed of Incorporation.pdf");
                    uploadFileDoI.SaveAs(filepath);

                    newVendor.DoIDocumentName = "Deed of Incorporation.pdf";
                    newVendor.DoIDocumentPath = "/Content/vendorfiles/" + userId + "/" + "Deed of Incorporation.pdf";
                }
                else
                {
                    TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                    return RedirectToAction("VendorLegal");
                }

                if (uploadFileCoE != null)
                {
                    if (uploadFileCoE.ContentLength > 0 && uploadFileCoE.ContentLength <= 2000000)
                    {
                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                        //var filename = Path.GetFileName(uploadFileCoE.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "Certificate of Establishment.pdf");
                        uploadFileCoE.SaveAs(filepath);

                        newVendor.CoEDocumentName = "Certificate of Establishment.pdf";
                        newVendor.CoEDocumentPath = "/Content/vendorfiles/" + userId + "/" + "Certificate of Establishment.pdf";
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("VendorLegal");
                    }
                }

                if (uploadFileDoC != null)
                {
                    if (uploadFileDoC.ContentLength > 0 && uploadFileDoC.ContentLength <= 2000000)
                    {
                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                        //var filename = Path.GetFileName(uploadFileDoC.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "Deed of Change.pdf");
                        uploadFileDoC.SaveAs(filepath);

                        newVendor.DoCDocumentName = "Deed of Change.pdf";
                        newVendor.DoCDocumentPath = "/Content/vendorfiles/" + userId + "/" + "Deed of Change.pdf";
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("VendorLegal");
                    }
                }

                if (uploadFileCoC != null)
                {
                    if (uploadFileCoC.ContentLength > 0 && uploadFileCoC.ContentLength <= 2000000)
                    {
                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                        //var filename = Path.GetFileName(uploadFileCoC.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "Certificate of Change.pdf");
                        uploadFileCoC.SaveAs(filepath);

                        newVendor.CoCDocumentName = "Certificate of Change.pdf";
                        newVendor.CoCDocumentPath = "/Content/vendorfiles/" + userId + "/" + "Certificate of Change.pdf";
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("VendorLegal");
                    }
                }

                db.T_Vendor_Legal.Add(newVendor);
                db.SaveChanges();
            }

            TempData["Message"] = "Data Updated";
            return RedirectToAction("VendorLegal");
        }

        public ActionResult VendorTax()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();
            Vendor_Tax vendorData = (from vendor in db.T_Vendor_Tax.Where(w => w.UserId == userId)
                                     select new Vendor_Tax
                                     {
                                         NPWP = vendor.NPWP,
                                         NPWPAddress = vendor.NPWPAddress,
                                         NPWPDocumentName = vendor.NPWPDocumentName,
                                         NPWPDocumentPath = vendor.NPWPDocumentPath,
                                         SKPKP = vendor.SKPKP,
                                         SKPKPDocumentName = vendor.SKPKPDocumentName,
                                         SKPKPDocumentPath = vendor.SKPKPDocumentPath,
                                         PPn = vendor.PPn,
                                         PPh = vendor.PPh
                                     }).FirstOrDefault();

            account.Username = dbUser.UserName;
            account.Email = dbUser.email;

            if (vendorData != null)
            {
                account.Vendor_NPWP = vendorData.NPWP;
                account.Vendor_NPWPAddress = vendorData.NPWPAddress;
                account.Vendor_NPWPDocumentName = vendorData.NPWPDocumentName;
                account.Vendor_NPWPDocumentPath = vendorData.NPWPDocumentPath;
                account.Vendor_SKPKP = vendorData.SKPKP;
                account.Vendor_SKPKPDocumentName = vendorData.SKPKPDocumentName;
                account.Vendor_SKPKPDocumentPath = vendorData.SKPKPDocumentPath;
                account.Vendor_PPn = vendorData.PPn;
                account.Vendor_PPh = vendorData.PPh;
            }

            return View(account);
        }

        public ActionResult SaveChanges_VendorTax(UserAccount account, HttpPostedFileBase uploadFileNPWP, HttpPostedFileBase uploadFileSKPKP)
        {
            string userId = Session["LogedUserId"].ToString();
            T_Vendor_Tax dbVendor_Tax = db.T_Vendor_Tax.Where(w => w.UserId == userId).FirstOrDefault();
            T_Vendor dbVendor = db.T_Vendor.Where(w => w.UserId == userId).FirstOrDefault();

            if (dbVendor != null)
                dbVendor.IsSubmitted = false;

            if (dbVendor_Tax != null)
            {
                dbVendor_Tax.NPWP = account.Vendor_NPWP;
                dbVendor_Tax.NPWPAddress = account.Vendor_NPWPAddress;
                dbVendor_Tax.SKPKP = account.Vendor_SKPKP;
                dbVendor_Tax.PPn = account.Vendor_PPn;
                dbVendor_Tax.PPh = account.Vendor_PPh;
                dbVendor_Tax.ModifyDate = DateTime.Now;

                if (uploadFileNPWP != null)
                {
                    if (uploadFileNPWP.ContentLength > 0 && uploadFileNPWP.ContentLength <= 2000000)
                    {
                        if (dbVendor_Tax.NPWPDocumentPath != null)
                        {
                            System.IO.File.Delete(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId + "\\" + dbVendor_Tax.NPWPDocumentName);
                        }

                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                        //var filename = Path.GetFileName(uploadFileSIUP.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "NPWP.pdf");
                        uploadFileNPWP.SaveAs(filepath);

                        dbVendor_Tax.NPWPDocumentName = "NPWP.pdf";
                        dbVendor_Tax.NPWPDocumentPath = "/Content/vendorfiles/" + userId + "/" + "NPWP.pdf";
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("VendorTax");
                    }
                }

                if (uploadFileSKPKP != null)
                {
                    if (uploadFileSKPKP.ContentLength > 0 && uploadFileSKPKP.ContentLength <= 2000000)
                    {
                        if (dbVendor_Tax.NPWPDocumentPath != null)
                        {
                            System.IO.File.Delete(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId + "\\" + dbVendor_Tax.SKPKPDocumentName);
                        }

                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                        //var filename = Path.GetFileName(uploadFileSIUP.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "SK PKP.pdf");
                        uploadFileSKPKP.SaveAs(filepath);

                        dbVendor_Tax.SKPKPDocumentName = "SK PKP.pdf";
                        dbVendor_Tax.SKPKPDocumentPath = "/Content/vendorfiles/" + userId + "/" + "SK PKP.pdf";
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("VendorTax");
                    }
                }

                db.SaveChanges();
            }
            else
            {
                T_Vendor_Tax newVendor = new T_Vendor_Tax();
                newVendor.UserId = userId;
                newVendor.NPWP = account.Vendor_NPWP;
                newVendor.NPWPAddress = account.Vendor_NPWPAddress;
                newVendor.SKPKP = account.Vendor_SKPKP;
                newVendor.PPn = account.Vendor_PPn;
                newVendor.PPh = account.Vendor_PPh;
                newVendor.CreatedDate = DateTime.Now;

                if (uploadFileNPWP.ContentLength > 0 && uploadFileNPWP.ContentLength <= 2000000)
                {
                    Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                    //var filename = Path.GetFileName(uploadFileTDP.FileName);
                    var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "NPWP.pdf");
                    uploadFileNPWP.SaveAs(filepath);

                    newVendor.NPWPDocumentName = "NPWP.pdf";
                    newVendor.NPWPDocumentPath = "/Content/vendorfiles/" + userId + "/" + "NPWP.pdf";
                }
                else
                {
                    TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                    return RedirectToAction("VendorLegal");
                }

                if (uploadFileSKPKP != null)
                {
                    if (uploadFileSKPKP.ContentLength > 0 && uploadFileSKPKP.ContentLength <= 2000000)
                    {
                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + userId);

                        //var filename = Path.GetFileName(uploadFileSKDP.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + userId), "SK PKP.pdf");
                        uploadFileSKPKP.SaveAs(filepath);

                        newVendor.SKPKPDocumentName = "SK PKP.pdf";
                        newVendor.SKPKPDocumentPath = "/Content/vendorfiles/" + userId + "/" + "SK PKP.pdf";
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("VendorTax");
                    }
                }

                db.T_Vendor_Tax.Add(newVendor);
                db.SaveChanges();
            }

            TempData["Message"] = "Data Updated";
            return RedirectToAction("VendorTax");
        }

        public ActionResult VendorBranches()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();

            account.Username = dbUser.UserName;
            account.Email = dbUser.email;
            account.vendorBranches = (from vendor in db.T_Vendor_Branches.Where(w => w.UserId == userId)
                                      join country in db.T_MsParameter.Where(w => w.FlagId == 1) on vendor.Country equals country.ParameterCode
                                      join city in db.T_MsParameter.Where(w => w.FlagId == 2) on new { JoinProperty1 = vendor.Country, JoinProperty2 = vendor.City } equals new { JoinProperty1 = city.ParentCode, JoinProperty2 = city.ParameterCode }
                                      select new Vendor_Branches
                                      {
                                          BranchesId = vendor.Id,
                                          Country = vendor.Country,
                                          CountryName = country.Description,
                                          City = vendor.City,
                                          CityName = city.Description,
                                          Address = vendor.Address,
                                          PostalCode = vendor.PostalCode,
                                          Email = vendor.Email,
                                          Phone = vendor.Phone,
                                          Fax = vendor.Fax,
                                          ContactPerson = vendor.ContactPerson,
                                          CPNumber = vendor.CPNumber,
                                          CPEmail = vendor.CPEmail
                                      }).OrderBy(o => o.BranchesId).ToList();

            List<DropdownModel> ListCountry = (from param in db.T_MsParameter.Where(w => w.FlagId == 1)
                                               select new DropdownModel
                                               {
                                                   stringValue = param.ParameterCode,
                                                   Text = param.Description
                                               }).OrderBy(o => o.Text).ToList();
            ViewBag.ListCountry = new SelectList(ListCountry, "stringValue", "Text");

            List<DropdownModel> ListCity = new List<DropdownModel>();
            ViewBag.ListCity = new SelectList(ListCity, "stringValue", "Text");

            return View(account);
        }

        public ActionResult SaveChanges_VendorBranches(UserAccount account)
        {
            string userId = Session["LogedUserId"].ToString();
            T_Vendor dbVendor = db.T_Vendor.Where(w => w.UserId == userId).FirstOrDefault();

            if (dbVendor != null)
                dbVendor.IsSubmitted = false;

            if (account.Branches_Id.ToString() != "")
            {
                T_Vendor_Branches dbVendor_Branches = db.T_Vendor_Branches.Where(w => w.UserId == userId && w.Id == account.Branches_Id).FirstOrDefault();

                dbVendor_Branches.Country = account.Vendor_Branches_Country;
                dbVendor_Branches.City = account.Vendor_Branches_City;
                dbVendor_Branches.Address = account.Vendor_Branches_Address;
                dbVendor_Branches.PostalCode = account.Vendor_Branches_PostalCode;
                dbVendor_Branches.Email = account.Vendor_Branches_Email;
                dbVendor_Branches.Phone = account.Vendor_Branches_Phone;
                dbVendor_Branches.Fax = account.Vendor_Branches_Fax;
                dbVendor_Branches.ContactPerson = account.Vendor_Branches_ContactPerson;
                dbVendor_Branches.CPNumber = account.Vendor_Branches_CPNumber;
                dbVendor_Branches.CPEmail = account.Vendor_Branches_CPEmail;
                dbVendor_Branches.ModifyDate = DateTime.Now;

                db.SaveChanges();
            }
            else
            {
                T_Vendor_Branches newVendor_Branches = new T_Vendor_Branches();

                newVendor_Branches.UserId = Session["LogedUserId"].ToString();
                newVendor_Branches.Country = account.Vendor_Branches_Country;
                newVendor_Branches.City = account.Vendor_Branches_City;
                newVendor_Branches.Address = account.Vendor_Branches_Address;
                newVendor_Branches.PostalCode = account.Vendor_Branches_PostalCode;
                newVendor_Branches.Email = account.Vendor_Branches_Email;
                newVendor_Branches.Phone = account.Vendor_Branches_Phone;
                newVendor_Branches.Fax = account.Vendor_Branches_Fax;
                newVendor_Branches.ContactPerson = account.Vendor_Branches_ContactPerson;
                newVendor_Branches.CPNumber = account.Vendor_Branches_CPNumber;
                newVendor_Branches.CPEmail = account.Vendor_Branches_CPEmail;
                newVendor_Branches.CreatedDate = DateTime.Now;

                db.T_Vendor_Branches.Add(newVendor_Branches);
                db.SaveChanges();
            }

            TempData["Message"] = "Data Updated";
            return RedirectToAction("VendorBranches");
        }

        public JsonResult VendorBranches_Edit(int BranchesId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Vendor_Branches dbBranches = db.T_Vendor_Branches.Where(w => w.Id == BranchesId).FirstOrDefault();
            result.vendorBranches = dbBranches;
            result.Status = true;

            if (dbBranches.Country != null)
            {
                List<DropdownModel> ListCity = (from param in db.T_MsParameter.Where(w => w.FlagId == 2 && w.ParentCode == dbBranches.Country)
                                                select new DropdownModel
                                                {
                                                    stringValue = param.ParameterCode,
                                                    Text = param.Description
                                                }).OrderBy(o => o.Text).ToList();
                ViewBag.ListCity = new SelectList(ListCity, "stringValue", "Text");

                result.listDropdownData = ListCity;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult VendorBranches_Delete(int BranchesId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Vendor_Branches dbBranches = db.T_Vendor_Branches.Where(w => w.Id == BranchesId).FirstOrDefault();
            db.T_Vendor_Branches.Remove(dbBranches);
            db.SaveChanges();

            result.Message = "Data branches deleted";
            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult VendorDocument()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();

            account.Username = dbUser.UserName;
            account.Email = dbUser.email;
            account.vendorDocument = (from vendor in db.T_Vendor_Document.Where(w => w.UserId == userId)
                                      select new Vendor_Document
                                      {
                                          DocumentId = vendor.Id,
                                          Description = vendor.Description,
                                          FileName = vendor.FileName,
                                          FilePath = vendor.FilePath,
                                          Notes = vendor.Notes
                                      }).OrderBy(o => o.DocumentId).ToList();

            return View(account);
        }

        public ActionResult SaveChanges_VendorDocument(UserAccount account, HttpPostedFileBase uploadFile)
        {
            string userId = Session["LogedUserId"].ToString();

            if (account.Vendor_Document_Id.ToString() != "")
            {
                T_Vendor_Document dbVendor_Document = db.T_Vendor_Document.Where(w => w.UserId == userId && w.Id == account.Vendor_Document_Id).FirstOrDefault();

                dbVendor_Document.Description = account.Vendor_Document_Description;

                if (uploadFile != null)
                {
                    if (uploadFile.ContentLength > 0 && uploadFile.ContentLength <= 2000000)
                    {
                        string Year = DateTime.Now.Year.ToString(), Month = "0" + DateTime.Now.Month.ToString(), Day = "0" + DateTime.Now.Day.ToString(), Hour = "0" + DateTime.Now.Hour.ToString(), Minute = "0" + DateTime.Now.Minute.ToString(), Second = "0" + DateTime.Now.Second.ToString();
                        string Time = Year + Month.Substring(Month.Length - 2) + Day.Substring(Day.Length - 2) + "_" + Hour.Substring(Hour.Length - 2) + Minute.Substring(Minute.Length - 2) + Second.Substring(Second.Length - 2);

                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + Time);

                        if (dbVendor_Document.FilePath != null)
                        {
                            System.IO.File.Delete(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + dbVendor_Document.FilePath.Substring(21, 16) + dbVendor_Document.FileName);
                            Directory.Delete(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + dbVendor_Document.FilePath.Substring(21, 15));
                        }

                        var filename = Path.GetFileName(uploadFile.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + Time), filename);
                        uploadFile.SaveAs(filepath);

                        dbVendor_Document.FileName = account.Vendor_Document_FileName;
                        dbVendor_Document.FilePath = "/Content/vendorfiles/" + Time + "/" + filename;
                    }
                    else
                    {
                        TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                        return RedirectToAction("VendorDocument");
                    }
                }

                dbVendor_Document.Notes = account.Vendor_Document_Notes;
                dbVendor_Document.ModifyDate = DateTime.Now;

                db.SaveChanges();
            }
            else
            {
                if (uploadFile.ContentLength > 0 && uploadFile.ContentLength <= 2000000)
                {
                    string Year = DateTime.Now.Year.ToString(), Month = "0" + DateTime.Now.Month.ToString(), Day = "0" + DateTime.Now.Day.ToString(), Hour = "0" + DateTime.Now.Hour.ToString(), Minute = "0" + DateTime.Now.Minute.ToString(), Second = "0" + DateTime.Now.Second.ToString();
                    string Time = Year + Month.Substring(Month.Length - 2) + Day.Substring(Day.Length - 2) + "_" + Hour.Substring(Hour.Length - 2) + Minute.Substring(Minute.Length - 2) + Second.Substring(Second.Length - 2);

                    T_Vendor_Document newVendor_Document = new T_Vendor_Document();

                    newVendor_Document.UserId = Session["LogedUserId"].ToString();
                    newVendor_Document.Description = account.Vendor_Document_Description;

                    Directory.CreateDirectory(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + Time);

                    var filename = Path.GetFileName(uploadFile.FileName);
                    var filepath = Path.Combine(Server.MapPath("~/Content/vendorfiles/" + Time), filename);
                    uploadFile.SaveAs(filepath);

                    newVendor_Document.FileName = filename;
                    newVendor_Document.FilePath = "/Content/vendorfiles/" + Time + "/" + filename;

                    newVendor_Document.Notes = account.Vendor_Document_Notes;
                    newVendor_Document.CreatedDate = DateTime.Now;

                    db.T_Vendor_Document.Add(newVendor_Document);
                    db.SaveChanges();
                }
                else
                {
                    TempData["Message"] = "Maximal file size is 2mb, upload document failed";
                    return RedirectToAction("VendorDocument");
                }
            }

            TempData["Message"] = "Data Updated";
            return RedirectToAction("VendorDocument");
        }

        public JsonResult VendorDocument_Edit(int DocumentId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Vendor_Document dbDocument = db.T_Vendor_Document.Where(w => w.Id == DocumentId).FirstOrDefault();
            result.vendorDocument = dbDocument;
            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult VendorDocument_Delete(int DocumentId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Vendor_Document dbDocument = db.T_Vendor_Document.Where(w => w.Id == DocumentId).FirstOrDefault();

            if (dbDocument.FilePath != null)
            {
                System.IO.File.Delete(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + dbDocument.FilePath.Substring(21, 16) + dbDocument.FileName);
                Directory.Delete(WebConfigurationManager.AppSettings["VendorDocumentPathFolder"] + dbDocument.FilePath.Substring(21, 15));
            }

            db.T_Vendor_Document.Remove(dbDocument);
            db.SaveChanges();

            result.Message = "Document deleted";
            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult VendorSubmit()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();
            Vendor_Submit vendorData = (from vendor in db.T_Vendor.Where(w => w.UserId == userId)
                                        select new Vendor_Submit
                                         {
                                             IsSubmitted = vendor.IsSubmitted,
                                             Status = vendor.IsSubmitted ? "Submitted" : "Not Submitted"
                                         }).FirstOrDefault();

            account.Username = dbUser.UserName;
            account.Email = dbUser.email;

            if (vendorData != null)
            {
                account.isSubmitted = vendorData.IsSubmitted;
                account.SubmitStatus = vendorData.Status;
            }

            return View(account);
        }

        public JsonResult VendorSubmit_Submit()
        {
            AjaxReturn result = new AjaxReturn();
            string userId = Session["LogedUserId"].ToString();
            T_Vendor dbVendor = db.T_Vendor.Where(w => w.UserId == userId).FirstOrDefault();
            T_Vendor_Bank dbVendor_Bank = db.T_Vendor_Bank.Where(w => w.UserId == userId).FirstOrDefault();
            T_Vendor_Legal dbVendor_Legal = db.T_Vendor_Legal.Where(w => w.UserId == userId).FirstOrDefault();
            T_Vendor_Tax dbVendor_Tax = db.T_Vendor_Tax.Where(w => w.UserId == userId).FirstOrDefault();

            if (dbVendor != null && dbVendor_Bank != null && dbVendor_Legal != null && dbVendor_Tax != null)
            {
                dbVendor.IsSubmitted = true;
                db.SaveChanges();

                //result.Message = "Data Submitted!";
                TempData["Message"] = "Data Submitted!";
                result.Status = true;
            }
            else
            {
                //result.Message = "Please fill all mandatory data before submitting your data!";
                TempData["Message"] = "Please fill all mandatory data before submitting your data!";
                result.Status = false;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult VendorItem()
        {
            UserAccount account = new UserAccount();
            string userId = Session["LogedUserId"].ToString();

            account.UserId = userId;
            T_MsUser dbUser = db.T_MsUser.Where(w => w.UserId == userId).First();

            account.Username = dbUser.UserName;
            account.Email = dbUser.email;
            account.vendorItem = (from vendor in db.T_Vendor_Item.Where(w => w.UserId == userId)
                                  join uom in db.T_MsParameter.Where(w => w.FlagId == 14) on vendor.PRIMARY_UOM_CODE equals uom.ParameterCode
                                  select new Vendor_Item
                                  {
                                      ItemId = vendor.Id,
                                      DESCRIPTION = vendor.DESCRIPTION,
                                      PRIMARY_UOM_CODE = vendor.PRIMARY_UOM_CODE,
                                      PRIMARY_UOM_CODE_DESCRIPTION = uom.Description,
                                      Notes = vendor.Notes
                                  }).OrderBy(o => o.ItemId).ToList();

            List<DropdownModel> ListUOM = (from param in db.T_MsParameter.Where(w => w.FlagId == 14)
                                           select new DropdownModel
                                           {
                                               stringValue = param.ParameterCode,
                                               Text = param.Description
                                           }).OrderBy(o => o.Text).ToList();
            ViewBag.ListUOM = new SelectList(ListUOM, "stringValue", "Text");

            return View(account);
        }

        public ActionResult SaveChanges_VendorItem(UserAccount account)
        {
            string userId = Session["LogedUserId"].ToString();

            if (account.Item_Id.ToString() != "")
            {
                T_Vendor_Item dbVendor_Item = db.T_Vendor_Item.Where(w => w.UserId == userId && w.Id == account.Item_Id).FirstOrDefault();

                dbVendor_Item.DESCRIPTION = account.Vendor_Item_Description;
                dbVendor_Item.PRIMARY_UOM_CODE = account.Vendor_Item_UOM;
                dbVendor_Item.Notes = account.Vendor_Item_Notes;
                dbVendor_Item.ModifyDate = DateTime.Now;

                db.SaveChanges();
            }
            else
            {
                T_Vendor_Item newVendor_Item = new T_Vendor_Item();

                newVendor_Item.UserId = Session["LogedUserId"].ToString();
                newVendor_Item.DESCRIPTION = account.Vendor_Item_Description;
                newVendor_Item.PRIMARY_UOM_CODE = account.Vendor_Item_UOM;
                newVendor_Item.Notes = account.Vendor_Item_Notes;
                newVendor_Item.CreatedDate = DateTime.Now;

                newVendor_Item.INVOICEABLE_ITEM_FLAG = "Y";
                newVendor_Item.INVOICE_ENABLED_FLAG = "Y";
                newVendor_Item.ALLOW_ITEM_DESC_UPDATE_FLAG = "N";
                newVendor_Item.TRANSACTION_TYPE = "CREATE";
                newVendor_Item.PROCESS_FLAG = "1";
                newVendor_Item.INVENTORY_ITEM_FLAG = "Y";
                newVendor_Item.STOCK_ENABLED_FLAG = "Y";
                newVendor_Item.MTL_TRANSACTIONS_ENABLED_FLAG = "Y";
                newVendor_Item.RESERVEABLE_TYPE = "1";
                newVendor_Item.COSTING_ENABLED_FLAG = "Y";
                newVendor_Item.INVENTORY_ASSET_FLAG = "Y";
                newVendor_Item.PURCHASING_ITEM_FLAG = "Y";
                newVendor_Item.PURCHASING_ENABLED_FLAG = "Y";
                newVendor_Item.RECEIPT_REQUIRED_FLAG = "Y";
                newVendor_Item.MUST_USE_APPROVED_VENDOR_FLAG = "N";
                newVendor_Item.CUSTOMER_ORDER_ENABLED_FLAG = "N";
                newVendor_Item.CUSTOMER_ORDER_FLAG = "N";
                newVendor_Item.SHIPPABLE_ITEM_FLAG = "N";
                newVendor_Item.SO_TRANSACTIONS_FLAG = "N";
                newVendor_Item.RETURNABLE_FLAG = "N";

                db.T_Vendor_Item.Add(newVendor_Item);
                db.SaveChanges();
            }

            TempData["Message"] = "Data Updated";
            return RedirectToAction("VendorItem");
        }

        public JsonResult VendorItem_Edit(int ItemId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Vendor_Item dbItem = db.T_Vendor_Item.Where(w => w.Id == ItemId).FirstOrDefault();
            result.vendorItem = dbItem;
            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult VendorItem_Delete(int ItemId)
        {
            AjaxReturn result = new AjaxReturn();

            T_Vendor_Item dbItem = db.T_Vendor_Item.Where(w => w.Id == ItemId).FirstOrDefault();
            db.T_Vendor_Item.Remove(dbItem);
            db.SaveChanges();

            result.Message = "Item Product Data deleted";
            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }



        public JsonResult ChangeListCity(string Country)
        {
            AjaxReturn result = new AjaxReturn();

            List<DropdownModel> ListCity = (from param in db.T_MsParameter.Where(w => w.FlagId == 2 && w.ParentCode == Country)
                                            select new DropdownModel
                                            {
                                                stringValue = param.ParameterCode,
                                                Text = param.Description
                                            }).OrderBy(o => o.Text).ToList();
            ViewBag.ListCity = new SelectList(ListCity, "stringValue", "Text");

            result.Message = "Success";
            result.listDropdownData = ListCity;

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
