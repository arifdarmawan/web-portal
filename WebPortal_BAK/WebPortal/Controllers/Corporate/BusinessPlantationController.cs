﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPortal.GeneralClass;
using WebPortal.Model;

namespace WebPortal.Controllers.Corporate
{
    public class BusinessPlantationController : Controller
    {
        //
        // GET: /BusinessPlantation/
        FrInternallEntities db = new FrInternallEntities();

        public ActionResult Index()
        {
            List<T_MsMenuContent> listMenuContent = db.T_MsMenuContent.Where(model => model.Menu == "Plantation").ToList();

            return View(listMenuContent);
        }

        [SessionTimeout]
        [HttpGet]
        public ActionResult EditBusinessPlantation()
        {
            List<T_MsMenuContent> listMenuContent = db.T_MsMenuContent.Where(model => model.Menu == "Plantation").ToList();
            listMenuContent.First().MenuContent = listMenuContent.First().MenuContent.Replace(";", System.Environment.NewLine);

            return View(listMenuContent);
        }

        [SessionTimeout]
        [HttpPost]
        public ActionResult EditBusinessPlantation(T_MsMenuContent content)
        {
            T_MsMenuContent menuContent = (from n in db.T_MsMenuContent
                                           where n.Menu == content.Menu
                                           select n).First();
            menuContent.MenuContent = content.MenuContent.Replace(System.Environment.NewLine, ";");
            db.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}
