﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPortal.GeneralClass;
using WebPortal.Model;

namespace WebPortal.Controllers.Corporate
{
    public class BusinessLocationMapController : Controller
    {
        //
        // GET: /BusinessLocationMap/
        FrInternallEntities db = new FrInternallEntities();

        public ActionResult Index()
        {
            List<T_MsMenuContent> listMenuContent = db.T_MsMenuContent.ToList();

            return View(listMenuContent);
        }
    }
}
