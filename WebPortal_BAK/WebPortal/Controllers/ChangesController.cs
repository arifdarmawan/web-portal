﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using WebPortal.GeneralClass;
using WebPortal.Model;
using WebPortal.Models;

namespace WebPortal.Controllers
{
    [NoDirectAccess]
    [SessionTimeout]
    public class ChangesController : Controller
    {
        //
        // GET: /Changes/
        FrInternallEntities db = new FrInternallEntities();

        public ActionResult Index()
        {
            List<T_ContentChanges> listChanges = db.T_ContentChanges.Where(w => !w.IsResponded).ToList();

            return View(listChanges);
        }

        public ActionResult Pending()
        {
            string user = Session["LogedUserId"].ToString();
            List<T_ContentChanges> listChanges = db.T_ContentChanges.Where(w => !w.IsResponded && w.PostBy == user).ToList();

            return View(listChanges);
        }

        public ActionResult View(int ChangesId)
        {
            var contentChanges = (from content in db.T_ContentChanges.Where(w => w.Id == ChangesId)

                                  // Get News Category From Master Category
                                  join category in db.T_MsCategory on content.CategoryCode equals category.CategoryCode into t
                                  from rt in t.DefaultIfEmpty()

                                  // Get File Group From Master File Group
                                  join filegroup in db.T_MsFileGroup on content.FileGroup equals filegroup.FileGroup into t2
                                  from rt2 in t2.DefaultIfEmpty()

                                  // Get Tutorial Parent From T_Tutorial (Self Join - 6 Level Parent)
                                  join parent in db.T_Tutorial on content.TutorialParent equals parent.TutorialCode into t3
                                  from rt3 in t3.DefaultIfEmpty()
                                  join parentLv2 in db.T_Tutorial on rt3.TutorialParent equals parentLv2.TutorialCode into t4
                                  from rt4 in t4.DefaultIfEmpty()
                                  join parentLv3 in db.T_Tutorial on rt4.TutorialParent equals parentLv3.TutorialCode into t5
                                  from rt5 in t5.DefaultIfEmpty()
                                  join parentLv4 in db.T_Tutorial on rt5.TutorialParent equals parentLv4.TutorialCode into t6
                                  from rt6 in t6.DefaultIfEmpty()
                                  join parentLv5 in db.T_Tutorial on rt6.TutorialParent equals parentLv5.TutorialCode into t7
                                  from rt7 in t7.DefaultIfEmpty()
                                  join parentLv6 in db.T_Tutorial on rt7.TutorialParent equals parentLv6.TutorialCode into t8
                                  from rt8 in t8.DefaultIfEmpty()

                                  // Get Project Name From Master Project
                                  join project in db.T_MsProject on content.ProjectCode equals project.ProjectCode into t9
                                  from rt9 in t9.DefaultIfEmpty()

                                  select new ContentChanges
                                  {
                                      Id = content.Id,
                                      Modul = content.Modul,
                                      Type = content.Type,
                                      NewsId = content.NewsId,
                                      Judul = content.Judul,
                                      Caption = content.Caption,
                                      Isi = content.Isi,
                                      CategoryCode = content.CategoryCode,
                                      CategoryName = rt.CategoryName,
                                      Image = content.Image,
                                      ImagePath = content.ImagePath,
                                      Attach = content.Attach,
                                      AttachPath = content.AttachPath,
                                      FileId = content.FileId,
                                      FileName = content.FileName,
                                      IdFileGroup = content.FileGroup,
                                      FileGroup = rt2.Description,
                                      Description = content.Description,
                                      LocationFile = content.LocationFile,
                                      TutorialCode = content.TutorialCode,
                                      TutorialTitle = content.TutorialTitle,
                                      TutorialDescription = content.TutorialDescription,
                                      VideoName = content.VideoName,
                                      VideoPath = content.VideoPath,
                                      TutorialParent = content.TutorialParent,
                                      TutorialParentTitle = rt8.TutorialTitle == null ? (rt7.TutorialTitle == null ? (rt6.TutorialTitle == null ? (rt5.TutorialTitle == null ? (rt4.TutorialTitle == null ? rt3.TutorialTitle : rt4.TutorialTitle + " > " + rt3.TutorialTitle) : rt5.TutorialTitle + " > " + rt4.TutorialTitle + " > " + rt3.TutorialTitle) : rt6.TutorialTitle + " > " + rt5.TutorialTitle + " > " + rt4.TutorialTitle + " > " + rt3.TutorialTitle) : rt7.TutorialTitle + " > " + rt6.TutorialTitle + " > " + rt5.TutorialTitle + " > " + rt4.TutorialTitle + " > " + rt3.TutorialTitle) : rt8.TutorialTitle + " > " + rt7.TutorialTitle + " > " + rt6.TutorialTitle + " > " + rt5.TutorialTitle + " > " + rt4.TutorialTitle + " > " + rt3.TutorialTitle,
                                      FAQId = content.FAQId,
                                      ProjectCode = content.ProjectCode,
                                      ProjectName = rt9.ProjectName,
                                      TypeFAQ = content.TypeFAQ,
                                      GroupFAQ = content.GroupFAQ,
                                      Question = content.Question,
                                      Answer = content.Answer,
                                      PostBy = content.PostBy,
                                      PostDate = content.PostDate,
                                      IsResponded = content.IsResponded,
                                      Response = content.Response,
                                      RespondDate = content.RespondDate,
                                  }).First();

            ChangesView detail = new ChangesView();
            detail.changes = contentChanges;


            if (contentChanges.Type == "Update")
            {
                if (contentChanges.Modul == "News")
                {
                    var publishedContent = (from news in db.T_News.Where(w => w.Id == contentChanges.NewsId)
                                            join category in db.T_MsCategory on news.CategoryCode equals category.CategoryCode
                                            select new NewsWithCategory
                                            {
                                                Id = news.Id,
                                                Judul = news.Judul,
                                                CategoryCode = news.CategoryCode,
                                                CategoryName = category.CategoryName,
                                                Caption = news.Caption,
                                                Isi = news.Isi,
                                                Image = news.Image,
                                                ImagePath = news.ImagePath,
                                                Attach = news.Attach,
                                                AttachPath = news.AttachPath,
                                                PostBy = news.PostBy,
                                                CreatedBy = news.CreatedBy,
                                                CreatedDate = news.CreatedDate,
                                                ModifyBy = news.ModifyBy,
                                                ModifyDate = news.ModifyDate
                                            }).First();

                    detail.publishedNews = publishedContent;
                }
                else if (contentChanges.Modul == "Tutorial")
                {
                    var publishedTutorial = (from tutorial in db.T_Tutorial.Where(w => w.TutorialCode == contentChanges.TutorialCode)
                                             join parent in db.T_Tutorial on tutorial.TutorialParent equals parent.TutorialCode into t
                                             from rt in t.DefaultIfEmpty()
                                             join parentLv2 in db.T_Tutorial on rt.TutorialParent equals parentLv2.TutorialCode into t2
                                             from rt2 in t2.DefaultIfEmpty()
                                             join parentLv3 in db.T_Tutorial on rt2.TutorialParent equals parentLv3.TutorialCode into t3
                                             from rt3 in t3.DefaultIfEmpty()
                                             join parentLv4 in db.T_Tutorial on rt3.TutorialParent equals parentLv4.TutorialCode into t4
                                             from rt4 in t4.DefaultIfEmpty()
                                             join parentLv5 in db.T_Tutorial on rt4.TutorialParent equals parentLv5.TutorialCode into t5
                                             from rt5 in t5.DefaultIfEmpty()
                                             join parentLv6 in db.T_Tutorial on rt5.TutorialParent equals parentLv6.TutorialCode into t6
                                             from rt6 in t6.DefaultIfEmpty()
                                             select new TutorialWithParent
                                             {
                                                 Id = tutorial.Id.Value,
                                                 TutorialCode = tutorial.TutorialCode,
                                                 TutorialTitle = tutorial.TutorialTitle,
                                                 TutorialDescription = tutorial.TutorialDescription,
                                                 VideoName = tutorial.VideoName,
                                                 VideoPath = tutorial.VideoPath,
                                                 TutorialParent = tutorial.TutorialParent,
                                                 TutorialParentTitle = rt6.TutorialTitle == null ? (rt5.TutorialTitle == null ? (rt4.TutorialTitle == null ? (rt3.TutorialTitle == null ? (rt2.TutorialTitle == null ? rt.TutorialTitle : rt2.TutorialTitle + " > " + rt.TutorialTitle) : rt3.TutorialTitle + " > " + rt2.TutorialTitle + " > " + rt.TutorialTitle) : rt4.TutorialTitle + " > " + rt3.TutorialTitle + " > " + rt2.TutorialTitle + " > " + rt.TutorialTitle) : rt5.TutorialTitle + " > " + rt4.TutorialTitle + " > " + rt3.TutorialTitle + " > " + rt2.TutorialTitle + " > " + rt.TutorialTitle) : rt6.TutorialTitle + " > " + rt5.TutorialTitle + " > " + rt4.TutorialTitle + " > " + rt3.TutorialTitle + " > " + rt2.TutorialTitle + " > " + rt.TutorialTitle,
                                                 CreatedBy = tutorial.CreatedBy,
                                                 CreatedDate = tutorial.CreatedDate,
                                                 ModifyBy = tutorial.ModifyBy,
                                                 ModifyDate = tutorial.ModifyDate
                                             }).First();

                    detail.publishedTutorial = publishedTutorial;
                }
                else if (contentChanges.Modul == "FAQ")
                {
                    var publisedFAQ = (from faq in db.T_FAQ.Where(w => w.Id == contentChanges.FAQId)
                                       join project in db.T_MsProject on faq.ProjectCode equals project.ProjectCode
                                       select new FAQProjectContent
                                       {
                                           Id = faq.Id,
                                           ProjectCode = faq.ProjectCode,
                                           ProjectName = project.ProjectName,
                                           GroupFAQ = faq.GroupFAQ,
                                           Question = faq.Q,
                                           Answer = faq.A
                                       }).First();

                    detail.publishedFAQ = publisedFAQ;
                }
            }

            return View(detail);
        }

        public ActionResult PendingView(int ChangesId)
        {
            var contentChanges = (from content in db.T_ContentChanges

                                  // Get News Category From Master Category
                                  join category in db.T_MsCategory on content.CategoryCode equals category.CategoryCode into t
                                  from rt in t.DefaultIfEmpty()

                                  // Get File Group From Master File Group
                                  join filegroup in db.T_MsFileGroup on content.FileGroup equals filegroup.FileGroup into t2
                                  from rt2 in t2.DefaultIfEmpty()

                                  // Get Tutorial Parent From T_Tutorial (Self Join - 6 Level Parent)
                                  join parent in db.T_Tutorial on content.TutorialParent equals parent.TutorialCode into t3
                                  from rt3 in t3.DefaultIfEmpty()
                                  join parentLv2 in db.T_Tutorial on rt3.TutorialParent equals parentLv2.TutorialCode into t4
                                  from rt4 in t4.DefaultIfEmpty()
                                  join parentLv3 in db.T_Tutorial on rt4.TutorialParent equals parentLv3.TutorialCode into t5
                                  from rt5 in t5.DefaultIfEmpty()
                                  join parentLv4 in db.T_Tutorial on rt5.TutorialParent equals parentLv4.TutorialCode into t6
                                  from rt6 in t6.DefaultIfEmpty()
                                  join parentLv5 in db.T_Tutorial on rt6.TutorialParent equals parentLv5.TutorialCode into t7
                                  from rt7 in t7.DefaultIfEmpty()
                                  join parentLv6 in db.T_Tutorial on rt7.TutorialParent equals parentLv6.TutorialCode into t8
                                  from rt8 in t8.DefaultIfEmpty()

                                  // Get Project Name From Master Project
                                  join project in db.T_MsProject on content.ProjectCode equals project.ProjectCode into t9
                                  from rt9 in t9.DefaultIfEmpty()

                                  select new ContentChanges
                                  {
                                      Id = content.Id,
                                      Modul = content.Modul,
                                      Type = content.Type,
                                      NewsId = content.NewsId,
                                      Judul = content.Judul,
                                      Caption = content.Caption,
                                      Isi = content.Isi,
                                      CategoryCode = content.CategoryCode,
                                      CategoryName = rt.CategoryName,
                                      Image = content.Image,
                                      ImagePath = content.ImagePath,
                                      Attach = content.Attach,
                                      AttachPath = content.AttachPath,
                                      FileId = content.FileId,
                                      FileName = content.FileName,
                                      IdFileGroup = content.FileGroup,
                                      FileGroup = rt2.Description,
                                      Description = content.Description,
                                      LocationFile = content.LocationFile,
                                      TutorialCode = content.TutorialCode,
                                      TutorialTitle = content.TutorialTitle,
                                      TutorialDescription = content.TutorialDescription,
                                      VideoName = content.VideoName,
                                      VideoPath = content.VideoPath,
                                      TutorialParent = content.TutorialParent,
                                      TutorialParentTitle = rt8.TutorialTitle == null ? (rt7.TutorialTitle == null ? (rt6.TutorialTitle == null ? (rt5.TutorialTitle == null ? (rt4.TutorialTitle == null ? rt3.TutorialTitle : rt4.TutorialTitle + " > " + rt3.TutorialTitle) : rt5.TutorialTitle + " > " + rt4.TutorialTitle + " > " + rt3.TutorialTitle) : rt6.TutorialTitle + " > " + rt5.TutorialTitle + " > " + rt4.TutorialTitle + " > " + rt3.TutorialTitle) : rt7.TutorialTitle + " > " + rt6.TutorialTitle + " > " + rt5.TutorialTitle + " > " + rt4.TutorialTitle + " > " + rt3.TutorialTitle) : rt8.TutorialTitle + " > " + rt7.TutorialTitle + " > " + rt6.TutorialTitle + " > " + rt5.TutorialTitle + " > " + rt4.TutorialTitle + " > " + rt3.TutorialTitle,
                                      FAQId = content.FAQId,
                                      ProjectCode = content.ProjectCode,
                                      ProjectName = rt9.ProjectName,
                                      TypeFAQ = content.TypeFAQ,
                                      GroupFAQ = content.GroupFAQ,
                                      Question = content.Question,
                                      Answer = content.Answer,
                                      PostBy = content.PostBy,
                                      PostDate = content.PostDate,
                                      IsResponded = content.IsResponded,
                                      Response = content.Response,
                                      RespondDate = content.RespondDate,
                                  }).Where(w => w.Id == ChangesId).First();

            ChangesView detail = new ChangesView();
            detail.changes = contentChanges;


            if (contentChanges.Type == "Update")
            {
                if (contentChanges.Modul == "News")
                {
                    var publishedContent = (from news in db.T_News
                                            join category in db.T_MsCategory on news.CategoryCode equals category.CategoryCode
                                            select new NewsWithCategory
                                            {
                                                Id = news.Id,
                                                Judul = news.Judul,
                                                CategoryCode = news.CategoryCode,
                                                CategoryName = category.CategoryName,
                                                Caption = news.Caption,
                                                Isi = news.Isi,
                                                Image = news.Image,
                                                ImagePath = news.ImagePath,
                                                Attach = news.Attach,
                                                AttachPath = news.AttachPath,
                                                PostBy = news.PostBy,
                                                CreatedBy = news.CreatedBy,
                                                CreatedDate = news.CreatedDate,
                                                ModifyBy = news.ModifyBy,
                                                ModifyDate = news.ModifyDate
                                            }).Where(w => w.Id == contentChanges.NewsId).First();

                    detail.publishedNews = publishedContent;
                }
                else if (contentChanges.Modul == "Tutorial")
                {
                    var publishedTutorial = (from tutorial in db.T_Tutorial
                                             join parent in db.T_Tutorial on tutorial.TutorialParent equals parent.TutorialCode into t
                                             from rt in t.DefaultIfEmpty()
                                             join parentLv2 in db.T_Tutorial on rt.TutorialParent equals parentLv2.TutorialCode into t2
                                             from rt2 in t2.DefaultIfEmpty()
                                             join parentLv3 in db.T_Tutorial on rt2.TutorialParent equals parentLv3.TutorialCode into t3
                                             from rt3 in t3.DefaultIfEmpty()
                                             join parentLv4 in db.T_Tutorial on rt3.TutorialParent equals parentLv4.TutorialCode into t4
                                             from rt4 in t4.DefaultIfEmpty()
                                             join parentLv5 in db.T_Tutorial on rt4.TutorialParent equals parentLv5.TutorialCode into t5
                                             from rt5 in t5.DefaultIfEmpty()
                                             join parentLv6 in db.T_Tutorial on rt5.TutorialParent equals parentLv6.TutorialCode into t6
                                             from rt6 in t6.DefaultIfEmpty()
                                             select new TutorialWithParent
                                             {
                                                 Id = tutorial.Id.Value,
                                                 TutorialCode = tutorial.TutorialCode,
                                                 TutorialTitle = tutorial.TutorialTitle,
                                                 TutorialDescription = tutorial.TutorialDescription,
                                                 VideoName = tutorial.VideoName,
                                                 VideoPath = tutorial.VideoPath,
                                                 TutorialParent = tutorial.TutorialParent,
                                                 TutorialParentTitle = rt6.TutorialTitle == null ? (rt5.TutorialTitle == null ? (rt4.TutorialTitle == null ? (rt3.TutorialTitle == null ? (rt2.TutorialTitle == null ? rt.TutorialTitle : rt2.TutorialTitle + " > " + rt.TutorialTitle) : rt3.TutorialTitle + " > " + rt2.TutorialTitle + " > " + rt.TutorialTitle) : rt4.TutorialTitle + " > " + rt3.TutorialTitle + " > " + rt2.TutorialTitle + " > " + rt.TutorialTitle) : rt5.TutorialTitle + " > " + rt4.TutorialTitle + " > " + rt3.TutorialTitle + " > " + rt2.TutorialTitle + " > " + rt.TutorialTitle) : rt6.TutorialTitle + " > " + rt5.TutorialTitle + " > " + rt4.TutorialTitle + " > " + rt3.TutorialTitle + " > " + rt2.TutorialTitle + " > " + rt.TutorialTitle,
                                                 CreatedBy = tutorial.CreatedBy,
                                                 CreatedDate = tutorial.CreatedDate,
                                                 ModifyBy = tutorial.ModifyBy,
                                                 ModifyDate = tutorial.ModifyDate
                                             }).Where(w => w.TutorialCode == contentChanges.TutorialCode).First();

                    detail.publishedTutorial = publishedTutorial;
                }
                else if (contentChanges.Modul == "FAQ")
                {
                    var publisedFAQ = (from faq in db.T_FAQ.Where(w => w.Id == contentChanges.FAQId)
                                       join project in db.T_MsProject on faq.ProjectCode equals project.ProjectCode
                                       select new FAQProjectContent
                                       {
                                           Id = faq.Id,
                                           ProjectCode = faq.ProjectCode,
                                           ProjectName = project.ProjectName,
                                           GroupFAQ = faq.GroupFAQ,
                                           Question = faq.Q,
                                           Answer = faq.A
                                       }).First();

                    detail.publishedFAQ = publisedFAQ;
                }
            }

            return View(detail);
        }

        public ActionResult Approve(int ChangesId)
        {
            TempData["Message"] = "Changes Approved";

            var contentChanges = db.T_ContentChanges.Where(w => w.Id == ChangesId).First();

            if (contentChanges.Modul == "News")
            {
                if (contentChanges.Type == "Update")
                {
                    UpdateNews(contentChanges);
                }
                else if (contentChanges.Type == "Add")
                {
                    AddNews(contentChanges);
                }
                else if (contentChanges.Type == "Delete")
                {
                    DeleteNews(contentChanges);
                }
            }
            else if (contentChanges.Modul == "Document")
            {
                if (contentChanges.Type == "Add")
                {
                    AddDocument(contentChanges);
                }
                else if (contentChanges.Type == "Delete")
                {
                    DeleteDocument(contentChanges);
                }
            }
            else if (contentChanges.Modul == "Tutorial")
            {
                if (contentChanges.Type == "Update")
                {
                    UpdateTutorial(contentChanges);
                }
                else if (contentChanges.Type == "Add")
                {
                    AddTutorial(contentChanges);
                }
                else if (contentChanges.Type == "Delete")
                {
                    DeleteTutorial(contentChanges);
                }
            }
            else if (contentChanges.Modul == "FAQ")
            {
                if (contentChanges.Type == "Update")
                {
                    UpdateFAQ(contentChanges);
                }
                else if (contentChanges.Type == "Add")
                {
                    AddFAQ(contentChanges);
                }
                else if (contentChanges.Type == "Delete")
                {
                    DeleteFAQ(contentChanges);
                }
            }

            contentChanges.IsResponded = true;
            contentChanges.Response = "A";
            contentChanges.RespondDate = DateTime.Now;
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Reject(int ChangesId)
        {
            TempData["Message"] = "Changes Rejected";

            var contentChanges = db.T_ContentChanges.Where(w => w.Id == ChangesId).First();
            contentChanges.IsResponded = true;
            contentChanges.Response = "R";
            contentChanges.RespondDate = DateTime.Now;

            if (contentChanges.Modul == "Tutorial")
            {
                if (contentChanges.Type == "Add")
                {
                    System.IO.File.Delete(WebConfigurationManager.AppSettings["VideoPathFolder"] + contentChanges.VideoPath.Substring(18, 16) + contentChanges.VideoName);
                    Directory.Delete(WebConfigurationManager.AppSettings["VideoPathFolder"] + contentChanges.VideoPath.Substring(18, 15));
                }
                else if (contentChanges.Type == "Update")
                {
                    var dbTutorial = db.T_Tutorial.Where(w => w.TutorialCode == contentChanges.TutorialCode).First();

                    if (dbTutorial.VideoPath != contentChanges.VideoPath)
                    {
                        System.IO.File.Delete(WebConfigurationManager.AppSettings["VideoPathFolder"] + contentChanges.VideoPath.Substring(18, 16) + contentChanges.VideoName);
                        Directory.Delete(WebConfigurationManager.AppSettings["VideoPathFolder"] + contentChanges.VideoPath.Substring(18, 15));
                    }
                }
            }
            else if (contentChanges.Modul == "News")
            {
                if (contentChanges.Type == "Add")
                {
                    System.IO.File.Delete(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + contentChanges.ImagePath.Substring(14, 16) + contentChanges.Image);
                    Directory.Delete(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + contentChanges.ImagePath.Substring(14, 15));

                    if (contentChanges.AttachPath != null)
                    {
                        System.IO.File.Delete(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + contentChanges.AttachPath.Substring(14, 16) + contentChanges.Attach);
                        Directory.Delete(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + contentChanges.AttachPath.Substring(14, 15));
                    }
                }
                else if (contentChanges.Type == "Update")
                {
                    var dbNews = db.T_News.Where(w => w.Id == contentChanges.NewsId).First();

                    if (dbNews.ImagePath != contentChanges.ImagePath)
                    {
                        System.IO.File.Delete(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + contentChanges.ImagePath.Substring(14, 16) + contentChanges.Image);
                        Directory.Delete(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + contentChanges.ImagePath.Substring(14, 15));
                    }

                    if (contentChanges.AttachPath != null)
                    {
                        if (dbNews.AttachPath != contentChanges.AttachPath)
                        {
                            System.IO.File.Delete(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + contentChanges.AttachPath.Substring(14, 16) + contentChanges.Attach);
                            Directory.Delete(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + contentChanges.AttachPath.Substring(14, 15));
                        }
                    }
                }
            }
            else if (contentChanges.Modul == "Document")
            {
                if (contentChanges.Type == "Add")
                {
                    System.IO.File.Delete(WebConfigurationManager.AppSettings["DocumentPathFolder"] + contentChanges.LocationFile.Substring(15, 16) + contentChanges.FileName);
                    Directory.Delete(WebConfigurationManager.AppSettings["DocumentPathFolder"] + contentChanges.LocationFile.Substring(15, 15));
                }
            }

            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Cancel(int ChangesId)
        {
            TempData["Message"] = "Changes Canceled";

            var contentChanges = db.T_ContentChanges.Where(w => w.Id == ChangesId).First();
            contentChanges.IsResponded = true;
            contentChanges.Response = "C";
            contentChanges.RespondDate = DateTime.Now;

            if (contentChanges.Modul == "Tutorial")
            {
                if (contentChanges.Type == "Add")
                {
                    System.IO.File.Delete(WebConfigurationManager.AppSettings["VideoPathFolder"] + contentChanges.VideoPath.Substring(18, 16) + contentChanges.VideoName);
                    Directory.Delete(WebConfigurationManager.AppSettings["VideoPathFolder"] + contentChanges.VideoPath.Substring(18, 15));
                }
                else if (contentChanges.Type == "Update")
                {
                    var dbTutorial = db.T_Tutorial.Where(w => w.TutorialCode == contentChanges.TutorialCode).First();

                    if (dbTutorial.VideoPath != contentChanges.VideoPath)
                    {
                        System.IO.File.Delete(WebConfigurationManager.AppSettings["VideoPathFolder"] + contentChanges.VideoPath.Substring(18, 16) + contentChanges.VideoName);
                        Directory.Delete(WebConfigurationManager.AppSettings["VideoPathFolder"] + contentChanges.VideoPath.Substring(18, 15));
                    }
                }
            }
            else if (contentChanges.Modul == "News")
            {
                if (contentChanges.Type == "Add")
                {
                    System.IO.File.Delete(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + contentChanges.ImagePath.Substring(14, 16) + contentChanges.Image);
                    Directory.Delete(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + contentChanges.ImagePath.Substring(14, 15));

                    if (contentChanges.AttachPath != null)
                    {
                        System.IO.File.Delete(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + contentChanges.AttachPath.Substring(14, 16) + contentChanges.Attach);
                        Directory.Delete(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + contentChanges.AttachPath.Substring(14, 15));
                    }
                }
                else if (contentChanges.Type == "Update")
                {
                    var dbNews = db.T_News.Where(w => w.Id == contentChanges.NewsId).First();

                    if (dbNews.ImagePath != contentChanges.ImagePath)
                    {
                        System.IO.File.Delete(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + contentChanges.ImagePath.Substring(14, 16) + contentChanges.Image);
                        Directory.Delete(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + contentChanges.ImagePath.Substring(14, 15));
                    }

                    if (contentChanges.AttachPath != null)
                    {
                        if (dbNews.AttachPath != contentChanges.AttachPath)
                        {
                            System.IO.File.Delete(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + contentChanges.AttachPath.Substring(14, 16) + contentChanges.Attach);
                            Directory.Delete(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + contentChanges.AttachPath.Substring(14, 15));
                        }
                    }
                }
            }
            else if (contentChanges.Modul == "Document")
            {
                if (contentChanges.Type == "Add")
                {
                    System.IO.File.Delete(WebConfigurationManager.AppSettings["DocumentPathFolder"] + contentChanges.LocationFile.Substring(15, 16) + contentChanges.FileName);
                    Directory.Delete(WebConfigurationManager.AppSettings["DocumentPathFolder"] + contentChanges.LocationFile.Substring(15, 15));
                }
            }

            db.SaveChanges();

            return RedirectToAction("Pending");
        }

        public void UpdateNews(T_ContentChanges changes)
        {
            var content = db.T_News.Where(w => w.Id == changes.NewsId).First();

            content.Judul = changes.Judul;
            content.Caption = changes.Caption;
            content.Isi = changes.Isi;
            content.CategoryCode = changes.CategoryCode.Value;

            if (content.ImagePath != changes.ImagePath)
            {
                System.IO.File.Delete(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + content.ImagePath.Substring(14, 16) + content.Image);
                Directory.Delete(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + content.ImagePath.Substring(14, 15));

                content.Image = changes.Image;
                content.ImagePath = changes.ImagePath;
            }

            if (content.AttachPath != changes.AttachPath)
            {
                if (content.AttachPath != null)
                {
                    System.IO.File.Delete(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + content.AttachPath.Substring(25, 16) + content.Attach);
                    Directory.Delete(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + content.AttachPath.Substring(25, 15));
                }

                content.Attach = changes.Attach;
                content.AttachPath = changes.AttachPath;
            }

            content.ModifyBy = changes.PostBy;
            content.ModifyDate = changes.PostDate;
            db.SaveChanges();
        }

        public void AddNews(T_ContentChanges changes)
        {
            T_News addNews = new T_News();
            addNews.Judul = changes.Judul;
            addNews.Caption = changes.Caption;
            addNews.CategoryCode = changes.CategoryCode.Value;
            addNews.Isi = changes.Isi;
            addNews.Image = changes.Image;
            addNews.ImagePath = changes.ImagePath;

            if (changes.Attach != null)
            {
                addNews.Attach = changes.Attach;
                addNews.AttachPath = changes.AttachPath;
            }

            addNews.CreatedBy = changes.PostBy;
            addNews.CreatedDate = changes.PostDate;
            addNews.PostBy = changes.PostBy;
            db.T_News.Add(addNews);
            db.SaveChanges();
        }

        public void DeleteNews(T_ContentChanges changes)
        {
            System.IO.File.Delete(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + changes.ImagePath.Substring(14, 16) + changes.Image);
            Directory.Delete(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + changes.ImagePath.Substring(14, 15));

            if (changes.AttachPath != null)
            {
                System.IO.File.Delete(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + changes.AttachPath.Substring(25, 16) + changes.Attach);
                Directory.Delete(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + changes.AttachPath.Substring(25, 15));
            }

            var news = db.T_News.Where(a => a.Id == changes.NewsId).First();
            db.T_News.Remove(news);
            db.SaveChanges();
        }

        public void AddDocument(T_ContentChanges changes)
        {
            T_Files addDocument = new T_Files();
            addDocument.NameFile = changes.FileName;
            addDocument.FileGroup = changes.FileGroup.Value;
            addDocument.Description = changes.Description;
            addDocument.LocationFile = changes.LocationFile;
            addDocument.CreatedBy = changes.PostBy;
            addDocument.CreatedDate = changes.PostDate;
            db.T_Files.Add(addDocument);
            db.SaveChanges();
        }

        public void DeleteDocument(T_ContentChanges changes)
        {
            System.IO.File.Delete(WebConfigurationManager.AppSettings["DocumentPathFolder"] + changes.LocationFile.Substring(15, 16) + changes.FileName);
            Directory.Delete(WebConfigurationManager.AppSettings["DocumentPathFolder"] + changes.LocationFile.Substring(15, 15));

            var doc = db.T_Files.Where(a => a.ID == changes.FileId).First();
            db.T_Files.Remove(doc);
            db.SaveChanges();
        }

        public void UpdateTutorial(T_ContentChanges changes)
        {
            var content = db.T_Tutorial.Where(w => w.TutorialCode == changes.TutorialCode).First();

            string lastTutorialCode = (from a in db.T_Tutorial.Where(w => w.TutorialParent == changes.TutorialParent).DefaultIfEmpty()
                                       select new
                                       {
                                           value = a != null ? a.TutorialCode : "00"
                                       }).OrderByDescending(o => o.value).Select(s => s.value).FirstOrDefault();
            int addCode = int.Parse(lastTutorialCode.Substring(lastTutorialCode.LastIndexOf(".") + 1, 2)) + 1;
            string TutorialCode = changes.TutorialParent + "." + ("0" + addCode.ToString()).Substring(0, 2);

            T_Tutorial addTutorial = new T_Tutorial();
            addTutorial.TutorialCode = TutorialCode;
            addTutorial.TutorialTitle = changes.TutorialTitle;
            addTutorial.TutorialDescription = changes.TutorialDescription;
            addTutorial.TutorialParent = changes.TutorialParent;
            addTutorial.VideoName = changes.VideoName;
            addTutorial.VideoPath = changes.VideoPath;
            addTutorial.CreatedBy = content.CreatedBy;
            addTutorial.CreatedDate = content.CreatedDate;
            addTutorial.ModifyBy = changes.PostBy;
            addTutorial.ModifyDate = changes.PostDate;

            if (content.VideoPath != changes.VideoPath)
            {
                System.IO.File.Delete(WebConfigurationManager.AppSettings["VideoPathFolder"] + content.VideoPath.Substring(18, 16) + content.VideoName);
                Directory.Delete(WebConfigurationManager.AppSettings["VideoPathFolder"] + content.VideoPath.Substring(18, 15));
            }

            db.T_Tutorial.Add(addTutorial);
            db.T_Tutorial.Remove(content);

            db.SaveChanges();
        }

        public void AddTutorial(T_ContentChanges changes)
        {
            string lastTutorialCode = (from a in db.T_Tutorial.Where(w => w.TutorialParent == changes.TutorialParent).DefaultIfEmpty()
                                       select new
                                       {
                                           value = a != null ? a.TutorialCode : "00"
                                       }).OrderByDescending(o => o.value).Select(s => s.value).FirstOrDefault();
            int addCode = int.Parse(lastTutorialCode.Substring(lastTutorialCode.LastIndexOf(".") + 1, 2)) + 1;
            string TutorialCode = changes.TutorialParent + "." + ("0" + addCode.ToString()).Substring(0, 2);

            T_Tutorial addTutorial = new T_Tutorial();
            addTutorial.TutorialCode = TutorialCode;
            addTutorial.TutorialTitle = changes.TutorialTitle;
            addTutorial.TutorialDescription = changes.TutorialDescription;
            addTutorial.TutorialParent = changes.TutorialParent;
            addTutorial.VideoName = changes.VideoName;
            addTutorial.VideoPath = changes.VideoPath;
            addTutorial.CreatedBy = changes.PostBy;
            addTutorial.CreatedDate = changes.PostDate;
            db.T_Tutorial.Add(addTutorial);
            db.SaveChanges();
        }

        public void DeleteTutorial(T_ContentChanges changes)
        {
            System.IO.File.Delete(WebConfigurationManager.AppSettings["VideoPathFolder"] + changes.VideoPath.Substring(18, 16) + changes.VideoName);
            Directory.Delete(WebConfigurationManager.AppSettings["VideoPathFolder"] + changes.VideoPath.Substring(18, 15));

            var tutorial = db.T_Tutorial.Where(a => a.TutorialCode == changes.TutorialCode).First();
            db.T_Tutorial.Remove(tutorial);
            db.SaveChanges();
        }

        public void UpdateFAQ(T_ContentChanges changes)
        {
            T_FAQ dbFAQ = db.T_FAQ.Where(w => w.Id == changes.FAQId).First();

            dbFAQ.ProjectCode = changes.ProjectCode;
            dbFAQ.GroupFAQ = changes.GroupFAQ;
            dbFAQ.Q = changes.Question;
            dbFAQ.A = changes.Answer;
            dbFAQ.ModifyBy = changes.PostBy;
            dbFAQ.ModifyDate = changes.PostDate;

            db.SaveChanges();
        }

        public void AddFAQ(T_ContentChanges changes)
        {
            T_FAQ newFAQ = new T_FAQ();

            newFAQ.ProjectCode = changes.ProjectCode;
            newFAQ.GroupFAQ = changes.GroupFAQ;
            newFAQ.Q = changes.Question;
            newFAQ.A = changes.Answer;
            newFAQ.CreatedBy = changes.PostBy;
            newFAQ.CreatedDate = changes.PostDate;

            db.T_FAQ.Add(newFAQ);
            db.SaveChanges();
        }

        public void DeleteFAQ(T_ContentChanges changes)
        {
            var dbFAQ = db.T_FAQ.Where(w => w.Id == changes.FAQId).First();
            db.T_FAQ.Remove(dbFAQ);
            db.SaveChanges();
        }
    }
}
