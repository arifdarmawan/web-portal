﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPortal.Model;
using WebPortal.Models;
using PagedList;
using WebPortal.GeneralClass;
using System.Text.RegularExpressions;

namespace WebPortal.Controllers.DataSetting
{
    //[NoDirectAccess]
    [SessionTimeout]
    public class UserController : Controller
    {
        //
        // GET: /User/
        oGeneral ogeneral = new oGeneral();
        FrInternallEntities db = new FrInternallEntities();
        

        public ActionResult Index(int? Page)
        {
            int startIndex = 0;
            int loadPage = 0;

            if (Page != null)
            {
                loadPage = Page.HasValue ? Page.Value - 1 : 0;
                startIndex = 20 * loadPage;
            }

            List<UserWithLevel> listUser = (from user in db.T_MsUser.OrderBy(o => o.UserName).Skip(startIndex).Take(20)
                                           join userLevel in db.T_MsUserGroup on user.UserLevel equals userLevel.UserLevel
                                           select new UserWithLevel
                                           {
                                               UserId = user.UserId,
                                               UserName = user.UserName,
                                               Email = user.email,
                                               UserLevel = user.UserLevel,
                                               Description = userLevel.Description
                                           }).ToList();

            int totalUser = db.T_MsUser.Count();
            int totalPages = (totalUser / 20) + (totalUser % 20 > 0 ? 1 : 0);

            PagingAndGroup mod = new PagingAndGroup();
            mod.listUser = listUser;
            mod.currentPage = loadPage + 1;
            mod.totalItem = totalUser;
            mod.totalPages = totalPages;

            mod = ogeneral.getPaging(mod);

            return View(mod);
        }

        [HttpGet]
        public ActionResult AddUser()
        {
            List<DropdownModel> UserGroupList = (from usergroup in db.T_MsUserGroup
                                                select new DropdownModel
                                                {
                                                    intValue = usergroup.UserLevel,
                                                    Text = usergroup.Description
                                                }).OrderBy(o => o.intValue).ToList();
            ViewBag.UserGroupList = new SelectList(UserGroupList, "intValue", "Text");

            List<DropdownModel> RegionList = new List<DropdownModel>();
            RegionList.Add(new DropdownModel { stringValue = "Jakarta", Text = "Jakarta" });
            RegionList.Add(new DropdownModel { stringValue = "Pekanbaru", Text = "Pekanbaru" });
            RegionList.Add(new DropdownModel { stringValue = "Balikpapan", Text = "Balikpapan" });
            RegionList.Add(new DropdownModel { stringValue = "Pontianak", Text = "Pontianak" });
            RegionList.Add(new DropdownModel { stringValue = "-", Text = "-" });
            ViewBag.RegionList = new SelectList(RegionList, "stringValue", "Text");

            return View();

        }

        [HttpPost]
        public ActionResult AddUser(T_MsUser user)
        {
            bool isTemporary = false;

            if (user.Region == "")
                ModelState.AddModelError("Region", "Please Choose Region"); 

            if (user.Jabatan == "")
                ModelState.AddModelError("Jabatan", "Please Fill User Position");

            if (user.Divisi == "")
                ModelState.AddModelError("Dision", "Please Fill User Division");

            if (user.email == "")
                ModelState.AddModelError("email", "Please Fill User Email");
            //else
            //{
            //    Match match = Regex.Match(user.email, @"^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$");

            //    if (!match.Success)
            //        ModelState.AddModelError("email", "Invalid Email Format");
            //}

            if (user.StartAccess != null || user.EndAccess != null)
            {
                if (user.StartAccess == null)
                {
                    ModelState.AddModelError("StartAccess", "Please Choose Start Date"); 
                }
                else if (user.EndAccess == null)
                {
                    ModelState.AddModelError("EndAccess", "Please Choose End Date"); 
                }
                else
                {
                    isTemporary = true;
                }
            }

            if (ModelState.IsValid)
            {
                T_MsUser newUser = new T_MsUser();

                newUser.UserId = user.UserId;
                newUser.UserName = user.UserName;
                newUser.Password = ogeneral.Encrypt(ogeneral.randomString(8));
                newUser.Region = user.Region;
                newUser.email = user.email;
                newUser.Jabatan = user.Jabatan;
                newUser.Divisi = user.Divisi;
                newUser.UserLevel = user.UserLevel;
                newUser.CreatedBy = Session["LogedUserID"].ToString();
                newUser.CreatedDate = DateTime.Now;
                newUser.Image = "/Design/assets/People/man.jpg";
                newUser.IsTemporary = isTemporary;

                if (isTemporary)
                {
                    newUser.StartAccess = user.StartAccess;
                    newUser.EndAccess = user.EndAccess;
                }

                db.T_MsUser.Add(newUser);
                db.SaveChanges();

                TempData["Message"] = "User Added";
                return RedirectToAction("Index");
            }

            List<DropdownModel> UserGroupList = (from usergroup in db.T_MsUserGroup
                                                 select new DropdownModel
                                                 {
                                                     intValue = usergroup.UserLevel,
                                                     Text = usergroup.Description
                                                 }).OrderBy(o => o.intValue).ToList();
            ViewBag.UserGroupList = new SelectList(UserGroupList, "intValue", "Text");

            List<DropdownModel> RegionList = new List<DropdownModel>();
            RegionList.Add(new DropdownModel { stringValue = "Jakarta", Text = "Jakarta" });
            RegionList.Add(new DropdownModel { stringValue = "Pekanbaru", Text = "Pekanbaru" });
            RegionList.Add(new DropdownModel { stringValue = "Balikpapan", Text = "Balikpapan" });
            RegionList.Add(new DropdownModel { stringValue = "Pontianak", Text = "Pontianak" });
            RegionList.Add(new DropdownModel { stringValue = "-", Text = "-" });
            ViewBag.RegionList = new SelectList(RegionList, "stringValue", "Text");

            return View(user);
        }

        [HttpGet]
        public ActionResult EditUser(string userId)
        {
            T_MsUser user = db.T_MsUser.Where(w => w.UserId == userId).FirstOrDefault();

            List<DropdownModel> UserGroupList = (from usergroup in db.T_MsUserGroup
                                                 select new DropdownModel
                                                 {
                                                     intValue = usergroup.UserLevel,
                                                     Text = usergroup.Description
                                                 }).OrderBy(o => o.intValue).ToList();
            ViewBag.UserGroupList = new SelectList(UserGroupList, "intValue", "Text");

            List<DropdownModel> RegionList = new List<DropdownModel>();
            RegionList.Add(new DropdownModel { stringValue = "Jakarta", Text = "Jakarta" });
            RegionList.Add(new DropdownModel { stringValue = "Pekanbaru", Text = "Pekanbaru" });
            RegionList.Add(new DropdownModel { stringValue = "Balikpapan", Text = "Balikpapan" });
            RegionList.Add(new DropdownModel { stringValue = "Pontianak", Text = "Pontianak" });
            RegionList.Add(new DropdownModel { stringValue = "-", Text = "-" });
            ViewBag.RegionList = new SelectList(RegionList, "stringValue", "Text");

            return View(user);
        }

        [HttpPost]
        public ActionResult EditUser(T_MsUser user)
        {
            bool isTemporary = false;

            if (user.Region == "")
                ModelState.AddModelError("Region", "Please Choose Region");

            if (user.Jabatan == "")
                ModelState.AddModelError("Jabatan", "Please Fill User Position");

            if (user.Divisi == "")
                ModelState.AddModelError("Dision", "Please Fill User Division");

            if (user.email == "")
                ModelState.AddModelError("email", "Please Fill User Email");

            if (user.StartAccess != null || user.EndAccess != null)
            {
                if (user.StartAccess == null)
                {
                    ModelState.AddModelError("StartAccess", "Please Choose Start Date"); 
                }
                else if (user.EndAccess == null)
                {
                    ModelState.AddModelError("EndAccess", "Please Choose End Date"); 
                }
                else
                {
                    isTemporary = true;
                }
            }

            if (ModelState.IsValid)
            {
                var dbUser = db.T_MsUser.Where(a => a.UserId.Equals(user.UserId)).FirstOrDefault();

                dbUser.UserName = user.UserName;
                dbUser.Region = user.Region;
                dbUser.email = user.email;
                dbUser.Jabatan = user.Jabatan;
                dbUser.Divisi = user.Divisi;
                dbUser.UserLevel = user.UserLevel;
                dbUser.ModifyBy = Session["LogedUserId"].ToString();
                dbUser.ModifyDate = DateTime.Now;
                dbUser.IsTemporary = isTemporary;

                if (isTemporary)
                {
                    dbUser.StartAccess = user.StartAccess;
                    dbUser.EndAccess = user.EndAccess;
                }

                db.SaveChanges();

                TempData["Message"] = "User Updated";
                return RedirectToAction("Index");
            }

            List<DropdownModel> UserGroupList = (from usergroup in db.T_MsUserGroup
                                                 select new DropdownModel
                                                 {
                                                     intValue = usergroup.UserLevel,
                                                     Text = usergroup.Description
                                                 }).OrderBy(o => o.intValue).ToList();
            ViewBag.UserGroupList = new SelectList(UserGroupList, "intValue", "Text");

            List<DropdownModel> RegionList = new List<DropdownModel>();
            RegionList.Add(new DropdownModel { stringValue = "Jakarta", Text = "Jakarta" });
            RegionList.Add(new DropdownModel { stringValue = "Pekanbaru", Text = "Pekanbaru" });
            RegionList.Add(new DropdownModel { stringValue = "Balikpapan", Text = "Balikpapan" });
            RegionList.Add(new DropdownModel { stringValue = "Pontianak", Text = "Pontianak" });
            RegionList.Add(new DropdownModel { stringValue = "-", Text = "-" });
            ViewBag.RegionList = new SelectList(RegionList, "stringValue", "Text");

            return View(user);
        }


        public ActionResult DeleteUser(string userId)
        {
            TempData["Message"] = "User Deleted";

            var user = db.T_MsUser.Where(a => a.UserId.Equals(userId)).FirstOrDefault();

            db.T_MsUser.Remove(user);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

    }
}
