﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPortal.GeneralClass;
using WebPortal.Model;
using WebPortal.Models;

namespace WebPortal.Controllers.DataSetting
{
    [NoDirectAccess]
    [SessionTimeout]
    public class ProjectController : Controller
    {
        //
        // GET: /Project/
        oGeneral ogeneral = new oGeneral();
        FrInternallEntities db = new FrInternallEntities();

        public ActionResult Index(int? Page)
        {
            int startIndex = 0;
            int loadPage = 0;

            if (Page != null)
            {
                loadPage = Page.HasValue ? Page.Value - 1 : 0;
                startIndex = 20 * loadPage;
            }

            List<T_MsProject> listProject = db.T_MsProject.OrderBy(o => o.ProjectName).Skip(startIndex).Take(20).ToList();

            int totalProject = db.T_MsProject.Count();
            int totalPages = (totalProject / 20) + (totalProject % 20 > 0 ? 1 : 0);

            PagingAndGroup mod = new PagingAndGroup();
            mod.listProject = listProject;
            mod.currentPage = loadPage + 1;
            mod.totalItem = totalProject;
            mod.totalPages = totalPages;

            mod = ogeneral.getPaging(mod);

            return View(mod);
        }

        [HttpGet]
        public ActionResult AddProject()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddProject(T_MsProject project)
        {
            if (ModelState.IsValid)
            {
                T_MsProject newProject = new T_MsProject();
                newProject.ProjectCode = project.ProjectCode;
                newProject.ProjectName = project.ProjectName;
                newProject.CreatedBy = Session["LogedUserId"].ToString();
                newProject.CreatedDate = DateTime.Now;

                db.T_MsProject.Add(newProject);
                db.SaveChanges();

                TempData["Message"] = "Project Added";
                return RedirectToAction("Index");
            }

            return View(project);
        }

        [HttpGet]
        public ActionResult EditProject(string projectCode)
        {
            T_MsProject project = db.T_MsProject.Where(w => w.ProjectCode == projectCode).First();

            return View(project);
        }

        [HttpPost]
        public ActionResult EditProject(T_MsProject project)
        {
            if (ModelState.IsValid)
            {
                T_MsProject dbProject = db.T_MsProject.Where(w => w.ProjectCode == project.ProjectCode).First();
                dbProject.ProjectName = project.ProjectName;
                dbProject.ModifyBy = Session["LogedUserId"].ToString();
                dbProject.ModifyDate = DateTime.Now;

                db.SaveChanges();

                TempData["Message"] = "Project Updated";
                return RedirectToAction("Index");
            }

            return View(project);
        }

        public ActionResult DeleteProject(string projectCode)
        {
            T_MsProject dbProject = db.T_MsProject.Where(w => w.ProjectCode == projectCode).First();

            db.T_MsProject.Remove(dbProject);
            db.SaveChanges();

            TempData["Message"] = "Project Deleted";
            return RedirectToAction("Index");
        }
    }
}
