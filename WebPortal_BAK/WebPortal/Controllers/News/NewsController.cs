﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using WebPortal.GeneralClass;
using WebPortal.Model;
using WebPortal.Models;

namespace WebPortal.Controllers.News
{
    public class NewsController : Controller
    {
        //
        // GET: /News/
        oGeneral ogeneral = new oGeneral();
        FrInternallEntities db = new FrInternallEntities();

        public ActionResult Index(int? Page, int? Category)
        {
            int startIndex = 0;
            int loadPage = 0;

            if (Page != null)
            {
                loadPage = Page.HasValue ? Page.Value - 1 : 0;
                startIndex = 5 * loadPage;
            }

            if (Category != null)
            {
                Session["NewsCategory"] = Category;
            }

            int loadCategory = Session["NewsCategory"] != null ? int.Parse(Session["NewsCategory"].ToString()) : -1;

            if (loadCategory > -1)
                Category = loadCategory;
            else
                Category = null;

            List<NewsWithCategory> listNews = (from news in db.T_News.Where(ss => ss.CategoryCode == loadCategory || Category == null)
                                               join category in db.T_MsCategory on news.CategoryCode equals category.CategoryCode
                                               select new NewsWithCategory
                                               {
                                                   Id = news.Id,
                                                   Judul = news.Judul,
                                                   CategoryCode = news.CategoryCode,
                                                   CategoryName = category.CategoryName,
                                                   Caption = news.Caption,
                                                   Isi = news.Isi,
                                                   Image = news.Image,
                                                   ImagePath = news.ImagePath,
                                                   Attach = news.Attach,
                                                   AttachPath = news.AttachPath,
                                                   PostBy = news.PostBy,
                                                   CreatedBy = news.CreatedBy,
                                                   CreatedDate = news.CreatedDate,
                                                   ModifyBy = news.ModifyBy,
                                                   ModifyDate = news.ModifyDate
                                               }).OrderByDescending(o => o.ModifyDate == null ? o.CreatedDate : o.ModifyDate).ToList();

            List<T_News> listlatestNews = db.T_News.OrderByDescending(o => o.ModifyDate == null ? o.CreatedDate : o.ModifyDate).Take(3).ToList();

            List<Grouping> listCategory = (from news in db.T_News
                                           group news by news.CategoryCode into grp
                                           join category in db.T_MsCategory on grp.FirstOrDefault().CategoryCode equals category.CategoryCode
                                           select new Grouping
                                           {
                                               CategoryCode = grp.FirstOrDefault().CategoryCode,
                                               CategoryName = category.CategoryName,
                                               TotalNews = grp.Count(c => c.Id != null)
                                           }).OrderBy(o => o.CategoryName).ToList();

            int totalNews = db.T_News.Count();
            int totalPages = (listNews.Count / 5) + (listNews.Count % 5 > 0 ? 1 : 0);

            listNews = listNews.Skip(startIndex).Take(5).ToList();

            PagingAndGroup mod = new PagingAndGroup();
            mod.listNews = listNews;
            mod.listLatestNews = listlatestNews;
            mod.listCategory = listCategory;
            mod.currentPage = loadPage + 1;
            mod.totalItem = totalNews;
            mod.totalPages = totalPages;

            mod = ogeneral.getPaging(mod);

            return View(mod);
        }

        public ActionResult NewsDetail(int NewsId)
        {
            List<NewsWithCategory> listNews = (from news in db.T_News.Where(ss => ss.Id.Equals(NewsId))
                                               join category in db.T_MsCategory on news.CategoryCode equals category.CategoryCode
                                               select new NewsWithCategory
                                               {
                                                   Id = news.Id,
                                                   Judul = news.Judul,
                                                   CategoryCode = news.CategoryCode,
                                                   CategoryName = category.CategoryName,
                                                   Caption = news.Caption,
                                                   Isi = news.Isi,
                                                   Image = news.Image,
                                                   ImagePath = news.ImagePath,
                                                   Attach = news.Attach,
                                                   AttachPath = news.AttachPath,
                                                   PostBy = news.PostBy,
                                                   CreatedBy = news.CreatedBy,
                                                   CreatedDate = news.CreatedDate,
                                                   ModifyBy = news.ModifyBy,
                                                   ModifyDate = news.ModifyDate
                                               }).ToList();

            List<T_News> listlatestNews = db.T_News.OrderByDescending(o => o.ModifyDate == null ? o.CreatedDate : o.ModifyDate).Take(3).ToList();
            List<T_CommentNews> listComment = db.T_CommentNews.Where(ss => ss.IdComment.Equals(NewsId)).OrderBy(o => o.Date).ToList();

            List<Grouping> listCategory = (from news in db.T_News
                                           group news by news.CategoryCode into grp
                                           join category in db.T_MsCategory on grp.FirstOrDefault().CategoryCode equals category.CategoryCode
                                           select new Grouping
                                           {
                                               CategoryCode = grp.FirstOrDefault().CategoryCode,
                                               CategoryName = category.CategoryName,
                                               TotalNews = grp.Count(c => c.Id != null)
                                           }).OrderBy(o => o.CategoryName).ToList();

            PagingAndGroup mod = new PagingAndGroup();
            mod.listNews = listNews;
            mod.listLatestNews = listlatestNews;
            mod.listComment = listComment;
            mod.listCategory = listCategory;
            mod.totalItem = db.T_News.ToList().Count;

            return View(mod);
        }

        public ActionResult Comment(PagingAndGroup com)
        {
            TempData["Message"] = "Comment Posted";

            com.comment.UserId = Session["LogedUserID"].ToString();
            com.comment.Email = Session["LogedEmail"].ToString();
            com.comment.Date = DateTime.Now;

            db.T_CommentNews.Add(com.comment);
            db.SaveChanges();

            return RedirectToAction("NewsDetail", "News", new { NewsId = com.comment.IdComment });
        }

        [NoDirectAccess]
        [SessionTimeout]
        [HttpGet]
        public ActionResult CreateNews()
        {
            List<DropdownModel> CategoryList = (from category in db.T_MsCategory
                                                select new DropdownModel
                                                {
                                                    intValue = category.CategoryCode,
                                                    Text = category.CategoryName
                                                }).ToList();
            ViewBag.CategoryList = new SelectList(CategoryList, "intValue", "Text");

            return View();
        }

        [NoDirectAccess]
        [SessionTimeout]
        [HttpPost]
        public ActionResult CreateNews(T_News news, HttpPostedFileBase uploadFile, HttpPostedFileBase uploadImage)
        {
            if (news != null && uploadImage != null)
            {
                string Year = DateTime.Now.Year.ToString(), Month = "0" + DateTime.Now.Month.ToString(), Day = "0" + DateTime.Now.Day.ToString(), Hour = "0" + DateTime.Now.Hour.ToString(), Minute = "0" + DateTime.Now.Minute.ToString(), Second = "0" + DateTime.Now.Second.ToString();
                string Time = Year + Month.Substring(Month.Length - 2) + Day.Substring(Day.Length - 2) + "_" + Hour.Substring(Hour.Length - 2) + Minute.Substring(Minute.Length - 2) + Second.Substring(Second.Length - 2);

                Directory.CreateDirectory(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + Time);
                List<T_MsApproval> listApproval = db.T_MsApproval.ToList();

                int userLevel = int.Parse(Session["UserLevel"].ToString());

                if (listApproval.Any(a => a.UserLevel == userLevel))
                {
                    T_News addNews = new T_News();
                    addNews.Judul = news.Judul;
                    addNews.Caption = news.Caption;
                    addNews.CategoryCode = news.CategoryCode;
                    addNews.Isi = news.Isi.Replace(System.Environment.NewLine, ";");

                    var imagename = Path.GetFileName(uploadImage.FileName);
                    var imagepath = Path.Combine(Server.MapPath("~/Content/news/" + Time), imagename);
                    uploadImage.SaveAs(imagepath);

                    addNews.Image = imagename;
                    addNews.ImagePath = "/Content/news/" + Time + "/" + imagename;

                    if (uploadFile != null)
                    {
                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + Time);

                        var filename = Path.GetFileName(uploadFile.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/news/attachment/" + Time), filename);
                        uploadFile.SaveAs(filepath);

                        addNews.Attach = filename;
                        addNews.AttachPath = "/Content/news/attachment/" + Time + "/" + filename;
                    }

                    addNews.CreatedBy = Session["LogedUserID"].ToString();
                    addNews.CreatedDate = DateTime.Now;
                    addNews.PostBy = Session["LogedUserID"].ToString();
                    db.T_News.Add(addNews);
                    TempData["Message"] = "News Posted";
                }
                else
                {
                    T_ContentChanges addNews = new T_ContentChanges();
                    addNews.Modul = "News";
                    addNews.Type = "Add";
                    addNews.Judul = news.Judul;
                    addNews.Caption = news.Caption;
                    addNews.CategoryCode = news.CategoryCode;
                    addNews.Isi = news.Isi.Replace(System.Environment.NewLine, ";");

                    var imagename = Path.GetFileName(uploadImage.FileName);
                    var imagepath = Path.Combine(Server.MapPath("~/Content/news/" + Time), imagename);
                    uploadImage.SaveAs(imagepath);

                    addNews.Image = imagename;
                    addNews.ImagePath = "/Content/news/" + Time + "/" + imagename;

                    if (uploadFile != null)
                    {
                        Directory.CreateDirectory(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + Time);

                        var filename = Path.GetFileName(uploadFile.FileName);
                        var filepath = Path.Combine(Server.MapPath("~/Content/news/attachment/" + Time), filename);
                        uploadFile.SaveAs(filepath);

                        addNews.Attach = filename;
                        addNews.AttachPath = "/Content/news/attachment/" + Time + "/" + filename;
                    }

                    addNews.PostBy = Session["LogedUserID"].ToString();
                    addNews.PostDate = DateTime.Now;
                    db.T_ContentChanges.Add(addNews);
                    TempData["Message"] = "News Posted, Waiting for Approval By SuperAdmin";
                }

                db.SaveChanges();
            }

            return RedirectToAction("ListNews");
        }

        [NoDirectAccess]
        [SessionTimeout]
        public ActionResult ListNews(string stringSearch)
        {
            List<NewsWithCategory> listNews = (from news in db.T_News
                                               join category in db.T_MsCategory on news.CategoryCode equals category.CategoryCode
                                               join changes in db.T_ContentChanges.Where(w => w.NewsId != null && !w.IsResponded) on news.Id equals changes.NewsId into t
                                               from rt in t.DefaultIfEmpty()
                                               select new NewsWithCategory
                                               {
                                                   Id = news.Id,
                                                   Judul = news.Judul,
                                                   CategoryCode = news.CategoryCode,
                                                   CategoryName = category.CategoryName,
                                                   Caption = news.Caption,
                                                   Isi = news.Isi,
                                                   Image = news.Image,
                                                   ImagePath = news.ImagePath,
                                                   Attach = news.Attach,
                                                   AttachPath = news.AttachPath,
                                                   PostBy = news.PostBy,
                                                   CreatedBy = news.CreatedBy,
                                                   CreatedDate = news.CreatedDate,
                                                   ModifyBy = news.ModifyBy,
                                                   ModifyDate = news.ModifyDate,
                                                   IsChanged = rt.NewsId == null ? false : true
                                               }).Where(w => !w.IsChanged).ToList();

            if (stringSearch != null)
            {
                listNews = listNews.Where(w => w.Judul.ToLower().Contains(stringSearch.ToLower()) || w.Isi.ToLower().Contains(stringSearch.ToLower()) || w.PostBy.ToLower().Contains(stringSearch.ToLower()) || (w.ModifyDate == null ? w.CreatedDate : w.ModifyDate.GetValueOrDefault()).ToString("dd MMMM yyyy").ToLower().Contains(stringSearch.ToLower())).ToList();
            }

            SearchModel mod = new SearchModel();
            mod.listNews = listNews;
            mod.stringSearch = "";

            return View(mod);
        }

        [NoDirectAccess]
        [SessionTimeout]
        [HttpGet]
        public ActionResult EditNews(int NewsId)
        {
            T_News news = db.T_News.Where(w => w.Id == NewsId).FirstOrDefault();

            news.Isi = news.Isi.Replace(";", System.Environment.NewLine);

            List<DropdownModel> CategoryList = (from category in db.T_MsCategory
                                                select new DropdownModel
                                                {
                                                    intValue = category.CategoryCode,
                                                    Text = category.CategoryName
                                                }).ToList();
            ViewBag.CategoryList = new SelectList(CategoryList, "intValue", "Text");

            return View(news);
        }

        [NoDirectAccess]
        [SessionTimeout]
        [HttpPost]
        public ActionResult EditNews(T_News news, HttpPostedFileBase uploadFile, HttpPostedFileBase uploadImage)
        {
            var dbNews = db.T_News.Where(a => a.Id.Equals(news.Id)).FirstOrDefault();

            string Year = DateTime.Now.Year.ToString(), Month = "0" + DateTime.Now.Month.ToString(), Day = "0" + DateTime.Now.Day.ToString(), Hour = "0" + DateTime.Now.Hour.ToString(), Minute = "0" + DateTime.Now.Minute.ToString(), Second = "0" + DateTime.Now.Second.ToString();
            string Time = Year + Month.Substring(Month.Length - 2) + Day.Substring(Day.Length - 2) + "_" + Hour.Substring(Hour.Length - 2) + Minute.Substring(Minute.Length - 2) + Second.Substring(Second.Length - 2);

            List<T_MsApproval> listApproval = db.T_MsApproval.ToList();

            int userLevel = int.Parse(Session["UserLevel"].ToString());

            if (listApproval.Any(a => a.UserLevel == userLevel))
            {
                dbNews.Judul = news.Judul;
                dbNews.Caption = news.Caption;
                dbNews.Isi = news.Isi.Replace(System.Environment.NewLine, ";");
                dbNews.CategoryCode = news.CategoryCode;
                dbNews.ModifyBy = Session["LogedUserID"].ToString();
                dbNews.ModifyDate = DateTime.Now;

                if (uploadImage != null)
                {
                    Directory.CreateDirectory(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + Time);
                    System.IO.File.Delete(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + dbNews.ImagePath.Substring(14, 16) + dbNews.Image);
                    Directory.Delete(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + dbNews.ImagePath.Substring(14, 15));

                    var imagename = Path.GetFileName(uploadImage.FileName);
                    var imagepath = Path.Combine(Server.MapPath("~/Content/news/" + Time), imagename);
                    uploadImage.SaveAs(imagepath);

                    dbNews.Image = imagename;
                    dbNews.ImagePath = "/Content/news/" + Time + "/" + imagename;
                }

                if (uploadFile != null)
                {
                    Directory.CreateDirectory(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + Time);

                    if (dbNews.AttachPath != null)
                    {
                        System.IO.File.Delete(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + dbNews.AttachPath.Substring(25, 16) + dbNews.Attach);
                        Directory.Delete(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + dbNews.AttachPath.Substring(25, 15));
                    }

                    var filename = Path.GetFileName(uploadFile.FileName);
                    var filepath = Path.Combine(Server.MapPath("~/Content/news/attachment/" + Time), filename);
                    uploadFile.SaveAs(filepath);

                    dbNews.Attach = filename;
                    dbNews.AttachPath = "/Content/news/attachment/" + Time + "/" + filename;
                }

                TempData["Message"] = "News Updated";
            }
            else
            {
                T_ContentChanges changes = new T_ContentChanges();
                changes.Modul = "News";
                changes.Type = "Update";
                changes.NewsId = news.Id;
                changes.Judul = news.Judul;
                changes.Caption = news.Caption;
                changes.Isi = news.Isi.Replace(System.Environment.NewLine, ";");
                changes.CategoryCode = news.CategoryCode;
                changes.PostBy = Session["LogedUserID"].ToString();
                changes.PostDate = DateTime.Now;

                if (uploadImage != null)
                {
                    Directory.CreateDirectory(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + Time);

                    var imagename = Path.GetFileName(uploadImage.FileName);
                    var imagepath = Path.Combine(Server.MapPath("~/Content/news/" + Time), imagename);
                    uploadImage.SaveAs(imagepath);

                    changes.Image = imagename;
                    changes.ImagePath = "/Content/news/" + Time + "/" + imagename;
                }
                else
                {
                    changes.Image = dbNews.Image;
                    changes.ImagePath = dbNews.ImagePath;
                }

                if (uploadFile != null)
                {
                    Directory.CreateDirectory(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + Time);

                    var filename = Path.GetFileName(uploadFile.FileName);
                    var filepath = Path.Combine(Server.MapPath("~/Content/news/attachment/" + Time), filename);
                    uploadFile.SaveAs(filepath);

                    changes.Attach = filename;
                    changes.AttachPath = "/Content/news/attachment/" + Time + "/" + filename;
                }
                else
                {
                    changes.Attach = dbNews.Attach;
                    changes.AttachPath = dbNews.AttachPath;
                }

                TempData["Message"] = "News Updated, Waiting for Approval By SuperAdmin";
                db.T_ContentChanges.Add(changes);
            }

            db.SaveChanges();

            return RedirectToAction("ListNews");
        }

        [NoDirectAccess]
        [SessionTimeout]
        public ActionResult DeleteNews(int NewsId)
        {
            var news = db.T_News.Where(a => a.Id.Equals(NewsId)).FirstOrDefault();
            List<T_MsApproval> listApproval = db.T_MsApproval.ToList();

            int userLevel = int.Parse(Session["UserLevel"].ToString());

            if (listApproval.Any(a => a.UserLevel == userLevel))
            {
                System.IO.File.Delete(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + news.ImagePath.Substring(14, 16) + news.Image);
                Directory.Delete(WebConfigurationManager.AppSettings["NewsImagePathFolder"] + news.ImagePath.Substring(14, 15));

                if (news.AttachPath != null)
                {
                    System.IO.File.Delete(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + news.AttachPath.Substring(25, 16) + news.Attach);
                    Directory.Delete(WebConfigurationManager.AppSettings["NewsAttachmentPathFolder"] + news.AttachPath.Substring(25, 15));
                }

                db.T_News.Remove(news);
                TempData["Message"] = "News Deleted";
            }
            else
            {
                T_ContentChanges changes = new T_ContentChanges();
                changes.Modul = "News";
                changes.Type = "Delete";
                changes.NewsId = news.Id;
                changes.Judul = news.Judul;
                changes.Caption = news.Caption;
                changes.Isi = news.Isi;
                changes.CategoryCode = news.CategoryCode;
                changes.PostBy = Session["LogedUserID"].ToString();
                changes.PostDate = DateTime.Now;
                changes.Image = news.Image;
                changes.ImagePath = news.ImagePath;
                changes.Attach = news.Attach;
                changes.AttachPath = news.AttachPath;

                db.T_ContentChanges.Add(changes);
                TempData["Message"] = "News Deleted, Waiting for Approval By SuperAdmin";
            }

            db.SaveChanges();

            return RedirectToAction("ListNews");
        }
    }
}
