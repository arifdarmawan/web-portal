﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebPortal
{
    public class ViewEngineConfig : RazorViewEngine
    {
        public ViewEngineConfig()
        {
            List<string> viewLocations = new List<string> {  
                    "~/Views/{1}/{0}.cshtml",  
                    "~/Views/{1}/{0}.cshtml",  
                    "~/Views/Shared/{0}.cshtml",  
                    "~/Views/Shared/{0}.cshtml",  
                    "~/Views/Corporate/{1}/{0}.cshtml",
                    "~/Views/SOP/{1}/{0}.cshtml",
                    "~/Views/Knowledge/{1}/{0}.cshtml",
                    "~/Views/DataSetting/{1}/{0}.cshtml"
            };

            this.PartialViewLocationFormats = viewLocations.ToArray();
            this.ViewLocationFormats = viewLocations.ToArray();
        }
    }
}