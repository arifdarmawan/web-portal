var prevScrollpos = window.pageYOffset;
		window.onscroll = function() {
		var currentScrollPos = window.pageYOffset;
		  if (prevScrollpos > currentScrollPos) {
			document.getElementById("navbar").style.top = "0";
			document.getElementById("navbar").style.transition = "top 0.3s";
		  } else {
			document.getElementById("navbar").style.top = "-40px"; 
			document.getElementById("navbar").style.transition = "top 0.3s";
		  }
		  prevScrollpos = currentScrollPos;
		}