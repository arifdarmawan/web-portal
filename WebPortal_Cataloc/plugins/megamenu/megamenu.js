	$( document ).ready( function () {
		$( '.dropdown-menu a.dropdown-toggle' ).hover(function ( e ) {
			var $el = $( this );
			var $parent = $( this ).offsetParent( ".dropdown-menu" );
			if ( !$( this ).next().hasClass( 'show' ) ) {
				$( this ).parents( '.dropdown-menu' ).first().find( '.show' ).removeClass( "show" );
			}
			var $subMenu = $( this ).next( ".dropdown-menu" );
			$subMenu.toggleClass( 'show' );
					
			if ( !$parent.parent().hasClass( 'navbar-nav' ) ) {
				$el.next().css( { "top": "0", "height":"100%", "min-width": "200px", "left": $parent.outerWidth() - 5 } );
			}

			return false;
		} );
	} );
