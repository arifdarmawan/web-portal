﻿function showLoading($object) {
    $object.append('<span class="loading-animation"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i></span>');
}

function removeLoading($object) {
   $object.remove();
}