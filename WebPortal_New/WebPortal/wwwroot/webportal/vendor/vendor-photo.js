﻿(function ($) {
    'use strict';

    var initialVendorPhoto = (function () {
        var btnphotoSubmit, formVendorPhoto, imgProfile, ImageAttachment, wrapBtnUploadPhoto;

        var initialDom = function () {
            btnphotoSubmit = $('#btn_Photo_submit'); formVendorPhoto = $('#form_vendor_photo'); imgProfile = $('#img_profile_num');
            ImageAttachment = $('#Image'); wrapBtnUploadPhoto = $('#wrap_btn_upload_photo');
        };

        var submitVendorPhoto = function () {
            btnphotoSubmit.click(function () {

                var formData = new FormData();
                var imgFile = imgProfile[0].files[0];

                if (imgFile.size > 1048576) {
                    wrapBtnUploadPhoto.after('<div class="invalid-feedback pull-left" id="inpt_file_upload">' + 'Max Upload file 1MB' + '</div>');
                }
                else {
                    $('.inpt_file_upload').remove();
                    formData.append('Image', ImageAttachment.val());

                    if (typeof imgFile !== 'undefined')
                    formData.append('Attachments', imgFile, 'Photo__' + imgFile.name);

                    $.when(WEBPORTAL.Services.POSTLocal(formData, WEBPORTAL.URLContext.SavePhoto, false, false)).done(function (result, status, xhr) {
                        if (result.code === 200) {
                            $.ajax({
                                url: WEBPORTAL.URLContext.DownloadFile + '?fileName=' + result.data,
                                success: function (result, status, xhr) {

                                    try {
                                        this.srcObject = stream;
                                    } catch (error) {
                                        var base64str = result.data;

                                        // decode base64 string, remove space for IE compatibilitys
                                        var binary = atob(base64str.replace(/\s/g, ''));
                                        var len = binary.length;
                                        var buffer = new ArrayBuffer(len);
                                        var view = new Uint8Array(buffer);
                                        for (var i = 0; i < len; i++) {
                                            view[i] = binary.charCodeAt(i);
                                        }

                                        // create the blob object with content-type "application/jpg"               
                                        var blob = new Blob([view], { type: "image/jpg" });
                                        var url = URL.createObjectURL(blob);

                                        this.src = url;
                                    }
                                    if (result.data != "") {
                                        document.getElementById("img_bar").src = this.src; // top bar section
                                        imgSideBar.html('<img src="' + this.src + '" class="rounded-circle userdp-sidebar-img" alt="image-user">'); // sidebar section
                                    }
                                },
                                error: function (result, status, xhr) {
                                    console.log(result);
                                },
                                timeout: 60000
                            });

                            setTimeout(function () {
                                WEBPORTAL.Utility.ConstructUpdateButton(btnphotoSubmit);
                                WEBPORTAL.Utility.ConstructNotificationSuccess("Save Photo Success");
                            }, 300);
                        } else {
                            setTimeout(function () {
                                WEBPORTAL.Utility.ConstructNotificationError(result.message);
                                WEBPORTAL.Utility.SubmitRemoveLoading(btnphotoSubmit);
                            }, 300);
                        }
                    });
                }
            });
        };

        var run = function () {
            initialDom();
            submitVendorPhoto();
        };

        return {
            run: run
        };
    })();

    initialVendorPhoto.run();

})(jQuery);