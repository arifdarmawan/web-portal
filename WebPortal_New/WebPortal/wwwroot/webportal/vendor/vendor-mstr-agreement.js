﻿(function ($) {
    'use strict';

    var initialVendorMstrAgreement = (function () {
        var masterAgreement, ckMasterAgreement, lblMstrAgreement, btnSubmitVendorMstrAgreement;

        var initialDom = function () {
            masterAgreement = $('#MasterAgreement');
            lblMstrAgreement = $('#lbl_master_agreement');
            btnSubmitVendorMstrAgreement = $('#btn_mstrAgreement_submit');
            ckMasterAgreement = $('#ckMasterAgreement');
        };

        var submitVendorBussEthics = function () {
            btnSubmitVendorMstrAgreement.click(function () {
                WEBPORTAL.Utility.SubmitLoading(btnSubmitVendorMstrAgreement);
                if (formValidation()) {

                    setTimeout(function () {
                        WEBPORTAL.Utility.SubmitRemoveLoading(btnSubmitVendorMstrAgreement);
                    }, 300);

                    $.when(WEBPORTAL.Services.POSTLocal(null, URL_CONTEXT_UpdateVendorMasterAgreement + "?masterAgreement=" + masterAgreement.val())).done(function (result, status, xhr) {
                        if (result.code === 200) {
                            setTimeout(function () {
                                WEBPORTAL.Utility.SubmitRemoveLoading(btnSubmitVendorMstrAgreement);
                                WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                            }, 500);
                        } else {
                            setTimeout(function () {
                                WEBPORTAL.Utility.SubmitRemoveLoading(btnSubmitVendorMstrAgreement);
                                WEBPORTAL.Utility.ConstructNotificationError(result.message);
                            }, 500);
                        }
                    });
                } else {

                    setTimeout(function () {
                        WEBPORTAL.Utility.SubmitRemoveLoading(btnSubmitVendorMstrAgreement);
                    }, 300);
                }

            });
        };

        var formValidation = function () {
            var ismasterAgreement = false,
                isFormValid = false;

            if (!ckMasterAgreement.is(":checked")) {
                if ($('#mstrAgreement_err').length == 0) {
                    lblMstrAgreement.after('<div class="invalid-feedback" id="mstrAgreement_err">' + 'Please indicate that you accept the Business Ethics.' + '</div>');

                    ismasterAgreement = false;
                }
            } else {
                $('#mstrAgreement_err').remove();
                masterAgreement.val(true);
                ismasterAgreement = true;
            }

            if (ismasterAgreement) {
                isFormValid = true;
            }

            return isFormValid;
        };

        var masterAgreementOnLoad = function () {
            if (masterAgreement.val() == 'True') {
                ckMasterAgreement.prop('checked', true);
            }
        };

        var run = function () {
            initialDom();
            masterAgreementOnLoad();
            submitVendorBussEthics();
        };

        return {
            run: run
        };
    })();

    initialVendorMstrAgreement.run();
})(jQuery);