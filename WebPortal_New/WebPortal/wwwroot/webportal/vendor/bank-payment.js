﻿(function ($) {
    'use strict';

    var initialBankPayment = (function () {
        let urlContext = '';
        let gridBankAccount, gridBsFieldPaymentMtd, bankAccountForm, btnSaveBankAccount, modalBankAccount, btnOpenBankAccountForm, slctBusinessFieldTop,
            id, masterDataBankId, area, masterDataCurrencyId, city, accountName, accountNumber, masterDataBusinessFieldId, mappingDataBusinessPaymentMethodId, mappingDataBusinessFieldTopId,
            lblMasterDataBankId, lblArea, lblMasterDataCurrencyId, lblCity, lblAccountName, lblAccountNumber, lblMappingDataBusinessFieldTopId, wrapGridBsFld, btnSaveVendorPayment,
            mappingDataBusinessPaymentMethodTempId, mappingDataBusinessFieldTopTempId, wrapBtnBankAttach, bankAttachmentFile, removeFile, downloadFile, isValidFile, checkValidFile,
            maxSizeFile, jpeg, png, pdf;

        var initialDom = function () {
            gridBankAccount = $('#grid_bank-account');
            gridBsFieldPaymentMtd = $('#grid_bs-fld-pymnt-mthd');
            btnOpenBankAccountForm = $('.btn-open__bank-accountForm');
            bankAccountForm = $('#vendor-frm-bank-account');
            btnSaveBankAccount = $('#btn-save-bank-account');
            modalBankAccount = $('#addbankModal');
            id = $('#Id');
            masterDataBankId = $('#MasterDataBankId');
            masterDataBusinessFieldId = $('#MasterDataBusinessFieldId');
            mappingDataBusinessPaymentMethodId = $('#MappingDataBusinessPaymentMethodId');
            mappingDataBusinessFieldTopId = $('#MappingDataBusinessFieldTopId');
            area = $('#Area');
            masterDataCurrencyId = $('#MasterDataCurrencyId');
            city = $('#City');
            accountName = $('#AccountName');
            accountNumber = $('#AccountNumber');
            lblMasterDataBankId = $('#lblMasterDataBankId');
            lblMasterDataCurrencyId = $('#lblMasterDataCurrencyId');
            lblArea = $('#lblArea');
            lblCity = $('#lblCity');
            lblAccountName = $('#lblAccountName');
            lblAccountNumber = $('#lblAccountNumber');
            lblMappingDataBusinessFieldTopId = $('#lbl_mp-dt-bs-fld_top_id');
            wrapGridBsFld = $('#wrap_grid_bs_fld');
            btnSaveVendorPayment = $('#bp_btn-save-vndr-pymnt');
            slctBusinessFieldTop = $('.slct-bs-fld_top');
            //mappingDataBusinessFieldTopTempId = mappingDataBusinessFieldTopId.val();
            mappingDataBusinessPaymentMethodTempId = mappingDataBusinessPaymentMethodId.val();
            wrapBtnBankAttach = $('#wrapBtnBankAttach');
            bankAttachmentFile = $('#bankAttachmentFile');
            downloadFile = $('.downloadFile');;
            removeFile = $('.removeFile');
            jpeg = "image/jpeg"; png = "image/png"; pdf = "application/pdf";
            maxSizeFile = 10485760;
        };

        var initialGridBankAccount = function () {
            gridBankAccount.DataTable({
                ajax: { url: URL_CONTEXT_GetVendorBankAccount, dataSrc: '' },
                //processing: true,
                //serverSide: true,
                searching: false,
                info: false,
                lengthChange: false,
                paging: false,
                columns: [
                    { data: "id", title: "Id", visible: false },
                    { data: "currencyId", visible: false },
                    { data: "currency", title: "Currency" },
                    { data: "bankId", visible: false },
                    { data: "bankName", title: "Bank Name" },
                    { data: "area", title: "Branch" },
                    { data: "city", title: "City" },
                    { data: "accountNumber", title: "Account Number" },
                    { data: "accountName", title: "Account Name" },
                    {
                        data: "status",
                        title: "",
                        createdCell: function (td, cellData, rowData, row, col) {
                            switch (cellData) {
                                case "Main":
                                    $(td).html('<a class="badge badge-success">Primary Account</a>');
                                    break;
                                default:
                                    $(td).html('<a class="badge badge-light">Set to Primary Account</a>');
                                    break;
                            }

                            $(td).click(function () {
                                $.when(WEBPORTAL.Services.POSTLocal(null, URL_CONTEXT_UpdateStatusVendorBankAccount + '?id=' + rowData.id)).done(function (result, status, xhr) {
                                    reloadDataSource();
                                });
                            });
                        }
                    },
                    {
                        data: null,
                        title: "",
                        width: 120,
                        createdCell: function (td, cellData, rowData, row, col) {
                            $(td).html('<div class="btn-group">'
                                + '<a href="#" class="btn btn-icon waves-effect waves-light btn-custom bp_tbl_btn_edit"> <i class="fa fa-edit"></i></a>'
                                + '<a href="#" class="btn btn-icon waves-effect waves-light btn-outline-custom bp_tbl_btn_delete"> <i class="fa fa-trash"></i></a>'
                                + '</div>');

                            $(td).find('.bp_tbl_btn_edit').click(function () {
                                modalBankAccount.find('.modal-dialog .modal-content .modal-body span#bp-mdl__bank-account-title').text('Update Bank');
                                btnSaveBankAccount.find('span').text("Edit Bank");
                                // set value
                                urlContext = URL_CONTEXT_UpdateVendorBankAccount;

                                $('#MasterDataBankId option[value="' + rowData.bankId + '"]').attr('selected', true);
                                $('#MasterDataCurrencyId option[value="' + rowData.currencyId + '"]').attr('selected', 'selected');

                                lblArea.addClass('active');
                                lblCity.addClass('active');
                                lblAccountName.addClass('active');
                                lblAccountNumber.addClass('active');

                                id.val(rowData.id);
                                masterDataBankId.val(rowData.bankId);
                                area.val(rowData.area);
                                masterDataCurrencyId.val(rowData.currencyId);
                                city.val(rowData.city);
                                accountName.val(rowData.accountName);
                                accountNumber.val(rowData.accountNumber);
                                downloadFile.val(rowData.attachment);
                                modalBankAccount.modal('show');
                            });

                            $(td).find('.bp_tbl_btn_delete').click(function () {
                                const swalWithBootstrapButtons = Swal.mixin({
                                    customClass: {
                                        confirmButton: 'btn btn-custom',
                                        cancelButton: 'btn btn-outline-custom'
                                    },
                                    buttonsStyling: false
                                })

                                swalWithBootstrapButtons.fire({
                                    title: 'Are you sure?',
                                    text: "You won't be able to revert this!",
                                    type: 'warning',
                                    showCancelButton: true,
                                    confirmButtonText: 'Yes, delete it!',
                                    cancelButtonText: 'No, cancel!',
                                    reverseButtons: true
                                }).then((result) => {
                                    if (result.value) {
                                        $.when(WEBPORTAL.Services.POSTLocal(null, URL_CONTEXT_DeleteVendorBankAccount + '?id=' + rowData.id)).done(function (result, status, xhr) {
                                            reloadDataSource();
                                            swalWithBootstrapButtons.fire(
                                                'Deleted!',
                                                'Your file has been deleted.',
                                                'success'
                                            )
                                        });
                                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                                        swalWithBootstrapButtons.fire(
                                            'Cancelled',
                                            'Your imaginary data is safe :)',
                                            'warning'
                                        )
                                    }
                                });
                            });
                        }
                    }
                ],
                columnDefs: [
                    {
                        orderable: false,
                        targets: [7, 8]
                    }
                    //{ width: "40%, "targets": 0 },
                ],
                initComplete: function (settings, json) {
                    gridBankAccount.find('thead th').addClass('font-weight-bold');
                }
            });
        };

        var initialGridBusinessFieldPaymentMethod = function () {
            gridBsFieldPaymentMtd.DataTable({
                ajax: { url: URL_CONTEXT_GetBusinessFieldPaymentMethod + '/?paramBusinessFieldId=' + masterDataBusinessFieldId.val(), dataSrc: '' },
                searching: false,
                info: false,
                lengthChange: false,
                ordering: false,
                paging: false,
                columns: [
                    {
                        data: "id",
                        title: "",
                        width: "10",
                        createdCell: function (td, cellData, rowData, row, col) {
                            $(td).html('<div class="form-check form-check-inline">' +
                                '<input type="radio" class="form-check-input" id="rg_bs-fld-pymnt-mtd" name="rg_bs-fld-pymnt-mtd" value=' + cellData + '>' +
                                '<label class="form-check-label"></label>' +
                                '</div>').click(function () {
                                    $(this).find('input[name="rg_bs-fld-pymnt-mtd"]').prop('checked', true);

                                    mappingDataBusinessPaymentMethodId.val($(this).find('input[name="rg_bs-fld-pymnt-mtd"]:checked').val());
                                });

                            if (cellData == mappingDataBusinessPaymentMethodId.val()) {
                                $(td).find('input[name="rg_bs-fld-pymnt-mtd"]').prop('checked', true);
                            }
                        }
                    },
                    {
                        data: "downPayment",
                        title: "Down Payment",
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (cellData === null) {
                                $(td).html('-');
                            }
                        }
                    },
                    {
                        data: "progressive",
                        title: "Progressive",
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (cellData === null) {
                                $(td).html('-');
                            }
                        }
                    },
                    {
                        data: "fullPayment",
                        title: "Full Payment",
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (cellData === null) {
                                $(td).html('-');
                            }
                        }
                    },
                    {
                        data: "retention",
                        title: "Retention",
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (cellData === null) {
                                $(td).html('-');
                            }
                        }
                    }
                ],
                columnDefs: [
                    {
                        render: function (data, type, row) {
                            return data + '%';
                        },
                        targets: [1, 2, 3, 4]
                    }
                ],
                initComplete: function (settings, json) {
                    gridBsFieldPaymentMtd.find('thead').addClass('thead-light');
                    gridBsFieldPaymentMtd.find('thead th').addClass('font-weight-bold');
                    gridBsFieldPaymentMtd.find('thead th:nth-child(2)').append('<a tabindex="0" class=" font-18" role="button" data-toggle="popover" data-placement="top" data-trigger="focus" title="" data-content="* Setelah Invoice Lengkap Diterima" data-original-title="Notes"><i class="fa fa-info-circle ml-2"></i></a>');
                    gridBsFieldPaymentMtd.find('thead th:nth-child(3)').append('<a tabindex="0" data-html="true" class=" font-18" role="button" data-toggle="popover" data-placement="top" data-trigger="focus" title="Notes" data-content="Setelah Kontrak/Perjanjian Ditandatangani; DP >2 Miliar wajib menyerahkan Bank Garansi/Surety Bond"><i class="fa fa-info-circle ml-2"></i></a>');
                    gridBsFieldPaymentMtd.find('thead th:nth-child(4)').append('<a tabindex="0" data-html="true" class=" font-18" role="button" data-toggle="popover" data-placement="top" data-trigger="focus" title="Notes" data-content="Setelah Kontrak/Perjanjian Ditandatangani; DP >2 Miliar wajib menyerahkan Bank Garansi/Surety Bond"><i class="fa fa-info-circle ml-2"></i></a>');
                    $("[data-toggle=popover]").popover();

                    if (json.length == 1) {
                        $(gridBsFieldPaymentMtd).find('tbody tr td input[name="rg_bs-fld-pymnt-mtd"]').prop('checked', true);
                        mappingDataBusinessPaymentMethodId.val(json[0].id);
                    }

                    // check if businessFieldPayment contains mappingDataBusinessPaymentMethodId
                    let isExist = 0;

                    $.each(json, function (index, value) {
                        if (value.id == mappingDataBusinessPaymentMethodId.val()) {
                            isExist++;
                        }
                    });

                    // reset mappingDataBusinessPaymentMethodId to zero
                    if (isExist == 0) {
                        mappingDataBusinessPaymentMethodId.val(0);
                    }

                    // set warning message when changes business fields
                    if (mappingDataBusinessPaymentMethodTempId != mappingDataBusinessPaymentMethodId.val()) {
                        wrapGridBsFld.append('<div class="invalid-feedback bp_opt-err-bs-paymnt-mtd">' + 'You need save your data after changes Business Field' + '</div>');
                    }
                }
            });
        };

        var openBankAccountForm = function () {
            btnOpenBankAccountForm.click(function () {
                modalBankAccount.find('.modal-dialog .modal-content .modal-body span#bp-mdl__bank-account-title').text('Add Bank');
                btnSaveBankAccount.find('span').text("Add Bank");
                modalBankAccount.modal('show');
                lblAccountName.addClass('active');
                resetBankAccountForm();
                urlContext = URL_CONTEXT_InsertVendorBankAccount;
            });
        };

        var submitBankAccount = function () {
            btnSaveBankAccount.click(function () {
                WEBPORTAL.Utility.SubmitLoading(btnSaveBankAccount);
                Array.prototype.filter.call(bankAccountForm, function (form) {
                    if (form.checkValidity() === false) {
                        form.classList.add('was-validated');
                        formValidationBankAccount();

                        setTimeout(function () {
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnSaveBankAccount);
                        }, 300);
                    } else {
                        form.classList.add('was-validated');

                        if (formValidationBankAccount()) {
                            var model = new FormData();

                            model.append('Id', id.val());
                            model.append('MasterDataCurrencyId', masterDataCurrencyId.val());
                            model.append('MasterDataBankId', masterDataBankId.val());
                            model.append('Area', area.val());
                            model.append('City', city.val());
                            model.append('AccountNumber', accountNumber.val());
                            model.append('AccountName', accountName.val());

                            if (typeof bankAttachmentFile[0].files[0] !== 'undefined')
                                model.append('Attachments', bankAttachmentFile[0].files[0], 'BANKACCOUNT__' + bankAttachmentFile[0].files[0].name);

                            $.when(WEBPORTAL.Services.POSTLocal(model, urlContext, false, false)).done(function (result, status, xhr) {
                                if (result.code === 200) {
                                    setTimeout(function () {
                                        $.when(modalBankAccount.modal('hide')).then(resetBankAccountForm());
                                        WEBPORTAL.Utility.SubmitRemoveLoading(btnSaveBankAccount);
                                        WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                                        reloadDataSource();
                                    }, 500);
                                } else {
                                    setTimeout(function () {
                                        modalBankAccount.modal('hide');
                                        WEBPORTAL.Utility.SubmitRemoveLoading(btnSaveBankAccount);
                                        WEBPORTAL.Utility.ConstructNotificationError(result.message);
                                    }, 500);
                                }
                            });
                        }
                    }
                });
            });
        };

        var reloadDataSource = function () {
            gridBankAccount.DataTable().ajax.reload();
        };

        var formValidationBankAccount = function () {
            let isBankName = false, isArea = false, isCurrency = false, isCity = false, isAccountName = false, isAccountNumber = false, isFormValid = false;

            // Bank Name
            if (masterDataBankId.val() === '' || masterDataBankId.val() === null) {
                if ($('.bp_slct_err_bank_name').length == 0) {
                    lblMasterDataBankId.after('<div class="invalid-feedback bp_slct_err_bank_name">' + 'Please select one.' + '</div>');

                    isBankName = false;
                }
            } else {
                $('.bp_slct_err_bank_name').remove();

                isBankName = true;
            }

            // Area
            if (area.val() === '' || area.val() === null) {
                if ($('.bp_inpt_err_area').length == 0) {
                    lblArea.after('<div class="invalid-feedback bp_inpt_err_area">' + 'Please fill the required field.' + '</div>');

                    isArea = false;
                }
            } else {
                $('.bp_inpt_err_area').remove();

                isArea = true;
            }

            // Currency
            if (masterDataCurrencyId.val() === '' || masterDataCurrencyId.val() === null) {
                if ($('.bp_slct_err_currency').length == 0) {
                    lblMasterDataCurrencyId.after('<div class="invalid-feedback bp_slct_err_currency">' + 'Please select one.' + '</div>');

                    isCurrency = false;
                }
            } else {
                $('.bp_slct_err_currency').remove();

                isCurrency = true;
            }

            // City
            if (city.val() === '' || city.val() === null) {
                if ($('.bp_inpt_err_city').length == 0) {
                    lblCity.after('<div class="invalid-feedback bp_inpt_err_city">' + 'Please fill the required field.' + '</div>');

                    isCity = false;
                }
            } else {
                $('.bp_inpt_err_city').remove();

                isCity = true;
            }

            // Account Name
            if (accountName.val() === '' || accountName.val() === null) {
                if ($('.bp_inpt_err_account_name').length == 0) {
                    lblAccountName.after('<div class="invalid-feedback bp_inpt_err_account_name">' + 'Please fill the required field.' + '</div>');

                    isAccountName = false;
                }
            } else {
                $('.bp_inpt_err_account_name').remove();

                isAccountName = true;
            }

            // Account Number
            if (accountNumber.val() === '' || accountNumber.val() === null) {
                if ($('.bp_inpt_err_account_number').length == 0) {
                    lblAccountNumber.after('<div class="invalid-feedback bp_inpt_err_account_number">' + 'Please fill the required field.' + '</div>');

                    isAccountNumber = false;
                }
            } else {
                $('.bp_inpt_err_account_number').remove();

                isAccountNumber = true;
            }

            if (isBankName && isArea && isCurrency && isCity && isAccountName && isAccountNumber)
                isFormValid = true;
            else
                isFormValid = false;

            return isFormValid;
        };

        var onChangeBehaviour = function () {
            masterDataBankId.change(function (e) {
                if (this.value !== '' || this.value !== null)
                    $('.bp_slct_err_bank_name').remove();
            });

            masterDataCurrencyId.change(function (e) {
                if (this.value !== '' || this.value !== null)
                    $('.bp_slct_err_currency').remove();
            });
        };

        var resetBankAccountForm = function () {
            id.val();
            $('#MasterDataBankId option:selected').attr('selected', false);
            masterDataBankId.val('');
            area.val('');
            $('#MasterDataCurrencyId option:selected').attr('selected', false);
            masterDataCurrencyId.val('');
            city.val('');
            //accountName.val('');
            accountNumber.val('');
            bankAttachmentFile.val('');
            downloadFile.val('');

            lblArea.removeClass('active');
            lblCity.removeClass('active');
            //lblAccountName.removeClass('active');
            lblAccountNumber.removeClass('active');

            bankAccountForm.removeClass('was-validated');
        }

        var submitVendorPayment = function () {
            btnSaveVendorPayment.click(function () {
                WEBPORTAL.Utility.SubmitLoading(btnSaveVendorPayment);

                if (formValidationBankPayment()) {
                    let model = {
                        MappingDataBusinessFieldTopConfigId: mappingDataBusinessFieldTopId.val(),
                        mappingDataBusinessFieldPaymentConfigId: mappingDataBusinessPaymentMethodId.val()
                    };

                    $.when(WEBPORTAL.Services.POSTLocal(model, URL_CONTEXT_InsertVendorPaymentMethod)).done(function (result, status, xhr) {
                        if (result.code === 200) {
                            setTimeout(function () {
                                btnSaveVendorPayment.constructUpdateButton();
                                WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                            }, 500);
                        } else {
                            setTimeout(function () {
                                WEBPORTAL.Utility.SubmitRemoveLoading(btnSaveVendorPayment);
                                WEBPORTAL.Utility.ConstructNotificationError(result.message);
                            }, 500);
                        }
                    });
                } else {
                    setTimeout(function () {
                        WEBPORTAL.Utility.SubmitRemoveLoading(btnSaveVendorPayment);
                    }, 500);
                }
            });
        };

        var setBusinessFieldDefaultOption = function () {
            if (slctBusinessFieldTop.children('option').length == 2) {
                mappingDataBusinessFieldTopId.val(slctBusinessFieldTop.children('option:last').val());
            }
        };

        var formValidationBankPayment = function () {
            let isBusinessFieldTop = false, isBusinessPaymentMethod = false, isFormValid = false;

            if (mappingDataBusinessFieldTopId.val() == 0) {
                if ($('.bp_slct-err-bs-paymnt-top').length == 0) {
                    lblMappingDataBusinessFieldTopId.after('<div class="invalid-feedback bp_slct-err-bs-paymnt-top">' + 'Please select one.' + '</div>');

                    isBusinessFieldTop = false;
                }
            } else {
                $('.bp_slct-err-bs-paymnt-top').remove();

                isBusinessFieldTop = true;
            }

            if (mappingDataBusinessPaymentMethodId.val() == 0) {
                if ($('.bp_opt-err-bs-paymnt-mtd').length == 0) {
                    wrapGridBsFld.after('<div class="invalid-feedback bp_opt-err-bs-paymnt-mtd">' + 'Please select one.' + '</div>');

                    isBusinessPaymentMethod = false;
                }
            } else {
                $('.bp_opt-err-bs-paymnt-mtd').remove();

                isBusinessPaymentMethod = true;
            }

            if (isBusinessFieldTop && isBusinessPaymentMethod) {
                isFormValid = true;
            }

            return isFormValid;
        };

        var constructUpdateButton = function () {
            if (mappingDataBusinessPaymentMethodId.val() != 0) {
                btnSaveVendorPayment.constructUpdateButton();
            } else {
                btnSaveVendorPayment.constructSaveButton();
            }
        };

        var onChangeUploadFile = function () {
            bankAttachmentFile.change(function (e) {
                validationUploadFile();
            });
        };

        var isDisableSubmitButton = function (status) {
            btnSaveBankAccount.attr('disabled', status);
        };

        var clickBankAttachment = function () {
            downloadFile.click(function () {
                var fileName = $(this).val();

                if (fileName != '') {
                    $.ajax({
                        url: WEBPORTAL.URLContext.DownloadFile + '?fileName=' + fileName,
                        method: 'GET',
                        success: function (result, status, xhr) {
                            var base64str = result.data;

                            // decode base64 string, remove space for IE compatibilitys
                            var binary = atob(base64str.replace(/\s/g, ''));
                            var len = binary.length;
                            var buffer = new ArrayBuffer(len);
                            var view = new Uint8Array(buffer);
                            for (var i = 0; i < len; i++) {
                                view[i] = binary.charCodeAt(i);
                            }

                            // create the blob object with content-type "application/pdf"               
                            var blob = new Blob([view], { type: "application/pdf" });

                            var a = document.createElement('a');
                            var url = URL.createObjectURL(blob);
                            a.href = url;
                            a.download = fileName.split("__").pop();
                            document.body.append(a);
                            a.click();
                            a.remove();
                            window.URL.revokeObjectURL(url);
                        }
                    });
                }
            });
        };

        var removeUploadFile = function () {
            removeFile.click(function () {
                bankAttachmentFile.val('');
                downloadFile.val('');
                $('#errBankAttachFile').remove();
                isDisableSubmitButton(false);
            });
        };

        var filterInputAccountNumber = function () {
            accountNumber.keydown(function (event) {
                var keyCode = ('which' in event) ? event.which : event.keyCode;

                var isNotWanted = (keyCode == 69 || keyCode == 190);
                return !isNotWanted;
            });
        };

        var validationUploadFile = function () {
            var typeFile = bankAttachmentFile[0].files[0].type;

            if (typeFile === png || typeFile === jpeg || typeFile === pdf) {
                $('#errBankAttachFile').remove();

                if (bankAttachmentFile[0].files[0].size < maxSizeFile) {
                    $('#errBankAttachFile').remove();
                    checkValidFile = true;
                    isDisableSubmitButton(false);
                } else {
                    $('#passwordHelpBlock').after('<div class="invalid-feedback pull-left" id="errBankAttachFile">' + 'Please upload file less than 10 MB.' + '</div>');
                    checkValidFile = false;
                    isDisableSubmitButton(true);
                }             
            }
            else {
                $('#errBankAttachFile').remove();
                $('#passwordHelpBlock').after('<div class="invalid-feedback pull-left" id="errBankAttachFile">' + 'File Not Valid' + '</div>');
                checkValidFile = false;
                isDisableSubmitButton(true);
            }
        };

        var run = function () {
            initialDom();
            initialGridBankAccount();
            initialGridBusinessFieldPaymentMethod();
            onChangeBehaviour();
            onChangeUploadFile();
            submitBankAccount();
            openBankAccountForm();
            constructUpdateButton();
            submitVendorPayment();
            setBusinessFieldDefaultOption();
            clickBankAttachment();
            removeUploadFile();
            filterInputAccountNumber();
        };

        return {
            run: run
        };
    })();

    initialBankPayment.run();
})(jQuery);