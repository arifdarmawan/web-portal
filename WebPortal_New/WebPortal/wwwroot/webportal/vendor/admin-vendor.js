﻿var gridVendor = (function () {
    var initialGrid = function () {
        $('#grid').DataTable({
            ajax: { url: "/Admin/GetAllVendor", dataSrc: '' },
            //processing: true,
            //serverSide: true,
            columns: [
                { data: 'Id', title: "Id", visible: false },
                { data: 'Name', title: "Nama Perusahaan" },
                { data: 'Address', title: "Alamat" },
                { data: 'CompanyPhone', title: "Telepon" },
                { data: 'BusinessFields', title: "Bidang Usaha", width:300 },
                {
                    data: 'Status',
                    title: "Status",
                    createdCell: function (td, cellData, rowData, row, col) {
                        $(td).html('<span class="badge badge-danger">' + cellData + '</span>');
                    }
                },
                {
                    data: null,
                    title: "",
                    createdCell: function (td, cellData, rowData, row, col) {
                        $(td).html('<div class="button-list">'
                            + '<a href="#" class= "btn btn-icon waves-effect waves-light btn-custom" onclick="#"><i class="fa fa-eye"></i></a >'
                            + '<a href="#" class="btn btn-icon waves-effect waves-light btn-custom" data-toggle="modal" data-target="#feedbackModal"> <i class="fa fa-edit"></i></a>'
                            + '</div >');
                    }
                }
            ]
        });
    };

    return {
        run: initialGrid
    };
})(jQuery);

gridVendor.run();
