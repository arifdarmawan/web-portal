﻿var initialMDB = initialMDB || {};
var id = $('#Id'),
    companyName = $("#Name"),
    lblCompanyName = $("#lblName"),
    masterDataBusinessTypeId = $('#MasterDataBusinessTypeId'),
    lblMasterDataBusinessTypeId = $('#lblMasterDataBusinessTypeId'),
    masterBusinessFieldsIdArray = $('#MasterBusinessFieldsIdArray'),
    lblMasterBusinessFieldsIdArray = $('#lblMasterBusinessFieldsIdArray'),
    address = $('#Address'),
    lblAddress = $('#lblAddress'),
    mailingAddress = $('#MailingAddress'),
    lblMailingAddress = $('#lblMailingAddress'),
    masterDataCountryId = $('#MasterDataCountryId'),
    lblMasterDataCountryId = $('#lblMasterDataCountryId'),
    masterDataRegionId = $('#MasterDataRegionId'),
    lblMasterDataRegionId = $('#lblMasterDataRegionId'),
    masterDataCityId = $('#MasterDataCityId'),
    lblMasterDataCityId = $('#lblMasterDataCityId'),
    postalCode = $('#PostalCode'),
    lblPostalCode = $('#lblPostalCode'),
    companyEmail = $('#CompanyEmail'),
    lblCompanyEmail = $('#lblCompanyEmail'),
    companyPhone = $('#CompanyPhone'),
    lblCompanyPhone = $('#lblCompanyPhone'),
    pic = $('#Pic'),
    lblPic = $('#lblPic'),
    idCardNumber = $('#IdCardNumber'),
    lblIdCardNumber = $('#lblIdCardNumber'),
    picEmail = $('#Picemail'),
    lblPicemail = $('#lblPicemail'),
    picPhone = $('#PICPhone'),
    lblPicPhone = $('#lblPicPhone'),
    ckMailingAddress = $('#ckMailingAddress'),
    basicInfoForm = $('.needs-validation'),
    sidebarCompanyName = $('#sidebar-cmp-name'),
    sidebarVendorPic = $('#sidebar-vendor-pic');

initialMDB.FormValidator = {};
initialMDB.Configuration = {};
initialMDB.Configuration.BasicInfo = {};
initialMDB.Configuration.BasicInfo.BusinessType = masterDataBusinessTypeId.find('option[value=""]').prop('disabled', true);
initialMDB.Configuration.BasicInfo.BusinessTypeRemoveMsgErr = masterDataBusinessTypeId.removeError();
initialMDB.Configuration.BasicInfo.BusinessFieldsRemoveMsgErr = masterBusinessFieldsIdArray.removeError();
initialMDB.Configuration.BasicInfo.MailingAddress = function () {
    ckMailingAddress.click(function () {
        var address = $('#Address'),
            mailingAddr = $('#MailingAddress')
        lblMailingAddr = $('#lblMailingAddress');

        if ($(this).is(':checked')) {
            if (address.val() == '') {
                mailingAddr.prop('disabled', false);
                address.focus();
                $(this).prop('checked', false);
            } else {
                mailingAddr.prop('disabled', true);
                lblMailingAddr.addClass('active');
            }

            mailingAddr.val(address.val());
            mailingAddr.text(address.val());
        } else {
            mailingAddr.prop('disabled', false);
        }
    });
};
initialMDB.Configuration.BasicInfo.Country = masterDataCountryId.find('option[value=""]').prop('disabled', true);
initialMDB.Configuration.BasicInfo.CountryRemoveMsgErr = masterDataCountryId.removeError();
initialMDB.Configuration.BasicInfo.Region = masterDataRegionId.find('option[value=""]').prop('disabled', true);
initialMDB.Configuration.BasicInfo.RegionRemoveMsgErr = masterDataRegionId.removeError();
initialMDB.Configuration.BasicInfo.City = masterDataCityId.find('option[value=""]').prop('disabled', true);
initialMDB.Configuration.BasicInfo.City = masterDataCityId.removeError();
initialMDB.FormValidator.SubmitButton = id.val() != 0 ? WEBPORTAL.Utility.ConstructUpdateButton($('#btn-save-basic-info')) : null;
Array.prototype.filter.call(basicInfoForm, function (form) {
    form.addEventListener('submit', function (event) {
        WEBPORTAL.Utility.SubmitLoading($('#btn-save-basic-info'));
        if (form.checkValidity() === false) {
            if (formValidation()) {
                let JSONBasicInfo = {
                    Id: id.val(),
                    Name: companyName.val().toUpperCase(),
                    MasterDataBusinessTypeId: masterDataBusinessTypeId.val(),
                    BusinessTypeName: masterDataBusinessTypeId.find('option:selected').text(),
                    MasterBusinessFieldsIdArray: masterBusinessFieldsIdArray.select2('val'),
                    MasterDataCountryId: masterDataCountryId.val(),
                    MasterDataRegionId: masterDataRegionId.val(),
                    MasterDataCityId: masterDataCityId.val(),
                    Address: address.val().toUpperCase(),
                    MailingAddress: mailingAddress.val().toUpperCase(),
                    PostalCode: postalCode.val(),
                    CompanyEmail: companyEmail.val(),
                    CompanyPhone: companyPhone.val(),
                    Pic: pic.val().toUpperCase(),
                    IdCardNumber: idCardNumber.val(),
                    Picemail: picEmail.val(),
                    PICPhone: picPhone.val()
                };

                $.when(WEBPORTAL.Services.POSTLocal(JSONBasicInfo, WEBPORTAL.URLContext.SaveBasicInfo)).done(function (result, status, xhr) {
                    if (result.code === 200) {
                        id.val(result.data);

                        setTimeout(function () {
                            WEBPORTAL.Utility.ConstructUpdateButton($('#btn-save-basic-info'));
                            WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                        }, 300);

                        setSidebarInfo(JSONBasicInfo.BusinessTypeName + " " + JSONBasicInfo.Name, JSONBasicInfo.Pic);
                    } else {
                        setTimeout(function () {
                            WEBPORTAL.Utility.ConstructNotificationError(result.message);
                            WEBPORTAL.Utility.SubmitRemoveLoading($('#btn-save-basic-info'));
                        }, 300);
                    }
                });
            } else {
                setTimeout(function () {
                    id.val() == 0 ? WEBPORTAL.Utility.SubmitRemoveLoading($('#btn-save-basic-info')) : WEBPORTAL.Utility.ConstructUpdateButton($('#btn-save-basic-info'));
                    WEBPORTAL.Utility.ConstructNotificationError('Save Data Error.');
                }, 300);
            }

            event.preventDefault();
            event.stopPropagation();
        }

        form.classList.add('was-validated');
    }, false);
});

function formValidation() {
    var isFormValid = false,
        isCompanyName = false,
        isBusinesType = false,
        isBusinessFields = false,
        isAddress = false,
        isMailingAddress = false,
        isCountry = false,
        isRegion = false,
        isCity = false,
        isPostalCode = false,
        isCompanyEmail = false,
        isCompanyPhone = false,
        isContactPerson = false,
        isIdCard = false,
        isEmailPerson = false,
        isPhoneNumberPerson = false;

    /* Business Type */
    if (masterDataBusinessTypeId.val() === '' || masterDataBusinessTypeId.val() === null) {
        if ($('.errMasterDataBusinessTypeId').length == 0) {
            lblMasterDataBusinessTypeId.after('<div class="invalid-feedback errMasterDataBusinessTypeId">' + 'Please select one.' + '</div>');

            isBusinesType = false;
        }
        //$('.select-wrapper.mdb-select.md-form.md-outline.select-business-type input').attr('required', true);
    } else {
        $('.errMasterDataBusinessTypeId').remove();
        //$('.select-wrapper.mdb-select.md-form.md-outline.select-business-type input').attr('required', false);
        isBusinesType = true;
    }

    /* Company Name */
    if (companyName.val() == '') {
        //$('.select-wrapper.md-form.md-outline span.caret').css('z-index', '0');

        if ($('.errCompanyName').length == 0) {
            lblCompanyName.after('<div class="invalid-feedback errCompanyName">' + 'Please fill the required field.' + '</div>');

            isCompanyName = false;
        }
    } else {
        $('.errCompanyName').remove();

        isCompanyName = true;
    }

    /* Business Fields */
    if (masterBusinessFieldsIdArray.select2('val').length == 0) {
        if ($('.errMasterBusinessFieldsIdArray').length == 0) {
            lblMasterBusinessFieldsIdArray.after('<div class="invalid-feedback errMasterBusinessFieldsIdArray">' + 'Please select one.' + '</div>');

            isBusinessFields = false;
        }
    } else {
        $('.errMasterBusinessFieldsIdArray').remove();

        isBusinessFields = true;
    }

    /* Address */
    if (address.val() == 0) {
        $('.select-wrapper.md-form.md-outline span.caret').css('z-index', '0');

        if ($('.errAddress').length == 0) {
            lblAddress.after('<div class="invalid-feedback errAddress">' + 'Please fill the required field.' + '</div>');

            isAddress = false;
        }
    } else {
        $('.errAddress').remove();

        isAddress = true;
    }

    /* Mailing Address */
    if (mailingAddress.val() == 0) {
        $('.select-wrapper.md-form.md-outline span.caret').css('z-index', '0');

        if ($('.errMailingAddress').length == 0) {
            lblMailingAddress.after('<div class="invalid-feedback errMailingAddress">' + 'Please fill the required field.' + '</div>');

            isMailingAddress = false;
        }
    } else {
        $('.errMailingAddress').remove();

        isMailingAddress = true;
    }

    /* Country */
    if (masterDataCountryId.val() === '' || masterDataCountryId.val() === null) {
        if ($('.errMasterDataCountryId').length == 0) {
            lblMasterDataCountryId.after('<div class="invalid-feedback errMasterDataCountryId">' + 'Please select one.' + '</div>');

            isCountry = false;
        }
    } else {
        $('.errMasterDataCountryId').remove();

        isCountry = true;
    }

    /* Regions */
    if (masterDataRegionId.val() === '' || masterDataRegionId.val() === null) {

        if ($('.errMasterDataRegionId').length == 0) {
            lblMasterDataRegionId.after('<div class="invalid-feedback errMasterDataRegionId">' + 'Please select one.' + '</div>');

            isRegion = false;
        }
    } else {
        $('.errMasterDataRegionId').remove();

        isRegion = true;
    }

    /* City */
    if (masterDataCityId.val() === '' || masterDataCityId.val() === null) {
        if ($('.errMasterDataCityId').length == 0) {
            lblMasterDataCityId.after('<div class="invalid-feedback errMasterDataCityId">' + 'Please select one.' + '</div>');

            isCity = false;
        }
    } else {
        $('.errMasterDataCityId').remove();
        $('.select-wrapper.mdb-select.md-form.md-outline.select-city input').attr('required', false);

        isCity = true;
    }

    /* Postal Code */
    if (postalCode.val() == '') {
        if ($('.errPostalCode').length == 0) {
            lblPostalCode.after('<div class="invalid-feedback errPostalCode">' + 'Please fill the required field.' + '</div>');

            isPostalCode = false;
        }
    } else {
        $('.errPostalCode').remove();

        isPostalCode = true;
    }

    /* Company Email */
    if (companyEmail.val() == '') {
        if ($('.errCompanyEmail').length == 0) {
            lblCompanyEmail.after('<div class="invalid-feedback errCompanyEmail">' + 'Please fill the required field.' + '</div>');

            isCompanyEmail = false;
        }
    } else if (!WEBPORTAL.Utility.CheckEmail(companyEmail.val())) {
        $('.errCompanyEmail').remove();
        lblCompanyEmail.after('<div class="invalid-feedback errCompanyEmail">' + 'Format email not valid.' + '</div>');

        isCompanyEmail = false;
    } else {
        $('.errCompanyEmail').remove();

        isCompanyEmail = true;
    }

    /* Company Phone */
    if (companyPhone.val() == '') {
        if ($('.errCompanyPhone').length == 0) {
            lblCompanyPhone.after('<div class="invalid-feedback errCompanyPhone">' + 'Please fill the required field.' + '</div>');

            isCompanyPhone = false;
        }
    } else {
        $('.errCompanyPhone').remove();

        isCompanyPhone = true;
    }

    /* Contact Person */
    if (pic.val() == '') {
        if ($('.errPic').length == 0) {
            lblPic.after('<div class="invalid-feedback errPic">' + 'Please fill the required field.' + '</div>');

            isContactPerson = false;
        }
    } else {
        $('.errPic').remove();

        isContactPerson = true;
    }

    /* ID Card */
    if (idCardNumber.val() == '') {
        if ($('.errIdCardNumber').length == 0) {
            lblIdCardNumber.after('<div class="invalid-feedback errIdCardNumber">' + 'Please fill the required field.' + '</div>');

            isIdCard = false;
        }
    } else {
        $('.errIdCardNumber').remove();

        if (idCardNumber.val().length < 16) {
            if ($('.errIdCardNumber').length == 0) {
                lblIdCardNumber.after('<div class="invalid-feedback errIdCardNumber">' + 'Please enter at least 16 digit length.' + '</div>');

                isIdCard = false;
            }          
        } else {
            $('.errIdCardNumber').remove();

            isIdCard = true;
        }
    }

    /* Email Person */
    if (picEmail.val() == '') {
        if ($('.errPicEmail').length == 0) {
            lblPicemail.after('<div class="invalid-feedback errPicEmail">' + 'Please fill the required field.' + '</div>');

            isEmailPerson = false;
        }
    } else if (!WEBPORTAL.Utility.CheckEmail(picEmail.val())) {
        $('.errPicEmail').remove();
        lblPicemail.after('<div class="invalid-feedback errPicEmail">' + 'Format email not valid.' + '</div>');

        isEmailPerson = false;
    }
    else {
        $('.errPicEmail').remove();

        isEmailPerson = true;
    }

    /* Phone Number Person */
    if (picPhone.val() == '') {
        if ($('.errPicPhone').length == 0) {
            lblPicPhone.after('<div class="invalid-feedback errPicPhone">' + 'Please fill the required field.' + '</div>');

            isPhoneNumberPerson = false;
        }
    } else {
        $('.errPicPhone').remove();

        isPhoneNumberPerson = true;
    }

    if (isCompanyName && isBusinesType && isBusinessFields && isAddress && isMailingAddress && isCountry && isRegion &&
        isCity && isPostalCode && isCompanyEmail && isCompanyPhone && isContactPerson && isIdCard && isEmailPerson && isPhoneNumberPerson) {

        isFormValid = true;
    } else {
        isFormValid = false;
    }

    return isFormValid;
};

// set sidebar company name and vendor pic name
function setSidebarInfo(companyInfo, vendorInfo) {
    sidebarCompanyName.text(companyInfo);
    sidebarVendorPic.text(vendorInfo);
}


// Country On Change
masterDataCountryId.change(function () {
    $.getJSON(URL_CONTEXT_GetRegionsByCountryId, { masterDataCountryId: this.value }, function (data) {
        var items = '';
        if (data.length != 0) {
            items += '<option value disabled>Select</option>';

            $.each(data, function (i, region) {
                items += '<option value="' + region.value + '">' + region.text + '</option>';
            });
        } else {
            items += '<option value disabled>Select</option>';
        }

        masterDataRegionId.html(items);
        masterDataCityId.html('<option value disabled>Select</option>');
    });
});

idCardNumber.keypress(function (e) {
    var max = 16;

    if (e.key.length === 1 && /\D/.test(e.key)) {
        e.preventDefault();
    }

    if (this.value.length == max) {
        e.preventDefault();
    } else if (this.value.length > max) {
        // Maximum exceeded
        this.value = this.value.substring(0, max);
    }
});

postalCode.on('keypress', function (event) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

companyPhone.on('keypress', function (event) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

picPhone.on('keypress', function (event) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});



// Region On Change
masterDataRegionId.change(function () {
    $.getJSON(URL_CONTEXT_GetCitiesByRegionId, { MasterDataRegionId: this.value }, function (data) {
        var items = '';
        if (data.length != 0) {
            items += '<option value disabled>Select</option>';

            $.each(data, function (i, cities) {
                items += '<option value="' + cities.value + '">' + cities.text + '</option>';
            });
        } else {
            items += '<option value disabled>Select</option>';
        }

        masterDataCityId.html(items);
    });
});

(function ($) {
    'use strict';

    masterBusinessFieldsIdArray.select2({
        maximumSelectionLength: 5
    });

    masterBusinessFieldsIdArray.on("select2:selecting", function (e) {

        if ($(this).val() && $(this).val().length >= 5) {
            e.preventDefault();
        }
    });

    if ($('#CkMailingAddress').val() === false) {
        ckMailingAddress.prop('checked', true);
    }

    initialMDB.Configuration.BasicInfo.MailingAddress();
})(jQuery);