﻿(function ($) {
    'use strict';

    var initialVendorLegal = (function () {
        var btnLglSubmit, formVendorLegal, wrapDynamicForm, datePicker, id, nib, lblNib, imgNib, nibAttachment, wrapBtnNibAttach,
            bussPermit, lblBussPermit, imgBussPermit, bussPermitAttachment, wrapBtnBussPermitAttach, siup, lblSiup, siupExpDate,
            lblSiupExpDate, imgSiup, siupAttachment, wrapBtnSiupAttach, tdp, lblTdp, tdpExpDate, lblTdpExpDate, imgTdp, tdpExpAttachment,
            wrapBtnTdpAttach, memoOfAsso, lblMemoOfAsso, imgMemoOfAsso, memoOfAsoAttachment, wrapBtnMemoAsso, decsLtrMenhukam, lblDecsLtrMenhukam,
            imgDecsLtrMenhukam, decsLtrMenhukamAttachment, wrapBtnDcsLtr, memoOfAssoChng, lblMemoOfAssoChng, imgMemoOfAssoChng, memoOfAssoChngAttachment,
            decsLtrMenhukamChng, lblDecsLtrMenhukamChng, imgDecsLtrMenhukamChng, decsLtrMenhukamChngAttachment, managementStructure, jobTitleOptions,
            signingId, lblSigningId, signingName, lblSignName, imgSigning, signinAttachment, wrapSignerFile, downloadFile, isPersonalVendorBusinessType,
            removeFile, maxSizeFile, imgNibFile, imgBussPermitFile, imgSiupFile, imgTdpFile, imgMemoOfAssoFile, imgDecsLtrMenhukamFile, imgMemoOfAssoChgFile,
            wrapbtnmemoofassochng, wrapbtndecsltrmenhukamchng, wrapbtnsignerfile, wrapbtnsignername, otherDocument, lblOtherDoc, wrapBtnOtherDoc, imgOtherDoc,
            otherAttachment, imgDecsLtrMenhukamChgFile, imgSignerFile, imgOtherFile, jpg, jpeg, png, pdf;

        var initialDom = function () {
            btnLglSubmit = $('.btn_lgl-submit'); formVendorLegal = $('#form_vendor_legal'); wrapDynamicForm = $('.wrap_dynmc_org_struc'); datePicker = $('.datepicker').pickadate({ formatSubmit: 'yyyy/mm/dd', min: new Date() });
            id = $('#Id'); nib = $('#Nib'); lblNib = $('#lbl_nib_num'); imgNib = $('#img_nib_num'); nibAttachment = $('#Nibattachment'); wrapBtnNibAttach = $('#wrap_btn_nib_attach');
            bussPermit = $('#BusinessPermit'); lblBussPermit = $('#lbl_buss_permit'); imgBussPermit = $('#img_buss_permit'); bussPermitAttachment = $('#BusinessPermitAttachment'); wrapBtnBussPermitAttach = $('#wrap_btn_buss_permit_attach');
            siup = $('#Siup'); lblSiup = $('#lbl_siup_num'); siupExpDate = $('#SiupexpiredDate'); lblSiupExpDate = $('#lbl_siup_exp_date'); imgSiup = $('#img_siup_num'); siupAttachment = $('#Siupattachment'); wrapBtnSiupAttach = $('#wrap_btn_siup_attach');
            tdp = $('#Tdp'); lblTdp = $('#lbl_tdp_num'); tdpExpDate = $('#TdpexpiredDate'); lblTdpExpDate = $('#lbl_tdp_exp_date'); imgTdp = $('#img_tdp_exp_date'); tdpExpAttachment = $('#Tdpattachment'); wrapBtnTdpAttach = $('#wrap_btn_tdp_attach');
            memoOfAsso = $('#MemorandumOfAssociation'); lblMemoOfAsso = $('#lbl_memo_of_asso'); imgMemoOfAsso = $('#img_memo_of_asso'); memoOfAsoAttachment = $('#MemorandumOfAssociationAttachment'); wrapBtnMemoAsso = $('#wrap_btn_memo_asso');
            decsLtrMenhukam = $('#DecissionLetterMenkumham'); lblDecsLtrMenhukam = $('#lbl_decs_ltr_menhukam'); imgDecsLtrMenhukam = $('#img_decs_ltr_menhukam'); decsLtrMenhukamAttachment = $('#DecissionLetterMenkumhamAttachment'); wrapBtnDcsLtr = $('#wrap_btn_dcs_ltr');
            memoOfAssoChng = $('#MemorandumOfAssociationChanging'); lblMemoOfAssoChng = $('#lbl_memo_of_asso_chng'); imgMemoOfAssoChng = $('#img_memo_of_asso_chng'); memoOfAssoChngAttachment = $('#MemorandumOfAssociationChangingAttachment');
            decsLtrMenhukamChng = $('#DecissionLetterMenkumhamChanging'); lblDecsLtrMenhukamChng = $('#lbl_decs_ltr_menhukam_chng'); imgDecsLtrMenhukamChng = $('#img_decs_ltr_menhukam_chng'); decsLtrMenhukamChngAttachment = $('#DecissionLetterMenkumhamChangingAttachment');
            managementStructure = $('#ManagementStructure'); jobTitleOptions = $('#JobTitleOptions');
            signingId = $('#SigningId'); lblSigningId = $('#lbl_slct_signId'); signingName = $('#Signing'); lblSignName = $('#lbl_sign_name'); imgSigning = $('#img_signer_name'); signinAttachment = $('#SigningAttachment');
            wrapSignerFile = $('#wrap_signer_file'); downloadFile = $('.downloadFile'); isPersonalVendorBusinessType = $('#IsPersonalVendorBusinessType'); removeFile = $('.removeFile'); maxSizeFile = 26214400;
            jpg = "jpg"; jpeg = "jpeg"; png = "png"; pdf = "pdf";
            otherDocument = $('#DocumentOther'); lblOtherDoc = $('#lbl_other_doc'); wrapBtnOtherDoc = $('#wrap_btn_other_doc'); imgOtherDoc = $('#img_other_doc'); otherAttachment = $('#OtherAttachment');
            wrapbtnmemoofassochng = $('#wrap_btn_memo_of_asso_chng'); wrapbtndecsltrmenhukamchng = $('#wrap_btn_decs_ltr_menhukam_chng'); wrapbtnsignername = $('#wrap_btn_signer_name');

            if (siup.val() != '')
                WEBPORTAL.Utility.ConstructUpdateButton(btnLglSubmit);

            if (signingId.val() != 2)
                wrapSignerFile.hide();
        };

        var submitVendorLegal = function () {
            btnLglSubmit.click(function () {
                WEBPORTAL.Utility.SubmitLoading(btnLglSubmit);
                Array.prototype.filter.call(formVendorLegal, function (form) {
                    if (form.checkValidity() === false) {
                        formValidation();

                        setTimeout(function () {
                            WEBPORTAL.Utility.ConstructNotificationError(WEBPORTAL.GlobalMessage.SubmitError);

                            if (id.val() == 0)
                                WEBPORTAL.Utility.SubmitRemoveLoading(btnLglSubmit);
                            else
                                WEBPORTAL.Utility.ConstructUpdateButton(btnLglSubmit);
                        }, 300);
                    } else {
                        if (formValidation()) {
                            var formData = new FormData();

                            imgNibFile = imgNib[0].files[0];
                            imgBussPermitFile = imgBussPermit[0].files[0];
                            imgSiupFile = imgSiup[0].files[0];
                            imgTdpFile = imgTdp[0].files[0];
                            imgMemoOfAssoFile = imgMemoOfAsso[0].files[0];
                            imgDecsLtrMenhukamFile = imgDecsLtrMenhukam[0].files[0];
                            imgMemoOfAssoChgFile = imgMemoOfAssoChng[0].files[0];
                            imgDecsLtrMenhukamChgFile = imgDecsLtrMenhukamChng[0].files[0];
                            imgSignerFile = imgSigning[0].files[0];
                            imgOtherFile = imgOtherDoc[0].files[0];
                            var jsonValue = { data: [] };

                            $.each(wrapDynamicForm.find('.row'), function (index, value) {
                                if ($(value).find('input').val() !== '' && $(value).find('select').val() !== null)
                                    jsonValue.data.push({ name: $(value).find('input').val(), jobTitleId: $(value).find('select').val() });
                            });

                            formData.append('Nib', nib.val());
                            formData.append('Nibattachment', nibAttachment.val());
                            formData.append('BusinessPermit', bussPermit.val());
                            formData.append('BusinessPermitAttachment', bussPermitAttachment.val());
                            formData.append('Siup', siup.val());
                            formData.append('SiupexpiredDate', siupExpDate.val());
                            formData.append('Siupattachment', siupAttachment.val());
                            formData.append('Tdp', tdp.val());
                            formData.append('TdpexpiredDate', tdpExpDate.val());
                            formData.append('Tdpattachment', tdpExpAttachment.val());
                            formData.append('MemorandumOfAssociation', memoOfAsso.val());
                            formData.append('MemorandumOfAssociationAttachment', memoOfAsoAttachment.val());
                            formData.append('DecissionLetterMenkumham', decsLtrMenhukam.val());
                            formData.append('DecissionLetterMenkumhamAttachment', decsLtrMenhukamAttachment.val());
                            formData.append('MemorandumOfAssociationChanging', memoOfAssoChng.val());
                            formData.append('MemorandumOfAssociationChangingAttachment', memoOfAssoChngAttachment.val());
                            formData.append('DecissionLetterMenkumhamChanging', decsLtrMenhukamChng.val());
                            formData.append('DecissionLetterMenkumhamChangingAttachment', decsLtrMenhukamChngAttachment.val());
                            formData.append('ManagementStructure', JSON.stringify(jsonValue));
                            formData.append('SigningId', signingId.val());

                            if (signingId.val() != 2) {
                                $.when(WEBPORTAL.Services.POSTLocal(null, URL_CONTEXT_DeleteVendorLegalAttachment + "?type=" + 'signingattachment', false, false)).done(function (result, status, xhr) {
                                    if (result.code === 200) {
                                        imgSignerFile = '';
                                        imgSigning.val('');
                                    }
                                });
                                formData.append('SigningAttachment', '');
                            } else {
                                formData.append('SigningAttachment', signinAttachment.val());
                                if (typeof imgSignerFile !== 'undefined')
                                    formData.append('Attachments', imgSignerFile, 'SIGNER__' + imgSignerFile.name);
                            }

                            formData.append('Signing', signingName.val());
                            formData.append('DocumentOther', otherDocument.val());
                            formData.append('OtherAttachment', otherAttachment.val());

                            //signinAttachment.val('').clone(true);

                            if (typeof imgNibFile !== 'undefined')
                                formData.append('Attachments', imgNibFile, 'NIB__' + imgNibFile.name);

                            if (typeof imgBussPermitFile !== 'undefined')
                                formData.append('Attachments', imgBussPermitFile, 'BUSSPERMIT__' + imgBussPermitFile.name);

                            if (typeof imgSiupFile !== 'undefined')
                                formData.append('Attachments', imgSiupFile, 'SIUP__' + imgSiupFile.name);

                            if (typeof imgTdpFile !== 'undefined')
                                formData.append('Attachments', imgTdpFile, 'TDP__' + imgTdpFile.name);

                            if (typeof imgMemoOfAssoFile !== 'undefined')
                                formData.append('Attachments', imgMemoOfAssoFile, 'MEMOASSO__' + imgMemoOfAssoFile.name);

                            if (typeof imgDecsLtrMenhukamFile !== 'undefined')
                                formData.append('Attachments', imgDecsLtrMenhukamFile, 'DECSLTR__' + imgDecsLtrMenhukamFile.name);

                            if (typeof imgMemoOfAssoChgFile !== 'undefined')
                                formData.append('Attachments', imgMemoOfAssoChgFile, 'MEMOASSOCHG__' + imgMemoOfAssoChgFile.name);

                            if (typeof imgDecsLtrMenhukamChgFile !== 'undefined')
                                formData.append('Attachments', imgDecsLtrMenhukamChgFile, 'DECSLTRCHG__' + imgDecsLtrMenhukamChgFile.name);                         

                            if (typeof imgOtherFile !== 'undefined')
                                formData.append('Attachments', imgOtherFile, 'OTHERDOC__' + imgOtherFile.name);

                            if (formValidationFileSize()) {
                                $.when(WEBPORTAL.Services.POSTLocal(formData, URL_CONTEXT_InsertVendorLegal, false, false)).done(function (result, status, xhr) {
                                    if (result.code === 200) {
                                        setAttachmentModelValue(result.data);

                                        setTimeout(function () {
                                            WEBPORTAL.Utility.ConstructUpdateButton(btnLglSubmit);
                                            WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                                        }, 300);
                                    } else {
                                        setTimeout(function () {
                                            WEBPORTAL.Utility.ConstructNotificationError(result.message);
                                            WEBPORTAL.Utility.SubmitRemoveLoading(btnLglSubmit);
                                        }, 300);
                                    }
                                });
                            } else {
                                setTimeout(function () {
                                    WEBPORTAL.Utility.ConstructNotificationError(WEBPORTAL.GlobalMessage.SubmitError);
                                    if (id.val() === 0)
                                        WEBPORTAL.Utility.SubmitRemoveLoading(btnLglSubmit);
                                    else
                                        WEBPORTAL.Utility.ConstructUpdateButton(btnLglSubmit);
                                }, 300);
                            }
                        } else {
                            setTimeout(function () {
                                WEBPORTAL.Utility.ConstructNotificationError(WEBPORTAL.GlobalMessage.SubmitError);
                                if (id.val() == 0)
                                    WEBPORTAL.Utility.SubmitRemoveLoading(btnLglSubmit);
                                else
                                    WEBPORTAL.Utility.ConstructUpdateButton(btnLglSubmit);
                            }, 300);
                        }
                    }
                    form.classList.add('was-validated');
                });
            });
        };

        var formValidation = function () {
            var isFormValid = false,
                isSiup = false,
                isSiupExpDate = false,
                isSiupAttachment = false,
                isTdp = false,
                isTdpExpDate = false,
                isTdpAttachment = false,
                isMemoOfAsso = false,
                isMemoOfAssoAttachment = false,
                isMgmStructure = false,
                isSigningId = false,
                isSigningName = false,
                isSigningAttachment = false;

            // SIUP Number
            if (siup.val() === '' || siup.val() === null) {
                if ($('.inpt_err_siup').length == 0) {
                    lblSiup.after('<div class="invalid-feedback inpt_err_siup">' + 'Please fill the required field.' + '</div>');

                    isSiup = false;
                }
            } else {
                $('.inpt_err_siup').remove();

                isSiup = true;
            }

            // SIUP Exp Date
            if (siupExpDate.val() === '' || siupExpDate.val() === null) {

                if ($('#inpt_err_siupExpDate').length == 0) {
                    lblSiupExpDate.after('<div class="invalid-feedback" id="inpt_err_siupExpDate">' + 'Please select date.' + '</div>');

                    isSiupExpDate = false;
                }
            } else {
                $('#inpt_err_siupExpDate').remove();

                isSiupExpDate = true;
            }

            // SIUP Attachment
            if (siupAttachment.val() === '' || siupAttachment.val() === null) {
                if (typeof imgSiup[0].files[0] === 'undefined') {
                    if ($('#inpt_file_siup').length == 0) {
                        wrapBtnSiupAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_siup">' + 'Please attach a files.' + '</div>');

                        isSiupAttachment = false;
                    }
                } else {
                    $('#inpt_file_siup').remove();

                    isSiupAttachment = true;
                }
            } else {
                $('#inpt_file_siup').remove();

                isSiupAttachment = true;
            }

            // TDP Number
            if (tdp.val() === '' || tdp.val() === null) {
                if ($('.inpt_err_tdp').length == 0) {
                    lblTdp.after('<div class="invalid-feedback inpt_err_tdp">' + 'Please fill the required field.' + '</div>');

                    isTdp = false;
                }
            } else {
                $('.inpt_err_tdp').remove();

                isTdp = true;
            }

            // TDP Exp Date
            if (tdpExpDate.val() === '' || tdpExpDate.val() === null) {
                if ($('#inpt_err_tdpExpDate').length == 0) {
                    lblTdpExpDate.after('<div class="invalid-feedback" id="inpt_err_tdpExpDate">' + 'Please select date.' + '</div>');

                    isTdpExpDate = false;
                }
            } else {
                $('#inpt_err_tdpExpDate').remove();

                isTdpExpDate = true;
            }

            // TDP Attachment
            if (tdpExpAttachment.val() == '' || tdpExpAttachment.val() == null) {
                if (typeof imgTdp[0].files[0] === 'undefined') {
                    if ($('#inpt_file_tdp').length == 0) {
                        wrapBtnTdpAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_tdp">' + 'Please attach a files.' + '</div>');

                        isTdpAttachment = false;
                    }
                } else {
                    $('#inpt_file_tdp').remove();

                    isTdpAttachment = true;
                }
            } else {
                $('#inpt_file_tdp').remove();

                isTdpAttachment = true;
            }

            // Akta Pendirian
            if (isPersonalVendorBusinessType.val() === 'False') {
                if (memoOfAsso.val() === '' || memoOfAsso.val() === null) {
                    if ($('.inpt_err_memo_of_asso').length == 0) {
                        lblMemoOfAsso.after('<div class="invalid-feedback inpt_err_memo_of_asso">' + 'Please fill the required field.' + '</div>');

                        isMemoOfAsso = false;
                    }
                } else {
                    $('.inpt_err_memo_of_asso').remove();

                    isMemoOfAsso = true;
                }

                // Akta Pendirian Attachment
                if (memoOfAsoAttachment.val() == '' || memoOfAsoAttachment.val() == null) {
                    if (typeof imgMemoOfAsso[0].files[0] === 'undefined') {
                        if ($('#inpt_file_memo_asso').length == 0) {
                            wrapBtnMemoAsso.after('<div class="invalid-feedback pull-left" id="inpt_file_memo_asso">' + 'Please attach a files.' + '</div>');

                            isMemoOfAssoAttachment = false;
                        }
                    } else {
                        $('#inpt_file_memo_asso').remove();

                        isMemoOfAssoAttachment = true;
                    }
                } else {
                    $('#inpt_file_memo_asso').remove();

                    isMemoOfAssoAttachment = true;
                }
            } else {
                isMemoOfAsso = true;
                isMemoOfAssoAttachment = true;
            }

            // Management Structure
            $.each(wrapDynamicForm.find('.row'), function (index, form) {
                if ($(form).find('input').val() == '') {
                    if ($(form).find('.inpt_err_mgm_strc').length == 0) {
                        $(form).find('label#lbl_inpt_dirship').after('<div class="invalid-feedback inpt_err_mgm_strc">' + 'Please fill the required field.' + '</div>');
                    }
                } else {
                    $(form).find('.inpt_err_mgm_strc').remove();
                }

                if ($(form).find('select').val() == null) {
                    if ($(form).find('.slct_err_mgm_strc').length == 0) {
                        $(form).find('label#lbl_slct_jobTitle').after('<div class="invalid-feedback slct_err_mgm_strc">' + 'Please fill the required field.' + '</div>');
                    }
                } else {
                    $(form).find('.slct_err_mgm_strc').remove();
                }
            });

            if (wrapDynamicForm.find('.invalid-feedback').length == 0)
                isMgmStructure = true;

            // Signer Select
            if (signingId.val() == '') {
                if ($('.vl_err_signer_id').length == 0) {
                    lblSigningId.after('<div class="invalid-feedback vl_err_signer_id">' + 'Please select one.' + '</div>');

                    isSigningId = false;
                }
            } else {
                $('.vl_err_signer_id').remove();

                isSigningId = true;
            }

            // Signer Name
            if (signingName.val() == '') {
                if ($('.vl_err_signer_name').length == 0) {
                    lblSignName.after('<div class="invalid-feedback vl_err_signer_name">' + 'Please fill the required field.' + '</div>');

                    isSigningName = false;
                }
            } else {
                $('.vl_err_signer_name').remove();

                isSigningName = true;
            }

            if (signingId.val() == 2) {
                if (signinAttachment.val() == '' || signinAttachment.val() == null) {
                    if (typeof imgSigning[0].files[0] === 'undefined') {
                        if ($('#inpt_file_signer_file').length == 0) {
                            wrapbtnsignername.after('<div class="invalid-feedback pull-left" id="inpt_file_signer_file">' + 'Please attach a files.' + '</div>');

                            isSigningAttachment = false;
                        }
                    } else {
                        $('#inpt_file_signer_file').remove();

                        isSigningAttachment = true;
                    }
                } else {
                    $('#inpt_file_signer_file').remove();

                    isSigningAttachment = true;
                }
            }
            else {
                $('#inpt_file_signer_file').remove();

                isSigningAttachment = true;
            }

            if (isSiup && isSiupAttachment && isSiupExpDate
                && isTdp && isTdpAttachment && isTdpExpDate && isMemoOfAsso && isMemoOfAssoAttachment
                && isMgmStructure && isSigningId && isSigningName && isSigningAttachment) {
                isFormValid = true;
            }

            return isFormValid;
        };

        var formValidationFileSize = function () {
            var isFormValid = false,
                isImgNibFile = false,
                isImgBussPermitFile = false,
                isImgSiupFile = false,
                isImgTdpFile = false,
                isImgMemoOfAssoFile = false,
                isImgDecsLtrMenhukamFile = false,
                isImgMemoOfAssoChgFile = false,
                isImgDecsLtrMenhukamChgFile = false,
                isImgSignerFile = false,
                isImgOtherFile = false;

            // 1 Nib
            if (typeof imgNibFile !== 'undefined') {
                if (imgNibFile.size > maxSizeFile) {
                    if ($('#inpt_file_nib').length === 0) {
                        wrapBtnNibAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_nib">' + 'Please upload file less than 25 MB.' + '</div>');

                        isImgNibFile = false;
                    }
                } else {
                    $('#inpt_file_nib').remove();

                    isImgNibFile = true;
                }
            } else {
                $('#inpt_file_nib').remove();

                isImgNibFile = true;
            }

            // 2 BussPermit
            if (typeof imgBussPermitFile !== 'undefined') {
                if (imgBussPermitFile.size > maxSizeFile) {
                    if ($('#inpt_file_buss_permit').length === 0) {
                        wrapBtnBussPermitAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_buss_permit">' + 'Please upload file less than 25 MB.' + '</div>');

                        isImgBussPermitFile = false;
                    }
                } else {
                    $('#inpt_file_buss_permit').remove();

                    isImgBussPermitFile = true;
                }
            } else {
                $('#inpt_file_buss_permit').remove();

                isImgBussPermitFile = true;
            }

            // 3 Siup
            if (typeof imgSiupFile !== 'undefined') {
                if (imgSiupFile.size > maxSizeFile) {
                    if ($('#inpt_file_siup').length === 0) {
                        wrapBtnSiupAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_siup">' + 'Please upload file less than 25 MB.' + '</div>');

                        isImgSiupFile = false;
                    }
                } else {
                    $('#inpt_file_siup').remove();

                    isImgSiupFile = true;
                }
            } else {
                $('#inpt_file_siup').remove();

                isImgSiupFile = true;
            }

            // 4 Tdp
            if (typeof imgTdpFile !== 'undefined') {
                if (imgTdpFile.size > maxSizeFile) {
                    if ($('#inpt_file_tdp').length === 0) {
                        wrapBtnTdpAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_tdp">' + 'Please upload file less than 25 MB.' + '</div>');

                        isImgTdpFile = false;
                    }
                } else {
                    $('#inpt_file_tdp').remove();

                    isImgTdpFile = true;
                }
            } else {
                $('#inpt_file_tdp').remove();

                isImgTdpFile = true;
            }

            // 5 MemoOfAsso
            if (typeof imgMemoOfAssoFile !== 'undefined') {
                if (imgMemoOfAssoFile.size > maxSizeFile) {
                    if ($('#inpt_file_memo_asso').length === 0) {
                        wrapBtnMemoAsso.after('<div class="invalid-feedback pull-left" id="inpt_file_memo_asso">' + 'Please upload file less than 25 MB.' + '</div>');
                        isImgMemoOfAssoFile = false;
                    }
                } else {
                    $('#inpt_file_memo_asso').remove();

                    isImgMemoOfAssoFile = true;
                }
            } else {
                $('#inpt_file_memo_asso').remove();

                isImgMemoOfAssoFile = true;
            }

            // 6 DecsLtrMenhukam
            if (typeof imgDecsLtrMenhukamFile !== 'undefined') {
                if (imgDecsLtrMenhukamFile.size > maxSizeFile) {
                    if ($('#inpt_file_dcs_ltr').length === 0) {
                        wrapBtnDcsLtr.after('<div class="invalid-feedback pull-left" id="inpt_file_dcs_ltr">' + 'Please upload file less than 25 MB.' + '</div>');
                        isImgDecsLtrMenhukamFile = false;
                    }
                } else {
                    $('#inpt_file_dcs_ltr').remove();

                    isImgDecsLtrMenhukamFile = true;
                }
            } else {
                $('#inpt_file_dcs_ltr').remove();

                isImgDecsLtrMenhukamFile = true;
            }

            // 7 MemoOfAssoChg
            if (typeof imgMemoOfAssoChgFile !== 'undefined') {
                if (imgMemoOfAssoChgFile.size > maxSizeFile) {
                    if ($('#inpt_file_memo_of_asso_chng').length === 0) {
                        wrapbtnmemoofassochng.after('<div class="invalid-feedback pull-left" id="inpt_file_memo_of_asso_chng">' + 'Please upload file less than 25 MB.' + '</div>');

                        isImgMemoOfAssoChgFile = false;
                    }
                } else {
                    $('#inpt_file_memo_of_asso_chng').remove();

                    isImgMemoOfAssoChgFile = true;
                }
            } else {
                $('#inpt_file_memo_of_asso_chng').remove();

                isImgMemoOfAssoChgFile = true;
            }

            // 8 DecsLtrMenhukamChg
            if (typeof imgDecsLtrMenhukamChgFile !== 'undefined') {
                if (imgDecsLtrMenhukamChgFile.size > maxSizeFile) {
                    if ($('#inpt_file_decs_ltr_menhukam_chng').length === 0) {
                        wrapbtndecsltrmenhukamchng.after('<div class="invalid-feedback pull-left" id="inpt_file_decs_ltr_menhukam_chng">' + 'Please upload file less than 25 MB.' + '</div>');

                        isImgDecsLtrMenhukamChgFile = false;
                    }
                } else {
                    $('#inpt_file_decs_ltr_menhukam_chng').remove();

                    isImgDecsLtrMenhukamChgFile = true;
                }
            } else {
                $('#inpt_file_decs_ltr_menhukam_chng').remove();

                isImgDecsLtrMenhukamChgFile = true;
            }

            // 9 Signer
            if (typeof imgSignerFile !== 'undefined') {
                if (imgSignerFile.size > maxSizeFile) {
                    if ($('#inpt_file_signer_file').length === 0) {
                        wrapbtnsignername.after('<div class="invalid-feedback pull-left" id="inpt_file_signer_file">' + 'Please upload file less than 25 MB.' + '</div>');

                        isImgSignerFile = false;
                    }
                } else {
                    $('#inpt_file_signer_file').remove();

                    isImgSignerFile = true;
                }
            } else {
                $('#inpt_file_signer_file').remove();

                isImgSignerFile = true;
            }

            // 10 Other Document
            if (typeof imgOtherFile !== 'undefined') {
                if (imgOtherFile.size > maxSizeFile) {
                    if ($('#inpt_file_other_doc_file').length === 0) {
                        wrapBtnOtherDoc.after('<div class="invalid-feedback pull-left" id="inpt_file_other_doc_file">' + 'Please upload file less than 25 MB.' + '</div>');

                        isImgOtherFile = false;
                    }
                } else {
                    $('#inpt_file_other_doc_file').remove();

                    isImgOtherFile = true;
                }
            } else {
                $('#inpt_file_other_doc_file').remove();

                isImgOtherFile = true;
            }

            if (isImgNibFile && isImgBussPermitFile && isImgSiupFile
                && isImgTdpFile && isImgMemoOfAssoFile && isImgDecsLtrMenhukamFile && isImgMemoOfAssoChgFile && isImgDecsLtrMenhukamChgFile
                && isImgSignerFile && isImgOtherFile) {
                isFormValid = true;
            }

            return isFormValid;
        };

        var imgNibOnChange = function () {
            imgNib.change(function (e) {
                var fileName = imgNib[0].files[0].name;
                var fileType = imgNib[0].files[0].type;

                if (fileType !== '') {
                    var [filenames, extension] = fileName.split('.').reduce((acc, val, i, arr) => (i == arr.length - 1) ? [acc[0].substring(1), val] : [[acc[0], val].join('.')], []);

                    var ext = extension;
                    var getExt = ext.toLowerCase();

                    if (getExt === jpg || getExt === png || getExt === jpeg || getExt === pdf) {
                        if (imgNib[0].files[0].size < maxSizeFile) {
                            $('#inpt_file_nib').remove();
                            btnLglSubmit.prop('disabled', false);
                            $('#inpt_file_Type_nib').remove();
                        } else {
                            wrapBtnNibAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_nib">' + 'Please upload file less than 25 MB.' + '</div>');
                        }
                    } else {
                        $('#inpt_file_nib').remove();
                        $('#inpt_file_Type_nib').remove();

                        wrapBtnNibAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_nib">' + 'File Not Valid' + '</div>');
                        btnLglSubmit.prop('disabled', true);
                    }

                } else {
                    $('#inpt_file_nib').remove();
                    $('#inpt_file_Type_nib').remove();

                    wrapBtnNibAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_nib">' + 'File Not Valid' + '</div>');
                    btnLglSubmit.prop('disabled', true);
                }
            });
        };

        var imgBussPermitOnChange = function () {
            imgBussPermit.change(function (e) {
                var fileName = imgBussPermit[0].files[0].name;
                var fileType = imgBussPermit[0].files[0].type;

                if (fileType !== '') {
                    var [filenames, extension] = fileName.split('.').reduce((acc, val, i, arr) => (i == arr.length - 1) ? [acc[0].substring(1), val] : [[acc[0], val].join('.')], []);

                    var ext = extension;
                    var getExt = ext.toLowerCase();

                    if (getExt === jpg || getExt === png || getExt === jpeg || getExt === pdf) {
                        if (imgBussPermit[0].files[0].size < maxSizeFile) {
                            $('#inpt_file_buss_permit').remove();
                            btnLglSubmit.prop('disabled', false);
                            $('#inpt_file_buss_Type_permit').remove();
                        } else {
                            wrapBtnBussPermitAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_buss_permit">' + 'Please upload file less than 25 MB.' + '</div>');
                        }
                    }
                    else {
                        $('#inpt_file_buss_permit').remove();
                        $('#inpt_file_buss_Type_permit').remove();

                        wrapBtnBussPermitAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_buss_Type_permit">' + 'File Not Valid' + '</div>');
                        btnLglSubmit.prop('disabled', true);
                    }

                } else {
                    $('#inpt_file_nib').remove();
                    $('#inpt_file_Type_nib').remove();

                    wrapBtnBussPermitAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_buss_Type_permit">' + 'File Not Valid' + '</div>');
                    btnLglSubmit.prop('disabled', true);
                }
            });
        };

        var imgSiupOnChange=function() {
            imgSiup.change(function (e) {
                var fileName = imgSiup[0].files[0].name;
                var fileType = imgSiup[0].files[0].type;

                if (fileType !== '') {
                    var [filenames, extension] = fileName.split('.').reduce((acc, val, i, arr) => (i == arr.length - 1) ? [acc[0].substring(1), val] : [[acc[0], val].join('.')], []);

                    var ext = extension;
                    var getExt = ext.toLowerCase();

                    if (getExt === jpg || getExt === png || getExt === jpeg || getExt === pdf) {
                        if (imgSiup[0].files[0].size < maxSizeFile) {
                            $('#inpt_file_siup').remove();
                            btnLglSubmit.prop('disabled', false);
                            $('#inpt_file_Type_siup').remove();
                        } else {
                            $('#inpt_file_siup').remove();
                            $('#inpt_file_Type_siup').remove();
                            wrapBtnSiupAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_siup">' + 'Please upload file less than 25 MB.' + '</div>');
                        }
                    }
                    else {
                        $('#inpt_file_siup').remove();
                        $('#inpt_file_Type_siup').remove();

                        wrapBtnSiupAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_siup">' + 'File Not Valid' + '</div>');
                        btnLglSubmit.prop('disabled', true);
                    }

                } else {
                    $('#inpt_file_siup').remove();
                    $('#inpt_file_Type_siup').remove();

                    wrapBtnSiupAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_siup">' + 'File Not Valid' + '</div>');
                    btnLglSubmit.prop('disabled', true);
                }
            });
        };

        var imgOtherDocOnChange = function () {
            imgOtherDoc.change(function (e) {
               
                var fileName = imgOtherDoc[0].files[0].name;
                var fileType = imgOtherDoc[0].files[0].type;

                if (fileType !== '') {
                    var [filenames, extension] = fileName.split('.').reduce((acc, val, i, arr) => (i == arr.length - 1) ? [acc[0].substring(1), val] : [[acc[0], val].join('.')], []);

                    var ext = extension;
                    var getExt = ext.toLowerCase();

                    if (getExt === jpg || getExt === png || getExt === jpeg || getExt === pdf) {
                        if (imgOtherDoc[0].files[0].size < maxSizeFile) {
                            $('#inpt_file_other_doc_file').remove();
                            btnLglSubmit.prop('disabled', false);
                            $('#inpt_file_Type_other_doc_file').remove();
                        } else {
                            wrapBtnOtherDoc.after('<div class="invalid-feedback pull-left" id="inpt_file_other_doc_file">' + 'Please upload file less than 25 MB.' + '</div>');
                        }
                       
                    }
                    else {
                        $('#inpt_file_other_doc_file').remove();
                        $('#inpt_file_Type_other_doc_file').remove();

                        wrapBtnOtherDoc.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_other_doc_file">' + 'File Not Valid' + '</div>');
                        btnLglSubmit.prop('disabled', true);
                    }

                } else {
                    $('#inpt_file_other_doc_file').remove();
                    $('#inpt_file_Type_other_doc_file').remove();

                    wrapBtnOtherDoc.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_other_doc_file">' + 'File Not Valid' + '</div>');
                    btnLglSubmit.prop('disabled', true);
                }
            });
        };

        var imgTdpOnChange = function () {
            imgTdp.change(function (e) {                
                var fileName = imgTdp[0].files[0].name;
                var fileType = imgTdp[0].files[0].type;

                if (fileType !== '') {
                    var [filenames, extension] = fileName.split('.').reduce((acc, val, i, arr) => (i == arr.length - 1) ? [acc[0].substring(1), val] : [[acc[0], val].join('.')], []);

                    var ext = extension;
                    var getExt = ext.toLowerCase();

                    if (getExt === jpg || getExt === png || getExt === jpeg || getExt === pdf) {
                        if (imgTdp[0].files[0].size < maxSizeFile) {
                            $('#inpt_file_tdp').remove();
                            btnLglSubmit.prop('disabled', false);
                            $('#inpt_file_Type_tdp').remove();
                        } else {
                            $('#inpt_file_tdp').remove();
                            $('#inpt_file_Type_tdp').remove();
                            wrapBtnTdpAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_tdp">' + 'Please upload file less than 25 MB.' + '</div>');
                        }
                    }
                    else {
                        $('#inpt_file_tdp').remove();
                        $('#inpt_file_Type_tdp').remove();

                        wrapBtnTdpAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_tdp">' + 'File Not Valid' + '</div>');
                        btnLglSubmit.prop('disabled', true);
                    }
                } else {
                    $('#inpt_file_tdp').remove();
                    $('#inpt_file_Type_tdp').remove();

                    wrapBtnTdpAttach.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_tdp">' + 'File Not Valid' + '</div>');
                    btnLglSubmit.prop('disabled', true);
                }
            });
        };

        var imgMemoOfAssoOnChange = function () {
            imgMemoOfAsso.change(function (e) {
                var fileName = imgMemoOfAsso[0].files[0].name;
                var fileType = imgMemoOfAsso[0].files[0].type;

                if (fileType !== '') {
                    var [filenames, extension] = fileName.split('.').reduce((acc, val, i, arr) => (i === arr.length - 1) ? [acc[0].substring(1), val] : [[acc[0], val].join('.')], []);

                    var ext = extension;
                    var getExt = ext.toLowerCase();

                    if (getExt === jpg || getExt === png || getExt === jpeg || getExt === pdf) {
                        if (imgMemoOfAsso[0].files[0].size < maxSizeFile) {
                            $('#inpt_file_memo_asso').remove();
                            btnLglSubmit.prop('disabled', false);
                            $('#inpt_file_Type_memo_asso').remove();
                        } else {
                            $('#inpt_file_memo_asso').remove();
                            $('#inpt_file_Type_memo_asso').remove();
                            wrapBtnMemoAsso.after('<div class="invalid-feedback pull-left" id="inpt_file_memo_asso">' + 'Please upload file less than 25 MB.' + '</div>');
                        }
                    }
                    else {
                        $('#inpt_file_memo_asso').remove();
                        $('#inpt_file_Type_memo_asso').remove();

                        wrapBtnMemoAsso.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_memo_asso">' + 'File Not Valid' + '</div>');
                        btnLglSubmit.prop('disabled', true);
                    }
                } else {
                    $('#inpt_file_memo_asso').remove();
                    $('#inpt_file_Type_memo_asso').remove();

                    wrapBtnMemoAsso.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_memo_asso">' + 'File Not Valid' + '</div>');
                    btnLglSubmit.prop('disabled', true);
                }
            });
        };

        var imgDecsLtrMenhukamOnChange = function () {
            imgDecsLtrMenhukam.change(function (e) {
                var fileName = imgDecsLtrMenhukam[0].files[0].name;
                var fileType = imgDecsLtrMenhukam[0].files[0].type;

                if (fileType !== '') {
                    var [filenames, extension] = fileName.split('.').reduce((acc, val, i, arr) => (i === arr.length - 1) ? [acc[0].substring(1), val] : [[acc[0], val].join('.')], []);

                    var ext = extension;
                    var getExt = ext.toLowerCase();

                    if (getExt === jpg || getExt === png || getExt === jpeg || getExt === pdf) {
                        if (imgDecsLtrMenhukam[0].files[0].size < maxSizeFile) {
                            $('#inpt_file_dcs_ltr').remove();
                            btnLglSubmit.prop('disabled', false);
                            $('#inpt_file_Type_dcs_ltr').remove();
                        } else {
                            wrapBtnDcsLtr.after('<div class="invalid-feedback pull-left" id="inpt_file_dcs_ltr">' + 'Please upload file less than 25 MB.' + '</div>');
                        }
                    }
                    else {
                        $('#inpt_file_dcs_ltr').remove();
                        $('#inpt_file_Type_dcs_ltr').remove();

                        wrapBtnDcsLtr.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_dcs_ltr">' + 'File Not Valid' + '</div>');
                        btnLglSubmit.prop('disabled', true);
                    }
                } else {
                    $('#inpt_file_dcs_ltr').remove();
                    $('#inpt_file_Type_dcs_ltr').remove();

                    wrapBtnDcsLtr.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_dcs_ltr">' + 'File Not Valid' + '</div>');
                    btnLglSubmit.prop('disabled', true);
                }
            });
        };

        var imgMemoOfAssoChngOnChange = function () {
            imgMemoOfAssoChng.change(function (e) {
                var fileName = imgMemoOfAssoChng[0].files[0].name;
                var fileType = imgMemoOfAssoChng[0].files[0].type;

                if (fileType !== '') {
                    var [filenames, extension] = fileName.split('.').reduce((acc, val, i, arr) => (i === arr.length - 1) ? [acc[0].substring(1), val] : [[acc[0], val].join('.')], []);

                    var ext = extension;
                    var getExt = ext.toLowerCase();

                    if (getExt === jpg || getExt === png || getExt === jpeg || getExt === pdf) {
                        if (imgMemoOfAssoChng[0].files[0].size < maxSizeFile) {
                            $('#inpt_file_memo_of_asso_chng').remove();
                            btnLglSubmit.prop('disabled', false);
                            $('#inpt_file_Type_memo_of_asso_chng').remove();
                        } else {
                            wrapbtnmemoofassochng.after('<div class="invalid-feedback pull-left" id="inpt_file_memo_of_asso_chng">' + 'Please upload file less than 25 MB.' + '</div>');
                        }
                    }
                    else {
                        $('#inpt_file_memo_of_asso_chng').remove();
                        $('#inpt_file_Type_memo_of_asso_chng').remove();

                        wrapbtnmemoofassochng.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_memo_of_asso_chng">' + 'File Not Valid' + '</div>');
                        btnLglSubmit.prop('disabled', true);
                    }

                } else {
                    $('#inpt_file_memo_of_asso_chng').remove();
                    $('#inpt_file_Type_memo_of_asso_chng').remove();

                    wrapbtnmemoofassochng.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_memo_of_asso_chng">' + 'File Not Valid' + '</div>');
                    btnLglSubmit.prop('disabled', true);
                }
            });
        };

        var imgDecsLtrMenhukamChngOnChange = function () {
            imgDecsLtrMenhukamChng.change(function (e) {
                var fileName = imgDecsLtrMenhukamChng[0].files[0].name;
                var fileType = imgDecsLtrMenhukamChng[0].files[0].type;

                if (fileType !== '') {
                    var [filenames, extension] = fileName.split('.').reduce((acc, val, i, arr) => (i === arr.length - 1) ? [acc[0].substring(1), val] : [[acc[0], val].join('.')], []);

                    var ext = extension;
                    var getExt = ext.toLowerCase();

                    if (getExt === jpg || getExt === png || getExt === jpeg || getExt === pdf) {
                        if (imgDecsLtrMenhukamChng[0].files[0].size < maxSizeFile) {
                            $('#inpt_file_decs_ltr_menhukam_chng').remove();
                            btnLglSubmit.prop('disabled', false);
                            $('#inpt_file_Type_decs_ltr_menhukam_chng').remove();
                        } else {
                            wrapbtndecsltrmenhukamchng.after('<div class="invalid-feedback pull-left" id="inpt_file_decs_ltr_menhukam_chng">' + 'Please upload file less than 25 MB.' + '</div>');
                        }
                    }
                    else {
                        $('#inpt_file_decs_ltr_menhukam_chng').remove();
                        $('#inpt_file_Type_decs_ltr_menhukam_chng').remove();

                        wrapbtndecsltrmenhukamchng.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_decs_ltr_menhukam_chng">' + 'File Not Valid' + '</div>');
                        btnLglSubmit.prop('disabled', true);
                    }
                } else {
                    $('#inpt_file_decs_ltr_menhukam_chng').remove();
                    $('#inpt_file_Type_decs_ltr_menhukam_chng').remove();

                    wrapbtndecsltrmenhukamchng.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_decs_ltr_menhukam_chng">' + 'File Not Valid' + '</div>');
                    btnLglSubmit.prop('disabled', true);
                }
            });
        };

        var imgSigningOnChange = function () {
            imgSigning.change(function (e) {
                var fileName = imgSigning[0].files[0].name;
                var fileType = imgSigning[0].files[0].type;

                if (fileType !== '') {
                    var [filenames, extension] = fileName.split('.').reduce((acc, val, i, arr) => (i === arr.length - 1) ? [acc[0].substring(1), val] : [[acc[0], val].join('.')], []);

                    var ext = extension;
                    var getExt = ext.toLowerCase();

                    if (getExt === jpg || getExt === png || getExt === jpeg || getExt === pdf) {
                        if (imgSigning[0].files[0].size < maxSizeFile) {
                            $('#inpt_file_signer_file').remove();
                            btnLglSubmit.prop('disabled', false);
                            $('#inpt_file_Type_signer_file').remove();
                        } else {
                            $('#inpt_file_Type_signer_file').remove();
                            $('#inpt_file_signer_file').remove();
                            wrapbtnsignername.after('<div class="invalid-feedback pull-left" id="inpt_file_signer_file">' + 'Please upload file less than 25 MB.' + '</div>');
                        }
                    }
                    else {
                        $('#inpt_file_signer_file').remove();
                        $('#inpt_file_Type_signer_file').remove();

                        wrapbtnsignername.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_signer_file">' + 'File Not Valid' + '</div>');
                        btnLglSubmit.prop('disabled', true);
                    }

                } else {
                    $('#inpt_file_signer_file').remove();
                    $('#inpt_file_Type_signer_file').remove();

                    wrapbtnsignername.after('<div class="invalid-feedback pull-left" id="inpt_file_Type_signer_file">' + 'File Not Valid' + '</div>');
                    btnLglSubmit.prop('disabled', true);
                }
            });
        };

        var constructOptionSelect = function () {
            var htmlOption = "";
            var obj = JSON.parse(jobTitleOptions.val());

            htmlOption += '<option value="" disabled selected>Select</option>';
            for (var i = 0; i < obj.length; i++) {
                htmlOption += '<option value=' + obj[i].Id + '>' + obj[i].Title + '</option>';
            }

            return htmlOption;
        };

        var initialCreateOrganizationStructure = function () {
            var showRmvBtn = 0;

            if (managementStructure.val() != '') {
                var structure = JSON.parse(managementStructure.val());

                if (structure.data.length != 0) {
                    for (var i = 0; i < structure.data.length; i++) {
                        var uniqueKey = new Date().getUTCMilliseconds();

                        var inputDirectorShip = '<div class="row ' + uniqueKey + '">'
                            + '<div class="col-12 col-md-4">'
                            + '<div class="md-form md-outline">'
                            + '<input type="text" id="input_dir_' + uniqueKey + '" class="form-control" value="' + structure.data[i].name + '" required>'
                            + '<label for="lbl_input_dir_' + uniqueKey + '" class="active" id="lbl_inpt_dirship">Directorship <span class="text-danger">*</span></label>'
                            + '</div>'
                            + '</div>'
                            + '<div class="col-12 col-md-2 select-outline">'
                            + '<select class="mdb-select md-form md-outline" id="slct_job_title_' + uniqueKey + '">'
                            + constructOptionSelect()
                            + '</select>'
                            + '<label class="title-form" id="lbl_slct_jobTitle">Position <span class="text-danger">*</span></label>'
                            + '</div>'
                            + '<div class="col-12 col-md-4 md-form">'
                            + '<div class="button-list">'
                            + '<button type="button" class="btn btn-icon waves-effect waves-light btn-custom btn_dynmc_add" id="btn_dynmc_add_' + uniqueKey + '"> <i class="fa fa-plus-circle"></i></button>';

                        if (showRmvBtn > 0) {
                            inputDirectorShip += '<button type="button" class="btn btn-icon waves-effect waves-light btn-outline-custom btn_dynmc_rmv" id="' + uniqueKey + '"> <i class="fa fa-minus"></i></button>'
                        }

                        inputDirectorShip += '</div></div></div>';

                        wrapDynamicForm.append(inputDirectorShip);
                        $('#slct_job_title_' + uniqueKey).val(structure.data[i].jobTitleId);
                        $('#slct_job_title_' + uniqueKey).materialSelect();

                        showRmvBtn++;
                    }
                } else {
                    var uniqueKey = new Date().getUTCMilliseconds();

                    var inputDirectorShip = '<div class="row ' + uniqueKey + '">'
                        + '<div class="col-12 col-md-4">'
                        + '<div class="md-form md-outline">'
                        + '<input type="text" id="input_dir_' + uniqueKey + '" class="form-control" required>'
                        + '<label for="lbl_input_dir_' + uniqueKey + '" id="lbl_inpt_dirship">Directorship <span class="text-danger">*</span></label>'
                        + '</div>'
                        + '</div>'
                        + '<div class="col-12 col-md-2 select-outline">'
                        + '<select class="mdb-select md-form md-outline" id="slct_job_title_' + uniqueKey + '">'
                        + constructOptionSelect()
                        + '</select>'
                        + '<label class="title-form" id="lbl_slct_jobTitle">Position <span class="text-danger">*</span></label>'
                        + '</div>'
                        + '<div class="col-12 col-md-4 md-form">'
                        + '<div class="button-list">'
                        + '<button type="button" class="btn btn-icon waves-effect waves-light btn-custom btn_dynmc_add" id="btn_dynmc_add_' + uniqueKey + '"> <i class="fa fa-plus-circle"></i></button>'
                        + '</div>'
                        + '</div>'
                        + '</div>';

                    wrapDynamicForm.append(inputDirectorShip);
                    $('#slct_job_title_' + uniqueKey).materialSelect();
                }
            } else {
                var uniqueKey = new Date().getUTCMilliseconds();

                var inputDirectorShip = '<div class="row ' + uniqueKey + '">'
                    + '<div class="col-12 col-md-4">'
                    + '<div class="md-form md-outline">'
                    + '<input type="text" id="input_dir_' + uniqueKey + '" class="form-control" required>'
                    + '<label for="lbl_input_dir_' + uniqueKey + '" id="lbl_inpt_dirship">Directorship <span class="text-danger">*</span></label>'
                    + '</div>'
                    + '</div>'
                    + '<div class="col-12 col-md-2 select-outline">'
                    + '<select class="mdb-select md-form md-outline" id="slct_job_title_' + uniqueKey + '">'
                    + constructOptionSelect()
                    + '</select>'
                    + '<label class="title-form" id="lbl_slct_jobTitle">Position <span class="text-danger">*</span></label>'
                    + '</div>'
                    + '<div class="col-12 col-md-4 md-form">'
                    + '<div class="button-list">'
                    + '<button type="button" class="btn btn-icon waves-effect waves-light btn-custom btn_dynmc_add" id="btn_dynmc_add_' + uniqueKey + '"> <i class="fa fa-plus-circle"></i></button>'
                    + '</div>'
                    + '</div>'
                    + '</div>';

                wrapDynamicForm.append(inputDirectorShip);
                $('#slct_job_title_' + uniqueKey).materialSelect();
            }
        };

        var dynamicCreateOrganizationStructure = function () {
            $(document).on('click', '.btn_dynmc_add', function () {
                var uniqueKey = new Date().getUTCMilliseconds();

                var inputDirectorShip = '<div class="row ' + uniqueKey + '">'
                    + '<div class="col-12 col-md-4">'
                    + '<div class="md-form md-outline">'
                    + '<input type="text" id="input_dir_' + uniqueKey + '" class="form-control" required>'
                    + '<label for="lbl_input_dir_' + uniqueKey + '" id="lbl_inpt_dirship">Directorship <span class="text-danger">*</span></label>'
                    + '</div>'
                    + '</div>'
                    + '<div class="col-12 col-md-2 select-outline">'
                    + '<select class="mdb-select md-form md-outline" id="slct_job_title_' + uniqueKey + '">'
                    + constructOptionSelect()
                    + '</select>'
                    + '<label class="title-form" id="lbl_slct_jobTitle">Position <span class="text-danger">*</span></label>'
                    + '</div>'
                    + '<div class="col-12 col-md-4 md-form">'
                    + '<div class="button-list">'
                    + '<button type="button" class="btn btn-icon waves-effect waves-light btn-custom btn_dynmc_add" id="btn_dynmc_add_' + uniqueKey + '"> <i class="fa fa-plus-circle"></i></button>'
                    + '<button type="button" class="btn btn-icon waves-effect waves-light btn-outline-custom btn_dynmc_rmv" id="' + uniqueKey + '"> <i class="fa fa-minus"></i></button>'
                    + '</div>'
                    + '</div>'
                    + '</div>';

                wrapDynamicForm.append(inputDirectorShip);
                $('#slct_job_title_' + uniqueKey).materialSelect();
            });

            $(document).on('click', '.btn_dynmc_rmv', function () {
                $('.' + this.id).remove();
            });
        };

        var setAttachmentModelValue = function (model) {
            nibAttachment.val(model.nibattachment);
            bussPermitAttachment.val(model.businessPermitAttachment);
            siupAttachment.val(model.siupattachment);
            tdpExpAttachment.val(model.tdpattachment);
            memoOfAsoAttachment.val(model.memorandumOfAssociationAttachment);
            decsLtrMenhukamAttachment.val(model.decissionLetterMenkumhamAttachment);
            memoOfAssoChngAttachment.val(model.memorandumOfAssociationChangingAttachment);
            decsLtrMenhukamChngAttachment.val(model.decissionLetterMenkumhamChangingAttachment);
            signinAttachment.val(model.signingAttachment);
            otherAttachment.val(model.otherAttachment);
        };

        var signerOnChange = function () {
            signingId.change(function () {
                if (this.value == 2) {
                    wrapSignerFile.show();
                } else {
                    wrapSignerFile.hide();
                }
            });
        };

        var downLoadFileAttachment = function () {
            downloadFile.click(function () {
                var fileName = $(this).val();

                if (fileName != '') {
                    $.ajax({
                        url: WEBPORTAL.URLContext.DownloadFile + '?fileName=' + fileName,
                        method: 'GET',
                        success: function (result, status, xhr) {
                            var base64str = result.data;

                            // decode base64 string, remove space for IE compatibilitys
                            var binary = atob(base64str.replace(/\s/g, ''));
                            var len = binary.length;
                            var buffer = new ArrayBuffer(len);
                            var view = new Uint8Array(buffer);
                            for (var i = 0; i < len; i++) {
                                view[i] = binary.charCodeAt(i);
                            }

                            // create the blob object with content-type "application/pdf"               
                            var blob = new Blob([view], { type: "application/pdf" });

                            var a = document.createElement('a');
                            var url = URL.createObjectURL(blob);
                            a.href = url;
                            a.download = fileName.split("__").pop();
                            document.body.append(a);
                            a.click();
                            a.remove();
                            window.URL.revokeObjectURL(url);
                        }
                    });
                }
            });
        };

        var removeFileAttachment = function () {
            removeFile.click(function () {
                $(this).parent().siblings('.file-path-wrapper').children().val('');
                $(this).children().removeClass('fa-times').addClass('fa-spinner');
                postDeleteAttachment($(this), $(this).attr('data-type'));

                if ($(this).attr('data-type') == "nibattachment") {
                    btnLglSubmit.prop('disabled', false);
                    $('#inpt_file_nib').remove();
                    $('#inpt_file_Type_nib').remove();

                    imgNibFile = '';
                    imgNib.val('');

                }
                if ($(this).attr('data-type') == "businesspermitattachment") {
                    btnLglSubmit.prop('disabled', false);
                    $('#inpt_file_buss_permit').remove();
                    $('#inpt_file_buss_Type_permit').remove();

                    imgBussPermitFile = '';
                    imgBussPermit.val('');
                }
                if ($(this).attr('data-type') == "siupattachment") {
                    imgSiup = imgSiup.val('');
                    btnLglSubmit.prop('disabled', false);
                    $('#inpt_file_siup').remove();
                    $('#inpt_file_Type_siup').remove();

                    imgSiupFile = '';
                    imgSiup.val('');
                }
                if ($(this).attr('data-type') == "tdpattachment") {
                    imgTdp = imgTdp.val('');
                    btnLglSubmit.prop('disabled', false);
                    $('#inpt_file_tdp').remove();
                    $('#inpt_file_Type_tdp').remove();

                    imgTdpFile = '';
                    imgTdp.val('');
                }
                if ($(this).attr('data-type') == "memorandumofassociationattachment") {
                    imgMemoOfAsso = imgMemoOfAsso.val('');
                    btnLglSubmit.prop('disabled', false);
                    $('#inpt_file_memo_asso').remove();
                    $('#inpt_file_Type_memo_asso').remove();

                    imgMemoOfAssoFile = '';
                    imgMemoOfAsso.val('');
                }
                if ($(this).attr('data-type') == "decissionlettermenkumhamattachment") {
                    imgDecsLtrMenhukam = imgDecsLtrMenhukam.val('');
                    btnLglSubmit.prop('disabled', false);
                    $('#inpt_file_dcs_ltr').remove();
                    $('#inpt_file_Type_dcs_ltr').remove();

                    imgDecsLtrMenhukamFile = '';
                    imgDecsLtrMenhukam.val('');
                }
                if ($(this).attr('data-type') == "memorandumofassociationchangingattachment") {
                    imgMemoOfAssoChng = imgMemoOfAssoChng.val('');
                    btnLglSubmit.prop('disabled', false);
                    $('#inpt_file_memo_of_asso_chng').remove();
                    $('#inpt_file_Type_memo_of_asso_chng').remove();

                    imgMemoOfAssoChgFile = '';
                    imgMemoOfAssoChng.val('');

                }
                if ($(this).attr('data-type') == "decissionlettermenkumhamchangingattachment") {
                    imgDecsLtrMenhukamChng = imgDecsLtrMenhukamChng.val('');
                    btnLglSubmit.prop('disabled', false);
                    $('#inpt_file_decs_ltr_menhukam_chng').remove();
                    $('#inpt_file_Type_decs_ltr_menhukam_chng').remove();

                    imgDecsLtrMenhukamChgFile = '';
                    imgDecsLtrMenhukamChng.val('');
                }
                if ($(this).attr('data-type') == "signingattachment") {
                    imgSigning = imgSigning.val('');
                    btnLglSubmit.prop('disabled', false);
                    $('#inpt_file_signer_file').remove();
                    $('#inpt_file_Type_signer_file').remove();

                    imgSignerFile = '';
                    imgSigning.val('');
                }
                if ($(this).attr('data-type') == "otherattachment") {
                    imgOtherDoc = imgOtherDoc.val('');
                    btnLglSubmit.prop('disabled', false);
                    $('#inpt_file_other_doc_file').remove();
                    $('#inpt_file_Type_other_doc_file').remove();

                    imgOtherFile = '';
                    imgOtherDoc.val('');
                }
            });
        };

        var postDeleteAttachment = function (elementDeleted, type) {
            $.when(WEBPORTAL.Services.POSTLocal(null, URL_CONTEXT_DeleteVendorLegalAttachment + "?type=" + type, false, false)).done(function (result, status, xhr) {
                if (result.code === 200) {
                    $(elementDeleted).children().removeClass('fa-spinner').addClass('fa-times');
                } else {
                    $(elementDeleted).children().removeClass('fa-spinner').addClass('fa-times');
                }
            });
        };

        var run = function () {
            initialDom();
            imgNibOnChange();
            imgBussPermitOnChange();
            imgSiupOnChange();
            imgTdpOnChange();
            imgMemoOfAssoOnChange();
            imgDecsLtrMenhukamOnChange();
            imgMemoOfAssoChngOnChange();
            imgDecsLtrMenhukamChngOnChange();
            imgSigningOnChange();
            imgOtherDocOnChange();
            submitVendorLegal();
            initialCreateOrganizationStructure();
            dynamicCreateOrganizationStructure();
            signerOnChange();
            downLoadFileAttachment();
            removeFileAttachment();
        };

        return {
            run: run
        };
    })();

    initialVendorLegal.run();
})(jQuery);