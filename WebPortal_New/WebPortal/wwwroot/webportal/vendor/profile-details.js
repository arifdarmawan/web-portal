﻿(function ($) {
    'use strict';

    var initialVendor = (function () {
        var wrapBasicInfo, wrapBankPayment, wrapLegal, wrapTax, wrapAgreement, wrapPhoto;


        var initialDom = function () {
            wrapBasicInfo = $('#vd_wrap_basic_info'); wrapBankPayment = $('#vd_wrap_bank_payment'); 
            wrapLegal = $('#vd_wrap_legal'); wrapTax = $('#vd_wrap_tax');
            wrapAgreement = $('#vd_wrap_agreement'); wrapPhoto = $('#vd_wrap_photo');
        };

        var initialForm = function () {
            loadVendorBasicInfo();
            loadVendorBankPayment();
            loadVendorLegal();
            loadVendorTax();
            loadVendorAgreement();
            loadVendorPhoto();
        };

        var loadVendorBasicInfo = function () {
            $.when(wrapBasicInfo.settingWrapCenter()).done(wrapBasicInfo.componentAddLoading());
            $.when(WEBPORTAL.Services.GETLocal(URL_CONTEXT_BasicInfoDetail)).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(wrapBasicInfo.resetWrapCenter()).done(wrapBasicInfo.html(result));
                }, 500);
            });
        };

        var loadVendorBankPayment = function () {
            $.when(wrapBankPayment.settingWrapCenter()).done(wrapBankPayment.componentAddLoading());
            $.when(WEBPORTAL.Services.GETLocal(URL_CONTEXT_BankPaymentDetail)).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(wrapBankPayment.resetWrapCenter()).done(wrapBankPayment.html(result));
                }, 500);
            });
        };

        var loadVendorLegal = function () {
            $.when(wrapLegal.settingWrapCenter()).done(wrapLegal.componentAddLoading());
            $.when(WEBPORTAL.Services.GETLocal(URL_CONTEXT_LegalDetail)).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(wrapLegal.resetWrapCenter()).done(wrapLegal.html(result));
                }, 500);
            });
        };

        var loadVendorTax = function () {
            $.when(wrapTax.settingWrapCenter()).done(wrapTax.componentAddLoading());
            $.when(WEBPORTAL.Services.GETLocal(URL_CONTEXT_TaxDetail)).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(wrapTax.resetWrapCenter()).done(wrapTax.html(result));
                }, 500);
            });
        };

        var loadVendorAgreement = function () {
            $.when(wrapAgreement.settingWrapCenter()).done(wrapAgreement.componentAddLoading());
            $.when(WEBPORTAL.Services.GETLocal(URL_CONTEXT_Agreement)).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(wrapAgreement.resetWrapCenter()).done(wrapAgreement.html(result));
                }, 500);
            });
        };

        var loadVendorPhoto = function () {
            $.when(wrapPhoto.settingWrapCenter()).done(wrapPhoto.componentAddLoading());
            $.when(WEBPORTAL.Services.GETLocal(URL_CONTEXT_PhotoProfile)).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(wrapPhoto.resetWrapCenter()).done(wrapPhoto.html(result));
                }, 500);
            });
        };

        var run = function () {
            initialDom();
            initialForm();
            
        };

        return {
            run: run
        }
    })(jQuery);

    initialVendor.run();
})(jQuery);