﻿(function ($) {
    'use strict';

    var initialChangePassword = (function () {
        let urlContext = '';
        let lblnewPassword, newpassword, lblrenewPassword, renewpassword, btnSave;
        
        var initialDom = function () {
            lblnewPassword = $('#lbl_newPassword');
            newpassword = $('#newPassword');
            lblrenewPassword = $('#lbl_renewPassword');
            renewpassword = $('#renewPassword');
            btnSave = $('.save-btn');
        }

        var formValidation = function () {
            var isFormValid = false,
                isnewPassword = false,
                isrenewPassword = false;

            /* New Password */
            if (newpassword.val() == '') {
                $('.errChangePasswordNew').remove();
                newpassword.addClass('invalid');
                if ($('.errChangePasswordNew').length == 0) {
                    
                    lblnewPassword.after('<div class="invalid-feedback errChangePasswordNew">' + 'Please fill the required field.' + '</div>');
                    isnewPassword = false;
                }
            } else if (newpassword.val().length < 5) {
                $('.errChangePasswordNew').remove();
                newpassword.addClass('invalid');
                lblnewPassword.after('<div class="invalid-feedback errChangePasswordNew">' + 'Password minimum 5 characters length.' + '</div>');
                isnewPassword = false;
            }
            else {
                newpassword.removeClass('invalid');
                $('.errChangePasswordNew').remove();
                isnewPassword = true;
            }

            /* Retype Password */
            if (renewpassword.val() == '') {
                $('.errChangePasswordRenew').remove();
                renewpassword.addClass('invalid');
                if ($('.errChangePasswordRenew').length == 0) {
                    lblrenewPassword.after('<div class="invalid-feedback errChangePasswordRenew">' + 'Please fill the required field.' + '</div>');
                    isrenewPassword = false;
                }
            } else if (renewpassword.val() != newpassword.val()) {
                $('.errChangePasswordRenew').remove();
                renewpassword.addClass('invalid');
                lblrenewPassword.after('<div class="invalid-feedback errChangePasswordRenew">' + 'Confirm password is not same as you new password.' + '</div>');
                isrenewPassword = false;
            }
            else {
                renewpassword.removeClass('invalid');
                $('.errChangePasswordRenew').remove();
                isrenewPassword = true;
            }

            if (isnewPassword && isrenewPassword) {
                isFormValid = true;
            } else {
                isFormValid = false;
            }

            return isFormValid;
        };

        var ChangePasswordValidation = function () {
            WEBPORTAL.Utility.SubmitLoading(btnSave);
            if (formValidation()) {
                var params = {
                    newPassword: WEBPORTAL.Utility.EncryptionHandler(newpassword.val())
                };

                $.when(WEBPORTAL.Services.POSTLocal(params, WEBPORTAL.URLContext.ChangePassword)).done(function (result, status, xhr) {
                    if (result.code === 200) {
                        resetChangePasswordForm();
                        setTimeout(function () {
                            WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnSave);
                        }, 300);

                    } else {

                        setTimeout(function () {
                            WEBPORTAL.Utility.ConstructNotificationError(result.message);
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnSave);
                        }, 300);
                        
                    }
                });
            } else {
                setTimeout(function () {
                    WEBPORTAL.Utility.SubmitRemoveLoading(btnSave, 'Save');
                }, 300);
            }
        }

        var eventSubmitForm = function () {
            btnSave.click(function () {
                ChangePasswordValidation();
            });
        };

        var eventEnterSubmitForm = function () {
            $('#newPassword, #renewPassword').keyup(function (e){
                if (e.keyCode === 13) {
                    ChangePasswordValidation();
                }
            });
        };

        var inputChangeRemoveErr = function () {
            newpassword.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    lblnewPassword.next().remove();
                }
            });

            renewpassword.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    lblrenewPassword.next().remove();
                }
            });
        };

        var resetChangePasswordForm = function () {
            newpassword.val('');
            renewpassword.val('');
        }

        var run = function () {
            initialDom();
            inputChangeRemoveErr();
            eventEnterSubmitForm();
            eventSubmitForm();
        };

        return {
            run: run
        };

    })();

    initialChangePassword.run();
})(jQuery);