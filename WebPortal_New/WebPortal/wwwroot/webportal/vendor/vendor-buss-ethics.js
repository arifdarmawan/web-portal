﻿(function ($) {
    'use strict';

    var initialVendorBussEthics = (function () {
        var formVendorBussEthcs, bussinessEthics, ckBussinessEthics, lblBussEthics, btnSubmitVendorBussEthcs;

        var initialDom = function () {
            formVendorBussEthcs = $('#form_vendor_bussEthics'); bussinessEthics = $('#IntegrityPact');
            lblBussEthics = $('#lbl_buss_ethc'); btnSubmitVendorBussEthcs = $('#btn_bussEthc_submit');
            ckBussinessEthics = $('#ckIntegrityPact');
        };

        var submitVendorBussEthics = function () {
            btnSubmitVendorBussEthcs.click(function () {
                WEBPORTAL.Utility.SubmitLoading(btnSubmitVendorBussEthcs);
                if (formValidation()) {

                    setTimeout(function () {
                        WEBPORTAL.Utility.SubmitRemoveLoading(btnSubmitVendorBussEthcs);
                    }, 300);

                    $.when(WEBPORTAL.Services.POSTLocal(null, URL_CONTEXT_UpdateVendorBussEthics + "?bussinessEthics=" + bussinessEthics.val())).done(function (result, status, xhr) {
                        if (result.code === 200) {
                            setTimeout(function () {
                                WEBPORTAL.Utility.SubmitRemoveLoading(btnSubmitVendorBussEthcs);
                                WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                            }, 500);
                        } else {
                            setTimeout(function () {
                                WEBPORTAL.Utility.SubmitRemoveLoading(btnSubmitVendorBussEthcs);
                                WEBPORTAL.Utility.ConstructNotificationError(result.message);
                            }, 500);
                        }
                    });
                } else {

                    setTimeout(function () {
                        WEBPORTAL.Utility.SubmitRemoveLoading(btnSubmitVendorBussEthcs);
                    }, 300);
                }

            });
        };

        var formValidation = function () {
            var isBussinessEthics = false,
                isFormValid = false;

            if (!ckBussinessEthics.is(":checked")) {
                if ($('#bussEthcs_err').length == 0) {
                    lblBussEthics.after('<div class="invalid-feedback" id="bussEthcs_err">' + 'Please indicate that you accept the Business Ethics.' + '</div>');

                    isBussinessEthics = false;
                }
            } else {
                $('#bussEthcs_err').remove();
                bussinessEthics.val(true);
                isBussinessEthics = true;
            }

            if (isBussinessEthics) {
                isFormValid = true;
            }

            return isFormValid;
        };

        var bussinessEthicsOnLoad = function () {
            if (bussinessEthics.val() == 'True') {
                ckBussinessEthics.prop('checked', true);
            }
        };

        var run = function () {
            initialDom();
            bussinessEthicsOnLoad();
            submitVendorBussEthics();
        };

        return {
            run: run
        };
    })();

    initialVendorBussEthics.run();
})(jQuery);