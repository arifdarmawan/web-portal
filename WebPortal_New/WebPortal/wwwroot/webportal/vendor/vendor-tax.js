﻿(function ($) {
    'use strict';

    var initialVendorTax = (function () {
        var typeNpwp, lblTypeNpwp, lblTaxPpnNpwp, npwp, lblNpwp,
            typePkp, lblTypePkp, lblTaxPpnPkp, pkp, lblPkp,
            typeBkp, lblTypeBkp, lblTaxPpnBkp,
            bkpCategoryId, lblBkpCategoryId, lblTaxPpnBkpFree,
            btnTaxSubmit, formVendorTax,
            scopeNpwp, scopePkp, scopeBkp, ppn,
            tempTypeNpwp;

        var initialDom = function () {
            typeNpwp = $('#TypeNpwp'); lblTypeNpwp = $('#lbl_slct_tax_npwp'); lblTaxPpnNpwp = $('#lbl_tax_ppn_npwp'); npwp = $('#Npwp'); lblNpwp = $('#lbl_inpt_tax_npwp');
            typePkp = $('#TypePkp'); lblTypePkp = $('#lbl_slct_tax_pkp'); lblTaxPpnPkp = $('#lbl_tax_ppn_pkp'); pkp = $('#Pkp'); lblPkp = $('#lbl_inpt_tax_pkp');
            typeBkp = $('#TypeBkp'); lblTypeBkp = $('#lbl_slct_tax_bkp'); lblTaxPpnBkp = $('#lbl_tax_ppn_bkb');
            bkpCategoryId = $('#MasterDataBkpCategoryId'); lblBkpCategoryId = $('#lbl_slct_tax_bkp_free'); lblTaxPpnBkpFree = $('#lbl_tax_ppn_bkp_free');
            btnTaxSubmit = $('#btn_tax_submit'); formVendorTax = $('#form_vendor_tax');
            scopeNpwp = $('#scope_npwp'); scopePkp = $('#scope_pkp'); scopeBkp = $('#scope_bkp'); ppn = 10;

            npwp.mask('00.000.000.0-000.000');

            if (typeNpwp.val() != '') {
                WEBPORTAL.Utility.ConstructUpdateButton(btnTaxSubmit);
                tempTypeNpwp = typeNpwp.val();
            }               
        };

        var submitVendorTax = function () {
            btnTaxSubmit.click(function () {
                WEBPORTAL.Utility.SubmitLoading(btnTaxSubmit);
                Array.prototype.filter.call(formVendorTax, function (form) {
                    if (formValidation()) {
                        let model = {
                            TypeNpwp: typeNpwp.val(),
                            Npwp: npwp.val(),
                            TypePkp: typePkp.val(),
                            Pkp: pkp.val(),
                            Ppn: ppn,
                            TypeBkp: typeBkp.val(),
                            MasterDataBkpCategoryId: bkpCategoryId.val()
                        };

                        $.when(WEBPORTAL.Services.POSTLocal(model, URL_CONTEXT_InsertVendorTax)).done(function (result, status, xhr) {
                            if (result.code === 200) {
                                setTimeout(function () {
                                    WEBPORTAL.Utility.SubmitRemoveLoading(btnTaxSubmit);
                                    WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                                    WEBPORTAL.Utility.ConstructUpdateButton(btnTaxSubmit);
                                }, 500);
                            } else {
                                setTimeout(function () {
                                    WEBPORTAL.Utility.SubmitRemoveLoading(btnTaxSubmit);
                                    WEBPORTAL.Utility.ConstructNotificationError(result.message);
                                }, 500);
                            }
                        });
                    } else {
                        setTimeout(function () {
                            if (typeof tempTypeNpwp !== 'undefined')
                                WEBPORTAL.Utility.ConstructUpdateButton(btnTaxSubmit);
                            else
                                WEBPORTAL.Utility.SubmitRemoveLoading(btnTaxSubmit);
                        }, 300);
                    }

                    form.classList.add('was-validated');
                });
            });
        };

        var formValidation = function () {
            var isTypeNpwp = false,
                isNpwp = false,
                isTypePkp = false,
                isPkp = false,
                isTypeBkp = false,
                isBkpFreePpn = false,
                isFormValid = false;

            if (typeNpwp.val() == '') {
                if ($('#tax_err_slct_npwp').length == 0) {
                    lblTypeNpwp.after('<div class="invalid-feedback" id="tax_err_slct_npwp">' + 'Please select one.' + '</div>');

                    isTypeNpwp = false;
                }
            } else {
                $('#tax_err_slct_npwp').remove();

                isTypeNpwp = true;
            }

            /* Scope NPWP */
            if (!scopeNpwp.is(':hidden')) {
                if (npwp.val() == '') {
                    if ($('#tax_err_inpt_npwp').length == 0) {
                        lblNpwp.after('<div class="invalid-feedback" id="tax_err_inpt_npwp">' + 'Please fill the required field.' + '</div>');

                        isNpwp = false;
                    }
                } else {
                    $('#tax_err_inpt_npwp').remove();

                    isNpwp = true;

                }

                if (typePkp.val() == '') {
                    if ($('#tax_err_slct_pkp').length == 0) {
                        lblTypePkp.after('<div class="invalid-feedback" id="tax_err_slct_pkp">' + 'Please select one.' + '</div>');

                        isTypePkp = false;
                    }
                } else {
                    $('#tax_err_slct_pkp').remove();

                    isTypePkp = true;
                }

                /* Scope PKP */
                if (!scopePkp.is(':hidden')) {
                    if (pkp.val() == '') {
                        if ($('#tax_err_inpt_pkp').length == 0) {
                            lblPkp.after('<div class="invalid-feedback" id="tax_err_inpt_pkp">' + 'Please fill the required field.' + '</div>');

                            isPkp = false;
                        }
                    } else {
                        $('#tax_err_inpt_pkp').remove();

                        isPkp = true;
                    }

                    if (typeBkp.val() == '') {
                        if ($('#tax_err_slct_bkp').length == 0) {
                            lblTypeBkp.after('<div class="invalid-feedback" id="tax_err_slct_bkp">' + 'Please select one.' + '</div>');

                            isTypeBkp = false;
                        }
                    } else {
                        $('#tax_err_slct_bkp').remove();

                        isTypeBkp = true;
                    }

                    /* Scope BKP Free PPN */
                    if (!scopeBkp.is(':hidden')) {
                        if (bkpCategoryId.val() == '') {
                            if ($('#tax_err_slct_bkp_free_ppn').length == 0) {
                                lblBkpCategoryId.after('<div class="invalid-feedback" id="tax_err_slct_bkp_free_ppn">' + 'Please select one.' + '</div>');

                                isBkpFreePpn = false;
                            }
                        } else {
                            $('#tax_err_slct_bkp_free_ppn').remove();

                            isBkpFreePpn = true;
                        }
                    } else {
                        isBkpFreePpn = true;
                    }
                } else {
                    isPkp = true;
                    isTypeBkp = true;
                    isBkpFreePpn = true;
                }

            } else {
                isNpwp = true;
                isTypePkp = true;
                isPkp = true;
                isTypeBkp = true;
                isBkpFreePpn = true;
            }

            if (isTypeNpwp && isNpwp && isTypePkp && isPkp && isTypeBkp && isBkpFreePpn) {
                isFormValid = true;
            }

            return isFormValid;
        };

        var selectNpwpOnChange = function () {
            typeNpwp.change(function () {
                if (this.value == 'NON_NPWP') {
                    scopeNpwp.attr('hidden', true);

                    if (scopeNpwp.is(':hidden')) {
                        // remove acquiring required
                        npwp.removeAttr('required');
                        typePkp.removeAttr('required');
                        pkp.removeAttr('required');
                        typeBkp.removeAttr('required');
                        bkpCategoryId.removeAttr('required');

                        // reset value
                        npwp.val('');
                        typePkp.val('');
                        pkp.val('');
                        ppn = 0;
                        typeBkp.val('');
                        bkpCategoryId.val('');

                        lblTaxPpnNpwp.removeAttr('hidden');
                    }
                } else if (this.value == 'NPWP') {
                    scopeNpwp.removeAttr('hidden');
                    lblTaxPpnNpwp.attr('hidden', true);

                    // attach acquiring required
                    npwp.attr('required', true);
                    typePkp.attr('required', true);
                    pkp.attr('required', true);
                    typeBkp.attr('required', true);
                    bkpCategoryId.attr('required', true);

                    // reset value
                    npwp.val('');
                    typePkp.val('');
                    pkp.val('');
                    //ppn = 0;
                    typeBkp.val('');
                    bkpCategoryId.val('');

                    if (npwp.val() == '')
                        lblNpwp.removeClass('active');
                }
            });

            typePkp.change(function () {
                if (this.value == 'NON_PKP') {
                    scopePkp.attr('hidden', true);
                    ppn = 0;
                    if (scopePkp.is(':hidden')) {
                        // remove acquiring required
                        pkp.removeAttr('required');
                        typeBkp.removeAttr('required');
                        bkpCategoryId.removeAttr('required');

                        // reset value
                        pkp.val('');
                        typeBkp.val('');
                        bkpCategoryId.val('');

                        lblTaxPpnPkp.removeAttr('hidden');
                    }

                } else if (this.value == 'PKP') {
                    scopePkp.removeAttr('hidden');

                    // attach acquiring required
                    pkp.attr('required', true);
                    typeBkp.attr('required', true);
                    bkpCategoryId.attr('required', true);
                    ppn = 10;
                    if (pkp.val() == '')
                        lblPkp.removeClass('active');

                    lblTaxPpnPkp.attr('hidden', true);
                }
            });

            typeBkp.change(function () {
                if (this.value == 'BKP') {
                    scopeBkp.attr('hidden', true);

                    if (scopeBkp.is(':hidden')) {
                        // remove acquiring required
                        bkpCategoryId.removeAttr('required');

                        // reset value
                        bkpCategoryId.val('');

                        lblTaxPpnBkp.removeAttr('hidden');
                    }
                    ppn = 10;
                } else if (this.value == 'BKP_FREE_PPN') {
                    scopeBkp.removeAttr('hidden');
                    ppn = 0;
                    // attach acquiring required
                    bkpCategoryId.attr('required', true);

                    lblTaxPpnBkp.attr('hidden', true);
                }
            });
        };

        var vendorTaxLoadFormData = function () {
            if (typeNpwp.val() == 'NON_NPWP') {
                // remove acquiring required
                npwp.removeAttr('required');
                pkp.removeAttr('required');
                typeBkp.removeAttr('required');
                bkpCategoryId.removeAttr('required');

                lblTaxPpnNpwp.removeAttr('hidden');
            } else if (typeNpwp.val() == 'NPWP') {
                scopeNpwp.removeAttr('hidden');

                if (typePkp.val() == 'NON_PKP') {
                    // remove acquiring required 
                    pkp.removeAttr('required');
                    typeBkp.removeAttr('required');
                    bkpCategoryId.removeAttr('required');

                    lblTaxPpnPkp.removeAttr('hidden');
                } else {
                    scopePkp.removeAttr('hidden');

                    if (typeBkp.val() == 'BKP') {
                        // remove acquiring required
                        bkpCategoryId.removeAttr('required');

                        lblTaxPpnBkp.removeAttr('hidden');
                    } else {
                        scopeBkp.removeAttr('hidden');
                    }
                }
            }
        };

        var run = function () {
            initialDom();
            selectNpwpOnChange();
            vendorTaxLoadFormData();
            submitVendorTax();
        };

        return {
            run: run
        };
    })();

    initialVendorTax.run();
})(jQuery);