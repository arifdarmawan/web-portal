﻿(function ($) {
    'use strict';

    var initalLogin = (function () {
        var loginForm, userId, lblUserId, password, lblPassword, btnSubmit, _KEY_LP, isRememberMe;

        var registerDom = function () {
            loginForm = $('.form-login');
            userId = $('#loginUserId');
            lblUserId = $('#lblLoginUserId');
            password = $('#loginPassword');
            lblPassword = $('#lblLoginPassword');
            btnSubmit = $('.btn-submit');
            _KEY_LP = $('#_KEY_LP');
            isRememberMe = $('#defaultLoginFormRemember');

            checkCookie();
        };

        var formValidation = function () {
            var isFormValid = false,
                isUserId = false,
                isPassword = false;

            /* User Id */
            if (_KEY_LP.val() == 'EMP') {
                if (userId.val() == '') {
                    userId.addClass('invalid');
                    if ($('.errUserId').length == 0) {
                        lblUserId.after('<div class="invalid-feedback errUserId">' + 'Please fill the required field.' + '</div>');
                        isUserId = false;
                    }
                }
                else {
                    userId.removeClass('invalid');
                    $('.errUserId').remove();
                    isUserId = true;
                }
            } else {
                if (userId.val() == '') {
                    userId.addClass('invalid');
                    if ($('.errUserId').length == 0) {
                        lblUserId.after('<div class="invalid-feedback errUserId">' + 'Please fill the required field.' + '</div>');
                        isUserId = false;
                    }
                } else if (!WEBPORTAL.Utility.CheckEmail(userId.val())) {
                    $('.errUserId').remove();
                    userId.addClass('invalid');
                    lblUserId.after('<div class="invalid-feedback errUserId">' + 'Format email not valid.' + '</div>');
                    isUserId = false;
                }
                else {
                    userId.removeClass('invalid');
                    $('.errUserId').remove();
                    isUserId = true;
                }
            }

            /* Password */
            if (password.val() == '') {
                $('.errPassword').remove();
                password.addClass('invalid');
                if ($('.errPassword').length == 0) {
                    lblPassword.after('<div class="invalid-feedback errPassword">' + 'Please fill the required field.' + '</div>');
                    isPassword = false;
                }
            } else if (password.val().length < 5) {
                $('.errPassword').remove();
                password.addClass('invalid');
                lblPassword.after('<div class="invalid-feedback errPassword">' + 'Password minimum 3 characters length.' + '</div>');
                isPassword = false;
            }
            else {
                password.removeClass('invalid');
                $('.errPassword').remove();
                isPassword = true;
            }

            if (isUserId && isPassword) {
                isFormValid = true;
            } else {
                isFormValid = false;
            }

            return isFormValid;
        };

        var formLoginValidation = function () {
            WEBPORTAL.Utility.SubmitLoading(btnSubmit);
            if (formValidation()) {
                var params = {
                    UserId: WEBPORTAL.Utility.EncryptionHandler(userId.val()),
                    Password: WEBPORTAL.Utility.EncryptionHandler(password.val())
                };

                $.when(WEBPORTAL.Services.POSTLocal(params, WEBPORTAL.URLContext.LoginVendor)).done(function (result, status, xhr) {
                    if (result.code === 200) {
                        if (isRememberMe.is(':checked'))
                            WEBPORTAL.CookieConfiguration.SetCookie("userid", userId.val(), 365);

                        if (result.isUserActive) {

                            //loginForm.submit();
                            
                            if (result.sessionUserLevel == 'VENDOR') {
                                window.location.href = WEBPORTAL.URLContext.VendorDashboard;
                            } else if (result.sessionUserLevel == 'ADMIN') {
                                window.location.href = WEBPORTAL.URLContext.AdminDashboard;
                            } else if (result.sessionUserLevel == 'JOB_APPLICANT') {
                                window.location.href = WEBPORTAL.URLContext.JobApplicantDashboard;
                            }
                            else {
                                window.location.href = WEBPORTAL.URLContext.HomeIndex;
                            }
                        } else {
                            $('.errPassword').remove();
                            password.addClass('invalid');
                            lblPassword.after('<div class="invalid-feedback errPassword">' + 'Please activate your email before login.' + '</div>');

                            setTimeout(function () {
                                WEBPORTAL.Utility.SubmitRemoveLoading(btnSubmit, 'SIGN IN');
                            }, 300);
                        }
                    } else {
                        $('.errPassword').remove();
                        password.addClass('invalid');
                        lblPassword.after('<div class="invalid-feedback errPassword">' + result.message + '</div>');

                        setTimeout(function () {
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnSubmit, 'SIGN IN');
                        }, 300);
                    }
                });
            } else {
                setTimeout(function () {
                    WEBPORTAL.Utility.SubmitRemoveLoading(btnSubmit, 'SIGN IN');
                }, 300);
            }
        }
        var eventSubmitForm = function () {
            btnSubmit.click(function () {
                formLoginValidation();
            });
        };
        var eventEnterSubmitForm = function () {
            $('#loginUserId, #loginPassword').keyup(function (e){
                if (e.keyCode === 13) {
                    formLoginValidation();
                }
            });
        }

        var inputChangeRemoveErr = function () {
            userId.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    lblUserId.next().remove();
                }
            });

            password.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    lblPassword.next().remove();
                }
            });
        };

        var checkCookie = function () {
            var userIdCookie = WEBPORTAL.CookieConfiguration.GetCookie('userid');

            if (userIdCookie != "") {
                userId.val(userIdCookie);
                lblUserId.addClass('active');
                isRememberMe.prop('checked', true);
            }
        };

        var run = function () {
            registerDom();
            inputChangeRemoveErr();
            eventSubmitForm();
            eventEnterSubmitForm();
        };

        return {
            run: run
        };
    })();

    initalLogin.run();
})(jQuery);
