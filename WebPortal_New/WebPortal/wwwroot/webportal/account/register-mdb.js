﻿(function ($) {
    'use strict';

    var initalRegister = (function () {
        var registerForm, email, lblEmail, passwordRegister, lblPasswordRegister, confirmPassword, lblConfirmPassword, btnSubmitRegister;

        var registerDom = function () {
            registerForm = $('.form-register');
            email = $('#registerEmail');
            lblEmail = $('#lblRegisterEmail');
            passwordRegister = $('#registerPassword');
            lblPasswordRegister = $('#lblRegisterPassword');
            confirmPassword = $('#registerConfirmPassword');
            lblConfirmPassword = $('#lblRegisterConfirmPassword');
            btnSubmitRegister = $('.btn-submit-register');
        };

        var formValidation = function () {
            var isFormRegisterValid = false,
                isEmailRegister = false,
                isPasswordRegister = false,
                isConfirmPassword = false;

            /* email */
            if (email.val() == '') {
                email.addClass('invalid');
                if ($('.errRegisterEmail').length == 0) {
                    lblEmail.after('<div class="invalid-feedback errRegisterEmail">' + 'Please fill the required field.' + '</div>');
                    isEmailRegister = false;
                }
            } else if (!WEBPORTAL.Utility.CheckEmail(email.val())) {
                $('.errRegisterEmail').remove();
                email.addClass('invalid');
                lblEmail.after('<div class="invalid-feedback errRegisterEmail">' + 'Format email not valid.' + '</div>');
                isEmailRegister = false;
            }
            else {
                $('.errRegisterEmail').remove();
                email.removeClass('invalid');
                isEmailRegister = true;
            }

            /* Password */
            if (passwordRegister.val() == '') {
                passwordRegister.addClass('invalid');
                if ($('.errRegisterPassword').length == 0) {
                    lblPasswordRegister.after('<div class="invalid-feedback errRegisterPassword">' + 'Please fill the required field.' + '</div>');
                    isPasswordRegister = false;
                }
            } else if (passwordRegister.val().length < 5) {
                $('.errRegisterPassword').remove();
                passwordRegister.addClass('invalid');
                lblPasswordRegister.after('<div class="invalid-feedback errRegisterPassword">' + 'Password minimum 3 characters length.' + '</div>');
                isPasswordRegister = false;
            }
            else {
                passwordRegister.removeClass('invalid');
                $('.errRegisterPassword').remove();
                isPasswordRegister = true;
            }

            /* Confirm Password */
            if (confirmPassword.val() == '') {
                confirmPassword.addClass('invalid');
                if ($('.errConfirmPassword').length == 0) {
                    lblConfirmPassword.after('<div class="invalid-feedback errConfirmPassword">' + 'Please fill the required field.' + '</div>');
                    isConfirmPassword = false;
                }
            } else if (passwordRegister.val() != confirmPassword.val()) {
                $('.errConfirmPassword').remove();
                confirmPassword.addClass('invalid');
                lblConfirmPassword.after('<div class="invalid-feedback errConfirmPassword">' + 'Password and Confirm Password not match.' + '</div>');
                isConfirmPassword = false;
            }
            else {
                $('.errConfirmPassword').remove();
                confirmPassword.removeClass('invalid');
                isConfirmPassword = true;
            }

            if (isEmailRegister && isPasswordRegister && isConfirmPassword) {
                isFormRegisterValid = true;
            } else {
                isFormRegisterValid = false;
            }

            return isFormRegisterValid;
        };

        var formRegisterValidation = function () {
            WEBPORTAL.Utility.SubmitLoading(btnSubmitRegister);
            if (formValidation()) {
                var params = {
                    Email: email.val(),
                    Password: passwordRegister.val(),
                    ConfirmPassword: confirmPassword.val(),
                    UserType: WEBPORTAL.UserType.Vendor
                };

                var params = {
                    Email: WEBPORTAL.Utility.EncryptionHandler(email.val()),
                    Password: WEBPORTAL.Utility.EncryptionHandler(passwordRegister.val()),
                    ConfirmPassword: WEBPORTAL.Utility.EncryptionHandler(confirmPassword.val()),
                    UserType: WEBPORTAL.Utility.EncryptionHandler(WEBPORTAL.UserType.Vendor)
                };

                $.when(WEBPORTAL.Services.POSTLocal(params, WEBPORTAL.URLContext.Register)).done(function (result, status, xhr) {
                    if (result.code === 400) {
                        $('.errRegisterEmail').remove();
                        email.addClass('invalid');
                        lblEmail.after('<div class="invalid-feedback errRegisterEmail">' + result.message + '</div>');

                        setTimeout(function () {
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnSubmitRegister, 'REGISTER');
                        }, 300);
                    } else if (result.code === 200) {
                        registerForm.submit();
                        window.location.href = WEBPORTAL.URLContext.AccountRegisterConfirmation + '?email=' + email.val();
                    } else {
                        $('.errConfirmPassword').remove();
                        confirmPassword.addClass('invalid');
                        lblConfirmPassword.after('<div class="invalid-feedback errPassword">' + result.message + '</div>');

                        setTimeout(function () {
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnSubmitRegister, 'REGISTER');
                        }, 300);
                    }
                });
            } else {
                setTimeout(function () {
                    WEBPORTAL.Utility.SubmitRemoveLoading(btnSubmitRegister, 'REGISTER');
                }, 300);
            }
        };

        var eventSubmitForm = function () {
            btnSubmitRegister.click(function () {
                formRegisterValidation();
            });
        };

        var eventEnterSubmitForm = function () {
            $('#registerEmail, #registerPassword, #registerConfirmPassword').keyup(function (e) {
                if (e.keyCode === 13) {
                    formRegisterValidation();
                }
            });
        };

        var inputChangeRemoveErr = function () {
            email.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    lblEmail.next().remove();
                }
            });

            passwordRegister.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    lblPasswordRegister.next().remove();
                }
            });

            confirmPassword.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    lblConfirmPassword.next().remove();
                }
            });
        };

        var run = function () {
            registerDom();
            inputChangeRemoveErr();
            eventSubmitForm();
            eventEnterSubmitForm();
        };

        return {
            run: run
        };
    })();

    initalRegister.run();
})(jQuery);
