﻿(function ($) {
    'use strict';

    var initialPurchaseOrder = (function () {
        let urlContext = '';
        let gridPurchaseOrder, dataTable;

        var initialDom = function () {
            gridPurchaseOrder = $('#grid_purchase_order');
        };

        var initialGridPurchaseOrder = function () {
            dataTable = gridPurchaseOrder.DataTable({
                ajax: { url: URL_CONTEXT_GetPurchaseOrder, dataSrc: '' },
                searching: false,
                info: false,
                lengthChange: false,
                paging: false,
                columns: [
                    { data: "id", visible: false },
                    { data: "poNumber", title: "PO Number" },
                    { data: "poDate", title: "PO Date" },
                    { data: "companyName", title: "Company Name" },
                    { data: "currency", title: "Currency" },
                    { data: "grandTotalString", className: "text-right", title: "Grand Total" },
                    {
                        data: 'poStatus',
                        className: "text-center",
                        title: "Status",
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (cellData === 'WAITING') {
                                $(td).html('<span class="badge badge-primary">' + "Pending" + '</span>');
                            } else if (cellData === 'PROCESS') {
                                $(td).html('<span class="badge badge-success">' + cellData + '</span>');
                            } else if (cellData === 'COMPLETED') {
                                $(td).html('<span class="badge badge-success">' + cellData + '</span>');
                            } else if (cellData === 'CANCEL') {
                                $(td).html('<span class="badge badge-danger">' + cellData + '</span>');
                            }
                        }
                    }
                ],
                "order": [[1, 'desc']],
                columnDefs: [
                    {
                        orderable: false,
                        targets: [1, 3]
                    }
                ],
                initComplete: function (settings, json) {
                    gridPurchaseOrder.find('thead th').addClass('font-weight-bold');
                }
            });
        };

        var gridRowOnClick = function () {
            gridPurchaseOrder.on('click', 'tr', function () {
                var rowData = dataTable.row(this).data();
                console.log(rowData);
                window.location.href = WEBPORTAL.URLContext.GetPurchaseOrderDetail + '/' + rowData.id;
            });
        };

        var run = function () {
            initialDom();
            initialGridPurchaseOrder();
            gridRowOnClick();
        };

        return {
            run: run
        };
    })();

    initialPurchaseOrder.run();
})(jQuery);