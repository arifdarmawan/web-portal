﻿(function ($) {
    'use strict';

    var initialPurchaseOrderDetails = (function () {
        let urlContext = '';
        let id, postatus,
            btnProcessPurchaseOrder, btnRejectPurchaseOrder;

        var initialDom = function () {
            id = $('#Id');
            postatus = $('#POStatus');
            btnProcessPurchaseOrder = $('#btn-Process-Purchase-Order');
            btnRejectPurchaseOrder = $('#btn-Reject-Purchase-Order');
        };

        var submitPurchaseOrder = function () {
            btnProcessPurchaseOrder.click(function () {
                WEBPORTAL.Utility.SubmitLoading(btnProcessPurchaseOrder);

                $.when(WEBPORTAL.Services.PUTLocal(null, URL_CONTEXT_ProcessPurchaseOrder, '?id=' + id.val())).done(function (result, status, xhr) {
                    if (result.code === 200) {
                        setTimeout(function () {
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnProcessPurchaseOrder);
                            WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                            window.location.href = WEBPORTAL.URLContext.PurchaseOrder;
                        }, 500);
                    } else {
                        setTimeout(function () {
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnProcessPurchaseOrder);
                            WEBPORTAL.Utility.ConstructNotificationError(result.message);
                        }, 500);
                    }
                });
            });
        };

        var rejectPurchaseOrder = function () {
            btnRejectPurchaseOrder.click(function () {
                WEBPORTAL.Utility.SubmitLoading(btnRejectPurchaseOrder);

                $.when(WEBPORTAL.Services.PUTLocal(null, URL_CONTEXT_RejectPurchaseOrder, '?id=' + id.val())).done(function (result, status, xhr) {
                    if (result.code === 200) {
                        setTimeout(function () {
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnRejectPurchaseOrder);
                            WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                            window.location.href = WEBPORTAL.URLContext.PurchaseOrder;
                        }, 500);
                    } else {
                        setTimeout(function () {
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnRejectPurchaseOrder);
                            WEBPORTAL.Utility.ConstructNotificationError(result.message);
                        }, 500);
                    }
                });
            });
        };

        var buttonBehaviour = function () {
            if (postatus.val() === "PROCESS") {
                btnProcessPurchaseOrder.find('span').text("PO on Process");
                btnProcessPurchaseOrder.prop('disabled', true);
                btnRejectPurchaseOrder.prop('hidden', true);
            }
            else if (postatus.val() === "CANCEL") {
                btnProcessPurchaseOrder.prop('hidden', true);
                btnRejectPurchaseOrder.prop('hidden', true);
            }
        };

        var run = function () {
            initialDom();
            buttonBehaviour();
            submitPurchaseOrder();
            rejectPurchaseOrder();
        };

        return {
            run: run
        };
    })();

    initialPurchaseOrderDetails.run();
})(jQuery);