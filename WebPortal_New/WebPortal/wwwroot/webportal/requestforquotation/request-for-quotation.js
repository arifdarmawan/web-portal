﻿(function ($) {
    'use strict';

    var initialRequestForQuotation = (function () {
        let urlContext = '';
        let grid_requestforquotation, dataTable;

        var initialDom = function () {
            grid_requestforquotation = $('#grid_request_for_quotation');
        };

        var initialGridRequestForQuotation = function () {
            dataTable = grid_requestforquotation.DataTable({
                ajax: { url: URL_CONTEXT_GetRequestForQuotation, dataSrc: '' },
                searching: false,
                info: false,
                lengthChange: false,
                paging: false,
                columns: [
                    { data: "Id", visible: false },
                    { data: "RFQNumber", title: "RFQ Number" },
                    { data: "CompanyName", title: "Company" },
                    { data: "ItemName", title: "Item/Service Name" },
                    { data: "Qty", className: "text-right", title: "Quantity" },
                    {
                        data: 'rfqstatus',
                        className: "text-center",
                        title: "Status",
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (cellData === 'PENDING') {
                                $(td).html('<span class="badge badge-primary">' + "Pending" + '</span>');
                            } else if (cellData === 'RFQ_ON_PROCESS') {
                                $(td).html('<span class="badge badge-danger">' + "On Process" + '</span>');
                            } else if (cellData === 'CLOSED') {
                                $(td).html('<span class="badge badge-success">' + cellData + '</span>');
                            } else if (cellData === 'CANCEL') {
                                $(td).html('<span class="badge badge-danger">' + cellData + '</span>');
                            }
                        }
                    }
                ],
                "order": [[1, 'desc']],
                columnDefs: [
                    {
                        orderable: false,
                        targets: [1, 3]
                    }
                ],
                initComplete: function (settings, json) {
                    grid_requestforquotation.find('thead th').addClass('font-weight-bold');
                }
            });
        };

        var gridRowOnClick = function () {
            grid_requestforquotation.on('click', 'tr', function () {
                var rowData = dataTable.row(this).data();
                window.location.href = WEBPORTAL.URLContext.GetRequestForQuotationDetail + '/' + rowData.Id;
            });
        };

        var run = function () {
            initialDom();
            initialGridRequestForQuotation();
            gridRowOnClick();
        };

        return {
            run: run
        };
    })();

    initialRequestForQuotation.run();
})(jQuery);