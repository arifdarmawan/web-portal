﻿(function ($) {
    'use strict';

    var initialRequestForQuotationDetails = (function () {
        let urlContext = '';
        let grid, id, rfqstatus, itemname, specification, qty, unitprice, totalprice, pricevaliddate, deliverytime, notes_rfq, lbl_item_name,
            lbl_specification, lbl_qty, lbl_unit_price, lbl_valid_date, lbl_delivery_time, lbl_notes, notes,
            temp_qty, temp_unitprice, temp_validdateprice, temp_deliverytime,
            btnopenModal, btnupdateitem, modalEditItem, btnRequestForQuotation, datePicker, downloadFile;

        var initialDom = function () {
            grid = $('#Grid');
            id = $('#Id');
            temp_qty = $('#temp_Qty');
            temp_unitprice = $('#temp_UnitPrice');
            temp_validdateprice = $('#temp_PriceValidDate');
            temp_deliverytime = $('#temp_DeliveryTime');
            itemname = $('#ItemName');
            lbl_item_name = $('#lbl_Item_Name');
            specification = $('#Specification');
            lbl_specification = $('#lbl_Specification');
            qty = $('#Qty');
            lbl_qty = $('#lbl_Qty');
            unitprice = $('#UnitPrice');
            lbl_unit_price = $('#lbl_Unit_Price');
            totalprice = $('#TotalPrice');
            pricevaliddate = $('#PriceValidDate');
            lbl_valid_date = $('#lbl_Valid_Date');
            deliverytime = $('#DeliveryTime');
            lbl_delivery_time = $('#lbl_Delivery_Time');
            notes = $('#Notes')
            lbl_notes = $('#lbl_Notes');
            btnopenModal = $('#btnOpenModal');
            modalEditItem = $('#editItemModal');
            btnupdateitem = $('#btn_Update_Item');
            notes_rfq = $('#notes_RFQ');
            rfqstatus = $('#Rfqstatus');
            btnRequestForQuotation = $('#btn-Request-For-Quotation');
            datePicker = $('.datepicker').pickadate({ formatSubmit: 'yyyy/mm/dd', min: new Date() });
            console.log(datePicker);
        };

        var ShowModalEditItem = function () {
            btnopenModal.click(function () {
                lbl_item_name.addClass('active');
                lbl_specification.addClass('active');
                lbl_qty.addClass('active');
                lbl_unit_price.addClass('active');
                lbl_valid_date.addClass('active');
                lbl_delivery_time.addClass('active');

                itemname.val($(this).closest('tr').attr('data-item-name'));
                specification.val($(this).closest('tr').attr('data-specification'));
                qty.val($(this).closest('tr').attr('data-qty'));

                //unitprice.val($(this).closest('tr').attr('data-unit-price'));
                //pricevaliddate.val($(this).closest('tr').attr('data-valid-date-price'));
                //deliverytime.val($(this).closest('tr').attr('data-delivery-time'));

                unitprice.val(temp_unitprice.val());
                pricevaliddate.val(temp_validdateprice.val());
                deliverytime.val(temp_deliverytime.val());

                modalEditItem.modal('show');
            });
        };

        var UpdateItem = function () {
            btnupdateitem.click(function () {
                totalprice = unitprice.val() * qty.val();
                grid.find('td').eq('4').text(unitprice.val());
                grid.find('td').eq('5').text(totalprice);

                temp_unitprice.val(unitprice.val());
                temp_validdateprice.val(pricevaliddate.val());
                temp_deliverytime.val(deliverytime.val());

                modalEditItem.modal('hide');
            });
        };

        var sendRequestForQuotation = function () {
            btnRequestForQuotation.click(function () {
                WEBPORTAL.Utility.SubmitLoading(btnRequestForQuotation);   

                var formData = new FormData();

                formData.append('Id', id.val());

                if (qty.val() == '')
                    qty.val(temp_qty.val());

                if (unitprice.val() == '')
                    unitprice.val(temp_unitprice.val());

                if (pricevaliddate.val() == '') 
                    pricevaliddate.val(temp_validdateprice.val());

                if (deliverytime.val() == '')
                    deliverytime.val(temp_deliverytime.val());
                
              
                formData.append('UnitPrice', unitprice.val());
                formData.append('PriceValidDate', pricevaliddate.val());
                formData.append('DeliveryTime', deliverytime.val());
                formData.append('TotalPrice', unitprice.val() * qty.val());
                formData.append('Notes', notes.val());

                $.when(WEBPORTAL.Services.POSTLocal(formData, URL_CONTEXT_ProcessRequestForQuotation, false, false)).done(function (result, status, xhr) {
                    if (result.code === 200) {
                        setTimeout(function () {
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnRequestForQuotation);
                            WEBPORTAL.Utility.ConstructNotificationSuccess(result.message);
                            window.location.href = WEBPORTAL.URLContext.RequestForQuotation;
                        }, 500);
                    } else {
                        setTimeout(function () {
                            WEBPORTAL.Utility.SubmitRemoveLoading(btnRequestForQuotation);
                            WEBPORTAL.Utility.ConstructNotificationError(result.message);
                        }, 500);
                    }
                });
            });
        };

        var buttonBehaviour = function () {
            if (rfqstatus.val() === "RFQ_ON_PROCESS") {
                //btnRequestForQuotation.find('span').text("RFQ on Process");
                btnRequestForQuotation.prop('disabled', false);
            }
            else if (rfqstatus.val() === "CLOSED") {
                notes.prop('disabled', true);
                btnupdateitem.prop('disabled', true);
                btnRequestForQuotation.prop('hidden', true);
                btnRequestForQuotation.find('span').text("Closed");
            }
        };

        var run = function () {
            initialDom();
            buttonBehaviour();
            sendRequestForQuotation();
            ShowModalEditItem();
            UpdateItem();
        };

        return {
            run: run
        };
    })();

    initialRequestForQuotationDetails.run();
})(jQuery);