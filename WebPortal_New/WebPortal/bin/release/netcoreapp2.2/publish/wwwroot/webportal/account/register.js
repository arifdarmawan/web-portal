﻿var initializeFormValidate = function () {
    // Initialize form validation on the registration form.
    // It has the name attribute "registration"
    $("form[name='register-form']").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
            registerEmail: {
                required: true,
                email: true,
            },
            registerPassword: {
                required: true,
                minlength: 5
            },
            registerConfirmPassword: {
                required: true,
                minlength: 5,
                equalTo: "#registerPassword"
            }
        },
        // Specify validation error messages
        messages: {
            registerEmail: {
                required: "Please provide a email",
                email: "Please provide a valid email"
            },
            registerPassword: {
                required: "Please provide a new password",
                minlength: "Your password must be at least 5 characters long"
            },
            registerConfirmPassword: {
                required: "Please provide a new password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Password not match. Please check again"
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            WEBPORTAL.Utility.ShowLoading($(".register-btn"));

            setTimeout(function () {
                $.when(register()).done(function (result) {
                    WEBPORTAL.Utility.RemoveLoading($(".register-btn span"));
                });
            }, 500);
        }
    });
};

var register = function () {
    var deferred = $.Deferred();

    var email = document.getElementById('registerEmail').value;
    var password = document.getElementById('registerPassword').value;
    var confirmPassword = document.getElementById('registerConfirmPassword').value;
    var type = document.getElementById('registerUserType').value;

    var getParams = {
        "Email": email,
        "Password": password,
        "ConfirmPassword": confirmPassword,
        "UserType": type
    };

    $.ajax({
        type: "POST",
        traditional: true,
        async: false,
        cache: false,
        url: '/Account/Register',
        data: getParams,
        success: function (result, status, xhr) {
            if (result.code === 400) {
                formMessageHandler(result.message);
            } else if (result.code === 200) {
                window.location.href = '/Account/RegisterConfirmation?email=' + email;
            } else {
                formMessageHandler(result.message);
            }
            
            deferred.resolve(result);
        },
        error: function (result, status, xhr) {
            deferred.reject(result);
        }
    });

    return deferred.promise();
}

function formMessageHandler(msg) {
    $('<label for="registerEmail" generated="true" class="error">' + msg + '</label>').insertAfter("#registerEmail");
}

$(function () {
    initializeFormValidate();
});