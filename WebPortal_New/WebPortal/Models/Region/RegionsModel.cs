﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.Region
{
    public class RegionsModel
    {
        public int Value { get; set; }
        public string Text { get; set; }
    }
}
