﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.RequestForQuotation
{
    public class RequestForQuotationModel
    {
        public int Id { get; set; }
        public int Prid { get; set; }
        public string Rfqnumber { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string VendorName { get; set; }
        public string VendorAddress { get; set; }
        public int? MasterDataVendorId { get; set; }
        public int? MasterItemCategoryId { get; set; }
        public int? MasterItemId { get; set; }
        public string ItemName { get; set; }
        public string Specification { get; set; }
        public string PurposeDesc { get; set; }
        public decimal? Qty { get; set; }
        public int CurrencyId { get; set; }
        public string Currency { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? TotalPrice { get; set; }
        public string UnitPriceString { get; set; }
        public string TotalPriceString { get; set; }
        public DateTime? PriceValidDate { get; set; }
        public int DeliveryTime { get; set; }
        public string Rfqstatus { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
