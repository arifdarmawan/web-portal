﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.RequestForQuotation
{
    public class GridRequestForQuotationModel
    {
        public int Id { get; set; }
        public string RFQNumber { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string ItemName { get; set; }
        public decimal Qty { get; set; }
        public string rfqstatus { get; set; }
    }
}
