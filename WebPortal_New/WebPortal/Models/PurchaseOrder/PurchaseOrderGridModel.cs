﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.PurchaseOrder
{
    public class PurchaseOrderGridModel
    {
        public int id { get; set; }
        public string poNumber { get; set; }
        public string poDate { get; set; }
        public string companyName { get; set; }
        public string currency { get; set; }
        public string poStatus { get; set; }
        public decimal? grandTotal { get; set; }
        public int? ppn { get; set; }
        public string grandTotalString { get; set; }
    }
}
