﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.PurchaseOrder
{
    public class PurchaseOrderModel
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string Npwp { get; set; }
        public string NpwpAddress { get; set; }
        public string VendorCompanyName { get; set; }
        public string VendorCompanyAddress { get; set; }
        public string VendorPhone { get; set; }
        public string VendorCity { get; set; }
        public string VendorRegion { get; set; }
        public string VendorpostalCode { get; set; }
        public string PONumber { get; set; }
        public string CreatedDate { get; set; }
        public string PaymentStatus { get; set; }
        public string POStatus { get; set; }
        public int TOP { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal Ppn { get; set; }
        public decimal GrandTotalAfterTax { get; set; }

        public string StringGrandTotal { get; set; }
        public string stringPpn { get; set; }
        public string StringGrandTotalAfterTax { get; set; }

        public List<PurchaseOrderItemModel> ListItem { get; set; }

    }
}