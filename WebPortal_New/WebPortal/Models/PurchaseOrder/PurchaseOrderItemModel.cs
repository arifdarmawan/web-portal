﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.PurchaseOrder
{
    public class PurchaseOrderItemModel
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string Specification { get; set; }
        public string PurposeDesc { get; set; }
        public decimal Qty { get; set; }
        public string UomName { get; set; }
        public string Currency { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? TotalPrice { get; set; }
        public string UnitPriceString { get; set; }
        public string TotalPriceString { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}