﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.City
{
    public class CityModel
    {
        public int Value { get; set; }
        public string Text { get; set; }
    }
}
