﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.Country
{
    public class CountryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
