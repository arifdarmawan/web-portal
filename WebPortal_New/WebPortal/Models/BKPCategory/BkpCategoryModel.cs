﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.BKPCategory
{
    public class BkpCategoryModel
    {
        public int Id { get; set; }
        public string Desc { get; set; }
    }
}
