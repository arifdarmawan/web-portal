﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.JobTitle
{
    public class JobTitleModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
