﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.ResponseModel
{
    public class AjaxResult : BaseResponse
    {
        public UserModel UserModel { get; set; }
    }
}
