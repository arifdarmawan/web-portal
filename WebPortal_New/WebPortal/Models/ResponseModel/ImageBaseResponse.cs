﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.ResponseModel
{
    public class ImageBaseResponse : BaseResponse
    {
        public int filesCount { get; set; }
        public long fileSizes { get; set; }
        public List<string> fileNames { get; set; }
        public List<string> filePath { get; set; }
        public MemoryStream file { get; set; }
    }
}
