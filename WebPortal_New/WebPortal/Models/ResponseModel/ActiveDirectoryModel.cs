﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.ResponseModel
{
    public class ActiveDirectoryModel
    {
        public string UserId { get; set; }
        public string Password { get; set; }
    }
}
