﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.BusinessFields
{
    public class BusinessFieldTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        public List<Models.BusinessFields.BusinessFieldsModel> BusinessFields { get; set; }
    }
}
