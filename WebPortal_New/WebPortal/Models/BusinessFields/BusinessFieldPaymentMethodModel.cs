﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.BusinessFields
{
    public class BusinessFieldPaymentMethodModel
    {
        public int Id { get; set; }
        public decimal? DownPayment { get; set; }
        public decimal? Progressive { get; set; }
        public decimal? FullPayment { get; set; }
        public decimal? Retention { get; set; }
    }
}
