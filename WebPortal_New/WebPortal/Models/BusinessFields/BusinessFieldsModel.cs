﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.BusinessFields
{
    public class BusinessFieldsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string KBLI { get; set; }
    }
}
