﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.BusinessFields
{
    public class BusinessFieldTopModel
    {
        public int Id { get; set; }
        public string Top { get; set; }
    }
}
