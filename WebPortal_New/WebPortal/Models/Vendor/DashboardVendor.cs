﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.Vendor
{
    public class DashboardVendor
    {
        public string MasterDataUserId { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string Siup { get; set; }
        public string SiupexpiredDate { get; set; }
        public string Tdp { get; set; }
        public string TdpexpiredDate { get; set; }
        public string Npwp { get; set; }
    }
}
