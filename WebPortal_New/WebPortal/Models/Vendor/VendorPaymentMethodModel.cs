﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.Vendor
{
    public class VendorPaymentMethodModel
    {
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        public int MappingDataBusinessFieldTopConfigId { get; set; }
        public int mappingDataBusinessFieldPaymentConfigId { get; set; }
        public int Top { get; set; }
        public decimal? DownPayment { get; set; }
        public decimal? Progressive { get; set; }
        public decimal? FullPayment { get; set; }
        public decimal? Retention { get; set; }

        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}
