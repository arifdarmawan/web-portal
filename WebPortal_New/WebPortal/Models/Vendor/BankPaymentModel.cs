﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.Vendor
{
    public class BankPaymentModel
    {
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        public int MasterDataCurrencyId { get; set; }
        public string Currency { get; set; }
        public int MasterDataBankId { get; set; }
        public string BankName { get; set; }
        public string Area { get; set; }
        public string City { get; set; }
        public string AccountNumber { get; set; }
        public string Status { get; set; }
        public string AccountName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiyBy { get; set; }
        public bool? IsDelete { get; set; }
        public string Attachment { get; set; }
        public List<IFormFile> Attachments { get; set; }
        public int MasterDataBusinessFieldId { get; set; }
        public int MappingDataBusinessFieldTopId { get; set; }
        public int MappingDataBusinessPaymentMethodId { get; set; }
        public string MappingDataBusinessFieldPaymentJson { get; set; }
        public List<SelectListItem> CurrencyOptions { get; set; }
        public List<SelectListItem> BankOptions { get; set; }
        public List<SelectListItem> BusinessFieldTopOptions { get; set; }
        public List<SelectListItem> ConstructBankOptions(List<Models.Bank.BankModel> banks)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Select", Selected = true });
            foreach (var single in banks)
            {
                result.Add(new SelectListItem { Value = single.Id.ToString(), Text = single.Name });
            }

            return result;
        }

        public List<SelectListItem> ConstructCurrencyOptions(List<Models.Currency.CurrencyModel> currencies)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Select", Selected = true });
            foreach (var single in currencies)
            {
                result.Add(new SelectListItem { Value = single.Id.ToString(), Text = single.Code });
            }

            return result;
        }

        public List<SelectListItem> ConstructBusinessFieldTopOptions(List<Models.BusinessFields.BusinessFieldTopModel> businessFieldTop)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Select", Selected = true });
            foreach (var single in businessFieldTop)
            {
                result.Add(new SelectListItem { Value = single.Id.ToString(), Text = single.Top + " Day" });
            }

            return result;
        }
    }
}
