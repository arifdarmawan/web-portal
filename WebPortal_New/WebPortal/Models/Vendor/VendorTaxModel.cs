﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.Vendor
{
    public class VendorTaxModel
    {
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        public string TypeNpwp { get; set; }
        public string Npwp { get; set; }
        public string TypePkp { get; set; }
        public string Pkp { get; set; }
        public string TypeBkp { get; set; }
        public int? MasterDataBkpCategoryId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool? IsDelete { get; set; }
        public int? Ppn { get; set; }
        public string BkpDesc { get; set; }
        public List<SelectListItem> NPWPOptions
        {
            get
            {
                var result = new List<SelectListItem>
                {
                    new SelectListItem { Value = "", Text = "Select", Selected = true },
                    new SelectListItem { Value = "NPWP", Text = "NPWP" },
                    new SelectListItem { Value = "NON_NPWP", Text = "Non NPWP" }
                };

                return result;
            }
        }

        public List<SelectListItem> PKPOptions
        {
            get
            {
                var result = new List<SelectListItem>
                {
                    new SelectListItem { Value = "", Text = "Select", Selected = true },
                    new SelectListItem { Value = "PKP", Text = "PKP" },
                    new SelectListItem { Value = "NON_PKP", Text = "Non PKP" }
                };

                return result;
            }
        }

        public List<SelectListItem> BKPOptions
        {
            get
            {
                var result = new List<SelectListItem>
                {
                    new SelectListItem { Value = "", Text = "Select", Selected = true },
                    new SelectListItem { Value = "BKP", Text = "BKP/JKP" },
                    new SelectListItem { Value = "BKP_FREE_PPN", Text = "BKP/JKP Bebas PPN" }
                };

                return result;
            }
        }

        public List<SelectListItem> BKPFreePPNOptions { get; set; }
        public List<SelectListItem> CostructBKPFreePPNOptions(List<Models.BKPCategory.BkpCategoryModel> models)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Select", Selected = true });
            foreach (var single in models)
            {
                result.Add(new SelectListItem { Value = single.Id.ToString(), Text = single.Desc });
            }

            return result;
        }
    }
}
