﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.Vendor
{
    public class VendorDataTablesModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string CompanyPhone { get; set; }
        public string Status { get; set; }
        public string BusinessFields { get; set; }
    }
}
