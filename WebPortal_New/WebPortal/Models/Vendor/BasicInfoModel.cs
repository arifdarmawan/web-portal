﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.Vendor
{
    public class BasicInfoModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int MasterDataUserId { get; set; }
        public int MasterDataBusinessTypeId { get; set; }
        public string BusinessTypeName { get; set; }
        public List<string> MasterBusinessFieldsIdArray { get; set; }
        public string BusinessFieldsJson { get; set; }
        public int MasterDataCountryId { get; set; }
        public string Country { get; set; }
        public int MasterDataRegionId { get; set; }
        public string Region { get; set; }
        public int MasterDataCityId { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string MailingAddress { get; set; }
        public string PostalCode { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyPhone { get; set; }
        public string Pic { get; set; }
        public string Picemail { get; set; }
        public string IdCardNumber { get; set; }
        public string PICPhone { get; set; }
        public bool IntegrityPact { get; set; }
        public bool? MasterAgreement { get; set; }
        public string RegistrationNumber { get; set; }
        public bool CkMailingAddress { get; set; }
        public string Status { get; set; }

        public List<BusinessFields.BusinessFieldsModel> BusinessFields { get; set; }
        public List<SelectListItem> BussinessTypeOptions { get; set; }
        public List<SelectListItem> BusinessFieldsOptions { get; set; }
        public List<SelectListItem> CountryOptions { get; set; }
        public List<SelectListItem> RegionOptions { get; set; }
        public List<SelectListItem> CityOptions { get; set; }
        public BasicInfoModel()
        {
            if (!String.IsNullOrEmpty(MailingAddress))
            {
                if (Address.Equals(MailingAddress))
                {
                    CkMailingAddress = true;
                }
            }
        }
        public List<SelectListItem> ConstructBussinessTypeOptions(List<Models.BusinessTypes.BusinessTypeModel> businessTypes)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Select", Selected = true });
            foreach (var single in businessTypes)
            {
                result.Add(new SelectListItem { Value = single.Id.ToString(), Text = single.Name });
            }

            return result;
        }

        public List<SelectListItem> ConstructBussinessFieldsOptions(List<Models.BusinessFields.BusinessFieldTypeModel> businessFieldTypes)
        {
            var result = new List<SelectListItem>();

            foreach (var types in businessFieldTypes)
            {
                var group = new SelectListGroup { Name = types.Name };

                foreach (var item in types.BusinessFields)
                {
                    result.Add(new SelectListItem { Value = item.Id.ToString(), Text = item.KBLI + " " + item.Name, Group = group });
                }
            }

            return result;
        }

        public List<SelectListItem> ConstructCountryOptions(List<Models.Country.CountryModel> cities)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Select", Selected = true });
            foreach (var single in cities)
            {
                result.Add(new SelectListItem { Value = single.Id.ToString(), Text = single.Name });
            }

            return result;
        }

        public List<SelectListItem> ConstructCityOptions(List<Models.City.CityModel> cities)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Select", Selected = true });
            if (cities != null)
            {              
                foreach (var single in cities)
                {
                    result.Add(new SelectListItem { Value = single.Value.ToString(), Text = single.Text });
                }
            }

            return result;
        }

        public List<SelectListItem> ConstructRegionsOptions(List<Models.Region.RegionsModel> regions)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Select", Selected = true });

            if (regions != null)
            {
                foreach (var single in regions)
                {
                    result.Add(new SelectListItem { Value = single.Value.ToString(), Text = single.Text });
                }
            }

            return result;
        }
    }
}
