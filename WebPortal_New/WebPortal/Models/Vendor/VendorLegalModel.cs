﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebPortal.Models.Vendor
{
    public class VendorLegalModel
    {
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        public string Nib { get; set; }
        public string Nibattachment { get; set; }
        public string BusinessPermit { get; set; }
        public string BusinessPermitAttachment { get; set; }
        public string Siup { get; set; }
        public DateTime? SiupexpiredDate { get; set; }
        public string Siupattachment { get; set; }
        public string Tdp { get; set; }
        public DateTime? TdpexpiredDate { get; set; }
        public string Tdpattachment { get; set; }
        public string MemorandumOfAssociation { get; set; }
        public string MemorandumOfAssociationAttachment { get; set; }
        public string DecissionLetterMenkumham { get; set; }
        public string DecissionLetterMenkumhamAttachment { get; set; }
        public string MemorandumOfAssociationChanging { get; set; }
        public string MemorandumOfAssociationChangingAttachment { get; set; }
        public string DecissionLetterMenkumhamChanging { get; set; }
        public string DecissionLetterMenkumhamChangingAttachment { get; set; }
        public string ManagementStructure { get; set; }
        public string Signing { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiyBy { get; set; }
        public bool? IsDelete { get; set; }
        public int? SigningId { get; set; }
        public string SigningName { get; set; }
        public string SigningAttachment { get; set; }
        public string DocumentOther { get; set; }
        public string OtherAttachment { get; set; }
        public List<IFormFile> Attachments { get; set; }
        public string JobTitleOptions { get; set; }
        public bool IsPersonalVendorBusinessType { get; set; }
        public string[] JobTitle { get; set; }
        public RootObject StructureOrg { get; set; }
        public RootObject ConstructStructureOrg(List<JobTitle.JobTitleModel> jobTitles, RootObject rootObject)
        {
            foreach (var single in rootObject.Data)
            {
                var checkJobTitles = jobTitles.FirstOrDefault(n => n.Id == Int32.Parse(single.JobTitleId));

                if (Int32.Parse(single.JobTitleId) == checkJobTitles.Id)
                {
                    single.JobTitleName = checkJobTitles.Title;
                }
            }

            return rootObject;
        }
        public List<SelectListItem> SignerOptions
        {
            get
            {
                var result = new List<SelectListItem>
                {
                    new SelectListItem { Value = "", Text = "Select", Selected = true },
                    new SelectListItem { Value = "1", Text = "Direktur" },
                    new SelectListItem { Value = "2", Text = "Kuasa Direktur" }
                };

                return result;
            }
        }
    }
}
