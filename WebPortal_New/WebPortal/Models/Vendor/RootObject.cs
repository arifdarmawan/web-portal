﻿using System.Collections.Generic;

namespace WebPortal.Models.Vendor
{
    public class RootObject
    {
        public List<ComponentObject> Data { get; set; }
    }

    public class ComponentObject
    {
        public string Name { get; set; }
        public string JobTitleId { get; set; }
        public string JobTitleName { get; set; }
    }
}
