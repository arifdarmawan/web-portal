﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using WebPortal.Helper;
using WebPortal.Models.RequestForQuotation;
using WebPortal.Models.ResponseModel;
using WebPortal.Models.Vendor;
using WebPortal.Service;

namespace WebPortal.Controllers
{
    [Route("[controller]/[action]")]
    public class RequestForQuotationController : BaseController
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        IConfiguration _configuration { get; }
        PortalService _portalService;

        const string IMAGE_KEY = "Images_";

        public RequestForQuotationController(IHttpContextAccessor httpContextAccessor, PortalService portalService, IConfiguration configuration) : base(httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _portalService = portalService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> RequestForQuotation()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        [HttpGet]
        public async Task<string> GetRequestForQuotation()
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetRequestForQuotationbyUserId") + "?userId=" + UserId);

            List<GridRequestForQuotationModel> model = response.Data != null ? JsonConvert.DeserializeObject<List<GridRequestForQuotationModel>>(Convert.ToString(response.Data)) : null;

            return JsonConvert.SerializeObject(model);
        }

        [Route("/RequestForQuotation/RequestForQuotationDetail/{id}")]
        public async Task<IActionResult> RequestForQuotationDetail(int Id)
        {
            ViewData["UserLevel"] = UserLevel;

            RequestForQuotationModel model = new RequestForQuotationModel();

            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetRequestForQuotationDetailByRfqId") + "?rfqId=" + Id);

            if (response.Data != null)
            {
                model = response.Data != null ? JsonConvert.DeserializeObject<RequestForQuotationModel>(Convert.ToString(response.Data)) : null;

                var resBasicInfo = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorBasicInfo") + "?userId=" + UserId);
                var resVendorPayment = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorPaymentTypeInfo") + "?masterDataUserId=" + UserId);
                var resVendorTax = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorTax") + "?masterDataUserId=" + UserId);

                BasicInfoModel vendor = resBasicInfo.Data != null ? JsonConvert.DeserializeObject<BasicInfoModel>(Convert.ToString(resBasicInfo.Data)) : null;
                VendorPaymentMethodModel modelVendorPayment = resVendorPayment.Data != null ? JsonConvert.DeserializeObject<VendorPaymentMethodModel>(Convert.ToString(resVendorPayment.Data)) : null;
                VendorTaxModel modelVendorTax = resVendorTax.Data != null ? JsonConvert.DeserializeObject<VendorTaxModel>(Convert.ToString(resVendorTax.Data)) : null;

                model.VendorName = vendor.Name;
                model.VendorAddress = vendor.Address;

                ViewData["VendorCity"] = vendor.City;
                ViewData["VendorRegion"] = vendor.Region;
                ViewData["VendorpostalCode"] = vendor.PostalCode;
                ViewData["VendorPhone"] = vendor.CompanyPhone;
                ViewData["TOP"] = modelVendorPayment.Top;
                ViewData["Ppn"] = modelVendorTax.Ppn.Value;
            }

            return PartialView(model);
        }

        [HttpPost]
        public async Task<BaseResponse> ProcessRequestForQuotation(RequestForQuotationModel data)
        {
            data.ModifyBy = UserLevel;
            data.ModifyDate = DateTime.Now;

            var response = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:ProcessRequestForQuotation"), data);

            return response;
        }

        [HttpPost]
        public async Task<BaseResponse> DeleteRFQAttachment(string type)
        {
            BaseResponse response = null;
            var result = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:DeleteRFQAttachment") + "?userId=" + UserId + "&type=" + type, response);

            return result;
        }
    }
}