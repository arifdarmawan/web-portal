﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using WebPortal.Models.PurchaseOrder;
using WebPortal.Models.ResponseModel;
using WebPortal.Models.Vendor;
using WebPortal.Service;

namespace WebPortal.Controllers
{
    [Route("[controller]/[action]")]
    public class PurchaseOrderController : BaseController
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        IConfiguration _configuration { get; }
        PortalService _portalService;

        public PurchaseOrderController(IHttpContextAccessor httpContextAccessor, PortalService portalService, IConfiguration configuration) : base(httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _portalService = portalService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> PurchaseOrder() 
        {
            ViewData["UserLevel"] = UserLevel;
            
            return View();
        }

        [HttpGet]
        public async Task<string> GetPurchaseOrder()
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetPurchaseOrderByVendorId") + "?userId=" + UserId);

            List<PurchaseOrderGridModel> model = response.Data != null ? JsonConvert.DeserializeObject<List<PurchaseOrderGridModel>>(Convert.ToString(response.Data)) : null;
            foreach(var single in model)
            {
                single.grandTotalString  = string.Format("{0:N2}", single.grandTotal + (single.grandTotal * single.ppn / 100));
            }

            return JsonConvert.SerializeObject(model);
        }

        [Route("/PurchaseOrder/PurchaseOrderDetail/{id}")]
        public async Task<IActionResult> PurchaseOrderDetail(int Id)
        {
            ViewData["UserLevel"] = UserLevel;

            PurchaseOrderModel model = new PurchaseOrderModel();

            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetPurchaseOrderById") + "?id=" + Id + "&userId=" + UserId);
            
            if (response.Data != null) 
            {
                model = response.Data != null ? JsonConvert.DeserializeObject<PurchaseOrderModel>(Convert.ToString(response.Data)) : null;

                var resBasicInfo = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorBasicInfo") + "?userId=" + UserId);
                var resVendorPayment = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorPaymentTypeInfo") + "?masterDataUserId=" + UserId);
                var resVendorTax = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorTax") + "?masterDataUserId=" + UserId);               

                BasicInfoModel vendor = resBasicInfo.Data != null ? JsonConvert.DeserializeObject<BasicInfoModel>(Convert.ToString(resBasicInfo.Data)) : null;
                VendorPaymentMethodModel modelVendorPayment = resVendorPayment.Data != null ? JsonConvert.DeserializeObject<VendorPaymentMethodModel>(Convert.ToString(resVendorPayment.Data)) : null;
                VendorTaxModel modelVendorTax = resVendorTax.Data != null ? JsonConvert.DeserializeObject<VendorTaxModel>(Convert.ToString(resVendorTax.Data)) : null;

                model.VendorCompanyName = vendor.Name;
                model.VendorCompanyAddress = vendor.Address;
                model.VendorCity = vendor.City;
                model.VendorRegion = vendor.Region;
                model.VendorpostalCode = vendor.PostalCode;
                model.VendorPhone = vendor.CompanyPhone;
                model.TOP = modelVendorPayment.Top;
                model.StringGrandTotal = string.Format("{0:N2}", model.GrandTotal);
                model.Ppn = modelVendorTax.Ppn.Value;
                model.stringPpn = string.Format("{0:N2}", model.GrandTotal * modelVendorTax.Ppn.Value / 100);
                model.GrandTotalAfterTax = model.GrandTotal + (model.GrandTotal * modelVendorTax.Ppn.Value / 100);
                model.StringGrandTotalAfterTax = string.Format("{0:N2}", model.GrandTotalAfterTax);

                foreach (var single in model.ListItem)
                {
                    single.UnitPriceString = string.Format("{0:N2}", single.UnitPrice);
                    single.TotalPriceString = string.Format("{0:N2}", single.TotalPrice);
                }

                ViewData["PO_Item"] = model.ListItem;
            }            

            return PartialView(model);
        }

        [HttpPut]
        public async Task<BaseResponse> ProcessPurchaseOrder([FromQuery] int id, string modifyBy)
        {
            var response = await _portalService.ProcessPurchaseOrder(id, UserLevel);

            return response;
        }

        [HttpPut]
        public async Task<BaseResponse> RejectPurchaseOrder([FromQuery] int id, string modifyBy)
        {
            var response = await _portalService.RejectPurchaseOrder(id, UserLevel);

            return response;
        }
    }
}