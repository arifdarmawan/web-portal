﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebPortal.Models;

namespace WebPortal.Controllers
{
    public class BaseController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        const string UserIdSessionKey = "_UserId";
        const string UserLevelSessionKey = "_UserLevel";
        const string UserEmailSessionKey = "_UserEmail";
        const string CompanyNameKey = "_CompanyName";
        const string VendorPicKey = "_VendorPic";
        const string PhotoKey = "_PhotoVendor";
        const string url = "_APIUrl";

        public BaseController(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        protected string UserId
        {
            get { return HttpContext.Session?.GetString(UserIdSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(UserIdSessionKey, value); }
        }
        protected string UserLevel
        {
            get { return HttpContext.Session?.GetString(UserLevelSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(UserLevelSessionKey, value); }
        }
        protected string UserEmail
        {
            get { return HttpContext.Session?.GetString(UserEmailSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(UserEmailSessionKey, value); }
        }
        protected string CompanyEmail
        {
            get { return HttpContext.Session?.GetString(CompanyNameKey) ?? ""; }
            set { HttpContext.Session.SetString(CompanyNameKey, value); }
        }
        protected string VendorPicName
        {
            get { return HttpContext.Session?.GetString(VendorPicKey) ?? ""; }
            set { HttpContext.Session.SetString(VendorPicKey, value); }
        }
        protected string VendorPhoto
        {
            get { return HttpContext.Session?.GetString(PhotoKey) ?? ""; }
            set { HttpContext.Session.SetString(PhotoKey, value); }
        }
        protected string URL
        {
            get { return HttpContext.Session?.GetString(url) ?? ""; }
            set { HttpContext.Session.SetString(url, value); }
        }

        protected void SetSessionEmail(string email)
        {
            UserEmail = email;
        }
        protected void SetSessionPhoto(string photo)
        {
            VendorPhoto = photo;
        }
        protected void SetSessionUrl(string url)
        {
            URL = url;
        }
        protected void SetSessionCompanyName(string businessType, string companyEmail)
        {
            CompanyEmail = businessType + " " + companyEmail;
        }
        protected void SetSessionVendorPicName(string picName)
        {
            VendorPicName = picName;
        }
    }
}