using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebPortal.Helper;
using WebPortal.Models.Vendor;
using WebPortal.Service;

namespace WebPortal.Controllers
{
    public class AdminController : BaseController
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly PortalService _portalService;

        public AdminController(IHttpContextAccessor httpContextAccessor, PortalService portalService) : base(httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _portalService = portalService;
        }

        public IActionResult Dashboard()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public async Task<IActionResult> Vendor()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public async Task<string> GetAllVendor()
        {
            var response = await _portalService.GetAllVendor();

            List<BasicInfoModel> vendors = response.Data != null ? JsonConvert.DeserializeObject<List<BasicInfoModel>>(Convert.ToString(response.Data)) : null;

            var dataTables = new List<VendorDataTablesModel>();

            foreach(var vendor in vendors)
            {
                var businessFieldsArray = vendor.BusinessFields.Select(n => n.Name).ToArray();

                dataTables.Add(new VendorDataTablesModel
                {
                    Id = vendor.Id,
                    Name = vendor.Name,
                    Address = vendor.Address,
                    CompanyPhone = vendor.CompanyPhone,
                    BusinessFields = GlobalHelper.ConstructSeparatedComma(businessFieldsArray),
                    Status = "Not verify"
                });
            }

            return JsonConvert.SerializeObject(dataTables);
        }     
    }
}