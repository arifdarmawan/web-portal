using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using WebPortal.Models.ResponseModel;
using WebPortal.Models.Vendor;
using WebPortal.Models.JobTitle;
using WebPortal.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.Net;

namespace WebPortal.Controllers
{
    [Route("[controller]/[action]")]
    public class VendorController : BaseController
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        IConfiguration _configuration { get; }
        PortalService _portalService;

        const string NIB_KEY = "NIB__";
        const string BUSSPERMIT_KEY = "BUSSPERMIT__";
        const string SIUP_KEY = "SIUP__";
        const string TDP_KEY = "TDP__";
        const string MEMOASSO_KEY = "MEMOASSO__";
        const string DECSLTR_KEY = "DECSLTR__";
        const string MEMOASSOCHG_KEY = "MEMOASSOCHG__";
        const string DECSLTRCHG_KEY = "DECSLTRCHG__";
        const string SIGNER_KEY = "SIGNER__";
        const string OTHERDOC_KEY = "OTHERDOC__";
        const string PHOTO_KEY = "Photo";

        public VendorController(IHttpContextAccessor httpContextAccessor, PortalService portalService, IConfiguration configuration) : base(httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _portalService = portalService;
        }

        [Route("/")]
        [Route("/[controller]")]
        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        public async Task<IActionResult> Data()
        {
            ViewData["UserLevel"] = UserLevel;
            BasicInfoModel model = new BasicInfoModel();

            var resBusinessType = await _portalService.GetBusinessType();
            var resBusinessFields = await _portalService.GetBusinessFields();
            var resCountry = await _portalService.GetCountries();
            var resBasicInfo = await _portalService.GetVendorBasicInfo(UserId);

            List<Models.BusinessTypes.BusinessTypeModel> businessTypes = resBusinessType.Data != null ? JsonConvert.DeserializeObject<List<Models.BusinessTypes.BusinessTypeModel>>(Convert.ToString(resBusinessType.Data)) : null;
            List<Models.BusinessFields.BusinessFieldTypeModel> businessFieldsByType = resBusinessFields.Data != null ? JsonConvert.DeserializeObject<List<Models.BusinessFields.BusinessFieldTypeModel>>(Convert.ToString(resBusinessFields.Data)) : null;
            List<Models.Country.CountryModel> countries = resCountry.Data != null ? JsonConvert.DeserializeObject<List<Models.Country.CountryModel>>(Convert.ToString(resCountry.Data)) : null;

            if (resBasicInfo.Data != null)
            {
                model = resBasicInfo.Data != null ? JsonConvert.DeserializeObject<BasicInfoModel>(Convert.ToString(resBasicInfo.Data)) : null;

                // handle dropdown cascade when update or edit data
                var resRegion = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetRegionsByCountryId") + "/?masterDataCountryId=" + model.MasterDataCountryId);
                var resCity = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetCitiesByRegionId") + "/?masterDataRegionId=" + model.MasterDataRegionId);

                List<Models.Region.RegionsModel> regions = resRegion.Data != null ? JsonConvert.DeserializeObject<List<Models.Region.RegionsModel>>(Convert.ToString(resRegion.Data)) : null;
                List<Models.City.CityModel> cities = resCity.Data != null ? JsonConvert.DeserializeObject<List<Models.City.CityModel>>(Convert.ToString(resCity.Data)) : null;

                model.BusinessFieldsJson = JsonConvert.SerializeObject(model.MasterBusinessFieldsIdArray);
                model.RegionOptions = model.ConstructRegionsOptions(regions);
                model.CityOptions = model.ConstructCityOptions(cities);
            }
            else
            {
                model.RegionOptions = model.ConstructRegionsOptions(null);
                model.CityOptions = model.ConstructCityOptions(null);
            }

            model.BussinessTypeOptions = model.ConstructBussinessTypeOptions(businessTypes);
            model.BusinessFieldsOptions = model.ConstructBussinessFieldsOptions(businessFieldsByType);
            model.CountryOptions = model.ConstructCountryOptions(countries);

            return View(model);
        }

        [HttpGet]
        public async Task<JsonResult> GetRegionsByCountry(int masterDataCountryId)
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetRegionsByCountryId") + "/?masterDataCountryId=" + masterDataCountryId);

            return Json(response.Data);
        }

        [HttpGet]
        public async Task<JsonResult> GetCitiesByRegion(int masterDataRegionId)
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetCitiesByRegionId") + "/?masterDataRegionId=" + masterDataRegionId);

            return Json(response.Data);
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        public async Task<IActionResult> Dashboard()
        {
            DashboardVendor model = new DashboardVendor();
            ViewData["UserLevel"] = UserLevel;

            var result = await _portalService.GetDataVerifikasiVendor(UserId);

            if (result.Data != null)
            {
                model = result.Data != null ? JsonConvert.DeserializeObject<DashboardVendor>(Convert.ToString(result.Data)) : null;
            }

            return View(model);
        }

        public async Task<BaseResponse> DownloadFile(string fileName)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:DownloadFile") + "/?fileName=" + fileName);
                response.Data = result;
            }
            catch (Exception ex)
            {
                throw;
            }

            return response.Data;
        }

        [HttpGet]
        public async Task<BaseResponse> GetBasicInfo()
        {
            return await _portalService.GetVendorBasicInfo(UserId);
        }

        [HttpPost]
        public async Task<BaseResponse> SaveBasicInfo(BasicInfoModel model)
        {
            BaseResponse response = new BaseResponse();

            model.MasterDataUserId = Int32.Parse(UserId);

            response = await _portalService.SaveVendorBasicInfo(model);

            if (response.Code == (int)HttpStatusCode.OK)
            {
                SetSessionCompanyName(model.BusinessTypeName, model.Name);
                SetSessionVendorPicName(model.Pic);
            }

            return response;
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        public async Task<IActionResult> BankPayment()
        {
            ViewData["UserLevel"] = UserLevel;
            BankPaymentModel model = new BankPaymentModel();

            var resCurrency = await _portalService.GetCurrency();
            var resBank = await _portalService.GetAllBank();
            var resBasicInfo = await _portalService.GetVendorBasicInfo(UserId);

            List<Models.Currency.CurrencyModel> currencies = resCurrency.Data != null ? JsonConvert.DeserializeObject<List<Models.Currency.CurrencyModel>>(Convert.ToString(resCurrency.Data)) : null;
            List<Models.Bank.BankModel> banks = resBank.Data != null ? JsonConvert.DeserializeObject<List<Models.Bank.BankModel>>(Convert.ToString(resBank.Data)) : null;

            if (resBasicInfo.Data != null)
            {
                BasicInfoModel basicInfo = resBasicInfo.Data != null ? JsonConvert.DeserializeObject<BasicInfoModel>(Convert.ToString(resBasicInfo.Data)) : null;
                var businessFieldId = Int32.Parse(basicInfo.MasterBusinessFieldsIdArray.FirstOrDefault());

                var resBusinessFieldTop = await _portalService.GetBusinessFieldsTOP(businessFieldId);
                var resVendorPaymentMethod = await _portalService.GetVendorPaymentMethod(Int32.Parse(UserId));

                model.AccountName = basicInfo.Name;

                List<Models.BusinessFields.BusinessFieldTopModel> businessFieldTop = resBusinessFieldTop.Data != null ? JsonConvert.DeserializeObject<List<Models.BusinessFields.BusinessFieldTopModel>>(Convert.ToString(resBusinessFieldTop.Data)) : null;
                Models.Vendor.VendorPaymentMethodModel vendorPaymentMethod = resVendorPaymentMethod.Data != null ? JsonConvert.DeserializeObject<Models.Vendor.VendorPaymentMethodModel>(Convert.ToString(resVendorPaymentMethod.Data)) : null;

                model.BusinessFieldTopOptions = model.ConstructBusinessFieldTopOptions(businessFieldTop);
                model.MasterDataBusinessFieldId = businessFieldId;

                if (vendorPaymentMethod != null)
                {
                    model.MappingDataBusinessFieldTopId = vendorPaymentMethod.MappingDataBusinessFieldTopConfigId;
                    model.MappingDataBusinessPaymentMethodId = vendorPaymentMethod.mappingDataBusinessFieldPaymentConfigId;
                }

                ViewData["IsDataFillingOut"] = true;
            }
            else
            {
                ViewData["IsDataFillingOut"] = false;
            }

            model.CurrencyOptions = model.ConstructCurrencyOptions(currencies);
            model.BankOptions = model.ConstructBankOptions(banks);

            return View(model);
        }

        [HttpGet]
        public async Task<string> GetVendorBankAccount()
        {
            var response = await _portalService.GetVendorBankAccount(UserId);

            return JsonConvert.SerializeObject(response.Data);
        }

        [HttpPost]
        public async Task<BaseResponse> InsertVendorBankAccount(BankPaymentModel model)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                if (model.Attachments != null)
                {
                    var resUploadImages = await _portalService.BasicUpload(model.Attachments);
                    if (resUploadImages.Code == 200)
                    {
                        model.MasterDataUserId = Int32.Parse(UserId);
                        model.CreatedBy = UserLevel;
                        model.ModifiyBy = UserLevel;
                        model.Attachment = resUploadImages.fileNames.FirstOrDefault();

                        var resInsertBank = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:InsertVendorBankAccount"), model);
                        if (resInsertBank.Code == 200)
                        {
                            response.Code = resInsertBank.Code;
                            response.Status = resInsertBank.Status;
                            response.Message = resInsertBank.Message;
                        }
                        else
                        {
                            response.Code = resInsertBank.Code;
                            response.Status = resInsertBank.Status;
                            response.Message = resInsertBank.Message;
                        }
                    }
                    else
                    {
                        response.Code = resUploadImages.Code;
                        response.Status = resUploadImages.Status;
                        response.Message = resUploadImages.Message;
                    }
                } else
                {
                    model.MasterDataUserId = Int32.Parse(UserId);
                    model.CreatedBy = UserLevel;
                    model.ModifiyBy = UserLevel;

                    var resInsertBank = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:InsertVendorBankAccount"), model);
                    if (resInsertBank.Code == 200)
                    {
                        response.Code = resInsertBank.Code;
                        response.Status = resInsertBank.Status;
                        response.Message = resInsertBank.Message;
                    }
                    else
                    {
                        response.Code = resInsertBank.Code;
                        response.Status = resInsertBank.Status;
                        response.Message = resInsertBank.Message;
                    }
                }               
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Message = ex.Message;
            }

            return response;
        }

        [HttpPost]
        public async Task<BaseResponse> UpdateVendorBankAccount(BankPaymentModel model)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                if (model.Attachments != null)
                {
                    var resUploadImages = await _portalService.BasicUpload(model.Attachments);

                    if (resUploadImages.Code == 200)
                    {
                        model.MasterDataUserId = Int32.Parse(UserId);
                        model.CreatedBy = UserLevel;
                        model.ModifiyBy = UserLevel;
                        model.Attachment = resUploadImages.fileNames.FirstOrDefault();

                        var resUpdateBank = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:UpdateVendorBankAccount"), model);
                        if (resUpdateBank.Code == 200)
                        {
                            response.Code = resUpdateBank.Code;
                            response.Status = resUpdateBank.Status;
                            response.Message = resUpdateBank.Message;
                        } else
                        {
                            response.Code = resUpdateBank.Code;
                            response.Status = resUpdateBank.Status;
                            response.Message = resUpdateBank.Message;
                        }
                    }
                } else
                {
                    model.MasterDataUserId = Int32.Parse(UserId);
                    model.CreatedBy = UserLevel;
                    model.ModifiyBy = UserLevel;

                    var resUpdateBank = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:UpdateVendorBankAccount"), model);
                    if (resUpdateBank.Code == 200)
                    {
                        response.Code = resUpdateBank.Code;
                        response.Status = resUpdateBank.Status;
                        response.Message = resUpdateBank.Message;
                    }
                    else
                    {
                        response.Code = resUpdateBank.Code;
                        response.Status = resUpdateBank.Status;
                        response.Message = resUpdateBank.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Message = ex.Message;
            }

            return response;
        }

        [HttpPost]
        public async Task<BaseResponse> UpdateStatusVendorBankAccount([FromQuery] int id)
        {
            var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
            BankPaymentModel model = null;

            queryString["id"] = id.ToString();
            queryString["masterDataUserId"] = UserId;
            queryString["status"] = "Main";
            queryString["modifyBy"] = UserLevel;

            var response = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:UpdateStatusVendorBankAccount") + "/?" + queryString.ToString(), model);

            return response;
        }

        [HttpPost]
        public async Task<BaseResponse> DeleteVendorBankAccount([FromQuery] int id)
        {
            BankPaymentModel model = null;
            var response = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:DeleteVendorBankAccount") + "/?id=" + id, model);

            return response;
        }

        [HttpPost]
        public async Task<BaseResponse> InsertVendorPaymentMethod(VendorPaymentMethodModel model)
        {
            model.MasterDataUserId = Int32.Parse(UserId);
            var response = await _portalService.InsertVendorBankPayment(model, UserLevel);

            return response;
        }

        [HttpGet]
        public async Task<string> GetBusinessFieldPaymentMethod([FromQuery] int paramBusinessFieldId)
        {
            var response = await _portalService.GetBusinessFieldsPaymentMethod(paramBusinessFieldId);

            return JsonConvert.SerializeObject(response.Data);
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        public async Task<IActionResult> Legal()
        {
            ViewData["UserLevel"] = UserLevel;
            VendorLegalModel model = new VendorLegalModel();

            var resJobTitles = await _portalService.GetJobTitles();
            var resVendorLegal = await _portalService.GetVendorLegal(Int32.Parse(UserId));
            var resIsPersonalVendorBusinessType = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:IsPersonalVendorBusinessType") + "?userId=" + UserId);

            List<Models.JobTitle.JobTitleModel> jobTitles = resJobTitles.Data != null ? JsonConvert.DeserializeObject<List<Models.JobTitle.JobTitleModel>>(Convert.ToString(resJobTitles.Data)) : null;

            if (resVendorLegal.Data != null)
                model = JsonConvert.DeserializeObject<VendorLegalModel>(Convert.ToString(resVendorLegal.Data));

            model.JobTitleOptions = JsonConvert.SerializeObject(jobTitles);

            if (resIsPersonalVendorBusinessType.Data != null)
            {
                model.IsPersonalVendorBusinessType = resIsPersonalVendorBusinessType.Data;
                ViewData["IsDataFillingOut"] = true;
            }
            else
            {
                ViewData["IsDataFillingOut"] = false;
            }

            return View(model);
        }

        [HttpPost]
        public async Task<BaseResponse> Legal(VendorLegalModel model)
        {
            if (model.Attachments != null)
            {
                var resImages = await _portalService.BasicUpload(model.Attachments);

                foreach (var single in resImages.fileNames)
                {
                    if (single.Contains(NIB_KEY))
                        model.Nibattachment = single;
                    if (single.Contains(BUSSPERMIT_KEY))
                        model.BusinessPermitAttachment = single;
                    if (single.Contains(SIUP_KEY))
                        model.Siupattachment = single;
                    if (single.Contains(TDP_KEY))
                        model.Tdpattachment = single;
                    if (single.Contains(MEMOASSO_KEY))
                        model.MemorandumOfAssociationAttachment = single;
                    if (single.Contains(DECSLTR_KEY))
                        model.DecissionLetterMenkumhamAttachment = single;
                    if (single.Contains(MEMOASSOCHG_KEY))
                        model.MemorandumOfAssociationChangingAttachment = single;
                    if (single.Contains(DECSLTRCHG_KEY))
                        model.DecissionLetterMenkumhamChangingAttachment = single;
                    if (single.Contains(SIGNER_KEY))
                        model.SigningAttachment = single;
                    if (single.Contains(OTHERDOC_KEY))
                        model.OtherAttachment = single;
                }
            }

            model.MasterDataUserId = Int32.Parse(UserId);
            model.CreatedBy = UserLevel;
            model.ModifiyBy = UserLevel;

            var response = await _portalService.InsertVendorLegal(model);

            return response;
        }

        [HttpPost]
        public async Task<BaseResponse> DeleteVendorLegalAttachment(string type)
        {
            BaseResponse response = null;
            var result = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:DeleteVendorLegalAttachment") + "?userId=" + UserId + "&type=" + type, response);

            return result;
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        public async Task<IActionResult> Tax()
        {
            ViewData["UserLevel"] = UserLevel;
            VendorTaxModel model = new VendorTaxModel();

            var resBkpCategories = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetBkpCategories"));
            var resVendorTax = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorTax") + "?masterDataUserId=" + UserId);

            List<Models.BKPCategory.BkpCategoryModel> bkpCategories = resBkpCategories.Data != null ? JsonConvert.DeserializeObject<List<Models.BKPCategory.BkpCategoryModel>>(Convert.ToString(resBkpCategories.Data)) : null;
            if (resVendorTax.Data != null)
                model = JsonConvert.DeserializeObject<VendorTaxModel>(Convert.ToString(resVendorTax.Data));

            model.BKPFreePPNOptions = model.CostructBKPFreePPNOptions(bkpCategories);

            return View(model);
        }

        [HttpPost]
        public async Task<BaseResponse> Tax(VendorTaxModel model)
        {
            model.MasterDataUserId = Int32.Parse(UserId);
            model.CreatedBy = UserLevel;
            model.ModifiedBy = UserLevel;

            var response = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:InsertVendorTax"), model);

            return response;
        }

        public IActionResult Products()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        public async Task<IActionResult> BusinessEthics()
        {
            ViewData["UserLevel"] = UserLevel;
            var response = await _portalService.GetVendorBasicInfo(UserId);

            BasicInfoModel model = new BasicInfoModel();

            if (response.Data != null)
                model = response.Data != null ? JsonConvert.DeserializeObject<BasicInfoModel>(Convert.ToString(response.Data)) : null;

            return View(model);
        }

        [HttpPost]
        public async Task<BaseResponse> BusinessEthics(bool bussinessEthics)
        {
            var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);

            queryString["masterDataUserId"] = UserId;
            queryString["businessEthics"] = bussinessEthics.ToString();
            BasicInfoModel model = null;

            return await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:UpdateBusinessEthicsStatus") + "/?" + queryString, model);
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        public async Task<IActionResult> MasterAgreeMent()
        {
            ViewData["UserLevel"] = UserLevel;
            var response = await _portalService.GetVendorBasicInfo(UserId);

            BasicInfoModel model = new BasicInfoModel();

            if (response.Data != null)
                model = response.Data != null ? JsonConvert.DeserializeObject<BasicInfoModel>(Convert.ToString(response.Data)) : null;

            return View(model);
        }

        [HttpPost]
        public async Task<BaseResponse> MasterAgreement(bool masterAgreement)
        {
            var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);

            queryString["masterDataUserId"] = UserId;
            queryString["masterAgreement"] = masterAgreement.ToString();
            BasicInfoModel model = null;

            return await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:UpdateMasterAgreementStatus") + "/?" + queryString, model);
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        public IActionResult Profile()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        [Route("/Vendor/BasicInfoDetail")]
        public async Task<PartialViewResult> _BasicInfoDetail()
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorBasicInfo") + "?userId=" + UserId);
            BasicInfoModel model = response.Data != null ? JsonConvert.DeserializeObject<BasicInfoModel>(Convert.ToString(response.Data)) : null;

            ViewData["BasicInfo"] = model;

            return PartialView(model);
        }

        [Route("/Vendor/BankPaymentDetail")]
        public async Task<PartialViewResult> _BankPaymentDetail()
        {
            BankPaymentModel modelBankAccount = new BankPaymentModel();
            VendorPaymentMethodModel modelVendorPayment = new VendorPaymentMethodModel();

            var resBankAccount = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorAccountPrimary") + "?userId=" + UserId);
            var resVendorPayment = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorPaymentTypeInfo") + "?masterDataUserId=" + UserId);

            if (resBankAccount.Data != null)
            {
                modelBankAccount = resBankAccount.Data != null ? JsonConvert.DeserializeObject<BankPaymentModel>(Convert.ToString(resBankAccount.Data)) : null;
                modelVendorPayment = resVendorPayment.Data != null ? JsonConvert.DeserializeObject<VendorPaymentMethodModel>(Convert.ToString(resVendorPayment.Data)) : null;
            }

            ViewData["ModelBankAccount"] = modelBankAccount;
            ViewData["ModelVendorPayment"] = modelVendorPayment;

            return PartialView();
        }

        [Route("/Vendor/LegalDetail")]
        public async Task<PartialViewResult> _LegalDetail()
        {
            RootObject structureOrgObject = new RootObject();
            var resVendorLegal = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorLegal") + "?masterDataUserId=" + UserId);
            var resJobTitles = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetJobTitles"));

            VendorLegalModel model = resVendorLegal.Data != null ? JsonConvert.DeserializeObject<VendorLegalModel>(Convert.ToString(resVendorLegal.Data)) : null;
            List<JobTitleModel> jobTitlesModel = resJobTitles.Data != null ? JsonConvert.DeserializeObject<List<JobTitleModel>>(Convert.ToString(resJobTitles.Data)) : null;
            if (model != null)
            {
                structureOrgObject = !string.IsNullOrEmpty(model.ManagementStructure) ? JsonConvert.DeserializeObject<RootObject>(model.ManagementStructure) : null;
                model.StructureOrg = model.ConstructStructureOrg(jobTitlesModel, structureOrgObject);

                if (model.SigningId == 1)
                {
                    model.SigningName = "Direktur";
                }
                else if(model.SigningId == 2)
                { 
                    model.SigningName = "Kuasa Direktur";
                }
            }

            ViewData["VendorLegal"] = model;

            return PartialView(model);
        }

        [Route("/Vendor/TaxDetail")]
        public async Task<PartialViewResult> _TaxDetail()
        {
            var resVendorTax = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorTax") + "?masterDataUserId=" + UserId);
            VendorTaxModel model = resVendorTax.Data != null ? JsonConvert.DeserializeObject<VendorTaxModel>(Convert.ToString(resVendorTax.Data)) : null;

            ViewData["VendorTax"] = model;

            return PartialView(model);
        }

        [Route("/Vendor/AgreementDetail")]
        public async Task<PartialViewResult> _AgreementDetail()
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorBasicInfo") + "?userId=" + UserId);
            BasicInfoModel model = response.Data != null ? JsonConvert.DeserializeObject<BasicInfoModel>(Convert.ToString(response.Data)) : null;
            ViewData["Agreement"] = model;

            return PartialView(model);
        }

        [Route("/Vendor/PhotoProfile")]
        public async Task<PartialViewResult> _PhotoProfile()
        {
            var response = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:getUserById") + "?id=" + UserId);
            UserModel model = response.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(response.Data)) : null;

            return PartialView(model);
        }

        [HttpPost]
        public async Task<BaseResponse> SavePhoto(UserModel model)
        {
            if (model.Attachments != null)
            {
                var resImages = await _portalService.UploadPhoto(model.Attachments);

                foreach (var single in resImages.fileNames)
                {
                    if (single.Contains(PHOTO_KEY))
                        model.Image = single; SetSessionPhoto(model.Image);
                }
            }

            var response = await _portalService.UpdateUserPhoto(Int32.Parse(UserId), model.Image);

            return response;
        }

        [HttpGet]
        public async Task<BaseResponse> GetDataVerifikasiVendor()
        {
            return await _portalService.GetDataVerifikasiVendor(UserId);
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        public IActionResult ChangePassword()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        [HttpPost]
        public async Task<BaseResponse> ChangePassword(string newPassword)
        {
            BaseResponse result = new BaseResponse();

            try
            {
                result = await _portalService.SetChangePassword(
                    FirstResource.System.Library.Extensions.EncryptionExtensions.DecryptStringAES(newPassword),
                    UserEmail);
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public IActionResult Inbox()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult Compose()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult Sent()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult InvoiceSend()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult InvoiceStatus()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult InvoiceDetails(string id)
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }
    }
}