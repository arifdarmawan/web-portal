using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using WebPortal.Helper;
using WebPortal.Models;
using WebPortal.Models.ResponseModel;
using WebPortal.Service;

namespace WebPortal.Controllers
{
    public class AccountController : BaseController
    {
        readonly IHttpContextAccessor _httpContextAccessor;
        readonly PortalService _portalService;
        readonly IConfiguration _configuration;
        readonly IHostingEnvironment _environment;

        public AccountController(
            IHttpContextAccessor httpContextAccessor,
            PortalService portalService,
            IConfiguration configuration,
            IHostingEnvironment environment) : base (httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _portalService = portalService;
            _configuration = configuration;
            _environment = environment;
        }

        #region Login
        [NoDirectAccess]
        public ViewResult Login()
        {
            return View();
        }

        public IActionResult Logout()
        {            
            HttpContext.Session.Clear(); // Clear all session
            return View();
        }

        [HttpPost]
        public async Task<BaseResponse> LoginVendor(LoginViewModel model)
        {
            BaseResponse response = new BaseResponse();

            Helper.EncryptionHelper.AESLoginEncryptionHelper(model);

            var resUser = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetUserByEmailOrUserId") + "/?email=" + model.UserId);
            var resVendor = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorInfoByEmail") + "/?email=" + model.UserId);
            var userModel = resUser.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(resUser.Data)) : null;
            WebPortal.Models.Vendor.BasicInfoModel vendorModel = resVendor.Data != null ? JsonConvert.DeserializeObject<WebPortal.Models.Vendor.BasicInfoModel>(Convert.ToString(resVendor.Data)) : null;

            try
            {
                if (resUser.Code == (int)HttpStatusCode.OK)
                {
                    if (userModel == null)
                    {
                        response.Code = 100;
                        response.Status = HttpStatusCode.BadRequest.ToString();
                        response.Message = "Email not registered. Please register for get access to our website.";
                    }
                    else
                    {
                        if (EncryptionHelper.Decrypt(userModel.Password) != model.Password)
                        {
                            response.Code = 101;
                            response.Status = HttpStatusCode.BadRequest.ToString();
                            response.Message = "Username and password not match.";
                        }
                        else
                        {
                            SetSession(userModel.Id, userModel.UserLevel);
                            SetSessionEmail(model.UserId);
                            SetSessionUrl(_configuration.GetValue<string>("WebPortalAPIEndpoint:BaseUrl") + "/api/FileUpload/DownloadPhoto");
                            if (userModel.Image != null)
                            { 
                                SetSessionPhoto(userModel.Image);
                            }

                            if (vendorModel != null)
                            {
                                SetSessionCompanyName(vendorModel.BusinessTypeName, vendorModel.Name);
                                SetSessionVendorPicName(vendorModel.Pic);
                            }                         
                            response.Code = (int)HttpStatusCode.OK;
                            response.Status = HttpStatusCode.OK.ToString();
                            response.Message = "Login success";
                            response.IsUserActive = userModel.IsActive;
                            response.SessionUserLevel = UserLevel;
                        }
                    }
                }
                else
                {
                    response.Code = resUser.Code;
                    response.Status = resUser.Status;
                    response.Message = "Internat Server Error. Please Contact Administrator.";
                }
            }
            catch (Exception e)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Message = "Internat Server Error. Please Contact Administrator.";
            }
            

            return response;
        }

        [HttpPost]
        public async Task<BaseResponse> LoginEmployee(LoginViewModel model)
        {
            BaseResponse response = new BaseResponse();

            var resUser = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetUserByEmailOrUserId") + "/?email=" + model.UserId);
            var userModel = resUser.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(resUser.Data)) : null;

            if (resUser.Code == (int)HttpStatusCode.OK)
            {             
                if (userModel == null)  // insert user ad
                {
                    var resUserAD = await _portalService.PostAsJsonAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:InsertUserFromAD"), model.UserId);

                    if (resUserAD.Code == (int)HttpStatusCode.OK)
                    {
                        userModel = resUserAD.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(resUserAD.Data)) : null;
                        SetSession(userModel.Id, userModel.UserLevel);

                        response = resUserAD;
                        response.SessionUserLevel = UserLevel;
                    } else
                    {
                        response.Code = resUserAD.Code;
                        response.Status = resUserAD.Status;
                        response.Message = "Internal Server Error. Please Contact Administrator.";
                    }
                } else // update user ad
                {
                    var resUserAD = await _portalService.PostAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:UpdateUserFromAD"), model);

                    if (resUserAD.Code == (int)HttpStatusCode.OK)
                    {
                        userModel = resUserAD.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(resUserAD.Data)) : null;
                        SetSession(userModel.Id, userModel.UserLevel);

                        response = resUserAD;
                        response.SessionUserLevel = UserLevel;
                    } else
                    {
                        response.Code = resUserAD.Code;
                        response.Status = resUserAD.Status;
                        response.Message = "Internal Server Error. Please Contact Administrator.";
                    }
                }
            } else
            {
                response.Code = resUser.Code;
                response.Status = resUser.Status;
                response.Message = "Internat Server Error. Please Contact Administrator.";
            }

            return response;
        }

        [HttpPost]
        public async Task<BaseResponse> Login(LoginViewModel model)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var userFound = await _portalService.LoginUser(model);

                if (userFound.Code == (int)HttpStatusCode.OK)
                {
                    if (userFound != null)
                    {
                        if (userFound.UserModel != null)
                        {
                            if (userFound.UserModel.IsUserAd)
                            {
                                response = await _portalService.UpdateUserAD(model);

                                response.IsUserActive = userFound.UserModel.IsActive;

                                SetSession(response.UserModel.Id, response.UserModel.UserLevel);

                                response.SessionUserLevel = UserLevel;
                            }
                            else if (EncryptionHelper.Decrypt(userFound.UserModel.Password) != model.Password)
                            {
                                response.Code = 601;
                                response.Status = HttpStatusCode.BadRequest.ToString();
                                response.Message = "Username and password not match.";
                            }
                            else
                            {
                                response.Code = (int)HttpStatusCode.OK;
                                response.Status = HttpStatusCode.OK.ToString();
                                response.Message = "Login success";

                                response.IsUserActive = userFound.UserModel.IsActive;

                                SetSession(userFound.UserModel.Id, userFound.UserModel.UserLevel);

                                response.SessionUserLevel = UserLevel;
                            }
                        }
                        else
                        {
                            response = await _portalService.InsertUserAD(model.UserId);

                            if (response.Code != (int)HttpStatusCode.OK)
                            {
                                response.Message = "Email not registered. Please register for get access to our website.";
                            }

                            if (response.UserModel != null)
                                SetSession(response.UserModel.Id, response.UserModel.UserLevel);

                            response.SessionUserLevel = UserLevel;
                        }
                    }
                }
                else
                {
                    response.Message = "Internal Server Error";
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }
        #endregion

        #region Register
        [NoDirectAccess]
        public async Task<ViewResult> Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Register(RegisterViewModel model)
        {
            BaseResponse result = new BaseResponse();

            try
            {
                Helper.EncryptionHelper.AESRegisterEncryptionHelper(model);

                var userFound = await _portalService.GetUserByEmail(model.Email);

                if (userFound.UserModel != null)
                {
                    if (userFound.UserModel.Email != null)
                    {
                        result.Code = (int)HttpStatusCode.BadRequest;
                        result.Status = HttpStatusCode.BadRequest.ToString();
                        result.Message = "Email already registered.";
                    }
                }
                else
                {
                    model.ActivationCode = Guid.NewGuid().ToString().Replace("-", "");
                    result = await _portalService.InsertUser(model);

                    var emailSetup = await ConstructRegisterEmailFormatModel(model.ActivationCode, model.Email);

                    MailService.SendMail(emailSetup, emailSetup.ConstructEmailBody(emailSetup));
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)HttpStatusCode.InternalServerError;
                result.Status = HttpStatusCode.InternalServerError.ToString();
                result.Message = ex.ToString();
            }

            return Json(result);
        }

        public ViewResult RegisterConfirmation(string email)
        {
            ViewData["email"] = email;

            return View();
        }
        
        public async Task<ViewResult> Activation(string activationCode)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                response = await _portalService.ActivationUser(activationCode);

                var emailSetup = await ConstructNotificationAdminConfirmationEmailFormatModel(response.Data, GlobalHelper.adminAppsEmail);

                MailService.SendMail(emailSetup, emailSetup.ConstructEmailBodyNotificationAdmin(emailSetup));
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }
            return View(response);
        }
        #endregion

        #region ForgotPassword
        public async Task<ViewResult> ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<BaseResponse> ForgotPassword(string email)
        {
            BaseResponse result = new BaseResponse();

            try
            {
                var rescheckEmail = await _portalService.GetAsync(_configuration.GetValue<string>("WebPortalAPIEndpoint:GetUserByEmail") + "?email=" + email);

                if (rescheckEmail.Data != null)
                {
                    result = await _portalService.ResetPassword(email);
                    
                    if (result.Code == (int)HttpStatusCode.OK)
                    {
                        var emailSetup = await ConstructForgotPasswordEmailFormatModel(Convert.ToString(result.Data), email);

                        MailService.SendMail(emailSetup, emailSetup.ConstructEmailBody(emailSetup));

                        result.Message = email;
                    }
                }
                else 
                {
                    rescheckEmail.Message = "Data Not Found";
                }
            }
            catch (Exception ex)
            {

                result.Message = ex.ToString();
            }

            return result;
        }

        public ViewResult ForgotPasswordVerification(string email)
        {
            ViewData["email"] = email;

            return View();
        }
        
        public async Task<ViewResult> NewPassword(string token)
        {
            ViewData["token"] = token;

            return View();
        }

        [HttpPost]
        public async Task<BaseResponse> NewPassword(string token, string newPassword)
        {
            BaseResponse result = new BaseResponse();

            try
            {
                result = await _portalService.SetNewPassword(token, 
                    FirstResource.System.Library.Extensions.EncryptionExtensions.DecryptStringAES(newPassword));
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<ViewResult> ForgotPasswordConfirmation()
        {
            return View();
        }

        public async Task<ViewResult> ResetPasswordConfirmation()
        {
            return View();
        }
        #endregion

        #region Profile
        public async Task<IActionResult> Profile()
        {
            return View();
        }
        #endregion

        #region helper
        private async Task<EmailFormatViewModel> ConstructRegisterEmailFormatModel(string activationCode, string email)
        {
            EmailFormatViewModel formatEmailModel = new EmailFormatViewModel(_configuration, _environment);

            string baseUrl = Request.Scheme + "://" + Request.Host;
            string activationUrl = baseUrl + "/Account/Activation/?activationCode=" + activationCode;

            formatEmailModel.Subject = "Email Activation";
            formatEmailModel.EmailRecipient = email;
            formatEmailModel.Link = activationUrl;
            formatEmailModel.LinkMessage = "Confirm Email Address";
            formatEmailModel.Message1 = "Please confirm your email address by clicking the link below.";
            formatEmailModel.Message2 = "We may need to send you critical information about our service and it is important that we have an accurate email address.";

            return await Task.FromResult(formatEmailModel);
        }

        private async Task<EmailFormatViewModel> ConstructForgotPasswordEmailFormatModel(string token, string email)
        {
            EmailFormatViewModel formatEmailModel = new EmailFormatViewModel(_configuration, _environment);

            string baseUrl = Request.Scheme + "://" + Request.Host;
            string activationUrl = baseUrl + "/Account/NewPassword/?token=" + token;

            formatEmailModel.Subject = "Reset Password";
            formatEmailModel.EmailRecipient = email;
            formatEmailModel.Link = activationUrl;
            formatEmailModel.LinkMessage = "Reset Password";
            formatEmailModel.Message1 = "Please reset your password by clicking the link below.";

            return await Task.FromResult(formatEmailModel);
        }

        private async Task<EmailFormatViewModel> ConstructNotificationAdminConfirmationEmailFormatModel(string userEmail, string adminEmail)
        {
            EmailFormatViewModel formatEmailModel = new EmailFormatViewModel(_configuration, _environment);

            formatEmailModel.Subject = "New user signed up";
            formatEmailModel.EmailRecipient = adminEmail;

            formatEmailModel.Message1 = "New user signed up";
            formatEmailModel.Message2 = userEmail;

            return await Task.FromResult(formatEmailModel);
        }

        private void SetSession(int userId, int userLevel)
        {
            UserId = userId.ToString();

            switch (userLevel)
            {
                case 0:
                    UserLevel = "ADMIN";
                    break;
                case 2:
                    UserLevel = "VENDOR";
                    break;
                case 3:
                    UserLevel = "CUSTOMER";
                    break;
                case 4:
                    UserLevel = "JOB_APPLICANT";
                    break;
                case 11:
                    UserLevel = "USER_FR";
                    break;
                case 12:
                    UserLevel = "USER_FAP";
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}