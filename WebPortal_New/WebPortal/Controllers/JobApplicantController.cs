﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebPortal.Controllers
{
    public class JobApplicantController : BaseController
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public JobApplicantController(IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Dashboard()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }
    }
}