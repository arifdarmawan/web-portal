﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using WebPortal.Helper;
using WebPortal.Models;
using WebPortal.Models.ResponseModel;
using WebPortal.Models.Vendor;
using WebPortal.Models.RequestForQuotation;

namespace WebPortal.Service
{
    public class PortalService
    {
        HttpClient Client { get; }
        IConfiguration Configuration { get; }
        public PortalService(HttpClient client, IConfiguration configuration)
        {
            Configuration = configuration;
            Client = client;
        }

        #region Function Global
        public async Task<BaseResponse> GetAsync(string uri)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(uri);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> PostAsync<T>(string uri, T model) where T : class
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsync(uri, new JsonContent(model));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }

            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> PostAsJsonAsync(string uri, string value)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsJsonAsync(uri, value);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> PutAsync<T>(string uri, T model) where T : class
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PutAsync(uri, model != null ? new JsonContent(model) : null);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region Account
        public async Task<BaseResponse> GetUserByEmail(string email)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetUserByEmail") + "/?email=" + email);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
                result.UserModel = result.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(result.Data)) : null;
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> InsertUser(RegisterViewModel model)
        {
            var result = new BaseResponse();

            try
            {
                UserModel user = new UserModel
                {
                    UserId = model.Email,
                    UserName = model.Email,
                    Email = model.Email,
                    Password = EncryptionHelper.Encrypt(model.Password),
                    UserLevel = model.UserType == "V" ? 2 : model.UserType == "C" ? 3 : 4,
                    ActivateLink = model.ActivationCode
                };

                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:InsertUser"), new JsonContent(user));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> ActivationUser(string activationCode)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsJsonAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:ActivationUser"), activationCode);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> LoginUser(LoginViewModel model)
        {
            BaseResponse result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetUserByEmailOrUserId") + "/?email=" + model.UserId);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();

                result.UserModel = result.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(result.Data)) : null;
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> InsertUserAD(string email)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsJsonAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:InsertUserFromAD"), email);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();

                result.UserModel = result.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(result.Data)) : null;
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> UpdateUserAD(LoginViewModel model)
        {
            var result = new BaseResponse();

            try
            {
                ActiveDirectoryModel param = new ActiveDirectoryModel
                {
                    UserId = model.UserId,
                    Password = model.Password
                };

                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:UpdateUserFromAD"), new JsonContent(param));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();

                result.UserModel = result.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(result.Data)) : null;
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> ResetPassword(string email)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsJsonAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:ResetPassword"), email);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> SetNewPassword(string token, string newPassword)
        {
            var result = new BaseResponse();

            try
            {
                var param = new NewPasswordModel
                {
                    Token = token,
                    NewPassword = EncryptionHelper.Encrypt(newPassword)
                };

                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:SetNewPassword"), new JsonContent(param));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> SetChangePassword(string newPassword, string userId)
        {
            var result = new BaseResponse();

            try
            {
                var param = new NewPasswordModel
                {
                    NewPassword = EncryptionHelper.Encrypt(newPassword),
                    UserId = userId
                };

                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:SetChangePassword"), new JsonContent(param));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region BusinessFileType
        public async Task<BaseResponse> GetBusinessType()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetBusinessType"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region BusinessFields
        public async Task<BaseResponse> GetBusinessFields()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetBusinessFieldsByGroupType"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> GetBusinessFieldsTOP(int businessFieldId)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetBusinessFieldTOP") + "/?paramBusinessFieldId=" + businessFieldId);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> GetBusinessFieldsPaymentMethod(int businessFieldId)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetBusinessFieldPaymentMethod") + "/?paramBusinessFieldId=" + businessFieldId);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region Country
        public async Task<BaseResponse> GetCountries()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetCountries"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region Region
        public async Task<BaseResponse> GetRegions()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetRegions"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region City
        public async Task<BaseResponse> GetCities()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetCities"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region Vendor
        public async Task<BaseResponse> SaveVendorBasicInfo(BasicInfoModel model)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:InsertVendorBasicInfo"), new JsonContent(model));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        public async Task<BaseResponse> GetVendorBasicInfo(string userId)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorBasicInfo") + "/?userId=" + userId);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> GetAllVendor()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetAllVendor"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> GetVendorBankAccount(string userId)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorBankAccount") + "/?masterDataUserId=" + userId);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> GetVendorPaymentMethod(int userId)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorPaymentMethod") + "/?masterDataUserId=" + userId);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> GetVendorLegal(int userId)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetVendorLegal") + "/?masterDataUserId=" + userId);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> InsertVendorBankAccount(BankPaymentModel model)
        {
            var result = new BaseResponse();

            var data = new BankPaymentModel
            {
                MasterDataUserId = model.MasterDataUserId,
                MasterDataCurrencyId = model.MasterDataCurrencyId,
                MasterDataBankId = model.MasterDataBankId,
                Area = model.Area,
                City = model.City,
                Status = model.Status,
                AccountNumber = model.AccountNumber,
                AccountName = model.AccountName,
                CreatedBy = model.CreatedBy,
                ModifiyBy = model.CreatedBy
            };

            try
            {
                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:InsertVendorBankAccount"), new JsonContent(data));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> UpdateVendorBankAccount(BankPaymentModel model)
        {
            var result = new BaseResponse();

            var data = new BankPaymentModel
            {
                Id = model.Id,
                MasterDataUserId = model.MasterDataUserId,
                MasterDataCurrencyId = model.MasterDataCurrencyId,
                MasterDataBankId = model.MasterDataBankId,
                Area = model.Area,
                City = model.City,
                AccountNumber = model.AccountNumber,
                AccountName = model.AccountName,
                CreatedBy = model.CreatedBy,
                ModifiyBy = model.CreatedBy
            };

            try
            {
                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:UpdateVendorBankAccount"), new JsonContent(data));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> UpdateStatusVendorBankAccount(int id, int masterDataUserId, string modifyBy)
        {
            var result = new BaseResponse();
            var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);

            queryString["id"] = id.ToString();
            queryString["masterDataUserId"] = masterDataUserId.ToString();
            queryString["status"] = "Main";
            queryString["modifyBy"] = modifyBy;

            try
            {
                var response = await Client.PutAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:UpdateStatusVendorBankAccount") + "/?" + queryString.ToString(), null);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> DeleteVendorBankAccount(int id)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.DeleteAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:DeleteVendorBankAccount") + "/?id=" + id);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> InsertVendorBankPayment(VendorPaymentMethodModel model, string userLevel)
        {
            var result = new BaseResponse();

            var data = new VendorPaymentMethodModel
            {
                MasterDataUserId = model.MasterDataUserId,
                MappingDataBusinessFieldTopConfigId = model.MappingDataBusinessFieldTopConfigId,
                mappingDataBusinessFieldPaymentConfigId = model.mappingDataBusinessFieldPaymentConfigId,
                CreatedBy = userLevel,
                ModifiedBy = userLevel
            };

            try
            {
                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:InsertVendorPaymentMethod"), new JsonContent(data));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }

            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> InsertVendorLegal(VendorLegalModel model)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:InsertVendorLegal"), new JsonContent(model));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }

            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> UpdateUserPhoto(int userId, string image)
        {
            var result = new BaseResponse();
            var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);

            queryString["userId"] = userId.ToString();
            queryString["image"] = image.ToString();

            try
            {
                var response = await Client.PutAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:UpdateUserPhoto") + "/?" + queryString.ToString(), null);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> GetDataVerifikasiVendor(string masterDataUserId) 
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetVerifikasiVendor") + "/?masterDataUserId=" + masterDataUserId);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region Currency
        public async Task<BaseResponse> GetCurrency()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetCurrency"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region Bank
        public async Task<BaseResponse> GetAllBank()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetAllBank"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region FileUpload
        public async Task<ImageBaseResponse> BasicUpload(List<IFormFile> files)
        {
            var result = new ImageBaseResponse();

            try
            {
                var multipartFormDataContent = new MultipartFormDataContent();

                foreach(var file in files)
                {
                    byte[] data;
                    using (var br = new BinaryReader(file.OpenReadStream()))
                        data = br.ReadBytes((int)file.OpenReadStream().Length);

                    ByteArrayContent bytes = new ByteArrayContent(data);

                    multipartFormDataContent.Add(bytes, "files", file.FileName);                 
                }

                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:BasicUpload"), multipartFormDataContent);
                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<ImageBaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<ImageBaseResponse> UploadPhoto(List<IFormFile> files)
        {
            var result = new ImageBaseResponse();

            try
            {
                var multipartFormDataContent = new MultipartFormDataContent();

                foreach (var file in files)
                {
                    byte[] data;
                    using (var br = new BinaryReader(file.OpenReadStream()))
                        data = br.ReadBytes((int)file.OpenReadStream().Length);

                    ByteArrayContent bytes = new ByteArrayContent(data);

                    multipartFormDataContent.Add(bytes, "files", file.FileName);
                }

                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:UploadPhoto"), multipartFormDataContent);
                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<ImageBaseResponse>();

            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region Job Title
        public async Task<BaseResponse> GetJobTitles()
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.GetAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:GetJobTitles"));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion

        #region Request For Quotation
        public async Task<BaseResponse> ProcessRequestForQuotation(RequestForQuotationModel model)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:ProcessRequestForQuotation"), new JsonContent(model));
                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }

            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion


        #region Purchase Order
        public async Task<BaseResponse> ProcessPurchaseOrder(int id, string modifyBy)
        {
            var result = new BaseResponse();
            var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);

            queryString["id"] = id.ToString();
            queryString["modifyBy"] = modifyBy;

            try
            {
                var response = await Client.PutAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:ProcessPurchaseOrder") + "/?" + queryString.ToString(), null);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> RejectPurchaseOrder(int id, string modifyBy)
        {
            var result = new BaseResponse();
            var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);

            queryString["id"] = id.ToString();
            queryString["modifyBy"] = modifyBy;

            try
            {
                var response = await Client.PutAsync(Configuration.GetValue<string>("WebPortalAPIEndpoint:RejectPurchaseOrder") + "/?" + queryString.ToString(), null);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
        #endregion
    }
}