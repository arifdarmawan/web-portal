﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebPortal.Service;

namespace WebPortal.Configuration
{
    public static class HttpClientConfig
    {
        public static IServiceCollection ManualSetSSL(this IServiceCollection services, IHostingEnvironment env, IConfiguration configuration)
        {
            if (env.IsDevelopment()) 
            {
                services.AddHttpClient<PortalService>("HttpClientWithSSLUntrusted", client => {
                    client.BaseAddress = new Uri(configuration.GetValue<string>("WebPortalAPIEndpoint:BaseUrl"));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                }).ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
                {
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    ServerCertificateCustomValidationCallback = (httpRequestMessage, cert, cetChain, policyErrors) => { return true; }
                });
            } else
            {
                services.AddHttpClient<PortalService>("HttpClientWithSSLUntrusted", client =>
                {
                    client.BaseAddress = new Uri(configuration.GetValue<string>("WebPortalAPIEndpoint:BaseUrl"));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                });
            }

            return services;
        }
    }
}
